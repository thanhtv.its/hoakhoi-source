﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcWebsiteOcc.Models;
using System.Web.Security;
using System.Web.Caching;

namespace MvcThienThaoNguyen
{
    public class OccRoleProvider : RoleProvider
    {

        private int _cachTimeOut = 20;

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        //Use
        public override string[] GetRolesForUser(string username)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return null; 
            }

            //check cache
            var cacheKey = string.Format("{0}_role", username);
            if (HttpRuntime.Cache[cacheKey] != null)
            {
                return (string[])HttpRuntime.Cache[cacheKey];
            }
            string[] roles = new string[]{};
            using (OccDBEntities dc = new OccDBEntities())
            {
                roles = (from a in dc.Roles
                         join b in dc.GroupRoles on a.RoleID equals b.RoleID
                         join c in dc.Groups on b.GroupID equals c.GroupID
                         join d in dc.UserGroups on c.GroupID equals d.GroupID
                         join e in dc.Users on d.UserID equals e.UserID
                         where e.UserName.Equals(username)
                         select a.RoleName).ToArray<string>();

                if (roles.Count() > 0)
                {
                    HttpRuntime.Cache.Insert(cacheKey, roles, null, DateTime.Now.AddMinutes(_cachTimeOut), Cache.NoSlidingExpiration);
                }

            }

            return roles;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        //use
        public override bool IsUserInRole(string username, string roleName)
        {
            var userRoles = GetRolesForUser(username);
            return userRoles.Contains(roleName);
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}