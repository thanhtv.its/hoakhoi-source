﻿(function () {


    //code dealing with product index page loadMore ajaxCall and smooth scroll down
    $(function () {

        var loadCount = 1;
        var loading = $("#loading");
        $("#loadMore").on("click", function (e) {

            e.preventDefault();
            $(document).on({

                ajaxStart: function () {
                    loading.show();
                },
                ajaxStop: function () {
                    loading.hide();
                }
            });
            //var url = "/news/_get_data_more";
            var grid = document.querySelector('#grid_event');
            var elems = [];
            var fragment = document.createDocumentFragment();

            //var $grid = $('#grid_event').masonry({
            //    itemSelector: '.grid-item',
            //    percentPosition: true,
            //    columnWidth: '.grid-item',
            //    gutter: 10
            //});
          

            // /swiper in tabs
            $.get(url + '?size=' + loadCount * itemcount + '&subD=' + subD, // url
                function (response) { // success callback
                    // response has teh same properties as the server returnedObject
                    $.each(response.model, function (i, l) {
                        var elem = getItemElement(l);
                        fragment.appendChild(elem);
                        //grid.appendChild(elem);
                        elems.push(elem);
                    });
                    // append elements to container
                    grid.appendChild(fragment);
                    // add and lay out newly appended elements
                    //msnry.appended(elems);
                    console.log(response.model);
                    $grid.imagesLoaded(function () {
                        $grid.append(elems).masonry('appended', elems);
                    });

                    var ajaxModelCount = response.count - (loadCount * itemcount);
                    if (ajaxModelCount <= 0) {
                        $("#loadMore").hide().fadeOut(2000);
                    }
                },
                'json' // dataType
            );


            function getItemElement(l) {
                var div_item = document.createElement('div');
                div_item.setAttribute('class', 'item grid-item');

                //love
                var div_love = document.createElement('div');
                div_love.setAttribute('class', 'love-this');
                var count_love = document.createElement('span');
                count_love.setAttribute('class', 'love-counter');
                count_love.innerHTML = l.Code;
                var add_love = document.createElement('span');
                add_love.setAttribute('class', 'love-add');
                count_love.innerHTML = "SBD: " + l.Code;
                var icon_love = document.createElement('span');
                icon_love.setAttribute('class', 'fa fa-bookmark');
                //add child
                div_love.appendChild(count_love);
                div_love.appendChild(add_love);
                div_love.appendChild(icon_love);

                //header
                var div_item_header = document.createElement('div');
                div_item_header.setAttribute('class', 'item-header');
                var a_item = document.createElement('a');
                a_item.setAttribute('class', 'image-hover');
                a_item.setAttribute('href', l.Link);

                var a_item_img = document.createElement('img');
                a_item_img.setAttribute('class', 'image-hover');
                a_item_img.setAttribute('src', '/uploaded/images/sukien/thi-sinh-du-thi/' + l.Image);

                //add img to a
                a_item.appendChild(a_item_img);
                //add a to div
                div_item_header.appendChild(a_item);

                //content
                var div_item_content = document.createElement('div');
                div_item_content.setAttribute('class', 'item-content');
                //var div_item_category = document.createElement('div');
                //div_item_category.setAttribute('class', 'content-category');
                //var a_item_category = document.createElement('a');
                //a_item_category.setAttribute('href', l.LinkCate);
                //a_item_category.style.color = "#e8b400";
                //a_item_category.innerHTML = l.CateName;
                //div_item_category.appendChild(a_item_category);

                var h3 = document.createElement('h3');
                var a_h3 = document.createElement('a');
                a_h3.setAttribute('href', l.Link);
                var a_h3_text = document.createTextNode(l.Name);
                a_h3.appendChild(a_h3_text);
                h3.appendChild(a_h3);

                var date_p = document.createElement('p');
                date_p.setAttribute('class', 'desc');
                var innerP = '';
                innerP += '<span class="tooltip" data="Lượt xem">';
                innerP += '<span><i class="fa fa-eye"></i></span>' + l.ViewCount;
                innerP += '</span>';
                innerP += '<span class="tooltip" data="Số tin nhắn">';
                innerP += '<span><i class="fa fa-envelope"></i></span>' + l.SMS;
                innerP += '</span>';
                innerP += '<span class="tooltip " data="Bình chọn qua web">';
                innerP += '<span><i class="fa fa-desktop"></i></span>' + l.Web;
                innerP += '</span>';
                innerP += '<span class=" tooltip top_ top_" data="Xếp hạng">';
                innerP += '<span class="first_ ">';
                innerP += '<img src="/Static/frontend/hoakhoi/images/one.png"> ';
                innerP += '</span>'
                innerP += '</span>'
                date_p.innerHTML = innerP;

                //add a to div
                //div_item_content.appendChild(div_item_category);
                div_item_content.appendChild(h3);
                div_item_content.appendChild(date_p);

                div_item.appendChild(div_love);
                div_item.appendChild(div_item_header);
                div_item.appendChild(div_item_content);

                //elem.appendChild($content);
                return div_item;
            }

            loadCount = loadCount + 1;

        });

    });


})();