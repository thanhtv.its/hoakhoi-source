﻿(function () {


    //code dealing with product index page loadMore ajaxCall and smooth scroll down
    $(function () {

        var loadCount = 1;
        var loading = $("#loading");
        $("#loadMore").on("click", function (e) {

            e.preventDefault();

            $(document).on({

                ajaxStart: function () {
                    loading.show();
                },
                ajaxStop: function () {
                    loading.hide();
                }
            });
            //var url = "/news/_get_data_more";
         
            var grid = document.querySelector('#grid');
            var elems = [];
            var fragment = document.createDocumentFragment();
            var itemHTML = "";

            var $grid = $('#grid').masonry({
                itemSelector: '.grid-item',
                percentPosition: true,
                columnWidth: '.grid-item',
                gutter: 10
            });

            // /swiper in tabs
            $.get(url + '?size=' + loadCount * itemcount + '&cateID=' + cateID + '&subD=' + subD, // url
                function (response) { // success callback
                    // response has teh same properties as the server returnedObject
                    $.each(response.model, function (i, l) {
                        var elem = getItemElement(l);
                        fragment.appendChild(elem);
                        //grid.appendChild(elem);
                        elems.push(elem);
                    });

                    // append elements to container
                    grid.appendChild(fragment);
                    // add and lay out newly appended elements
                    //msnry.appended(elems);
                    $grid.imagesLoaded(function () {
                        $grid.append(elems).masonry('appended', elems);
                    });

                    var ajaxModelCount = response.count - (loadCount * itemcount);
                    if (ajaxModelCount <= 0) {
                        $("#loadMore").hide().fadeOut(2000);
                    }


                },
                'json' // dataType
            );


            function getItemElement(l) {
                var elem = document.createElement('li');

                var div_item = document.createElement('div');
                div_item.setAttribute('class', 'item grid-item');

                var div_item_header = document.createElement('div');
                div_item_header.setAttribute('class', 'item-header');
                var a_item = document.createElement('a');
                a_item.setAttribute('class', 'image-hover');
                a_item.setAttribute('href', l.Link);

                var a_item_img = document.createElement('img');
                a_item_img.setAttribute('class', 'image-hover');
                a_item_img.setAttribute('src', '/Uploaded/Images/tintuc/' + l.Folder + '/' + l.NewsImage + '');

                //add img to a
                a_item.appendChild(a_item_img);
                //add a to div
                div_item_header.appendChild(a_item);

                var div_item_content = document.createElement('div');
                div_item_content.setAttribute('class', 'item-content');
                var div_item_category = document.createElement('div');
                div_item_category.setAttribute('class', 'content-category');
                var a_item_category = document.createElement('a');
                a_item_category.setAttribute('href', l.LinkCate);
                a_item_category.style.color = "#e8b400";
                a_item_category.innerHTML = l.NewsCateName;
                div_item_category.appendChild(a_item_category);

                var h3 = document.createElement('h3');
                var a_h3 = document.createElement('a');
                a_h3.setAttribute('href', l.Link);
                var a_h3_text = document.createTextNode(l.NewsTitle);
                a_h3.appendChild(a_h3_text);
                h3.appendChild(a_h3);
                var date_p = document.createElement('p');
                date_p.innerHTML = l.CreateDateString;
                var div_item_short = document.createElement('div');
                div_item_short.setAttribute('class', 'short_des');
                var short_html = l.ShortDescription;
                div_item_short.innerHTML = short_html;

                //add a to div
                div_item_content.appendChild(div_item_category);
                div_item_content.appendChild(h3);
                div_item_content.appendChild(date_p);
                div_item_content.appendChild(div_item_short);


                div_item.appendChild(div_item_header);
                div_item.appendChild(div_item_content);
                elem.appendChild(div_item);

                //elem.appendChild($content);
                return div_item;
            }

            loadCount = loadCount + 1;

        });

    });


})();