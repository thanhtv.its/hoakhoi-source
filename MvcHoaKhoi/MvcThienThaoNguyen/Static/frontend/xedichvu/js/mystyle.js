﻿if ($().swiper) {
    var gioi_thieu = $('.gioi_thieu_slide');
    var gioi_thieu_thumb = $('.gioi_thieu_slide_thumb');
    if (gioi_thieu.length) {
        gioi_thieu = new Swiper(gioi_thieu, {
            nextButton: '.about_btn_n',
            prevButton: '.about_btn_p',
            spaceBetween: 10,
        });
    }
    if (gioi_thieu_thumb.length) {
        gioi_thieu_thumb = new Swiper(gioi_thieu_thumb, {
            spaceBetween: 10,
            centeredSlides: true,
            slidesPerView: 'auto',
            touchRatio: 0.2,
            slideToClickedSlide: true
        });
    }
    gioi_thieu.params.control = gioi_thieu_thumb;
    gioi_thieu_thumb.params.control = gioi_thieu;
}