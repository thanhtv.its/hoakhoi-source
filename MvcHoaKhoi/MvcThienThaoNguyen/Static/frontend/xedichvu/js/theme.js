﻿'use strict';

// Cache
var body = $('body');
var mainSlider = $('#main-slider');
var imageCarousel = $('.img-carousel');
var partnersCarousel = $('#partners');
var testimonialsCarousel = $('#testimonials');
var testimonialsCarouselAlt = $('#testimonials-alt');
var carCarousel = $('.car-carousel');
var mySlider = $('.my_slide');
var topProductsCarousel = $('#top-products-carousel');
var featuredProductsCarousel = $('#featured-products-carousel');
var sidebarProductsCarousel = $('#sidebar-products-carousel');
var hotDealsCarousel = $('#hot-deals-carousel');
var owlCarouselSelector = $('.owl-carousel');
var isotopeContainer = $('.isotope');
var isotopeFiltrable = $('#filtrable a');
var toTop = $('#to-top');
var hover = $('.thumbnail');
var navigation = $('.navigation');

var superfishMenu = $('ul.sf-menu');

var priceSliderRange = $('#slider-range');

var swiperOffersBest = $('.swiper--offers-best .swiper-container');
var swiperOffersPopular = $('.swiper--offers-popular .swiper-container');
var swiperOffersEconomic = $('.swiper--offers-economic .swiper-container');

var nhan_vien = $('.nhan_vien .swiper-container');
var tai_xe = $('.tai_xe .swiper-container');
var chon_tai_xe = $('.chon_tai_xe .swiper-container');



var vip_1 = $('#vip_1');
var vip_2 = $('#vip_2');
var vip_3 = $('#vip_3');

var xe4_cho_1 = $('#xe4_cho_1');
var xe4_cho_2 = $('#xe4_cho_2');
var xe4_cho_3 = $('#xe4_cho_3');
var xe4_cho_4 = $('#xe4_cho_4');
var xe4_cho_5 = $('#xe4_cho_5');
var xe4_cho_6 = $('#xe4_cho_6');
var xe4_cho_7 = $('#xe4_cho_7');
var xe4_cho_8 = $('#xe4_cho_8');

var xe7_cho_1 = $('#xe7_cho_1');
var xe7_cho_2 = $('#xe7_cho_2');
var xe7_cho_3 = $('#xe7_cho_3');

var xe16_cho_1 = $('#xe16_cho_1');
var xe16_cho_2 = $('#xe16_cho_2');
var xe16_cho_3 = $('#xe16_cho_3');

var xe29_cho_1 = $('#xe29_cho_1');
var xe29_cho_2 = $('#xe29_cho_2');

var xe35_cho_1 = $('#xe35_cho_1');
var xe35_cho_2 = $('#xe35_cho_2');

var xe45_cho_1 = $('#xe45_cho_1');

jQuery(window).load(function () { jQuery('body').scrollspy({ offset: 100, target: '.navigation' }); });
jQuery(window).load(function () { jQuery('body').scrollspy('refresh'); });
jQuery(window).resize(function () { jQuery('body').scrollspy('refresh'); });
jQuery(window).load(function () {
    if (location.hash != '') {
        var hash = '#' + window.location.hash.substr(1);
        if (hash.length) {
            jQuery('html,body').delay(0).animate({
                scrollTop: jQuery(hash).offset().top - 44 + 'px'
            }, {
                duration: 1200,
                easing: "easeInOutExpo"
            });
        }
    }
});

$(function () {
    //var dd = new DropDown($('#dd'));
   
    //$(document).click(function () {
    //    // all dropdowns
    //    $('.wrapper-dropdown-3').removeClass('active');
    //});

    $('#dd').on('click', function (event) {
        $(this).toggleClass('active');
    }); 
   

});
//function DropDown(el) {
//    this.dd = el;
//    this.placeholder = this.dd.children('span');
//    this.opts = this.dd.find('ul.dropdown > li');
//    this.val = '';
//    this.index = 1;
//    this.initEvents();

//    $('#dd').on('click', function (event) {
//                    $(this).toggleClass('active');
//                    return false;
//                });
//}
//DropDown.prototype = {
//    initEvents: function () {
//        var obj = this;

//        obj.dd.on('click', function (event) {
//            $(this).toggleClass('active');
//            return false;
//        });

//        obj.opts.on('click', function () {
//            var opt = $(this);
//            obj.val = opt.text();
//            obj.index = opt.index();
//            obj.placeholder.text(obj.val);
//        });
//    },
//    getValue: function () {
//        return this.val;
//    },
//    getIndex: function () {
//        return this.index;
//    }
//}



$(window).scroll(function () {
    if ($(this).scrollTop() > 1) {
        $('#top_menu').addClass('on_top_menu');
    } else {
        $('#top_menu').removeClass('on_top_menu');
    }
});

// Slide in/out with fade animation function
jQuery.fn.slideFadeToggle = function (speed, easing, callback) {
    return this.animate({ opacity: 'toggle', height: 'toggle' }, speed, easing, callback);
};
//
jQuery.fn.slideFadeIn = function (speed, easing, callback) {
    return this.animate({ opacity: 'show', height: 'show' }, speed, easing, callback);
};
jQuery.fn.slideFadeOut = function (speed, easing, callback) {
    return this.animate({ opacity: 'hide', height: 'hide' }, speed, easing, callback);
};

jQuery(document).ready(function () {
    // Prevent empty links
    // ---------------------------------------------------------------------------------------
    $('a[href=#]').on('click', function (event) {
        event.preventDefault();
    });
    var div = $('#styleSwitcher');
  
    $('#styleSwitcherOpen').on('click', function (event) {
        
        
        div.hasClass("active") ? div.animate({ left: "-" + div.width() + "px" }, 300).removeClass("active") : div.animate({ left: "0" }, 300).addClass("active")
        !div.hasClass("active") ? $('#styleSwitcherOpen').animate({ left: "" + div.width() + "px" }, 0) : $('#styleSwitcherOpen').animate({ left: "0" }, 0)
        !div.hasClass("active") ? $('#content_book').toggle() : $('#content_book').toggle()
        //if (div.hasClass('active') == true) {
        //    $('#styleSwitcherOpen').css('left', div.width() + 'px');
        //}
        //else {
        //    $('#styleSwitcherOpen').css('left', '0px');
        //}                                   
    });


    // Sticky header/menu
    // ---------------------------------------------------------------------------------------
    if ($().sticky) {

        //$('#top_menu').sticky({ topSpacing: 0 });
        //$('.header.fixed').on('sticky-start', function() { console.log("Started"); });
        //$('.header.fixed').on('sticky-end', function() { console.log("Ended"); });
    }
    // SuperFish menu
    // ---------------------------------------------------------------------------------------
    if ($().superfish) {
        superfishMenu.superfish();
    }
    //    $('ul.sf-menu a').on('click', function(){
    //        body.scrollspy('refresh');
    //    });
    // Fixed menu toggle
    $('.menu-toggle').on('click', function () {
        if (navigation.hasClass('opened')) {
            navigation.removeClass('opened').addClass('closed');
        } else {
            navigation.removeClass('closed').addClass('opened');
        }
    });
    $('.menu-toggle-close').on('click', function () {
        if (navigation.hasClass('opened')) {
            navigation.removeClass('opened').addClass('closed');
        } else {
            navigation.removeClass('closed').addClass('opened');
        }
    });
    //
    if ($('.content-area.scroll').length) {
        $('.open-close-area').on('click', function () {
            if ($('.wrapper').hasClass('opened')) {
                $('.wrapper').removeClass('opened').addClass('closed');
            } else {
                $('.wrapper').removeClass('closed').addClass('opened');
            }
        });
    }
    // Smooth scrolling
    // ----------------------------------------------------------------------------------------
    $('.sf-menu a').on('click', function () {

        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 43
        }, {
            duration: 1000,
            easing: 'easeInOutExpo'
        });
        return false;
    });
    // BootstrapSelect
    // ---------------------------------------------------------------------------------------
    if ($().selectpicker) {
        $('.selectpicker').selectpicker();
    }
    // prettyPhoto
    // ---------------------------------------------------------------------------------------
    if ($().prettyPhoto) {
        $("a[data-gal^='prettyPhoto']").prettyPhoto({
            theme: 'dark_square'
        });
    }
    // Scroll totop button
    // ---------------------------------------------------------------------------------------
    $(window).scroll(function () {
        if ($(this).scrollTop() > 1) {
            toTop.css({ bottom: '15px' });
        } else {
            toTop.css({ bottom: '-100px' });
        }
    });
    toTop.on('click', function () {
        $('html, body').animate({ scrollTop: '0px' }, 800);
        return false;
    });
    // Add hover class for correct view on mobile devices
    // ---------------------------------------------------------------------------------------
    /*hover.on('hover',
        function () {
            $(this).addClass('hover');
        },
        function () {
            $(this).removeClass('hover');
        }
    );*/
    // Ajax / load external content in tabs
    // ---------------------------------------------------------------------------------------
    $('[data-toggle="tabajax"]').on('click', function (e) {
        e.preventDefault();
        var loadurl = $(this).attr('href');
        var targ = $(this).attr('data-target');
        $.get(loadurl, function (data) {
            $(targ).html(data);
        });
        $(this).tab('show');
    });
    // Sliders
    // ---------------------------------------------------------------------------------------
    if ($().owlCarousel) {
        var owl = $('.owl-carousel');
        owl.on('changed.owl.carousel', function (e) {
            // update prettyPhoto
            if ($().prettyPhoto) {
                $("a[data-gal^='prettyPhoto']").prettyPhoto({
                    theme: 'dark_square'
                });
            }
        });
        // Main slider
        if (mainSlider.length) {
            mainSlider.owlCarousel({
                items: 1,
                autoplay: 1000,
                slideSpeed: 2000,
                paginationSpeed: 2000,
                autoplayHoverPause: true,
                loop: true,
                margin: 0,
                dots: true,
                nav: true,
                thumbs: true,
                thumbImage: true,
                thumbContainerClass: 'owl-thumbs',
                thumbItemClass: 'owl-thumb-item',
                transitionStyle: "auto",
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
                responsiveRefreshRate: 100,
                responsive: {
                    0: { items: 1 },
                    479: { items: 1 },
                    768: { items: 1 },
                    991: { items: 1 },
                    1024: { items: 1 }
                }
            });
        }
        // Top products carousel
        if (topProductsCarousel.length) {
            topProductsCarousel.owlCarousel({
                autoplay: false,
                loop: true,
                margin: 30,
                dots: false,
                nav: true,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
                responsive: {
                    0: { items: 1 },
                    479: { items: 2 },
                    768: { items: 3 },
                    991: { items: 4 },
                    1024: { items: 5 },
                    1280: { items: 6 }
                }
            });
        }
        // Featured products carousel
        if (featuredProductsCarousel.length) {
            featuredProductsCarousel.owlCarousel({
                autoplay: false,
                loop: true,
                margin: 30,
                dots: false,
                nav: true,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
                responsive: {
                    0: { items: 1 },
                    479: { items: 1 },
                    768: { items: 2 },
                    991: { items: 3 },
                    1024: { items: 4 }
                }
            });
        }
        // Sidebar products carousel
        if (sidebarProductsCarousel.length) {
            sidebarProductsCarousel.owlCarousel({
                autoplay: false,
                loop: true,
                margin: 30,
                dots: true,
                nav: false,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
                responsive: {
                    0: { items: 1 },
                    479: { items: 1 },
                    768: { items: 1 },
                    991: { items: 1 },
                    1024: { items: 1 }
                }
            });
        }
        // Partners carousel
        if (partnersCarousel.length) {
            partnersCarousel.owlCarousel({
                autoplay: false,
                loop: true,
                margin: 30,
                dots: false,
                nav: true,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
                responsive: {
                    0: { items: 1 },
                    479: { items: 2 },
                    768: { items: 3 },
                    991: { items: 4 },
                    1024: { items: 4 },
                    1280: { items: 5 }
                }
            });
        }
        // Testimonials carousel
        if (testimonialsCarousel.length) {
            testimonialsCarousel.owlCarousel({
                autoplay: false,
                loop: true,
                margin: 0,
                dots: true,
                nav: false,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
                responsive: {
                    0: { items: 1 },
                    479: { items: 1 },
                    768: { items: 1 },
                    991: { items: 1 },
                    1024: { items: 1 },
                    1280: { items: 1 }
                }
            });
        }
        // Testimonials carousel alt version
        if (testimonialsCarouselAlt.length) {
            testimonialsCarouselAlt.owlCarousel({
                autoplay: false,
                loop: true,
                margin: 0,
                dots: true,
                nav: false,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
                responsive: {
                    0: { items: 1 },
                    479: { items: 1 },
                    768: { items: 1 },
                    991: { items: 2 },
                    1024: { items: 2 },
                    1280: { items: 2 }
                }
            });
        }
        // Images carousel
        if (imageCarousel.length) {
            imageCarousel.owlCarousel({
                autoplay: false,
                loop: false,
                margin: 0,
                dots: true,
                nav: false,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
                responsiveRefreshRate: 100,
                responsive: {
                    0: { items: 1 },
                    479: { items: 1 },
                    768: { items: 1 },
                    991: { items: 1 },
                    1024: { items: 1 }
                }
            });
        }
        // Car carousel
        if (carCarousel.length) {
            carCarousel.owlCarousel({
                autoplay: true,
                loop: true,
                margin: 0,
                dots: true,
                nav: true,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
                responsiveRefreshRate: 100,
                responsive: {
                    0: { items: 2 },
                    479: { items: 2 },
                    768: { items: 2 },
                    991: { items: 3 },
                    1024: { items: 4 }
                }
            });
        }


        if (mySlider.length) {
            mySlider.owlCarousel({
                loop: true,
                items: 1,
                dots: true,

                thumbs: true,
                thumbImage: true,
                thumbContainerClass: 'owl-thumbs',
                thumbItemClass: 'owl-thumb-item',

            });
        }



        // on tab click
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            updater();
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            updater();
        });
    }
    // Sliders
    // ---------------------------------------------------------------------------------------
    if ($().swiper) {
        if (swiperOffersBest.length) {
            swiperOffersBest = new Swiper(swiperOffersBest, {
                direction: 'horizontal',
                slidesPerView: 3,
                spaceBetween: 10,
                autoplay: 2000,
                loop: false,
                paginationClickable: true,
                pagination: '.swiper-pagination',
                nextButton: '.yeuthich_btn_n',
                prevButton: '.yeuthich_btn_p'
            });
        }
        if (swiperOffersPopular.length) {
            swiperOffersPopular = new Swiper(swiperOffersPopular, {
                direction: 'horizontal',
                slidesPerView: 3,
                spaceBetween: 10,
                autoplay: 2000,
                loop: false,
                paginationClickable: true,
                pagination: '.swiper-pagination',
                nextButton: '.pop_btn_n',
                prevButton: '.pop_btn_p'
            });
        }
        if (swiperOffersEconomic.length) {
            swiperOffersEconomic = new Swiper(swiperOffersEconomic, {
                direction: 'horizontal',
                slidesPerView: 3,
                spaceBetween: 10,
                autoplay: 2000,
                loop: false,
                paginationClickable: true,
                pagination: '.swiper-pagination',
                nextButton: '.tietkiem_btn_n',
                prevButton: '.tietkiem_btn_p'
            });
        }
        var swiper = new Swiper('.navigation', {
            scrollbar: '.swiper-scrollbar',
            direction: 'vertical',
            slidesPerView: 'auto',
            mousewheelControl: true,
            freeMode: true
        });
        if ($('.content-area.scroll').length) {
            var swiper2 = new Swiper('.content-area.scroll', {
                scrollbar: '.swiper-scrollbar',
                direction: 'vertical',
                slidesPerView: 'auto',
                mousewheelControl: true,
                freeMode: true
            });
        }

        if (nhan_vien.length) {
            nhan_vien = new Swiper(nhan_vien, {
                direction: 'horizontal',
                slidesPerView: 4,
                spaceBetween: 20,
                //autoplay: 2000,
               
                paginationClickable: true,
                nextButton: '.nhan_vien_btn_n',
                prevButton: '.nhan_vien_btn_p',
            });
        }
        if (tai_xe.length) {
            tai_xe = new Swiper(tai_xe, {
                direction: 'horizontal',
                slidesPerView: 4,
                spaceBetween: 20,
                //autoplay: 2000,
            
                paginationClickable: true,
                nextButton: '.tai_xe_btn_n',
                prevButton: '.tai_xe_btn_p',
            });
        }
        if (chon_tai_xe.length) {
            chon_tai_xe = new Swiper(chon_tai_xe, {
                direction: 'horizontal',

                slidesPerView: 4,
                spaceBetween: 20,
                nextButton: '.chon_tai_xe_btn_n',
                prevButton: '.chon_tai_xe_btn_p',
            });
        }


   //     // /swiper in tabs
   //     $.get('/common/GetXe', // url
   //    function (response) { // success callback
   //        // response has teh same properties as the server returnedObject
   //        for (var i = 0; i < response.length; i++) {
   //            var tenslide = $("#chitiet_" + response[i].XeID);
   //            if (tenslide.length) {
   //                tenslide = new Swiper(tenslide, {
   //                    pagination: "#chitiet_" + response[i].XeID + " .row.car-thumbnails",
   //                    loop: false,
            
   //                    grabCursor: true,
   //                    paginationClickable: true,
   //                    nextButton: '.chitiet_btn_n_' + response[i].XeID,
   //                    prevButton: '.chitiet_btn_p_' + response[i].XeID,
   //                    paginationBulletRender: function (index, className) {
   //                        return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>';
   //                    }
   //                });
   //            }

   //        }

   //    },
   //    'json' // dataType
   //);

        //xe vip
        if (vip_1.length) {
            vip_1 = new Swiper(vip_1, {
                pagination: '#vip_1 .row.car-thumbnails',
                paginationClickable: true,
                paginationBulletRender: function (index, className) {
                    return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>';
                }
            });
        }
        if (vip_2.length) {
            vip_2 = new Swiper(vip_2, {
                pagination: '#vip_2 .row.car-thumbnails',
                paginationClickable: true,
                paginationBulletRender: function (index, className) {
                    return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>';
                }
            });
        }
        if (vip_3.length) {
            vip_3 = new Swiper(vip_3, {
                pagination: '#vip_3 .row.car-thumbnails',
                paginationClickable: true,
                paginationBulletRender: function (index, className) {
                    return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>';
                }
            });
        }

        //loaixe 4 cho

        if (xe4_cho_1.length) {
            xe4_cho_1 = new Swiper(xe4_cho_1, {
                pagination: '#xe4_cho_1 .row.car-thumbnails',
                paginationClickable: true,
                paginationBulletRender: function (index, className) {
                    return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>';
                }
            });
        }
        if (xe4_cho_2.length) {
            xe4_cho_2 = new Swiper(xe4_cho_2, {
                pagination: '#xe4_cho_2 .row.car-thumbnails',
                paginationClickable: true,
                paginationBulletRender: function (index, className) {
                    return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>';
                }
            });
        }
        if (xe4_cho_3.length) {
            xe4_cho_3 = new Swiper(xe4_cho_3, {
                pagination: '#xe4_cho_3 .row.car-thumbnails',
                paginationClickable: true,
                paginationBulletRender: function (index, className) {
                    return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>';
                }
            });
        }
        if (xe4_cho_4.length) {
            xe4_cho_4 = new Swiper(xe4_cho_4, {
                pagination: '#xe4_cho_4 .row.car-thumbnails',
                paginationClickable: true,
                paginationBulletRender: function (index, className) {
                    return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>';
                }
            });
        }
        if (xe4_cho_5.length) {
            xe4_cho_5 = new Swiper(
                xe4_cho_5, {
                    pagination: '#xe4_cho_5 .row.car-thumbnails',
                    paginationClickable: true,
                    paginationBulletRender: function (index, className) {
                        return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>';
                    }
                });
        }
        if (xe4_cho_6.length) {
            xe4_cho_6 = new Swiper(xe4_cho_6, {
                pagination: '#xe4_cho_6 .row.car-thumbnails',
                paginationClickable: true,
                paginationBulletRender: function (index, className) {
                    return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>';
                }
            });
        }
        if (xe4_cho_7.length) {
            xe4_cho_7 = new Swiper(xe4_cho_7, {
                pagination: '#xe4_cho_7 .row.car-thumbnails',
                paginationClickable: true,
                paginationBulletRender: function (index, className) {
                    return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>';
                }
            });
        }
        if (xe4_cho_8.length) {
            xe4_cho_8 = new Swiper(xe4_cho_8, {
                pagination: '#xe4_cho_8 .row.car-thumbnails',
                paginationClickable: true,
                paginationBulletRender: function (index, className) {
                    return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>';
                }
            });
        }





        //loaixe 7 cho

        if (xe7_cho_1.length) {
            xe7_cho_1 = new Swiper(xe7_cho_1, {
                pagination: '#xe7_cho_1 .row.car-thumbnails', paginationClickable: true,
                paginationBulletRender: function (index, className) { return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>'; }
            });
        }
        if (xe7_cho_2.length) {
            xe7_cho_2 = new Swiper(xe7_cho_2, {
                pagination: '#xe7_cho_2 .row.car-thumbnails', paginationClickable: true,
                paginationBulletRender: function (index, className) { return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>'; }
            });
        }
        if (xe7_cho_3.length) {
            xe7_cho_3 = new Swiper(xe7_cho_3, {
                pagination: '#xe7_cho_3 .row.car-thumbnails', paginationClickable: true,
                paginationBulletRender: function (index, className) { return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>'; }
            });
        }




        //loaixe 16 cho

        if (xe16_cho_1.length) {
            xe16_cho_1 = new Swiper(xe16_cho_1, {
                pagination: '#xe16_cho_1 .row.car-thumbnails', paginationClickable: true,
                paginationBulletRender: function (index, className) { return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>'; }
            });
        }
        if (xe16_cho_2.length) {
            xe16_cho_2 = new Swiper(xe16_cho_2, {
                pagination: '#xe16_cho_2 .row.car-thumbnails', paginationClickable: true,
                paginationBulletRender: function (index, className) { return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>'; }
            });
        }
        if (xe16_cho_3.length) {
            xe16_cho_3 = new Swiper(xe16_cho_3, {
                pagination: '#xe16_cho_3 .row.car-thumbnails', paginationClickable: true,
                paginationBulletRender: function (index, className) { return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>'; }
            });
        }



        //loaixe 29 cho

        if (xe29_cho_1.length) {
            xe29_cho_1 = new Swiper(xe29_cho_1, {
                pagination: '#xe29_cho_1 .row.car-thumbnails', paginationClickable: true,
                paginationBulletRender: function (index, className) { return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>'; }
            });
        }
        if (xe29_cho_2.length) {
            xe29_cho_2 = new Swiper(xe29_cho_2, {
                pagination: '#xe29_cho_2 .row.car-thumbnails', paginationClickable: true,
                paginationBulletRender: function (index, className) { return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>'; }
            });
        }


        //loaixe 35 cho

        if (xe35_cho_1.length) {
            xe35_cho_1 = new Swiper(xe35_cho_1, {
                pagination: '#xe35_cho_1 .row.car-thumbnails', paginationClickable: true,
                paginationBulletRender: function (index, className) { return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>'; }
            });
        }
        if (xe35_cho_2.length) {
            xe35_cho_2 = new Swiper(xe35_cho_2, {
                pagination: '#xe35_cho_2 .row.car-thumbnails', paginationClickable: true,
                paginationBulletRender: function (index, className) { return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>'; }
            });
        }

        //loaixe 45 cho

        if (xe45_cho_1.length) {
            xe45_cho_1 = new Swiper(xe45_cho_1, {
                pagination: '#xe45_cho_1 .row.car-thumbnails', paginationClickable: true,
                paginationBulletRender: function (index, className) { return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>'; }
            });
        }





    }
    // countdown
    // ---------------------------------------------------------------------------------------
    if ($().countdown) {
        var austDay = new Date();
        austDay = new Date(austDay.getFullYear() + 1, 1 - 1, 26);
        $('#dealCountdown1').countdown({ until: austDay });
        $('#dealCountdown2').countdown({ until: austDay });
        $('#dealCountdown3').countdown({ until: austDay });
    }
    // Google map
    // ---------------------------------------------------------------------------------------

    // Price range / need jquery ui
    // ---------------------------------------------------------------------------------------
    if ($.ui) {
        if ($(priceSliderRange).length) {
            $(priceSliderRange).slider({
                range: true,
                min: 0,
                max: 500,
                values: [75, 300],
                slide: function (event, ui) {
                    $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
                }
            });
            $("#amount").val(
                "$" + $("#slider-range").slider("values", 0) +
                " - $" + $("#slider-range").slider("values", 1)
            );
        }
    }
    // Shop categories widget slide in/out
    // ---------------------------------------------------------------------------------------
    $('.car-categories .arrow').on('click', function (event) {

        $(this).parent().parent().find('ul.children').removeClass('active');
        $(this).parent().parent().find('.fa-angle-up').addClass('fa-angle-down').removeClass('fa-angle-up');
        if ($(this).parent().find('ul.children').is(":visible")) {
            //$(this).find('.fa-angle-up').addClass('fa-angle-down').removeClass('fa-angle-up');
            //$(this).parent().find('ul.children').removeClass('active');
        }
        else {
            $(this).find('.fa-angle-down').addClass('fa-angle-up').removeClass('fa-angle-down');
            $(this).parent().find('ul.children').addClass('active');
        }
        $(this).parent().parent().find('ul.children').each(function () {
            if (!$(this).hasClass('active')) {
                $(this).slideFadeOut();
            }
            else {
                $(this).slideFadeIn();
            }
        });
    }
    );
    $('.car-categories ul.children').each(function () {
        if (!$(this).hasClass('active')) {
            $(this).hide();
        }
    });
    // Ripple effect on click for buttons
    // ---------------------------------------------------------------------------------------
    $(".ripple-effect").on('click', function (e) {
        var rippler = $(this);

        // create .ink element if it doesn't exist
        if (rippler.find(".ink").length == 0) {
            rippler.append("<span class='ink'></span>");
        }

        var ink = rippler.find(".ink");

        // prevent quick double clicks
        ink.removeClass("animate");

        // set .ink diametr
        if (!ink.height() && !ink.width()) {
            var d = Math.max(rippler.outerWidth(), rippler.outerHeight());
            ink.css({ height: d, width: d });
        }

        // get click coordinates
        var x = e.pageX - rippler.offset().left - ink.width() / 2;
        var y = e.pageY - rippler.offset().top - ink.height() / 2;

        // set .ink position and add class .animate
        ink.css({
            top: y + 'px',
            left: x + 'px'
        }).addClass("animate");
    })
    updater();
});

jQuery(window).load(function () {
    // Preloader
    $('#status').fadeOut();
    $('#preloader').delay(200).fadeOut(200);
    // Isotope
    if ($().isotope) {
        isotopeContainer.isotope({ // initialize isotope
            itemSelector: '.isotope-item' // options...
            //,transitionDuration: 0 // disable transition
        });
        isotopeFiltrable.on('click', function () { // filter items when filter link is clicked
            var selector = $(this).attr('data-filter');
            isotopeFiltrable.parent().removeClass('current');
            $(this).parent().addClass('current');
            isotopeContainer.isotope({ filter: selector });
            return false;
        });
        isotopeContainer.isotope('reLayout'); // layout/reLayout
    }
    // Scroll to hash
    if (location.hash != '') {
        var hash = '#' + window.location.hash.substr(1);
        if (hash.length) {
            body.delay(0).animate({
                scrollTop: jQuery(hash).offset().top
            }, {
                duration: 1200,
                easing: "easeInOutExpo"
            });
        }
    }
    // OwlSliders
    if ($().owlCarousel) {
        // Hot deal carousel
        // must initialized after counters
        if (hotDealsCarousel.length) {
            hotDealsCarousel.owlCarousel({
                autoplay: false,
                loop: true,
                margin: 30,
                dots: true,
                nav: false,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ],
                responsive: {
                    0: { items: 1 },
                    479: { items: 1 },
                    768: { items: 1 },
                    991: { items: 1 },
                    1024: { items: 1 }
                }
            });
        }
    }
    updater();
});

function updater() {
    if ($().sticky) {
        $('.header.fixed').sticky('update');
    }

    // refresh swiper slider
    if ($().swiper) {


//        $.get('/common/GetXe', // url
//function (response) { // success callback
//    // response has teh same properties as the server returnedObject
//    for (var i = 1; i < response.length; i++) {
//        var tenslide1 = $("#chitiet_" + response[i].XeID);
//        if (typeof (tenslide1.length) == "undefined") {
//            tenslide1.update();
//            tenslide1.onResize();
//        }

//    }

//},
//'json' // dataType
//);

//        $.get('/common/GetXe', // url
//    function (response) { // success callback
//        // response has teh same properties as the server returnedObject
//        for (var i = 0; i < response.length; i++) {
//            var tenslide2 = $("#chitiet_" + response[i].XeID);
//            if (tenslide2.length) {
//                tenslide2 = new Swiper(tenslide2, {
//                    pagination: "#chitiet_" + response[i].XeID + " .row.car-thumbnails",
//                    loop: false,
             
//                    grabCursor: true,
//                    paginationClickable: true,
//                    nextButton: '.chitiet_btn_n_' + response[i].XeID,
//                    prevButton: '.chitiet_btn_p_' + response[i].XeID,
//                    paginationBulletRender: function (index, className) {
//                        return '<div class="slide_page col-xs-2 col-sm-2 col-md-3  ' + className + '"><a href="#"></a></div>';
//                    }
//                });
//            }

//        }

//    },
//    'json' // dataType
//);
        if (typeof (vip_1.length) == 'undefined') { vip_1.update(); vip_1.onResize(); }
        if (typeof (vip_2.length) == 'undefined') { vip_2.update(); vip_2.onResize(); }
        if (typeof (vip_3.length) == 'undefined') { vip_3.update(); vip_3.onResize(); }

        if (typeof (xe4_cho_1.length) == 'undefined') { xe4_cho_1.update(); xe4_cho_1.onResize(); }
        if (typeof (xe4_cho_2.length) == 'undefined') { xe4_cho_2.update(); xe4_cho_2.onResize(); }
        if (typeof (xe4_cho_3.length) == 'undefined') { xe4_cho_3.update(); xe4_cho_3.onResize(); }
        if (typeof (xe4_cho_4.length) == 'undefined') { xe4_cho_4.update(); xe4_cho_4.onResize(); }
        if (typeof (xe4_cho_5.length) == 'undefined') { xe4_cho_5.update(); xe4_cho_5.onResize(); }
        if (typeof (xe4_cho_6.length) == 'undefined') { xe4_cho_6.update(); xe4_cho_6.onResize(); }
        if (typeof (xe4_cho_7.length) == 'undefined') { xe4_cho_7.update(); xe4_cho_7.onResize(); }
        if (typeof (xe4_cho_8.length) == 'undefined') { xe4_cho_8.update(); xe4_cho_8.onResize(); }

        if (typeof (xe7_cho_1.length) == 'undefined') { xe7_cho_1.update(); xe7_cho_1.onResize(); }
        if (typeof (xe7_cho_2.length) == 'undefined') { xe7_cho_2.update(); xe7_cho_2.onResize(); }
        if (typeof (xe7_cho_3.length) == 'undefined') { xe7_cho_3.update(); xe7_cho_3.onResize(); }

        if (typeof (xe16_cho_1.length) == 'undefined') { xe16_cho_1.update(); xe16_cho_1.onResize(); }
        if (typeof (xe16_cho_2.length) == 'undefined') { xe16_cho_2.update(); xe16_cho_2.onResize(); }
        if (typeof (xe16_cho_3.length) == 'undefined') { xe16_cho_3.update(); xe16_cho_3.onResize(); }

        if (typeof (xe29_cho_1.length) == 'undefined') { xe29_cho_1.update(); xe29_cho_1.onResize(); }
        if (typeof (xe29_cho_2.length) == 'undefined') { xe29_cho_2.update(); xe29_cho_2.onResize(); }

        if (typeof (xe35_cho_1.length) == 'undefined') { xe35_cho_1.update(); xe35_cho_1.onResize(); }
        if (typeof (xe35_cho_2.length) == 'undefined') { xe35_cho_2.update(); xe35_cho_2.onResize(); }

        if (typeof (xe45_cho_1.length) == 'undefined') { xe45_cho_1.update(); xe45_cho_1.onResize(); }

        //
        if (typeof (swiperOffersBest.length) == 'undefined') {
            swiperOffersBest.update();
            swiperOffersBest.onResize();
            if ($(window).width() > 991) {
                swiperOffersBest.params.slidesPerView = 3;
                //swiperOffersBest.stopAutoplay();
                //swiperOffersBest.startAutoplay();
            }
            else {
                if ($(window).width() < 768) {
                    swiperOffersBest.params.slidesPerView = 2;
                    //swiperOffersBest.stopAutoplay();
                    //swiperOffersBest.startAutoplay();
                }
                else {
                    swiperOffersBest.params.slidesPerView = 2;
                    //swiperOffersBest.stopAutoplay();
                    //swiperOffersBest.startAutoplay();
                }
            }
        }
        //
        if (typeof (swiperOffersPopular.length) == 'undefined') {
            swiperOffersPopular.update();
            swiperOffersPopular.onResize();
            if ($(window).width() > 991) {
                swiperOffersPopular.params.slidesPerView = 3;
                //swiperOffersPopular.stopAutoplay();
                //swiperOffersPopular.startAutoplay();
            }
            else {
                if ($(window).width() < 768) {
                    swiperOffersPopular.params.slidesPerView = 2;
                    //swiperOffersPopular.stopAutoplay();
                    //swiperOffersPopular.startAutoplay();
                }
                else {
                    swiperOffersPopular.params.slidesPerView = 2;
                    //swiperOffersPopular.stopAutoplay();
                    //swiperOffersPopular.startAutoplay();
                }
            }
        }
        //
        if (typeof (swiperOffersEconomic.length) == 'undefined') {
            swiperOffersEconomic.update();
            swiperOffersEconomic.onResize();
            if ($(window).width() > 991) {
                swiperOffersEconomic.params.slidesPerView = 3;
                //swiperOffersEconomic.stopAutoplay();
                //swiperOffersEconomic.startAutoplay();
            }
            else {
                if ($(window).width() < 768) {
                    swiperOffersEconomic.params.slidesPerView = 2;
                    //swiperOffersEconomic.stopAutoplay();
                    //swiperOffersEconomic.startAutoplay();
                }
                else {
                    //swiperOffersEconomic.params.slidesPerView = 2;
                    //swiperOffersEconomic.stopAutoplay();
                    //swiperOffersEconomic.startAutoplay();
                }
            }
        }

        if (typeof (tai_xe.length) == 'undefined') {
            tai_xe.update();
            tai_xe.onResize();
            if ($(window).width() > 991) {
                tai_xe.params.slidesPerView = 4;
                //swiperOffersPopular.stopAutoplay();
                //swiperOffersPopular.startAutoplay();
            }
            else {
                if ($(window).width() < 768) {
                    tai_xe.params.slidesPerView = 2;
                    //swiperOffersPopular.stopAutoplay();
                    //swiperOffersPopular.startAutoplay();
                }
                else {
                    tai_xe.params.slidesPerView = 2;
                    //swiperOffersPopular.stopAutoplay();
                    //swiperOffersPopular.startAutoplay();
                }
            }
        }
        if (typeof (nhan_vien.length) == 'undefined') {
            nhan_vien.update();
            nhan_vien.onResize();
            if ($(window).width() > 991) {
                nhan_vien.params.slidesPerView = 4;
                //swiperOffersPopular.stopAutoplay();
                //swiperOffersPopular.startAutoplay();
            }
            else {
                if ($(window).width() < 768) {
                    nhan_vien.params.slidesPerView = 2;
                    //swiperOffersPopular.stopAutoplay();
                    //swiperOffersPopular.startAutoplay();
                }
                else {
                    nhan_vien.params.slidesPerView = 2;
                    //swiperOffersPopular.stopAutoplay();
                    //swiperOffersPopular.startAutoplay();
                }
            }
        }
        if (typeof (chon_tai_xe.length) == 'undefined') {
            chon_tai_xe.update();
            chon_tai_xe.onResize();
            if ($(window).width() > 991) {
                chon_tai_xe.params.slidesPerView = 4;
                //swiperOffersPopular.stopAutoplay();
                //swiperOffersPopular.startAutoplay();
            }
            else {
                if ($(window).width() < 768) {
                    chon_tai_xe.params.slidesPerView = 2;
                    //swiperOffersPopular.stopAutoplay();
                    //swiperOffersPopular.startAutoplay();
                }
                else {
                    chon_tai_xe.params.slidesPerView = 2;
                    //swiperOffersPopular.stopAutoplay();
                    //swiperOffersPopular.startAutoplay();
                }
            }
        }




    }

    // refresh waypoints
    //$.waypoints('refresh');
    // refresh owl carousels/sliders
    //owlCarouselSelector.trigger('refresh');
    //owlCarouselSelector.trigger('refresh.owl.carousel');

    //$('#formSearchUpDate').datetimepicker();
    //$('#formSearchOffDate').datetimepicker();

    //$('#formSearchUpDate2').datetimepicker();
    //$('#formSearchOffDate2').datetimepicker();

    //$('#formSearchUpDate3').datetimepicker();
    //$('#formSearchOffDate3').datetimepicker();

    //$('#formFindCarDate').datetimepicker();
}

jQuery(window).resize(function () {
    // Refresh isotope
    if ($().isotope) {
        isotopeContainer.isotope('reLayout'); // layout/relayout on window resize
    }
    updater();
});

jQuery(window).scroll(function () {
    if ($().sticky) {
        $('.header.fixed').sticky('update');
    }
});