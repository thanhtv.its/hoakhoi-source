﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MvcWebsiteOcc.Models
{
    public static class OccConvert
    {
        public static String LayUrl(this String convertString, int id)
        {
            string url = KhongDau(convertString.ToLower()).ToLower() + "-" + id;

            return url;
        }

        public static int GetTweets(string url)
        {

            string jsonString = new System.Net.WebClient().DownloadString("http://urls.api.twitter.com/1/urls/count.json?url=" + url);

            var json = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<dynamic>(jsonString);
            int count = 0;
            if (json.ContainsKey("count"))
            {
                 count = (int)json["count"];
            }

            return  count;
        }

        public static int GetLikes(string url)
        {

            string jsonString = new System.Net.WebClient().DownloadString("http://graph.facebook.com/?ids=" + url);

            dynamic json = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize(jsonString, typeof(object));
            int count = 0;

            if (json[url].ContainsKey("shares"))
            {
                count = (int)json[url]["shares"]; 
            }
            return count;
        }

        public static int GetPlusOnes(string url)
        {

            string googleApiUrl = "https://clients6.google.com/rpc"; //?key=AIzaSyCKSbrvQasunBoV16zDH9R33D88CeLr9gQ";

            string postData = @"[{""method"":""pos.plusones.get"",""id"":""p"",""params"":{""nolog"":true,""id"":""" + url + @""",""source"":""widget"",""userId"":""@viewer"",""groupId"":""@self""},""jsonrpc"":""2.0"",""key"":""p"",""apiVersion"":""v1""}]";

            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(googleApiUrl);
            request.Method = "POST";
            request.ContentType = "application/json-rpc";
            request.ContentLength = postData.Length;

            System.IO.Stream writeStream = request.GetRequestStream();
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] bytes = encoding.GetBytes(postData);
            writeStream.Write(bytes, 0, bytes.Length);
            writeStream.Close();

            System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse();
            System.IO.Stream responseStream = response.GetResponseStream();
            System.IO.StreamReader readStream = new System.IO.StreamReader(responseStream, Encoding.UTF8);
            string jsonString = readStream.ReadToEnd();

            readStream.Close();
            responseStream.Close();
            response.Close();

            var json = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<dynamic>(jsonString);
            int count = Int32.Parse(json[0]["result"]["metadata"]["globalCounts"]["count"].ToString().Replace(".0", ""));

            return count;
        }

        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }


        public static string FullPath(this UrlHelper url, string path)
        {
            var requestUrl = url.RequestContext.HttpContext.Request.Url;

            var fullUrl = string.Format("{0}{1}",
                                                  requestUrl.GetLeftPart(UriPartial.Authority),
                                                  url.Content(path));

            return fullUrl;
        }


        public static String ViewDateFull(this DateTime date)
        {

            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("vi-VN");
            string viewdate = date.ToString("dd MMMM yyyy", culture);
            return viewdate;
        }

        public static String ViewMonthName(this DateTime date)
        {
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("vi-VN");
            string viewdate = date.ToString("MMMM", culture);
            return viewdate;
        }

        public static String KhongDau(this String unicode)
        {
            unicode = Regex.Replace(unicode, "[áàảãạăắằẳẵặâấầẩẫậ]", "a");
            unicode = Regex.Replace(unicode, "[óòỏõọôồốổỗộơớờởỡợ]", "o");
            unicode = Regex.Replace(unicode, "[éèẻẽẹêếềểễệ]", "e");
            unicode = Regex.Replace(unicode, "[íìỉĩị]", "i");
            unicode = Regex.Replace(unicode, "[úùủũụưứừửữự]", "u");
            unicode = Regex.Replace(unicode, "[ýỳỷỹỵ]", "y");
            unicode = Regex.Replace(unicode, "[đ]", "d");
            //unicode = Regex.Replace(unicode, "[-\\s+/]+", "-");
            unicode = Regex.Replace(unicode, "\\W+", "-"); //Nếu bạn muốn thay dấu khoảng trắng thành dấu "_" hoặc dấu cách " " thì thay kí tự bạn muốn vào đấu "-"
            return unicode;
        }


        public static string ConvertToUnSign(string text)
        {
            for (int i = 33; i < 48; i++)
            {
                text = text.Replace(((char)i).ToString(), "");
            }
            for (int i = 58; i < 65; i++)
            {
                text = text.Replace(((char)i).ToString(), "");

            }
            for (int i = 91; i < 97; i++)
            {
                text = text.Replace(((char)i).ToString(), "");
            }
            for (int i = 123; i < 127; i++)
            {
                text = text.Replace(((char)i).ToString(), "");
            }

            //text = text.Replace(" ", "-"); //Comment lại để không đưa khoảng trắng thành ký tự -

            Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");

            string strFormD = text.Normalize(System.Text.NormalizationForm.FormD);

            return regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');

        }

        /// <summary>
        /// Wraps matched strings in HTML span elements styled with a background-color
        /// </summary>
        /// <param name="text"></param>
        /// <param name="keywords">Comma-separated list of strings to be highlighted</param>
        /// <param name="cssClass">The Css color to apply</param>
        /// <param name="fullMatch">false for returning all matches, true for whole word matches only</param>
        /// <returns>string</returns>
        public static string HighlightKeyWords(this string text, string keywords, string cssClass, bool fullMatch)
        {
            if (text == String.Empty || keywords == String.Empty || cssClass == String.Empty)
                return text;
            var words = keywords.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (!fullMatch)
                return words.Select(word => word.Trim()).Aggregate(text,
                             (current, pattern) =>
                             Regex.Replace(current,
                                             pattern,
                                               string.Format("<span style=\"background-color:{0}\">{1}</span>",
                                               cssClass,
                                               "$0"),
                                               RegexOptions.IgnoreCase));
            return words.Select(word => "\\b" + word.Trim() + "\\b")
                        .Aggregate(text, (current, pattern) =>
                                  Regex.Replace(current,
                                  pattern,
                                    string.Format("<span style=\"background-color:{0}\">{1}</span>",
                                    cssClass,
                                    "$0"),
                                    RegexOptions.IgnoreCase));

        }
    }
}