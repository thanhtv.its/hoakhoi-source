﻿using MvcWebsiteOcc.Areas.administrators.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace MvcWebsiteOcc.Models
{
    public class OccDBEntities : DbContext
    {
        public OccDBEntities()
            : base("name = OccDBEntities")
        {
        }
        //system
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<GroupRole> GroupRoles { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }

        public DbSet<Settings> Settings { get; set; }


        //book car
        public DbSet<StatusOrder> StatusOrders { get; set; }
        public DbSet<BookCar> BookCars { get; set; }
        public DbSet<BooksService> BooksServices { get; set; }

        //car
        public DbSet<CarService> CarServices { get; set; }
        public DbSet<CarServicesImage> CarServicesImages { get; set; }
        public DbSet<CarType> CarTypes { get; set; }
        public DbSet<CarModel> CarModels { get; set; }
        public DbSet<CarModelsImage> CarModelsImages { get; set; }
        public DbSet<CarViewType> CarViewTypes { get; set; }                

        //Department
        public DbSet<Department> Departments { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<StaffView> StaffViews { get; set; }


        //language
        public DbSet<Language> Languages { get; set; }

        //hotline
        public DbSet<HotLine> HotLines { get; set; }

        //ỏther
        public DbSet<Orientation> Orientations { get; set; }

        //addressbook
        public DbSet<AddressBook> AddressBooks { get; set; }

        //website
        public DbSet<WebsiteDetail> WebsiteDetails { get; set; }

        public DbSet<Slider> Sliders { get; set; }
        //public DbSet<Sliders_Description> Sliders_Descriptions { get; set; }
        public DbSet<AboutUs> AboutUs { get; set; }
        public DbSet<AboutUs_Description> AboutUs_Descriptions { get; set; }
        public DbSet<Contact> Contacts { get; set; }

        //thanh tich
        public DbSet<AboutUs_Achievements> AboutUs_Achievements { get; set; }
        public DbSet<AboutSlider> AboutSliders { get; set; }
        

        //news
        public DbSet<NewsCategory> NewsCategories { get; set; }
        public DbSet<TypeNew> TypeNews { get; set; }
        public DbSet<News> News { get; set; }
        //public DbSet<News_Description> News_Descriptions { get; set; }

        //event
        public DbSet<EventsCategory> EventsCategories { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventsDetail> EventsDetails { get; set; }
        public DbSet<EventsTimeCategory> EventsTimeCategories { get; set; }
        public DbSet<EventsTime> EventsTimes { get; set; }

        public DbSet<EventsGirl> EventsGirls { get; set; }
        public DbSet<EventsGirlImage> EventsGirlImages { get; set; }

        public DbSet<EventsGirlStatus> EventsGirlStatus { get; set; }

        //Album
        public DbSet<AlbumsCategory> AlbumsCategories { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<AlbumImage> AlbumImages { get; set; }

        //adv
        public DbSet<Advertisement> Advertisements { get; set; }

        //File download
        public DbSet<FileDownload> FileDownloads { get; set; }

    }

    public partial class User
    {
        [Key]
        public int UserID { get; set; }

        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [Display(Name = "Họ tên")]
        public string FullName { get; set; }

        [Display(Name = "Email")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}",
       ErrorMessage = "E-mail sai định dạng.")]
        public string Email { get; set; }

        [Display(Name = "Ngày sinh")]
        public string BirthDay { get; set; }

        [Display(Name = "Số điện thoại")]
        public string Phone { get; set; }

        [Display(Name = "Mobile")]
        public string Mobile { get; set; }

        [Display(Name = "Ngày tạo")]
        [Required(ErrorMessage = "Ngày tạo không được trống !")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Hoạt động")]
        public bool? IsActive { get; set; }


        public virtual ICollection<UserGroup> UserGroups { get; set; }

    }

    public class LocalPasswordModel
    {
        [Required(ErrorMessage = "Bạn chưa nhập mật khẩu hiện tại.")]
        [DataType(DataType.Password)]
        [Remote("_checkForUserPassword", "validateform", "administrators", AdditionalFields = "UserName", ErrorMessage = "Mật khẩu hiện tại không đúng.")]
        [Display(Name = "Mật khẩu hiện tại")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Mật khẩu mới không được trống.")]
        [StringLength(100, ErrorMessage = "Mật khẩu tối thiểu 4 ký tự.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu mới")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu mới")]
        [Compare("NewPassword", ErrorMessage = "Mật khẩu mới nhập lại chưa đúng.")]
        public string ConfirmPassword { get; set; }
    }

    public partial class UserView
    {
        public int UserID { get; set; }

        [Display(Name = "Tên đăng nhập")]
        [Required(ErrorMessage = "Tên đăng nhập không được trống !")]
        [StringLength(100, MinimumLength = 4, ErrorMessage = "Tên đăng nhập ít nhất có 4 ký tự.")]
        [Remote("_checkUser", "validateform", "administrators", AdditionalFields = "UserID", ErrorMessage = "Tên bạn chọn đã tồn tại !")]
        public string UserName { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        [Required(ErrorMessage = "Mật khẩu không được trống !")]
        [StringLength(100, ErrorMessage = "Mật khẩu tối thiểu 4 ký tự.", MinimumLength = 4)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu")]
        [System.Web.Mvc.CompareAttribute("Password", ErrorMessage = "Mật khẩu nhập lại chưa chính xác")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Họ tên")]
        [Required(ErrorMessage = "Họ tên không được trống !")]
        public string FullName { get; set; }

        [Display(Name = "Email")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}",
        ErrorMessage = "E-mail sai định dạng.")]
        public string Email { get; set; }

        [Display(Name = "Ngày sinh")]
        public string BirthDay { get; set; }

        [Display(Name = "Số điện thoại")]
        public string Phone { get; set; }

        [Display(Name = "Mobile")]
        public string Mobile { get; set; }

        [Display(Name = "Hoạt động")]
        public bool? IsActive { get; set; }
    }
    public partial class Role
    {
        [Key]
        public int RoleID { get; set; }

        [Display(Name = "User Name")]
        public string RoleName { get; set; }

        [Display(Name = "Role Name")]
        public string RoleNameDes { get; set; }


        public virtual ICollection<GroupRole> GroupRoles { get; set; }

    }
    public partial class UserGroup
    {
        [Key]
        public int UGroupId { get; set; }

        public int UserID { get; set; }
        public int GroupID { get; set; }

        public virtual User User { get; set; }
        public virtual Group Group { get; set; }
    }
    public partial class Group
    {
        [Key]
        public int GroupID { get; set; }

        [Display(Name = "Tên nhóm quyền")]
        [Required(ErrorMessage = "Tên nhóm quyền không được trống !")]
        [Remote("_checkGroup", "validateform", "administrators", AdditionalFields = "GroupID", ErrorMessage = "Tên bạn chọn đã tồn tại !")]
        public string GroupName { get; set; }

        [Display(Name = "Mô tả")]
        public string Description { get; set; }

        public virtual ICollection<UserGroup> UserGroups { get; set; }
        public virtual ICollection<GroupRole> GroupRoles { get; set; }

    }
    public partial class GroupRole
    {
        [Key]
        public int GroupRolesID { get; set; }

        public int RoleID { get; set; }
        public int GroupID { get; set; }

        public virtual Group Group { get; set; }
        public virtual Role Role { get; set; }

    }

    public partial class Language
    {
        [Key]
        public int Lang_ID { get; set; }

        [Display(Name = "Mã ngôn ngữ")]
        [Required(ErrorMessage = "Mã ngôn ngữ không được trống")]
        public string Lang_Code { get; set; }

        [Display(Name = "Tên ngôn ngữ")]
        [Required(ErrorMessage = "Tên ngôn ngữ không được trống")]
        public string Lang_Name { get; set; }

        [Display(Name = "Vị trí")]
        [Required(ErrorMessage = "Vị trí không được trống")]
        public int Lang_Sort { get; set; }

        [Display(Name = "Tên file ngôn ngữ")]
        [Required(ErrorMessage = "Đường dẫn không được trống")]
        public string Lang_File { get; set; }

        [Display(Name = "Quốc kỳ")]
        public string Lang_Flag { get; set; }

        [Display(Name = "File Xml")]
        public string Lang_Xml { get; set; }

        [Display(Name = "Mặc định")]
        public bool IsDefault { get; set; }


    }
}