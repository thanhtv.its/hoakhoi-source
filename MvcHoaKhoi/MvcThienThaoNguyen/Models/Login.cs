﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MvcWebsiteOcc.Models
{
    public class Login
    {
        [Required(ErrorMessage = "Tài khoản trống", AllowEmptyStrings = false)]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Mật khẩu trống", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}