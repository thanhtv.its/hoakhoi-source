﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace MvcWebsiteOcc.Models
{
    public class OccSendMail
    {
        /// <summary>
        /// Hàm thực thi gửi email. 
        /// </summary>
        /// <param name="smtpUserName">Tên đăng nhập email gửi thư: vd:neozeuz12</param>
        /// <param name="smtpPassword">Mật khẩu của email gửi thư</param>
        /// <param name="smtpHost">Host email. vd smtp.gmail.com</param>
        /// <param name="smtpPort">Port vd: 465</param>
        /// <param name="toEmail">Email nhận vd: neozeuz12@gmail.com</param>
        /// <param name="subject">Chủ đề</param>
        /// <param name="body">Nội dung thư gửi</param>
        /// <returns>True-Thành công/False-Thất bại</returns>
        /// 
        public bool SendTwo(string smtpUserName, string smtpPass, string smtpHost, int smtpPort, string toMail, string toMail2, string subject, string body)
        {
            try
            {
                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.EnableSsl = true;
                    smtpClient.Host = smtpHost;
                    smtpClient.Port = smtpPort;
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(smtpUserName, smtpPass);
                    var msg = new MailMessage
                    {
                        IsBodyHtml = true,
                        BodyEncoding = Encoding.UTF8,
                        From = new MailAddress(smtpUserName),
                        Subject = subject,
                        Body = body,
                        Priority = MailPriority.Normal,
                    };

                    msg.To.Add(toMail);
                    msg.To.Add(toMail2);
                    smtpClient.Send(msg);
                    return true;
                }
            }
            catch (Exception)
            {

                return false;
            }

        }

        public void SendToMe(string smtpUserName, string smtpPass, string smtpHost, int smtpPort, string toMail, string subject, string body)
        {
            try
            {
                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.EnableSsl = true;
                    smtpClient.Host = smtpHost;
                    smtpClient.Port = smtpPort;
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(smtpUserName, smtpPass);
                    var msg = new MailMessage
                    {
                        IsBodyHtml = true,
                        BodyEncoding = Encoding.UTF8,
                        From = new MailAddress(smtpUserName),
                        Subject = subject,
                        Body = body,
                        Priority = MailPriority.Normal,
                    };

                    msg.To.Add(toMail);
                    smtpClient.Send(msg);
                  
                }
            }
            catch (Exception)
            {
            }

        }

        public bool SendContact(string smtpUserName, string smtpPass, string smtpHost, int smtpPort, string toMail, string subject, string body)
        {
            try
            {
                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.EnableSsl = true;
                    smtpClient.Host = smtpHost;
                    smtpClient.Port = smtpPort;
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new NetworkCredential(smtpUserName, smtpPass);
                    var msg = new MailMessage
                    {
                        IsBodyHtml = true,
                        BodyEncoding = Encoding.UTF8,
                        From = new MailAddress(smtpUserName),
                        Subject = subject,
                        Body = body,
                        Priority = MailPriority.Normal,
                    };

                    msg.To.Add(toMail);
                    smtpClient.Send(msg);
                    return true;
                }
            }
            catch (Exception)
            {

                return false;
            }

        }


    }
}