﻿using Insya.Localization;
using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MvcThienThaoNguyen.Models
{
    public class EventRoute : RouteBase
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();

        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            var url = httpContext.Request.Headers["HOST"];

            //Uri myUri = new Uri(, UriKind.Absolute);
            var sub = GetSubDomain(httpContext.Request.Url);

            var index = url.IndexOf(".");

            if (index < 0)
                return null;

            var subDomain = url.Substring(0, index);


            if (subDomain == "sukien")
            {
                var routeData = new RouteData(this, new MvcRouteHandler());
                routeData.Values.Add("controller", "events"); //Goes to the User1Controller class
                routeData.Values.Add("action", "index"); //Goes to the Index action on the User1Controller

                return routeData;
            }

            if (sub == "ids")
            {
                var routeData = new RouteData(this, new MvcRouteHandler());
                routeData.Values.Add("controller", "logins"); //Goes to the User1Controller class
                routeData.Values.Add("action", "index"); //Goes to the Index action on the User1Controller

                return routeData;
            }

            if (sub != null)
            {
                var events = occSysDB.Events.ToList();

                if (events.Count > 0)
                {
                    foreach (var evt in events)
                    {
                        if (subDomain == evt.Subdomain)
                        {
                            var routeData = new RouteData(this, new MvcRouteHandler());
                            routeData.Values.Add("controller", "events"); //Goes to the User1Controller class
                            routeData.Values.Add("action", "view_detail"); //Goes to the Index action on the User1Controller
                            routeData.Values.Add("subdomain", evt.Subdomain);
                            return routeData;
                        }
                    }
                }
 
            }

            return null;
        }

        public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
        {
            //Implement your formating Url formating here
            return null;
        }

        private static string GetSubDomain(Uri url)
        {

            if (url.HostNameType == UriHostNameType.Dns)
            {

                string host = url.Host;

                var nodes = host.Split('.');
                int startNode = 0;
                if (nodes.Length > 2)
                {
                    if (nodes[0] == "www") startNode = 1;
                    return string.Format("{0}", nodes[startNode]);
                }
            }

            return null;
        }
    }
}