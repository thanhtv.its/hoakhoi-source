﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcWebsiteOcc.Areas.administrators.Models
{
    public class OccEventsModels : DbContext
    {
        public OccEventsModels()
            : base("name=OccDBEntities")
        {
        }
    }

    public partial class EventsCategory
    {
        [Key]
        public int EventCateID { get; set; }

        [Display(Name = "Tên thể loại")]
        [Required(ErrorMessage = "Dữ liệu không được trống !")]
        public string EventCateName { get; set; }

        [Display(Name = "Vị trí")]
        public int SortOrder { get; set; }

        public int Lang_ID { get; set; }
        public int? LinkID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        //Seo
        [Display(Name = "Meta Title")]
        [StringLength(100, ErrorMessage = "Tối đa 100 ký tự.")]
        public string MetaTitle { get; set; }

        [Display(Name = "Meta Keywords")]
        public string MetaKeywords { get; set; }

        [Display(Name = "Meta Description")]
        [StringLength(256, ErrorMessage = "Tối đa 256 ký tự.")]
        public string MetaDescription { get; set; }

        //public virtual ICollection<News> News { get; set; }
        public virtual Language Language { get; set; }
        //public virtual ICollection<NewsCategories_Description> NewsCategories_Descriptions { get; set; }
    }

    public partial class Event
    {
        [Key]
        public int EventID { get; set; }

        [Display(Name = "Tên sự kiện")]
        [Required(ErrorMessage = "Dữ liệu không được trống !")]
        public string EventName { get; set; }

        [Display(Name = "Giới thiệu ngắn")]
        public string ShortDescription { get; set; }

        [Display(Name = "Poster")]
        public string EventPoster { get; set; }

        [Display(Name = "File giới thiệu")]
        public string FileDownload { get; set; }

        [Display(Name = "Logo")]
        public string EventLogo { get; set; }
        public string Folder { get; set; }

        [Display(Name = "Địa điểm")]
        public string Address { get; set; }

        [Display(Name = "Vị trí")]
        public int SortOrder { get; set; }

        [Display(Name = "Bắt đầu")]
        public DateTime TimeStart { get; set; }

        [Display(Name = "Kết thúc")]
        public DateTime TimeEnd { get; set; }

        public int Lang_ID { get; set; }
        public int? LinkID { get; set; }
        public int? EventCateID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public string latitude { get; set; }
        public string longitude { get; set; }

        public string BackGround { get; set; }

        public string Subdomain { get; set; }

        //Seo
        [Display(Name = "Meta Title")]
        [StringLength(100, ErrorMessage = "Tối đa 100 ký tự.")]
        public string MetaTitle { get; set; }

        [Display(Name = "Meta Keywords")]
        public string MetaKeywords { get; set; }

        [Display(Name = "Meta Description")]
        [StringLength(256, ErrorMessage = "Tối đa 256 ký tự.")]
        public string MetaDescription { get; set; }

        //public virtual ICollection<News> News { get; set; }
        public virtual Language Language { get; set; }
        public virtual EventsCategory EventsCategory { get; set; }
        public virtual ICollection<EventsDetail> EventsDetails { get; set; }
        public virtual ICollection<EventsTimeCategory> EventsTimeCategories { get; set; }
        public virtual ICollection<EventsGirl> EventsGirls { get; set; }
        public virtual ICollection<News> News { get; set; }
        public virtual ICollection<Album> Albums { get; set; }
    }

    public partial class EventsDetail 
    { 
        [Key]
        public int DetailID { get; set; }

        [Display(Name = "Tên nội dung")]
        [Required(ErrorMessage = "Dữ liệu không được trống !")]
        public string DetailName { get; set; }

        [Display(Name = "Giới thiệu")]
        public string DetailDescription { get; set; }

         [Display(Name = "Vị trí")]
        public int SortOrder { get; set; }

        public int Lang_ID { get; set; }
        public int? LinkID { get; set; }
        public int? EventID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public virtual Event Event { get; set; }
        public virtual Language Language { get; set; }
    }

    public partial class EventsTimeCategory
    {
        [Key]
        public int ETimeCateID { get; set; }

        [Display(Name = "Tên mốc sự kiện")]
        [Required(ErrorMessage = "Dữ liệu không được trống !")]
        public string ETimeName { get; set; }

        [Display(Name = "Nội dung")]
        public string ETimeDescription { get; set; }

        [Display(Name = "Bắt đầu")]
        public DateTime ETimeStart { get; set; }

        [Display(Name = "Kết thúc")]
        public DateTime ETimeEnd { get; set; }


        [Display(Name = "Vị trí")]
        public int SortOrder { get; set; }

        public int Lang_ID { get; set; }
        public int? LinkID { get; set; }
        public int? EventID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public virtual Event Event { get; set; }
        public virtual Language Language { get; set; }
        public virtual ICollection<EventsTime> EventsTimes { get; set; }
    }

    public partial class EventsTime
    {
        [Key]
        public int ETimeID { get; set; }

        [Display(Name = "Tên chi tiết lịch trình")]
        [Required(ErrorMessage = "Dữ liệu không được trống !")]
        public string ETimeName { get; set; }

        [Display(Name = "Nội dung")]
        public string ETimeContent { get; set; }

        [Display(Name = "Giới thiệu")]
        public string ETimeDescription { get; set; }

        [Display(Name = "Thời gian")]
        public string ETime { get; set; }

        [Display(Name = "Vị trí")]
        public int SortOrder { get; set; }

        public int Lang_ID { get; set; }
        public int? LinkID { get; set; }
        public int? ETimeCateID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public virtual EventsTimeCategory EventsTimeCategory { get; set; }
        public virtual Language Language { get; set; }
    }

    public partial class EventsGirl
    {
        [Key]
        public int EGirlID { get; set; }

        [Display(Name = "Họ tên")]
        [Required(ErrorMessage = "Dữ liệu không được trống !")]
        public string Name { get; set; }

        [Display(Name = "Nơi sinh")]
        public string AddressBorn { get; set; }

        [Display(Name = "Chỗ ở hiện tại")]
        public string AddressNow { get; set; }

        [Display(Name = "Ngày sinh")]
        public DateTime Birtday { get; set; }

        [Display(Name = "Lớp")]
        public string EClass { get; set; }

        [Display(Name = "Trường")]
        public string School { get; set; }

        [Display(Name = "Điểm tổng kết")]
        public string Grade { get; set; }

        [Display(Name = "Số di động")]
        public string Mobile { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Facebook")]
        public string Facebook { get; set; }

        [Display(Name = "Chiều cao")]
        public string Height { get; set; }

        [Display(Name = "Cân nặng")]
        public string Weight { get; set; }

        [Display(Name = "Số đo 3 vòng")]
        public string ThreeRing { get; set; }

        [Display(Name = "Sở thích")]
        public string Love { get; set; }

        [Display(Name = "Ảnh")]
        public string Image { get; set; }

        [Display(Name = "Sự kiện")]
        public int? EventID { get; set; }

        [Display(Name = "Trạng thái")]
        public int? EStatusID { get; set; }

        [Display(Name = "Số báo danh")]
        public string Code { get; set; }
        [Display(Name = "Tổng lượt xem")]
        public int? ViewCount { get; set; }
        [Display(Name = "Số SMS")]
        public int? SMS { get; set; }
        [Display(Name = "Bình chọn trên website")]
        public int? Web { get; set; }
        [Display(Name = "Xếp hạng")]
        public int? Rank { get; set; }

        public string Link { get; set; }

        public virtual Event Event { get; set; }
        public virtual EventsGirlStatus EventsGirlStatus { get; set; }
        public virtual ICollection<EventsGirlImage> EventsGirlImages { get; set; }
    }

    public partial class EventsGirlImage 
    {
        [Key]
        public int EImgID { get; set; }

        public string EImgName { get; set; }

        public string EImgFolder { get; set; }

        public int EGirlID { get; set; }

        public virtual EventsGirl EventsGirl { get; set; }
    }

    public partial class EventsGirlStatus
    {
        [Key]
        public int EStatusID { get; set; }

        [Display(Name = "Tên dịch vụ")]
        [Required(ErrorMessage = "Dữ liệu không được trống")]
        public string EStatusName { get; set; }

        [Display(Name = "Vị trí")]
        public int? SortOrder { get; set; }

        public bool? IsActive { get; set; }

        public string Color { get; set; }

        public virtual ICollection<EventsGirl> EventsGirls { get; set; }
    }
}