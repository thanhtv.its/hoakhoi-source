﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcWebsiteOcc.Areas.administrators.Models
{
    public class OccCarsModels : DbContext
    {
        public OccCarsModels()
            : base("name=OccDBEntities")
        {
        }
    }

    public partial class CarService
    {
        [Key]
        public int ServID { get; set; }

        [Display(Name = "Tên dịch vụ")]
        [Required(ErrorMessage = "Dữ liệu không được trống")]
        public string ServName { get; set; }

        [Display(Name = "Giới thiệu")]
        public string ServDescription { get; set; }

        public string ServIcon { get; set; }

        [Display(Name = "Vị trí")]
        public int? SortOrder { get; set; }

        public int? LinkID { get; set; }

        public int Lang_ID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public virtual Language Language { get; set; }

        public virtual ICollection<CarServicesImage> CarServicesImages { get; set; }
    }

    public partial class CarServicesImage
    {
        [Key]
        public int CImgID { get; set; }

        [Display(Name = "Tiêu đề")]
        public string ImageName { get; set; }

        [Display(Name = "Tiêu đề")]
        public string Folder { get; set; }

        public int ServID { get; set; }

        public virtual CarService CarService { get; set; }
    }

    public partial class CarType
    {
        [Key]
        public int TypeID { get; set; }

        [Display(Name = "Tên loại")]
        [Required(ErrorMessage = "Dữ liệu không được trống")]
        public string TypeName { get; set; }

        [Display(Name = "Vị trí")]
        public int? SortOrder { get; set; }

        public int? LinkID { get; set; }

        public int Lang_ID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public virtual Language Language { get; set; }

        public virtual ICollection<CarModel> CarModels { get; set; }
    }

    public partial class CarModel
    {
        [Key]
        public int ModID { get; set; }

        [Display(Name = "Tên dòng xe")]
        [Required(ErrorMessage = "Dữ liệu không được trống")]
        public string ModName { get; set; }

        [Display(Name = "Giới thiệu")]
        public string ModDescription { get; set; }

        public string ModImage { get; set; }

        [Display(Name = "Hãng xe")]
        public string ModBrand { get; set; }

        [Display(Name = "Màu sắc")]
        public string ModColor { get; set; }

        [Display(Name = "Đời xe")]
        public string ModYear { get; set; }

        public string Folder { get; set; }

        [Display(Name = "Giá")]
        public decimal ModPrice { get; set; }

        [Display(Name = "Đánh giá")]
        public decimal ModRate { get; set; }

        public int? TypeID { get; set; }

        public int? LinkID { get; set; }

        public int Lang_ID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public virtual Language Language { get; set; }
        public virtual CarType CarType { get; set; }

        public virtual ICollection<CarModelsImage> CarModelsImages { get; set; }
        public virtual ICollection<Staff> Staffs { get; set; }
        public virtual ICollection<CarViewType> CarViewTypes { get; set; }
    }

    public partial class CarModelsImage
    {
        [Key]
        public int ModImgID { get; set; }

        [Display(Name = "Tiêu đề")]
        public string ImageName { get; set; }

        [Display(Name = "Tiêu đề")]
        public string Folder { get; set; }

        public int ModID { get; set; }

        public virtual CarModel CarModel { get; set; }
    }

    public partial class CarViewType
    {
        [Key]
        public int vID { get; set; }

        public int ModID { get; set; }

        public int? ViewType { get; set; }

        public virtual CarModel CarModel { get; set; }
    }

}