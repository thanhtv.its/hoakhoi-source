﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcWebsiteOcc.Areas.administrators.Models
{
    public class OccWebsiteModels : DbContext
    {
        public OccWebsiteModels()
            : base("name=OccDBEntities")
        {
        }
    }
    public class AnalyticsSummary
    {
        public TimeSpan AveragePageLoadTime { get; set; }
        public IDictionary<string, int> TopSearches { get; set; }
        public IDictionary<string, int> TopReferrers { get; set; }
        public IDictionary<string, int> PageViews { get; set; }
        public IDictionary<string, string> PageTitles { get; set; }
        public double PercentExitRate { get; set; }
        public double PageviewsPerVisit { get; set; }
        public double EntranceBounceRate { get; set; }
        public TimeSpan AverageTimeOnSite { get; set; }
        public double PercentNewVisits { get; set; }
        public int TotalPageViews { get; set; }
        public int TotalVisits { get; set; }
        public int TotalMonth { get; set; }
        public int TotalWeek { get; set; }
        public int TotalToDay { get; set; }
        public IList<int> Visits { get; set; }
    }

    public class Settings
    {
        public static string DefaultId
        {
            get
            {
                return "Site/Settings";
            }
        }
        public string Id { get; set; }
        public string SiteId { get; set; }
        public string SessionToken { get; set; }
    }

    public class Config
    {
        [Display(Name = "Site")]
        [Required(ErrorMessage = "Select a site to work with")]
        public string SiteId { get; set; }

        public IDictionary<string, IDictionary<string, string>> Sites { get; set; }

        public Config()
        {
            Sites = new Dictionary<string, IDictionary<string, string>>();
        }
    }

    public class Contact
    {
        [Key]
        public int conID { get; set; }

        [Display(Name = "Họ Tên")]
        [Required(ErrorMessage = "Tên không được trống.")]
        //[Remote("CheckForContact", "ValidationView", "", ErrorMessage = "Tên không được trống.")]
        [StringLength(100)]
        public string CusName { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email không được trống.")]
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Địa chỉ E-mail không đúng")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}",
     ErrorMessage = "E-mail sai định dạng.")]
        public string CusEmail { get; set; }

        [Display(Name = "Số điện thoại")]
        [Required(ErrorMessage = "Số điện thoại không được trống.")]
        [StringLength(12, ErrorMessage = "Số điện thoại tối đa 12 số.")]
        [RegularExpression(@"^[0-9]{0,15}$", ErrorMessage = "Số điện thoại không chính xác")]
        public string CusPhone { get; set; }

        [Display(Name = "Nội dung")]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Nội dung không được trống")]
        public string CusMessage { get; set; }

        [Display(Name = "Ngày gửi")]
        public System.DateTime CreateDate { get; set; }
    }

    public partial class WebsiteDetail
    {
        [Key]
        public int Web_ID { get; set; }

        public string Logo { get; set; }

        [Display(Name = "Tên công ty")]
        public string NameCompany { get; set; }

        [Display(Name = "Tên rút gọn")]
        public string NameShort { get; set; }

        [Display(Name = "Slogan")]
        public string Slogan { get; set; }

        [Display(Name = "Giới thiệu")]
        public string Description { get; set; }

        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }

        [Display(Name = "Số điện thoại")]
        public string Phone { get; set; }

        [Display(Name = "Fax")]
        public string Fax { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Website")]
        public string Website { get; set; }

        [Display(Name = "Hotline")]
        public string Hotline { get; set; }

        [Display(Name = "Facebook")]
        public string Facebook { get; set; }

        [Display(Name = "Twitter")]
        public string Twitter { get; set; }

        [Display(Name = "Google")]
        public string Google { get; set; }

        public string latitude { get; set; }
        public string longitude { get; set; }

        public bool? IsActive { get; set; }
        public int? SortOrder { get; set; }
        public int? LinkID { get; set; }

        public int? Lang_ID { get; set; }
        public virtual Language Language { get; set; }
    }



    public partial class HotLine
    {
        [Key]
        public int HotID { get; set; }

        [Display(Name = "Tiêu đề")]
        public string HotTitle { get; set; }

        [Display(Name = "Vị trí")]
        public int? SortOrder { get; set; }

        [Display(Name = "Hotline")]
        public string HotNumber { get; set; }

        public int Lang_ID { get; set; }

        public int? LinkID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public virtual Language Language { get; set; }

        //public virtual ICollection<Sliders_Description> Sliders_Descriptions { get; set; }
    }

    public partial class Orientation
    {
        [Key]
        public int OriID { get; set; }

        [Display(Name = "Tiêu đề")]
        public string OriTitle { get; set; }

        [Display(Name = "Vị trí")]
        public int? SortOrder { get; set; }

        [Display(Name = "Chi tiết")]
        public string OriDescription { get; set; }

        public int Lang_ID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public int? LinkID { get; set; }

        public virtual Language Language { get; set; }

        //public virtual ICollection<Sliders_Description> Sliders_Descriptions { get; set; }
    }

    public partial class AddressBook
    {
        [Key]
        public int AddID { get; set; }

        [Display(Name = "Tiêu đề")]
        public string AddTitle { get; set; }

        [Display(Name = "Vị trí")]
        public int? SortOrder { get; set; }

        [Display(Name = "Chi tiết")]
        public string AddDescription { get; set; }

        public int Lang_ID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public int? LinkID { get; set; }

        public virtual Language Language { get; set; }

        //public virtual ICollection<Sliders_Description> Sliders_Descriptions { get; set; }
    }

    public partial class AboutUs_Achievements
    {
        [Key]
        public int AchiID { get; set; }

        public string AchiImage { get; set; }
    }

    public partial class Slider
    {
        [Key]
        public int SlideID { get; set; }

        [Display(Name = "Ảnh")]
        public string SlideImage { get; set; }

        [Display(Name = "Vị trí")]

        public int? SortOrder { get; set; }

        [Display(Name = "Kiểu hiển thị")]
        public int Style { get; set; }

        [Display(Name = "Link")]
        public string Link { get; set; }

        public int Lang_ID { get; set; }

        public int? LinkID { get; set; }

        [Display(Name = "Tiêu đề")]
        public string SlideTitle { get; set; }

        [Display(Name = "Slogan")]
        public string SlideSlogan { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public bool? IsBlank { get; set; }

        public virtual Language Language { get; set; }

        //public virtual ICollection<Sliders_Description> Sliders_Descriptions { get; set; }
    }

    public partial class AboutSlider
    {
        [Key]
        [Column(Order = 1)]
        public int ASID { get; set; }

        [Display(Name = "Tiêu đề")]
        public string ASTitle { get; set; }

        [Display(Name = "Giới thiệu")]
        public string ASDescription { get; set; }

        public string ASImage { get; set; }

        public string ASVideo { get; set; }

        public int? SortOrder { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }
    }

    public partial class AboutUs
    {
        [Key]
        public string AboutID { get; set; }

        [Display(Name = "Ảnh")]
        public string Image { get; set; }

        public virtual ICollection<AboutUs_Description> AboutUs_Descriptions { get; set; }

    }

    public partial class AboutUs_Description
    {
        [Key]
        [Column(Order = 2)]
        public string AboutID { get; set; }

        [Key]
        [Column(Order = 1)]
        public int Lang_ID { get; set; }

        [Display(Name = "Tên công ty")]
        [Required(ErrorMessage = "Tên công ty không được trống")]
        public string NameCompany { get; set; }

        [Display(Name = "Giới thiệu")]
        public string Description { get; set; }

        public virtual AboutUs AboutUs { get; set; }
        public virtual Language Language { get; set; }

    }
}