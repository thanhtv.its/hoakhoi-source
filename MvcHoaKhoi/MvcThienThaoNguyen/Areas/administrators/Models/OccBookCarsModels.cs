﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcWebsiteOcc.Areas.administrators.Models
{
    public class OccBookCarsModels : DbContext
    {
        public OccBookCarsModels()
            : base("name=OccDBEntities")
        {
        }
    }

    public partial class BooksService
    {
        [Key]
        public int CServID { get; set; }

        [Display(Name = "Tên dịch vụ")]
        [Required(ErrorMessage = "Dữ liệu không được trống")]
        public string CServName { get; set; }

        [Display(Name = "Đơn giá")]
        public decimal CServPrice { get; set; }

        [Display(Name = "Đơn vị tính")]
        [Required(ErrorMessage = "Dữ liệu không được trống")]
        public string UnitPrice { get; set; }

        [Display(Name = "Vị trí")]
        public int? SortOrder { get; set; }

        public int? LinkID { get; set; }

        public int? Lang_ID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public virtual Language Language { get; set; }

        public virtual ICollection<BookCar> BookCars { get; set; }
    }

    public partial class BookCar
    {
        [Key]
        public int BookID { get; set; }

        [Display(Name = "Mã lệnh")]
        public string OrderID { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Tên Khách hàng")]
        [Required(ErrorMessage = "Bạn chưa nhập tên")]
        public string CusName { get; set; }

        [Display(Name = "Địa chỉ")]

        public string CusAddress { get; set; }

        [Display(Name = "Giới tính")]
        [Required(ErrorMessage = "Dữ liệu không được trống")]
        public int CusSex { get; set; }

        [Display(Name = "E-mail")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}",
ErrorMessage = "E-mail sai định dạng.")]
        public string CusEmail { get; set; }

        [Display(Name = "Di động")]
        [Required(ErrorMessage = "Bạn chưa nhập số di động")]
        public string CusMobile { get; set; }

        [Display(Name = "Yêu cầu khác")]
        public string CusMessage { get; set; }

        [Display(Name = "Phương thức thanh toán")]
        public string CheckOutType { get; set; }

        [Required(ErrorMessage = "Bạn chưa chọn xe")]
        [Remote("_checkCarInBook", "validateform", "administrators", ErrorMessage = "Bạn chưa chọn xe!")]
        public int ModID { get; set; }
        public int? StaffID { get; set; }

        public int? Man { get; set; }
        public int? Child { get; set; }
        public int? Senior { get; set; }
        public int? Wheelchair { get; set; }

        [Display(Name = "Ngày xuất phát")]
        public DateTime DateStart { get; set; }
        [Display(Name = "Ngày kết thúc")]
        public DateTime DateEnd { get; set; }

        [Display(Name = "Giờ xuất phát")]
        public string TimeStart { get; set; }
        [Display(Name = "Giờ về")]
        public string TimeEnd { get; set; }

        public string StartLat { get; set; }
        public string StartLong { get; set; }


        [Required(ErrorMessage = "Hãy nhập điểm xuất phát")]
        public string StartAddress { get; set; }
        [Required(ErrorMessage = "Hãy nhập đến đến của hành trình")]
        public string EndAddress { get; set; }

        public string EndLat { get; set; }
        public string EndLong { get; set; }

        public string Total { get; set; }

        public int? StaID { get; set; }

        public bool? hasVAT { get; set; }

        public virtual CarModel CarModel { get; set; }
        public virtual Staff Staff { get; set; }
        public virtual ICollection<BooksService> BooksServices { get; set; }
        public virtual StatusOrder StatusOrders { get; set; }
    }

    public partial class StatusOrder
    {
        [Key]
        public int StaID { get; set; }

        [Display(Name = "Tên dịch vụ")]
        [Required(ErrorMessage = "Dữ liệu không được trống")]
        public string StaName { get; set; }

        [Display(Name = "Vị trí")]
        public int? SortOrder { get; set; }

        public bool? IsActive { get; set; }

        public string Color { get; set; }

        public virtual ICollection<BookCar> BookCars { get; set; }
    }

}