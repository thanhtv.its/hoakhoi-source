﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcWebsiteOcc.Areas.administrators.Models
{
    public class OccNewsModels : DbContext
    {
        public OccNewsModels()
            : base("name=OccDBEntities")
        {
        }
    }

    #region News
    public partial class NewsCategory
    {
        [Key]
        public int NewCatID { get; set; }

        [Display(Name = "Chuyên mục cha")]
        public int ParentID { get; set; }

        //Seo
        [Display(Name = "Meta Title")]
        [StringLength(100, ErrorMessage = "Tối đa 100 ký tự.")]
        public string MetaTitle { get; set; }

         [Display(Name = "Mô tả")]
        public string NewsCateDescription { get; set; }
        

        [Display(Name = "Meta Keywords")]
        public string MetaKeywords { get; set; }

        [Display(Name = "Meta Description")]
        [StringLength(256, ErrorMessage = "Tối đa 256 ký tự.")]
        public string MetaDescription { get; set; }

        [Display(Name = "Vị trí")]
        public int SortOrder { get; set; }

        public int Lang_ID { get; set; }
        public int? LinkID { get; set; }
        public int? TID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        [Display(Name = "Tên chuyên mục")]
        [Required(ErrorMessage = "Tên chuyên mục không được trống !")]
        public string NewsCatName { get; set; }

        [Display(Name = "Chuyên mục cha")]
        public string ParentName { get; set; }

        public virtual ICollection<News> News { get; set; }
        public virtual Language Language { get; set; }
        public virtual TypeNew TypeNews { get; set; }
        //public virtual ICollection<NewsCategories_Description> NewsCategories_Descriptions { get; set; }
    }

    public partial class TypeNew
    {
        [Key]
        public int TID { get; set; }

        [Display(Name = "Tên chuyên mục")]
        [Required(ErrorMessage = "Tên chuyên mục không được trống !")]
        public string TName { get; set; }

        public virtual ICollection<NewsCategory> NewsCategories { get; set; }
    }

    public partial class News
    {
        [Key]
        public int NewsID { get; set; }

        [Display(Name = "Ảnh")]
        public string NewsImage { get; set; }

        [Display(Name = "Video")]
        public string NewsVideo { get; set; }

        [Display(Name = "Ngày tạo")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        [Display(Name = "Nổi bật")]
        public bool? IsHome { get; set; }
        public bool? IsHot { get; set; }

        [Display(Name = "Chuyên mục")]
        [Required(ErrorMessage = "Bạn chưa chọn chuyên mục")]
        public int? NewCatID { get; set; }

        [Display(Name = "File Download")]
        public string FileDownload { get; set; }

        [Display(Name = "Lượt xem")]
        public int ViewCount { get; set; }

        public string Folder { get; set; }

        public int Lang_ID { get; set; }

        [Display(Name = "Tiêu đề")]
        [Required(ErrorMessage = "Tiêu đề không được trống")]
        public string NewsTitle { get; set; }

        [Display(Name = "Tác giả")]
        public string NewsAuthor { get; set; }

        [Display(Name = "Mô tả ngắn")]
        public string ShortDescription { get; set; }

        public string Link { get; set; }
        public string LinkCate { get; set; }
        public string NewsCateName { get; set; }
        public string CreateDateString { get; set; }
        
        [Display(Name = "Nội dung tin tức")]
        public string FullDescription { get; set; }
        public int? LinkID { get; set; }
        public int? EventID { get; set; }

        public virtual Language Language { get; set; }
        public virtual NewsCategory NewsCategories { get; set; }
        public virtual Event Event { get; set; }
        //public virtual ICollection<News_Description> News_Descriptions { get; set; }

    }

    //public partial class News_Description
    //{
    //    [Key]
    //    [Column(Order = 1)]
    //    public int NewsID { get; set; }

    //    [Key]
    //    [Column(Order = 2)]
    //    public int Lang_ID { get; set; }

    //    [Display(Name = "Tiêu đề")]
    //    [Required(ErrorMessage = "Tiêu đề không được trống")]
    //    public string NewsTitle { get; set; }

    //    [Display(Name = "Tác giả")]
    //    public string NewsAuthor { get; set; }

    //    [Display(Name = "Mô tả ngắn")]
    //    public string ShortDescription { get; set; }

    //    [Display(Name = "Nội dung tin tức")]
    //    public string FullDescription { get; set; }


    //    public virtual News News { get; set; }
    //    public virtual Language Language { get; set; }

    //}

    #endregion

    #region Album
    public partial class AlbumsCategory
    {
        [Key]
        public int AlCateID { get; set; }

        [Display(Name = "Tên nhóm album")]
        [Required(ErrorMessage = "Tên không được trống !")]
        public string AlCateName { get; set; }

        [Display(Name = "Vị trí")]
        public int SortOrder { get; set; }

        public int Lang_ID { get; set; }
        public int? LinkID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public virtual ICollection<Album> Albums { get; set; }
        public virtual Language Language { get; set; }
        //public virtual ICollection<NewsCategories_Description> NewsCategories_Descriptions { get; set; }
    }
    public partial class Album
    {
        [Key]
        public int AlbumID { get; set; }

        [Display(Name = "Tên Album")]
        [Required(ErrorMessage = "Tên album không được trống")]
        public string AlbumName { get; set; }

        [Display(Name = "Giới thiệu")]
        public string Description { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Ảnh đại diện")]
        public string Image { get; set; }

        public string Folder { get; set; }

        public int? Lang_ID { get; set; }
        public int? LinkID { get; set; }
        public int? EventID { get; set; }
        public int? AlCateID { get; set; }
        public int? SortOrder { get; set; }

        public string Link { get; set; }
        public string LinkCate { get; set; }
        public string CateName { get; set; }
        public string CreateDateString { get; set; }

        public int? Love { get; set; }

        public virtual Language Language { get; set; }
        public virtual Event Event { get; set; }
        public virtual AlbumsCategory AlbumsCategories { get; set; }
        public virtual ICollection<AlbumImage> AlbumImages { get; set; }
    }
    public partial class AlbumImage
    {
        [Key]
        public int ImgID { get; set; }

        public string ImageName { get; set; }

        [Display(Name = "Thư mục")]
        public string Folder { get; set; }

        public int AlbumID { get; set; }

        public virtual Album Album { get; set; }
    }


    #endregion

    #region Advs
    public partial class Advertisement
    {
        [Key]
        public int AdvID { get; set; }

        [Display(Name = "Tiêu đề")]
        public string AdvTitle { get; set; }

        [Display(Name = "Ảnh")]
        public string AdvImage { get; set; }

        [Display(Name = "Slogan")]
        public string AdvSlogan { get; set; }

        [Display(Name = "Giới thiệu")]
        public string AdvShortDescription { get; set; }

        [Display(Name = "Link")]
        public string AdvLink { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public bool? IsHome { get; set; }

        public int? Lang_ID { get; set; }
        public int? LinkID { get; set; }

        [Display(Name = "Ngày bắt đầu")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Ngày kết thúc")]
        public DateTime EndDate { get; set; }

        public int AdvStyle { get; set; }

        public virtual Language Language { get; set; }
    }

    #endregion

    #region File Download
    public partial class FileDownload
    {
        [Key]
        public int FileID { get; set; }

        public int Lang_ID { get; set; }

        [Display(Name = "Tiêu đề")]
        public string FileTitle { get; set; }

        [Display(Name = "Tên File")]
        public string FileName { get; set; }

        [Display(Name = "Giới thiệu")]
        public string FileDescription { get; set; }

        public int SortOrder { get; set; }
        [Display(Name = "Trạng thái")]
        public bool IsActive { get; set; }

        public DateTime CreateDate { get; set; }

        public string FileImage { get; set; }

        public virtual Language Language { get; set; }
    }
    #endregion
}