﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcWebsiteOcc.Areas.administrators.Models
{
    public class OccDepartmentModels : DbContext
    {
        public OccDepartmentModels()
            : base("name=OccDBEntities")
        {
        }
    }

    public partial class Department
    {
        [Key]
        public int DepID { get; set; }

        [Display(Name = "Tiêu đề")]
        [Required(ErrorMessage = "Dữ liệu không được trống")]
        public string DepName { get; set; }

        [Display(Name = "Vị trí")]
        public int? SortOrder { get; set; }

        public int? LinkID { get; set; }

        public int Lang_ID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public virtual Language Language { get; set; }

        public virtual ICollection<Position> Positions { get; set; }
    }

    public partial class Position
    {
        [Key]
        public int PosID { get; set; }

        public int? DepID { get; set; }

        [Display(Name = "Tiêu đề")]
        [Required(ErrorMessage = "Dữ liệu không được trống")]
        public string PosName { get; set; }

        [Display(Name = "Vị trí")]
        public int? SortOrder { get; set; }

        public int? LinkID { get; set; }

        public int Lang_ID { get; set; }

        [Display(Name = "Trạng thái")]
        public bool? IsActive { get; set; }

        public virtual Language Language { get; set; }
        public virtual Department Department { get; set; }

        public virtual ICollection<Staff> Staffs { get; set; }
    }

    public partial class Staff
    {
        [Key]
        public int StaffID { get; set; }

        [Display(Name = "Tên nhân viên")]
        [Required(ErrorMessage = "Dữ liệu không được trống")]
        public string StaffName { get; set; }

        [Display(Name = "Ảnh")]
        public string StaffImage { get; set; }

        [Display(Name = "Quê quán")]
        public string StaffAddress { get; set; }

        [Display(Name = "Ngày sinh")]
        public DateTime? StaffBirthDay { get; set; }

        [Display(Name = "Số điện thoại")]
        public string StaffPhone { get; set; }

        [Display(Name = "Di động")]
        public string StaffMobile { get; set; }

        [Display(Name = "Email")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}",
     ErrorMessage = "E-mail sai định dạng.")]
        public string StaffEmail { get; set; }

        [Display(Name = "Giới thiệu")]
        public string StaffDescription { get; set; }

        [Display(Name = "Facebook")]
        public string StaffFace { get; set; }

        [Display(Name = "Google")]
        public string StaffGoogle { get; set; }

        [Display(Name = "Twitter")]
        public string StaffTwitter { get; set; }

        [Display(Name = "Skype")]
        public string StaffSkype { get; set; }

        [Display(Name = "Yahoo")]
        public string StaffYahoo { get; set; }

        [Display(Name = "Xe vận hành")]
        public int? ModID { get; set; }

        [Display(Name = "Bằng lái")]
        public string DriverLicense { get; set; }

        [Display(Name = "Biển số xe")]
        public string CarNumber { get; set; }

        public int? LinkID { get; set; }
        public int Lang_ID { get; set; }

        public virtual Language Language { get; set; }
        public virtual CarModel CarModel { get; set; }

        public virtual ICollection<Position> Positions { get; set; }
        public virtual ICollection<StaffView> StaffViews { get; set; }
    }

    public partial class StaffView
    {
        [Key]
        public int ViewType { get; set; }

        public string ViewName { get; set; }

        public virtual ICollection<Staff> Staffs { get; set; }
    }

}
