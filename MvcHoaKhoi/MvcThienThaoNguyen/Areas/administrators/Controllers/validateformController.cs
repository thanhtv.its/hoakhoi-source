﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcWebsiteOcc.Areas.administrators.Controllers
{
    public class validateformController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();
        /// <summary>
        /// Check hroup Role
        /// </summary>
        /// <param name="GroupID"></param>
        /// <param name="GroupName"></param>
        /// <returns></returns>
        public JsonResult _checkGroup(int GroupID, string GroupName)
        {
            //add method 
            if (GroupID == 0)
            {
                //get name to check
                var nameCheck = occSysDB.Groups.ToList();
                //if have something to check
                if (nameCheck.Count > 0)
                {
                    foreach (var grp in nameCheck)
                    {
                        //if it exits
                        if (grp.GroupName.ToLower().Trim() == GroupName.ToLower().Trim())
                        {
                            return Json(false, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                //if have anything t0 check
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);

                }

            }
            //edit method
            else
            {
                var nameNow = occSysDB.Groups.FirstOrDefault(p => p.GroupID.Equals(GroupID));
                //name not change
                if (nameNow.GroupName.ToLower().Trim() == GroupName.ToLower().Trim())
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                //change it so we have to check it with orther
                else
                {
                    var nameList = occSysDB.Groups.Where(c => c.GroupID != GroupID).ToList();

                    foreach (var check in nameList)
                    {
                        if (check.GroupName.ToLower().Trim() == GroupName.ToLower().Trim())
                        {
                            return Json(false, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }


            //anything else
            return Json(true, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// check uesr name
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public JsonResult _checkUser(int UserID, string UserName)
        {
            //add method 
            if (UserID == 0)
            {
                //get name to check
                var nameCheck = occSysDB.Users.ToList();
                //if have something to check
                if (nameCheck.Count > 0)
                {
                    foreach (var grp in nameCheck)
                    {
                        //if it exits
                        if (grp.UserName.ToLower().Trim() == UserName.ToLower().Trim())
                        {
                            return Json(false, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                //if have anything t0 check
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);

                }

            }
            //edit method
            else
            {
                var nameNow = occSysDB.Users.FirstOrDefault(p => p.UserID.Equals(UserID));
                //name not change
                if (nameNow.UserName.ToLower().Trim() == UserName.ToLower().Trim())
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                //change it so we have to check it with orther
                else
                {
                    var nameList = occSysDB.Users.Where(c => c.UserID != UserID).ToList();

                    foreach (var check in nameList)
                    {
                        if (check.UserName.ToLower().Trim() == UserName.ToLower().Trim())
                        {
                            return Json(false, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }


            //anything else
            return Json(true, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// check car in book
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public JsonResult _checkCarInBook(int? ModID)
        {
            //add method 
            if (ModID == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);

            }
            //edit method
            else
            {
                //anything else
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult _checkForUserPassword(string OldPassword, string UserName)
        {
            var nowUser = occSysDB.Users.FirstOrDefault(p => p.UserName == UserName);
            string checkPass =  OccConvert.MD5Hash(OldPassword.Trim()).ToUpper();
            if (nowUser != null)
            {
                if (nowUser.Password.ToUpper() == checkPass)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }


    }
}
