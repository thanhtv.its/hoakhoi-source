﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.IO;
using MvcWebsiteOcc.Areas.administrators.Models;

namespace MvcWebsiteOcc.Areas.administrators.Controllers
{
    public class department_managerController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();

        /// <summary>
        ///   Department
        /// </summary>
        /// <returns></returns>
        #region Department
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult departs_manager()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.Departments.OrderBy(p => p.SortOrder).ToList();
            return View(list);
        }
        [HttpPost]
        public ActionResult departs_manager(IEnumerable<int> itemID, IEnumerable<int> updateID, List<int> SortOrder, string deleteOK, string update)
        {      //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.Departments.OrderBy(p => p.SortOrder).ToList();
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var del = occSysDB.Departments.FirstOrDefault(p => p.DepID.Equals(id));
                    //already
                    if (del != null)
                    {
                        var poses = del.Positions.ToList();
                        if (poses.Count == 0)
                        {
                            //delete slide
                            occSysDB.Departments.Remove(del);
                            //save action
                            occSysDB.SaveChanges();
                        }
                        else
                        {
                            ViewBag.Error = "Mục này đang được sử dụng !";
                            return View(list);
                        }

                    }
                }
            }

            //update sortorder
            int sortorder = 0;
            if (SortOrder != null && update == "OK")
            {
                foreach (var slideid in updateID)
                {
                    //get slider
                    var slideUpdate = occSysDB.Departments.FirstOrDefault(p => p.DepID.Equals(slideid));
                    if (slideUpdate != null)
                    {
                        //update sort order
                        slideUpdate.SortOrder = SortOrder[sortorder];
                    }
                    //up index
                    sortorder++;
                }
                //save
                occSysDB.SaveChanges();
            }

            return RedirectToAction("departs_manager");
        }
        #endregion
        #region add
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult departs_add(int? linkID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliders = occSysDB.Departments.ToList();
            if (sliders.Count > 0)
            {
                ViewBag.SortOrder = sliders.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            if (linkID != null)
            {
                var linkObj = occSysDB.Departments.FirstOrDefault(p => p.DepID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }

            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult departs_add(Department newDetail, string save, string LinkID)
        {
            if (ModelState.IsValid)
            {

                //get slide
                var alls = occSysDB.HotLines.ToList();
                //new slide
                Department newHot = new Department();

                if (newDetail.IsActive != null)
                {
                    newHot.IsActive = true;
                }
                else
                {
                    newHot.IsActive = false;
                }

                newHot.DepName = newDetail.DepName;
                newHot.Lang_ID = newDetail.Lang_ID;

                if (newDetail.SortOrder != null)
                {
                    newHot.SortOrder = newDetail.SortOrder;
                }
                else
                {
                    if (alls.Count > 0)
                    {
                        newHot.SortOrder = alls.Count + 1;
                    }
                    else
                    {
                        newHot.SortOrder = 1;
                    }
                }

                //link obj
                if (LinkID != null)
                {
                    newHot.LinkID = int.Parse(LinkID);
                }
                else
                {
                    newHot.LinkID = 0;
                }

                //add slide
                occSysDB.Departments.Add(newHot);

                occSysDB.SaveChanges();

                if (save == "save")
                {
                    return RedirectToAction("departs_manager");
                }
                else
                {
                    return RedirectToAction("departs_add");
                }


            }
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliderChecks = occSysDB.Departments.ToList();
            if (sliderChecks.Count > 0)
            {
                ViewBag.SortOrder = sliderChecks.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //if something wrong 
            return View(newDetail);
        }


        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult departs_edit(int id)
        {
            //get detail
            var detail = occSysDB.Departments.FirstOrDefault(p => p.DepID.Equals(id));
            ViewBag.Detail = detail;

            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.Departments.Where(p => p.LinkID == detail.DepID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.Departments.FirstOrDefault(p => p.DepID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            //var detail_des = detail.Sliders_Descriptions.ToList();

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult departs_edit(Department updateDeatail, int editID)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var sliders = occSysDB.Departments.ToList();
                //get update
                var update = occSysDB.Departments.FirstOrDefault(p => p.DepID.Equals(editID));
                update.DepName = updateDeatail.DepName;
                update.Lang_ID = updateDeatail.Lang_ID;
                if (updateDeatail.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }

                if (updateDeatail.SortOrder != null)
                {
                    update.SortOrder = updateDeatail.SortOrder;
                }
                else
                {
                    if (sliders.Count > 0)
                    {
                        update.SortOrder = sliders.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }
                //save
                occSysDB.SaveChanges();

                return RedirectToAction("departs_manager");
            }

            //get detail
            var detail = occSysDB.Departments.FirstOrDefault(p => p.DepID.Equals(editID));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }
        #endregion
        #endregion

        /// <summary>
        ///   Position
        /// </summary>
        /// <returns></returns>
        #region Position
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult pos_manager()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.Positions.OrderBy(p => p.SortOrder).ToList();
            return View(list);
        }
        [HttpPost]
        public ActionResult pos_manager(IEnumerable<int> itemID, IEnumerable<int> updateID, List<int> SortOrder, string deleteOK, string update)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get list
            var list = occSysDB.Positions.OrderBy(p => p.SortOrder).ToList();
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var del = occSysDB.Positions.FirstOrDefault(p => p.PosID.Equals(id));
                    //already
                    if (del != null)
                    {
                        var staffs = del.Staffs.ToList();
                        if (staffs.Count == 0)
                        {
                            //delete slide
                            occSysDB.Positions.Remove(del);
                            //save action
                            occSysDB.SaveChanges();
                        }
                        else
                        {
                            ViewBag.Error = "Danh mục đang được sử dụng !";
                            return View(list);
                        }

                    }
                }
            }

            //update sortorder
            int sortorder = 0;
            if (SortOrder != null && update == "OK")
            {
                foreach (var slideid in updateID)
                {
                    //get slider
                    var slideUpdate = occSysDB.Positions.FirstOrDefault(p => p.PosID.Equals(slideid));
                    if (slideUpdate != null)
                    {
                        //update sort order
                        slideUpdate.SortOrder = SortOrder[sortorder];
                    }
                    //up index
                    sortorder++;
                }
                //save
                occSysDB.SaveChanges();
            }

            return RedirectToAction("pos_manager");
        }
        #endregion
        #region add
        public ActionResult _position_deps_add(int? langID, int? linkID)
        {
            var list = occSysDB.Departments.Where(p => p.Language.IsDefault == true).ToList();
            if (langID != null)
            {
                list = occSysDB.Departments.Where(p => p.Lang_ID == langID).ToList();
            }

            ViewBag.LinkID = linkID;

            return PartialView(list);
        }

        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult pos_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            if (langID != null)
            {
                ViewBag.LangAdd = langID;
            }

            //get order slide
            var sliders = occSysDB.Positions.ToList();
            if (sliders.Count > 0)
            {
                ViewBag.SortOrder = sliders.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            if (linkID != null)
            {
                var linkObj = occSysDB.Positions.FirstOrDefault(p => p.PosID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }

            }

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult pos_add(Position newDetail, string save, string LinkID)
        {
            if (ModelState.IsValid)
            {

                //get slide
                var alls = occSysDB.Positions.ToList();
                //new slide
                Position newHot = new Position();

                newHot.PosName = newDetail.PosName;
                newHot.Lang_ID = newDetail.Lang_ID;
                newHot.DepID = newDetail.DepID;

                if (newDetail.IsActive != null)
                {
                    newHot.IsActive = true;
                }
                else
                {
                    newHot.IsActive = false;
                }

                if (newDetail.SortOrder != null)
                {
                    newHot.SortOrder = newDetail.SortOrder;
                }
                else
                {
                    if (alls.Count > 0)
                    {
                        newHot.SortOrder = alls.Count + 1;
                    }
                    else
                    {
                        newHot.SortOrder = 1;
                    }
                }


                //link obj
                if (LinkID != null)
                {
                    newHot.LinkID = int.Parse(LinkID);
                }
                else
                {
                    newHot.LinkID = 0;
                }



                //add slide
                occSysDB.Positions.Add(newHot);

                occSysDB.SaveChanges();

                if (save == "save")
                {
                    return RedirectToAction("pos_manager");
                }
                else
                {
                    return RedirectToAction("pos_add");
                }


            }
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliderChecks = occSysDB.Positions.ToList();
            if (sliderChecks.Count > 0)
            {
                ViewBag.SortOrder = sliderChecks.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //if something wrong 
            return View(newDetail);
        }


        #endregion
        #region edit
        public ActionResult _position_deps_edit(int? id, int? langID)
        {
            var list = occSysDB.Departments.ToList();
            if (langID != null)
            {
                list = occSysDB.Departments.Where(p => p.Lang_ID == langID).ToList();
            }
            ViewBag.DepID = id;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult pos_edit(int id)
        {
            //get detail
            var detail = occSysDB.Positions.FirstOrDefault(p => p.PosID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.Positions.Where(p => p.LinkID == detail.PosID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.Positions.FirstOrDefault(p => p.PosID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult pos_edit(Position updateDeatail, int editID)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var sliders = occSysDB.Positions.ToList();
                //get update
                var update = occSysDB.Positions.FirstOrDefault(p => p.PosID.Equals(editID));
                update.PosName = updateDeatail.PosName;
                update.Lang_ID = updateDeatail.Lang_ID;
                update.DepID = updateDeatail.DepID;
                if (updateDeatail.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }

                if (updateDeatail.SortOrder != null)
                {
                    update.SortOrder = updateDeatail.SortOrder;
                }
                else
                {
                    if (sliders.Count > 0)
                    {
                        update.SortOrder = sliders.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }
                //save
                occSysDB.SaveChanges();

                return RedirectToAction("pos_manager");
            }

            //get detail
            var detail = occSysDB.Positions.FirstOrDefault(p => p.PosID.Equals(editID));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }
        #endregion
        #endregion

        /// <summary>
        ///   Staff
        /// </summary>
        /// <returns></returns>
        #region Staff
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult staffs_manager()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.Staffs.OrderByDescending(p => p.StaffID).ToList();

            //var driver = occSysDB.Positions.FirstOrDefault(p => p.PosID == 9);
            //var divers = driver.Staffs.ToList();
            //var staffView = occSysDB.StaffViews.FirstOrDefault(p => p.ViewType == 3);
            //foreach (var item in divers)
            //{
            //    item.StaffViews.Add(staffView);
            //    occSysDB.SaveChanges();
            //}


            return View(list);
        }
        [HttpPost]
        public ActionResult staffs_manager(IEnumerable<int> itemID, IEnumerable<int> updateID, List<int> SortOrder, string deleteOK, string update)
        {
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var del = occSysDB.Staffs.FirstOrDefault(p => p.StaffID.Equals(id));
                    //already
                    if (del != null)
                    {
                        var posDels = del.Positions.ToList();
                        if (posDels.Count > 0)
                        {
                            foreach (var pos in posDels)
                            {
                                pos.Staffs.Remove(del);
                                occSysDB.SaveChanges();
                            }
                        }

                        var staffViews = del.StaffViews.ToList();
                        if (staffViews.Count > 0)
                        {
                            foreach (var stv in staffViews)
                            {
                                stv.Staffs.Remove(del);
                                occSysDB.SaveChanges();

                            }
                        }

                        //delete slide
                        occSysDB.Staffs.Remove(del);
                        //save action
                        occSysDB.SaveChanges();
                    }
                }
            }

            //update sortorder
            //int sortorder = 0;
            //if (SortOrder != null && update == "OK")
            //{
            //    foreach (var slideid in updateID)
            //    {
            //        //get slider
            //        var slideUpdate = occSysDB.Positions.FirstOrDefault(p => p.PosID.Equals(slideid));
            //        if (slideUpdate != null)
            //        {
            //            //update sort order
            //            slideUpdate.SortOrder = SortOrder[sortorder];
            //        }
            //        //up index
            //        sortorder++;
            //    }
            //    //save
            //    occSysDB.SaveChanges();
            //}

            return RedirectToAction("staffs_manager");
        }
        #endregion
        #region add
        public ActionResult _staff_position_add(int? langID, int? linkStaffID)
        {
            //get department link
            var depLinks = occSysDB.Departments.Where(p => p.Language.IsDefault == true).ToList();
            //get list position default
            var list = occSysDB.Positions.Where(p => p.Language.IsDefault == true).ToList();
            if (linkStaffID != null)
            {
                var staff = occSysDB.Staffs.FirstOrDefault(p => p.StaffID == linkStaffID);
                var listStaffInPos = staff.Positions.ToList();
                ViewBag.StaffPositions = listStaffInPos;
            }

            //if have lang
            if (langID != null)
            {
                depLinks = occSysDB.Departments.Where(p => p.Lang_ID == langID).ToList();
                list = occSysDB.Positions.Where(p => p.Lang_ID == langID).ToList();
            }

            ViewBag.LinkStaffID = linkStaffID;
            ViewBag.AllPositions = list;

            return PartialView(depLinks);
        }

        public ActionResult _staff_carmodels_add(int? langID, int? linkCarID)
        {
            //get list position default
            var list = occSysDB.CarModels.Where(p => p.Language.IsDefault == true).ToList();
            //if have lang
            if (langID != null)
            {
                list = occSysDB.CarModels.Where(p => p.Lang_ID == langID).ToList();
            }

            ViewBag.LinkCarID = linkCarID;

            return PartialView(list);
        }

        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult staffs_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            if (langID != null)
            {
                ViewBag.LangAdd = langID;
            }


            //get order slide
            var sliders = occSysDB.Staffs.ToList();
            if (sliders.Count > 0)
            {
                ViewBag.SortOrder = sliders.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }
            //get linkID in lang default
            if (linkID != null)
            {
                var linkObj = occSysDB.Staffs.FirstOrDefault(p => p.StaffID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }
            }

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult staffs_add(Staff newDetail, string save, IEnumerable<int> PosID, HttpPostedFileBase file_image, string LinkID, IEnumerable<int> StaffView, string birthDay)
        {
            if (ModelState.IsValid)
            {
                bool formIsvalid = false;
                if (file_image != null && LinkID != null)
                {
                    formIsvalid = true;
                }
                if (file_image == null && LinkID != null)
                {
                    formIsvalid = true;
                }
                if (file_image != null && LinkID == null)
                {
                    formIsvalid = true;
                }

                if (formIsvalid == true)
                {
                    //get slide
                    var alls = occSysDB.Staffs.ToList();
                    //new slide
                    Staff newHot = new Staff();

                    newHot.StaffName = newDetail.StaffName;
                    newHot.StaffAddress = newDetail.StaffAddress;
                    if (newDetail.StaffPhone != null)
                    {
                        newHot.StaffPhone = newDetail.StaffPhone.Replace("_", "").ToString();
                    }

                    if (newDetail.StaffMobile != null)
                    {
                        newHot.StaffMobile = newDetail.StaffMobile.Replace("_", "").ToString(); 
                    }
                    
                 
                    newHot.StaffEmail = newDetail.StaffEmail;
                    newHot.StaffDescription = newDetail.StaffDescription;
                    newHot.ModID = newDetail.ModID;
                    newHot.CarNumber = newDetail.CarNumber;
                    newHot.DriverLicense = newDetail.DriverLicense;
                    newHot.StaffFace = newDetail.StaffFace;
                    newHot.StaffTwitter = newDetail.StaffTwitter;
                    newHot.StaffGoogle = newDetail.StaffGoogle;
                    newHot.StaffSkype = newDetail.StaffSkype;
                    newHot.StaffYahoo = newDetail.StaffYahoo;

                    newHot.Lang_ID = newDetail.Lang_ID;
                    if (birthDay != null)
                    {
                        string newsDate = birthDay.Replace("/", "").Replace("_", "").ToString();

                        if (newsDate != "")
                        {
                            newHot.StaffBirthDay = DateTime.ParseExact(birthDay, "dd/MM/yyyy", null);
                        } 
                    }
                   

                    if (LinkID != null)
                    {
                        newHot.LinkID = int.Parse(LinkID);
                    }
                    else
                    {
                        newHot.LinkID = 0;
                    }

                    //add slide
                    occSysDB.Staffs.Add(newHot);
                    //save first
                    occSysDB.SaveChanges();

                    if (file_image != null)
                    {
                        //upload image
                        string duoiAnh = Path.GetExtension(file_image.FileName);
                        string fileName = OccConvert.KhongDau(newHot.StaffName.ToLower()) + "-" + newHot.StaffID + duoiAnh;
                        //create folder
                        var newPathImage = Server.MapPath("/Uploaded/Images/nhanvien");
                        string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));

                        if (!Directory.Exists(newPathImage))
                        {
                            Directory.CreateDirectory(newPathImage);
                        }
                        //save file to folder
                        file_image.SaveAs(fileUrl);

                        //get image
                        newHot.StaffImage = fileName;
                    }
                    else
                    {
                        //get image
                        newHot.StaffImage = newDetail.StaffImage;
                    }


                    if (StaffView != null)
                    {
                        foreach (var vID in StaffView)
                        {
                            var staffV = occSysDB.StaffViews.FirstOrDefault(p => p.ViewType == vID);
                            staffV.Staffs.Add(newHot);
                            occSysDB.SaveChanges();
                        }
                    }

                    if (PosID != null)
                    {
                        foreach (var posID in PosID)
                        {
                            var position = occSysDB.Positions.FirstOrDefault(p => p.PosID == posID);
                            if (position != null)
                            {
                                position.Staffs.Add(newHot);
                                occSysDB.SaveChanges();
                            }
                        }
                    }

                    occSysDB.SaveChanges();
                    if (save == "save")
                    {
                        return RedirectToAction("staffs_manager");
                    }
                    else
                    {
                        return RedirectToAction("staffs_add");
                    }
                }
                else
                {
                    ViewBag.Mess = "Bạn chưa upload ảnh slide";
                }
            }
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliderChecks = occSysDB.Positions.ToList();
            if (sliderChecks.Count > 0)
            {
                ViewBag.SortOrder = sliderChecks.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //if something wrong 
            return View(newDetail);
        }


        #endregion
        #region edit
        public ActionResult _staff_position_edit(int? langID, int? staffID)
        {
            //get department link
            var depLinks = occSysDB.Departments.Where(p => p.Lang_ID == langID).ToList();
            //get list position default
            var list = occSysDB.Positions.Where(p => p.Lang_ID == langID).ToList();
            if (staffID != null)
            {
                var staff = occSysDB.Staffs.FirstOrDefault(p => p.StaffID == staffID);
                var listStaffInPos = staff.Positions.ToList();
                ViewBag.StaffPositions = listStaffInPos;
            }
            ViewBag.StaffID = staffID;
            ViewBag.AllPositions = list;


            return PartialView(depLinks);
        }

        public ActionResult _staff_carmodels_edit(int? langID, int? modID)
        {
            var list = occSysDB.CarModels.Where(p => p.Lang_ID == langID).ToList();
            ViewBag.CarID = modID;
            return PartialView(list);
        }

        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult staffs_edit(int id)
        {
            //get detail
            var detail = occSysDB.Staffs.FirstOrDefault(p => p.StaffID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.Staffs.Where(p => p.LinkID == detail.StaffID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.Staffs.FirstOrDefault(p => p.StaffID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult staffs_edit(Staff updateDeatail, int editID, IEnumerable<int> PosID, IEnumerable<int> StaffView, string birthDay)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var sliders = occSysDB.Staffs.ToList();
                //get update
                var update = occSysDB.Staffs.FirstOrDefault(p => p.StaffID.Equals(editID));

                if (birthDay != null)
                {
                    string newsDate = birthDay.Replace("/", "").Replace("_", "").ToString();

                    if (newsDate != "")
                    {
                        update.StaffBirthDay = DateTime.ParseExact(birthDay, "dd/MM/yyyy", null);
                    }
                }
                

                update.StaffName = updateDeatail.StaffName;
                update.StaffAddress = updateDeatail.StaffAddress;
                if (updateDeatail.StaffPhone != null)
                {
                    update.StaffPhone = updateDeatail.StaffPhone.Replace("_", "").ToString(); 
                }

                if (updateDeatail.StaffMobile != null)
                {
                    update.StaffMobile = updateDeatail.StaffMobile.Replace("_", "").ToString();
                }

              

                update.StaffEmail = updateDeatail.StaffEmail;
                update.StaffDescription = updateDeatail.StaffDescription;
                update.ModID = updateDeatail.ModID;
                update.CarNumber = updateDeatail.CarNumber;
                update.DriverLicense = updateDeatail.DriverLicense;
                update.StaffFace = updateDeatail.StaffFace;
                update.StaffTwitter = updateDeatail.StaffTwitter;
                update.StaffGoogle = updateDeatail.StaffGoogle;
                update.StaffSkype = updateDeatail.StaffSkype;
                update.StaffYahoo = updateDeatail.StaffYahoo;

                //reset staff view
                var staffViews = update.StaffViews.ToList();
                if (staffViews.Count > 0)
                {
                    foreach (var stv in staffViews)
                    {
                        update.StaffViews.Remove(stv);
                    }
                    occSysDB.SaveChanges();
                }

                if (StaffView != null)
                {
                    foreach (var vID in StaffView)
                    {
                        var staffV = occSysDB.StaffViews.FirstOrDefault(p => p.ViewType == vID);
                        update.StaffViews.Add(staffV);
                        occSysDB.SaveChanges();
                    }
                }

                //reset position
                var posDels = update.Positions.ToList();
                if (posDels.Count > 0)
                {
                    foreach (var pos in posDels)
                    {
                        update.Positions.Remove(pos);
                    }
                    occSysDB.SaveChanges();
                }

                if (PosID != null)
                {
                    foreach (var posID in PosID)
                    {
                        var position = occSysDB.Positions.FirstOrDefault(p => p.PosID == posID);
                        if (position != null)
                        {
                            position.Staffs.Add(update);
                            occSysDB.SaveChanges();
                        }
                    }
                }

                //save
                occSysDB.SaveChanges();

                return RedirectToAction("staffs_manager");
            }

            //get detail
            var detail = occSysDB.Positions.FirstOrDefault(p => p.PosID.Equals(editID));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }
        #endregion
        #endregion

    }
}
