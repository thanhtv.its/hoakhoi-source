﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.IO;
using MvcWebsiteOcc.Areas.administrators.Models;
using System.Text.RegularExpressions;

namespace MvcWebsiteOcc.Areas.administrators.Controllers
{
    public class bookcars_managerController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();


        /// <summary>
        ///   Book Order
        /// </summary>
        /// <returns></returns>
        /// 
        #region Book Order
        #region view
        public ActionResult _menu_bookoder()
        {
            var statusLst = occSysDB.StatusOrders.Where(p => p.IsActive == true).OrderBy(p => p.SortOrder).ToList();

            ViewBag.NewOrder = occSysDB.BookCars.Where(p => p.StaID == null).Count();

            return PartialView(statusLst);
        }

        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult bookorders_manager(string status)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.BookCars.Where(p => p.StaID == null).OrderByDescending(p => p.CreateDate).ToList();

            if (status != null)
            {
                int a = status.LastIndexOf("-");
                string title = status.Substring(a).Replace("-", "");
                string b = Regex.Match(title, @"\d+").Value;
                int id = int.Parse(b);

                var statusOrder = occSysDB.StatusOrders.FirstOrDefault(p => p.StaID == id);
                if (statusOrder != null)
                {
                    list = occSysDB.BookCars.Where(p => p.StaID == statusOrder.StaID).OrderByDescending(p => p.CreateDate).ToList();
                }
            }


            return View(list);
        }
        [HttpPost]
        public ActionResult bookorders_manager(IEnumerable<int> itemID, IEnumerable<int> updateID, List<int> SortOrder, string deleteOK, string update, string status)
        {
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var del = occSysDB.BookCars.FirstOrDefault(p => p.BookID.Equals(id));
                    //already
                    if (del != null)
                    {
                        var serviceDels = del.BooksServices.ToList();

                        if (serviceDels.Count > 0)
                        {
                            foreach (var serDel in serviceDels)
                            {
                                del.BooksServices.Remove(serDel);
                            }
                        }

                        //delete slide
                        occSysDB.BookCars.Remove(del);

                        //save action
                        occSysDB.SaveChanges();
                    }
                }
            }
            return RedirectToAction("bookorders_manager", "bookcars_manager", new { status = status });
        }
        #endregion

        #region details
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult bookorders_detail(int id)
        {
            //get detail
            var detail = occSysDB.BookCars.FirstOrDefault(p => p.BookID.Equals(id));
            ViewBag.Detail = detail;

            if (detail.StaID == 1)
            {
                detail.StaID = 2;
                occSysDB.SaveChanges();
            }


            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult bookorders_detail(BookCar updateDeatail, int editID)
        {
            if (ModelState.IsValid)
            {

                //get update
                var update = occSysDB.BookCars.FirstOrDefault(p => p.BookID.Equals(editID));

                //update.CServName = updateDeatail.CServName;
                //update.CServPrice = updateDeatail.CServPrice;
                //update.UnitPrice = updateDeatail.UnitPrice;

                //save
                occSysDB.SaveChanges();

                return RedirectToAction("bookservices_manager");
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }
        #endregion

        #region detail edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult bookorders_detail_edit(int id)
        {
            //get detail
            var detail = occSysDB.BookCars.FirstOrDefault(p => p.BookID.Equals(id));
            ViewBag.Detail = detail;

            //service
            var bookServices = occSysDB.BooksServices.ToList();
            ViewBag.Services = bookServices;

            //cars
            var cars = occSysDB.CarModels.Where(p => p.ModID != detail.ModID && p.Language.IsDefault == true).ToList();
            ViewBag.Cars = cars;

            //staff
            var staffView = occSysDB.StaffViews.FirstOrDefault(p => p.ViewType == 3);
            var drivers = staffView.Staffs.Where(p => p.StaffID != detail.StaffID && p.Language.IsDefault == true).ToList();
            ViewBag.Drivers = drivers;

            //Status
            var status = occSysDB.StatusOrders.OrderBy(p => p.SortOrder).ToList();
            ViewBag.Status = status;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(detail);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult bookorders_detail_edit(BookCar updateDeatail, int editID, string price, string date_start_end, IEnumerable<int> book_services, string save, string emailCus, string emailTitle)
        {
            //get detail
            var detail = occSysDB.BookCars.FirstOrDefault(p => p.BookID.Equals(editID));
            ViewBag.Detail = detail;

            //service
            var bookServices = occSysDB.BooksServices.ToList();
            ViewBag.Services = bookServices;

            //cars
            var cars = occSysDB.CarModels.Where(p => p.ModID != detail.ModID && p.Language.IsDefault == true).ToList();
            ViewBag.Cars = cars;

            //staff
            var staffView = occSysDB.StaffViews.FirstOrDefault(p => p.ViewType == 3);
            var drivers = staffView.Staffs.Where(p => p.StaffID != detail.StaffID && p.Language.IsDefault == true).ToList();
            ViewBag.Drivers = drivers;

            //Status
            var status = occSysDB.StatusOrders.OrderBy(p => p.SortOrder).ToList();
            ViewBag.Status = status;

            //send mail
            if (save == "sendMail")
            {
                //get send mail status
                bool ok = bookorder_send_mail(editID, emailCus, emailTitle);
                //if ok
                if (ok == true)
                {
                    ViewBag.OK = "Đã gửi mail thành công cho khách hàng";
                }
                else
                {
                    ViewBag.Error = "Gửi mail không thành công";
                }
                return View(detail);
            }

                //submit to edit book order
            else
            {
                if (ModelState.IsValid)
                {
                    //get update
                    var update = occSysDB.BookCars.FirstOrDefault(p => p.BookID.Equals(editID));

                    update.ModID = updateDeatail.ModID;
                    update.StaffID = updateDeatail.StaffID;

                    if (updateDeatail.CheckOutType != null)
                    {
                        update.CheckOutType = updateDeatail.CheckOutType;
                    }
                    else
                    {
                        update.CheckOutType = "1";
                    }

                    if (updateDeatail.hasVAT != null)
                    {
                        update.hasVAT = updateDeatail.hasVAT;
                    }
                    else
                    {
                        update.hasVAT = false;
                    }

                    update.CusName = updateDeatail.CusName;
                    update.CusAddress = updateDeatail.CusAddress;
                    update.CusMobile = updateDeatail.CusMobile;
                    update.CusEmail = updateDeatail.CusEmail;
                    update.CusMessage = updateDeatail.CusMessage;

                    update.CheckOutType = updateDeatail.CheckOutType;

                    update.StartAddress = updateDeatail.StartAddress;
                    update.EndAddress = updateDeatail.EndAddress;

                    string[] date = date_start_end.Split('-');
                    string startDate = date[0].Trim();
                    string endDate = date[1].Trim();

                    update.DateStart = DateTime.ParseExact(startDate, "dd/MM/yyyy", null);
                    update.DateEnd = DateTime.ParseExact(endDate, "dd/MM/yyyy", null);

                    if (book_services != null)
                    {
                        foreach (var bserv in book_services)
                        {
                            var service = occSysDB.BooksServices.FirstOrDefault(p => p.CServID == bserv);
                            if (service != null)
                            {
                                service.BookCars.Add(update);
                                occSysDB.SaveChanges();
                            }
                        }
                    }

                    update.TimeStart = updateDeatail.TimeStart;
                    update.TimeEnd = updateDeatail.TimeEnd;


                    update.StaID = updateDeatail.StaID;


                    update.Total = updateDeatail.Total;



                    //save
                    occSysDB.SaveChanges();
                    //get detail
                    var details = occSysDB.BookCars.FirstOrDefault(p => p.BookID.Equals(editID));
                    return View(details);
                }

                //get lang
                var lang = occSysDB.Languages.ToList();
                ViewBag.LangList = lang;

                //if something wrong 
                return View(updateDeatail);
            }

        }
        [HttpPost]
        public bool bookorder_send_mail(int sendID, string emailCus, string emailTitle)
        {
            var newOrder = occSysDB.BookCars.FirstOrDefault(p => p.BookID == sendID);
            if (emailCus != null)
            {

                //SENT MAIL
                string smtpUserName = "neozeuz12@gmail.com";
                string smtpPassword = "osekuwoljkseyyer";
                string smtpHost = "smtp.gmail.com";
                int smtpPort = 25;

                //read template mail
                var sr = new StreamReader(Server.MapPath("/Views/EmailTemplates/EmailCus.txt"));

                //sent to
                //string emailTo = "info@occ.com.vn"; // Khi có liên hệ sẽ gửi về thư của mình
                string emailTo = emailCus; // Khi có liên hệ sẽ gửi về thư của mình
                string emailTo2 = "hattn@xedichvu.vn"; // Khi có liên hệ sẽ gửi về thư của mình

                string subject = "HÓA ĐƠN ĐẶT XE TRỰC TUYẾN - " + newOrder.OrderID;

                if (emailTitle != null)
                {
                    subject = emailTitle;
                }

                //email content
                var car = occSysDB.CarModels.FirstOrDefault(p => p.ModID == newOrder.ModID);

                //[0]
                string ngaydat = OccConvert.ViewDateFull(newOrder.CreateDate);

                //[1]
                string anhxe = Request.Url.Host.ToLower() + "/Uploaded/Images/dongxe/" + newOrder.CarModel.Folder + "/" + newOrder.CarModel.ModImage;
                //[2]
                string tenxe = car.ModName;
                //[3]
                string hangxe = car.ModBrand;
                //[4]
                string kieuxe = car.CarType.TypeName;

                //[5]
                string tenkh = newOrder.CusName;
                //[6]
                string diachikh = newOrder.CusAddress;
                //[7]
                string sodienthoai = newOrder.CusMobile;
                //[8]
                string email = newOrder.CusEmail;

                //string anhlotrinh = Request.Url.Host.ToLower() + "/Uploaded/Images/anh-lenh-dat-xe/" + newOrder.OrderID + ".png";
                //[9]
                string anhlotrinh = Request.Url.Host.ToLower() + "/Uploaded/Images/anh-lenh-dat-xe/TTN-19022016-59.png";
                //[10]
                string ngaydi = newOrder.DateStart.ToString("dd/MM/yyyy");
                //[11]
                string diemdi = newOrder.StartAddress;
                //[12]
                string ngayve = newOrder.DateEnd.ToString("dd/MM/yyyy");
                //[13]
                string diemden = newOrder.EndAddress;


                //[14]
                string songuoi = (newOrder.Man + newOrder.Child).ToString();
                //[15]
                string nguoilon = newOrder.Man.ToString();
                //[16]
                string treem = newOrder.Child.ToString();
                //[17]
                string nguoigia = newOrder.Senior.ToString();
                //[18]
                string khuyettat = newOrder.Wheelchair.ToString();

                //[17]
                //string linkedit = Request.Url.Host.ToLower() + "/administrators/bookcars_manager/bookorders_detail_edit/" + newOrder.BookID;

                //[19]
                string uoctinhgia = newOrder.Total + " ₫";
                if (newOrder.Total == null)
                {
                    uoctinhgia = "Chưa ước tính chi phí";
                }
                //[20]
                string coVat = "<span style='font-size: 12px; color: #BB0B32; font-weight: bold; text-align: right;'>Không có VAT</span>";

                if (newOrder.hasVAT == true)
                {
                    coVat = "<span style='font-size: 12px; color: #20A639; font-weight: bold; text-align: right;'>Có VAT</span>";
                }
                //[21]
                string soluoc = newOrder.CusMessage;



                //and email content
                string body = sr.ReadToEnd();
                string mailBody = string.Format(body,
                                                ngaydat,
                                                anhxe, tenxe, hangxe, kieuxe,
                                                tenkh, diachikh, sodienthoai, email,
                                                anhlotrinh, ngaydi, diemdi, ngayve, diemden,
                                                songuoi, nguoilon, treem, nguoigia, khuyettat,
                                                uoctinhgia, coVat, soluoc);

                //string body = string.Format("Bạn vừa nhận được 1 lệnh đặt xe mới có mã lệnh <b>{0}</b>.",
                //    newOrder.OrderID);

                //email service
                OccSendMail serviceMail = new OccSendMail();

                //check send
                bool kq = serviceMail.SendTwo(
                    smtpUserName,
                    smtpPassword,
                    smtpHost,
                    smtpPort,
                    emailTo,
                    emailTo2,
                    subject,
                    mailBody);

                serviceMail.SendToMe(
                      smtpUserName,
                      smtpPassword,
                      smtpHost,
                      smtpPort,
                      "info@occ.com.vn",
                      subject,
                      mailBody);

            

                //if ok
                if (kq)
                {
                    ModelState.AddModelError("", "Cảm ơn bạn đã liên hệ với chúng tôi.");
                    //return Json(true, JsonRequestBehavior.AllowGet);
                    return true;
                }
                //oh, have a fuking something wrong
                else
                {
                    ModelState.AddModelError("", "Gửi tin nhắn thất bại, vui lòng thử lại.");
                    //return Json(false, JsonRequestBehavior.AllowGet);
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
        #endregion

        #region _Cars
        public ActionResult _cars(int modID)
        {
            var detail = occSysDB.CarModels.FirstOrDefault(p => p.ModID == modID);
            return PartialView(detail);
        }
        #endregion

        #region _Driver
        public ActionResult _driver(int staffID)
        {
            var detail = occSysDB.Staffs.FirstOrDefault(p => p.StaffID == staffID);
            return PartialView(detail);
        }
        #endregion

        #endregion


        /// <summary>
        ///   Book Service 
        /// </summary>
        /// <returns></returns>
        /// 
        #region Book Service
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult bookservices_manager()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.BooksServices.ToList();
            return View(list);
        }
        [HttpPost]
        public ActionResult bookservices_manager(IEnumerable<int> itemID, IEnumerable<int> updateID, List<int> SortOrder, string deleteOK, string update)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.BooksServices.ToList();
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var del = occSysDB.BooksServices.FirstOrDefault(p => p.CServID.Equals(id));
                    //already
                    if (del != null)
                    {
                        var books = del.BookCars.ToList();
                        if (books.Count == 0)
                        {
                            //delete slide
                            occSysDB.BooksServices.Remove(del);
                            //save action
                            occSysDB.SaveChanges();
                        }
                        else
                        {
                            ViewBag.Error = "Mục này đang được sử dụng !";
                            return View(list);
                        }

                    }
                }
            }

            //update sortorder
            int sortorder = 0;
            if (SortOrder != null && update == "OK")
            {
                foreach (var slideid in updateID)
                {
                    //get slider
                    var slideUpdate = occSysDB.BooksServices.FirstOrDefault(p => p.CServID.Equals(slideid));
                    if (slideUpdate != null)
                    {
                        //update sort order
                        slideUpdate.SortOrder = SortOrder[sortorder];
                    }
                    //up index
                    sortorder++;
                }
                //save
                occSysDB.SaveChanges();
            }

            return RedirectToAction("bookservices_manager");
        }
        #endregion
        #region add
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult bookservices_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            if (langID != null)
            {
                ViewBag.LangAdd = langID;
            }
            //get order slide
            var sliders = occSysDB.BooksServices.Where(p => p.LinkID == 0).ToList();
            if (sliders.Count > 0)
            {
                ViewBag.SortOrder = sliders.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }
            //get linkID in lang default
            if (linkID != null)
            {
                var linkObj = occSysDB.BooksServices.FirstOrDefault(p => p.CServID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult bookservices_add(BooksService newDetail, string save, string LinkID, string price)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var alls = occSysDB.BooksServices.ToList();
                //new slide
                BooksService newHot = new BooksService();

                newHot.CServName = newDetail.CServName;

                if (price.Length == 0)
                    newHot.CServPrice = 0;
                else
                    newHot.CServPrice = decimal.Parse(price.Replace(",", ""));

                newHot.UnitPrice = newDetail.UnitPrice;

                //set lang
                newHot.Lang_ID = newDetail.Lang_ID;

                //set link ID
                if (LinkID != null)
                {
                    newHot.LinkID = int.Parse(LinkID);
                }
                else
                {
                    newHot.LinkID = 0;
                }
                //set active
                if (newDetail.IsActive != null)
                {
                    newHot.IsActive = true;
                }
                else
                {
                    newHot.IsActive = false;
                }
                //set order
                if (newDetail.SortOrder != null)
                {
                    newHot.SortOrder = newDetail.SortOrder;
                }
                else
                {
                    if (alls.Count > 0)
                    {
                        newHot.SortOrder = alls.Count + 1;
                    }
                    else
                    {
                        newHot.SortOrder = 1;
                    }
                }

                //add slide
                occSysDB.BooksServices.Add(newHot);
                //save first
                occSysDB.SaveChanges();

                if (save == "save")
                {
                    return RedirectToAction("bookservices_manager");
                }
                else
                {
                    return RedirectToAction("bookservices_add");
                }

            }
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliderChecks = occSysDB.BooksServices.Where(p => p.LinkID == 0).ToList();
            if (sliderChecks.Count > 0)
            {
                ViewBag.SortOrder = sliderChecks.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //if something wrong 
            return View(newDetail);
        }


        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult bookservices_edit(int id)
        {
            //get detail
            var detail = occSysDB.BooksServices.FirstOrDefault(p => p.CServID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.BooksServices.Where(p => p.LinkID == detail.CServID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.BooksServices.FirstOrDefault(p => p.CServID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult bookservices_edit(BooksService updateDeatail, int editID, string price)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var alls = occSysDB.BooksServices.Where(p => p.LinkID == 0).ToList();
                //get update
                var update = occSysDB.BooksServices.FirstOrDefault(p => p.CServID.Equals(editID));

                update.CServName = updateDeatail.CServName;
                if (price.Length == 0)
                    update.CServPrice = 0;
                else
                    update.CServPrice = decimal.Parse(price.Replace(",", ""));

                update.UnitPrice = updateDeatail.UnitPrice;
                //set active
                if (updateDeatail.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }
                //set order
                if (updateDeatail.SortOrder != null)
                {
                    update.SortOrder = updateDeatail.SortOrder;
                }
                else
                {
                    if (alls.Count > 0)
                    {
                        update.SortOrder = alls.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }

                //save
                occSysDB.SaveChanges();

                return RedirectToAction("bookservices_manager");
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }



        #endregion
        #endregion

        /// <summary>
        ///   Book Status 
        /// </summary>
        /// <returns></returns>
        /// 
        #region Book Statuse
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult bookstatus_manager()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.StatusOrders.OrderBy(p => p.SortOrder).ToList();
            return View(list);
        }
        [HttpPost]
        public ActionResult bookstatus_manager(IEnumerable<int> itemID, IEnumerable<int> updateID, List<int> SortOrder, string deleteOK, string update)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.StatusOrders.ToList();
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var del = occSysDB.StatusOrders.FirstOrDefault(p => p.StaID.Equals(id));
                    //already
                    if (del != null)
                    {
                        var books = del.BookCars.ToList();
                        if (books.Count == 0)
                        {
                            //delete slide
                            occSysDB.StatusOrders.Remove(del);
                            //save action
                            occSysDB.SaveChanges();
                        }
                        else
                        {
                            ViewBag.Error = "Mục này đang được sử dụng !";
                            return View(list);
                        }

                    }
                }
            }

            //update sortorder
            int sortorder = 0;
            if (SortOrder != null && update == "OK")
            {
                foreach (var slideid in updateID)
                {
                    //get slider
                    var slideUpdate = occSysDB.StatusOrders.FirstOrDefault(p => p.StaID.Equals(slideid));
                    if (slideUpdate != null)
                    {
                        //update sort order
                        slideUpdate.SortOrder = SortOrder[sortorder];
                    }
                    //up index
                    sortorder++;
                }
                //save
                occSysDB.SaveChanges();
            }

            return RedirectToAction("bookstatus_manager");
        }
        #endregion
        #region add
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult bookstatus_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliders = occSysDB.StatusOrders.ToList();
            if (sliders.Count > 0)
            {
                ViewBag.SortOrder = sliders.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult bookstatus_add(StatusOrder newDetail, string save)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var alls = occSysDB.StatusOrders.ToList();
                //new slide
                StatusOrder newHot = new StatusOrder();

                newHot.StaName = newDetail.StaName;
                //set active
                if (newDetail.IsActive != null)
                {
                    newHot.IsActive = true;
                }
                else
                {
                    newHot.IsActive = false;
                }
                //set order
                if (newDetail.SortOrder != null)
                {
                    newHot.SortOrder = newDetail.SortOrder;
                }
                else
                {
                    if (alls.Count > 0)
                    {
                        newHot.SortOrder = alls.Count + 1;
                    }
                    else
                    {
                        newHot.SortOrder = 1;
                    }
                }

                //add slide
                occSysDB.StatusOrders.Add(newHot);
                //save first
                occSysDB.SaveChanges();

                if (save == "save")
                {
                    return RedirectToAction("bookstatus_manager");
                }
                else
                {
                    return RedirectToAction("bookstatus_add");
                }

            }
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliderChecks = occSysDB.StatusOrders.ToList();
            if (sliderChecks.Count > 0)
            {
                ViewBag.SortOrder = sliderChecks.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //if something wrong 
            return View(newDetail);
        }


        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult bookstatus_edit(int id)
        {
            //get detail
            var detail = occSysDB.StatusOrders.FirstOrDefault(p => p.StaID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult bookstatus_edit(StatusOrder updateDeatail, int editID)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var alls = occSysDB.StatusOrders.ToList();
                //get update
                var update = occSysDB.StatusOrders.FirstOrDefault(p => p.StaID.Equals(editID));

                update.StaName = updateDeatail.StaName;

                //set active
                if (updateDeatail.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }
                //set order
                if (updateDeatail.SortOrder != null)
                {
                    update.SortOrder = updateDeatail.SortOrder;
                }
                else
                {
                    if (alls.Count > 0)
                    {
                        update.SortOrder = alls.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }

                //save
                occSysDB.SaveChanges();

                return RedirectToAction("bookstatus_manager");
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }



        #endregion
        #endregion

    }
}
