﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcWebsiteOcc.Areas.administrators.Models;

using System.Web.UI;
using MvcWebsiteOcc.Models;
using System.Threading;

//using Google.Apis.Analytics.v3;
//using Google.Apis.Analytics.v3.Data;
//using Google.Apis.Auth.OAuth2;
//using Google.Apis.Auth.OAuth2.Web;

//using Google.Apis.Util.Store;
//using Google.Apis.Services;

using System.Security.Cryptography.X509Certificates;
using System.IO;
using Google.GData.Client;
using Google.GData.Analytics;

namespace MvcWebsiteOcc.Areas.administrators.Controllers
{
    public class dashboardController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();

        private const string ApplicationName = "MvcThienThaoNguyen";

        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult Index()
        {
            //ViewBag.ThongKe = AnalyticsSummary();

            var orders = occSysDB.BookCars.OrderByDescending(p => p.CreateDate).Take(5).ToList();
            ViewBag.Orders = orders;

            var staffs = occSysDB.Staffs.Where(p => p.Language.Lang_Code == "vi").ToList();
            var cars = occSysDB.CarModels.Where(p => p.Language.Lang_Code == "vi").ToList();
            var news = occSysDB.News.Where(p => p.Language.Lang_Code == "vi").ToList();
            var books = occSysDB.BookCars.ToList();

            ViewBag.NV = staffs.Count;
            ViewBag.XE = cars.Count;
            ViewBag.TIN = news.Count;
            ViewBag.DON = books.Count;

            return View();
        }

        #region Change Password current user
        [ChildActionOnly]
        public ActionResult _changePasswordUser()
        {
            return View();
        }
        [HttpPost]
        public ActionResult _changePasswordUser(LocalPasswordModel model, string UserName)
        {
            if (ModelState.IsValid)
            {
                // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                var nowUser = occSysDB.Users.FirstOrDefault(p => p.UserName == UserName);
                if (nowUser != null)
                {
                    nowUser.Password = OccConvert.MD5Hash(model.ConfirmPassword);
                    occSysDB.SaveChanges();
                    return View("index");
                }
            }

            // If we got this far, something failed, redisplay form
            return View("index");

        }

        #endregion
        public static GOAuth2RequestFactory RefreshAuthenticate()
        {
            OAuth2Parameters parameters = new OAuth2Parameters()
            {
                RefreshToken = "1/vwDSwiB6RmpL_vAFXcxGvZIFxWFM3IRfG7Tpu2OHpm0MEudVrK5jSpoR30zcRFq6",
                AccessToken = "ya29.lAJ2YCeLY1C-EQqsOMBUsv5-qFI5Lkx9DFwltLecL1TNdh9g9aobHZ0sOcIrZ8y44w",
                ClientId = "984953187795-flij6urtu939u9i7mi484mkt2092a10e.apps.googleusercontent.com",
                ClientSecret = "1DT_ooxEZLMBhaS-tTkbxZqN",
                Scope = "https://www.googleapis.com/analytics/v3/data/ga",
                AccessType = "offline",
                TokenType = "refresh"
            };
            string authUrl = OAuthUtil.CreateOAuth2AuthorizationUrl(parameters);
            return new GOAuth2RequestFactory(null, "MvcThienThaoNguyen", parameters);
        }
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public AnalyticsSummary AnalyticsSummary()
        {
            var authFactory = new GAuthSubRequestFactory("analytics", "MvcThienThaoNguyen")
            {
                //GAuthToken = "4/vQA_qQtyFiuWUVjaAN9gAcikDd1ZMpS1KKacfhlNS_0",
                Token = "ya29.lQJDjxI9uBwzDQ-jR5-dgQ7e6_ntn7Sj7TG6nL5Kf1eC0-pzqjhzx6chfJfyjGFY1A"

            };


            var to = DateTime.Today;
            var from = DateTime.Parse("2015-10-01");

            var model = new AnalyticsSummary
            {
                Visits = new List<int>(),
                PageViews = new Dictionary<string, int>(),
                PageTitles = new Dictionary<string, string>(),
                TopReferrers = new Dictionary<string, int>(),
                TopSearches = new Dictionary<string, int>()
            };

            var analytics = new AnalyticsService(authFactory.ApplicationName) { RequestFactory = authFactory };

            // Get from All Visits
            var visits = new DataQuery("ga:112003699", from, to)
            {
                Metrics = "ga:visits",
                Dimensions = "ga:date",
                Sort = "ga:date"
            };
            foreach (DataEntry entry in analytics.Query(visits).Entries)
            {
                var value = entry.Metrics.First().IntegerValue;

                model.Visits.Add(value);
            }


            // Get from month Visits
            DateTime datem = DateTime.Today;
            var firstDayOfMonth = new DateTime(datem.Year, datem.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            var visitsMonth = new DataQuery("ga:112003699", firstDayOfMonth, lastDayOfMonth)
            {
                Metrics = "ga:visits",
                Dimensions = "ga:date",
                Sort = "ga:date"
            };
            foreach (DataEntry entry in analytics.Query(visitsMonth).Entries)
            {
                var value = entry.Metrics.First().IntegerValue;

                model.TotalMonth = value;
            }

            // Get from week Visits
            DateTime datew = DateTime.Today;

            var culture = System.Threading.Thread.CurrentThread.CurrentCulture;
            var diff = datew.DayOfWeek - culture.DateTimeFormat.FirstDayOfWeek;
            if (diff < 0)
                diff += 7;
            var firstDayOfWeek = datew.AddDays(-diff).Date;

            var lastDayOfWeek = firstDayOfWeek.AddDays(6);
            var visitsWeek = new DataQuery("ga:112003699", firstDayOfWeek, lastDayOfWeek)
            {
                Metrics = "ga:visits",
                Dimensions = "ga:date",
                Sort = "ga:date"
            };
            foreach (DataEntry entry in analytics.Query(visitsWeek).Entries)
            {
                var value = entry.Metrics.First().IntegerValue;

                model.TotalWeek = value;
            }

            // Get from week Visits
            DateTime dateToDay = DateTime.Today;
            var visitsToDay = new DataQuery("ga:112003699", dateToDay, dateToDay)
            {
                Metrics = "ga:visits",
                Dimensions = "ga:date",
                Sort = "ga:date"
            };
            foreach (DataEntry entry in analytics.Query(visitsWeek).Entries)
            {
                var value = entry.Metrics.First().IntegerValue;

                model.TotalToDay = value;
            }


            // Get Site Usage
            var siteUsage = new DataQuery("ga:112003699", from, to)
            {
                Metrics = "ga:visits,ga:pageviews,ga:percentNewVisits,ga:avgTimeOnSite,ga:entranceBounceRate,ga:exitRate,ga:pageviewsPerVisit,ga:avgPageLoadTime"
            };
            var siteUsageResult = (DataEntry)analytics.Query(siteUsage).Entries.FirstOrDefault();
            if (siteUsageResult != null)
            {
                foreach (var metric in siteUsageResult.Metrics)
                {
                    switch (metric.Name)
                    {
                        case "ga:visits":
                            model.TotalVisits = metric.IntegerValue;
                            break;
                        case "ga:pageviews":
                            model.TotalPageViews = metric.IntegerValue;
                            break;
                        case "ga:percentNewVisits":
                            model.PercentNewVisits = metric.FloatValue;
                            break;
                        case "ga:avgTimeOnSite":
                            model.AverageTimeOnSite = TimeSpan.FromSeconds(metric.FloatValue);
                            break;
                        case "ga:entranceBounceRate":
                            model.EntranceBounceRate = metric.FloatValue;
                            break;
                        case "ga:exitRate":
                            model.PercentExitRate = metric.FloatValue;
                            break;
                        case "ga:pageviewsPerVisit":
                            model.PageviewsPerVisit = metric.FloatValue;
                            break;
                        case "ga:avgPageLoadTime":
                            model.AveragePageLoadTime = TimeSpan.FromSeconds(metric.FloatValue);
                            break;
                    }
                }
            }

            // Get Top Pages
            var topPages = new DataQuery("ga:112003699", from, to)
            {
                Metrics = "ga:pageviews",
                Dimensions = "ga:pagePath,ga:pageTitle",
                Sort = "-ga:pageviews",
                NumberToRetrieve = 20
            };
            foreach (DataEntry entry in analytics.Query(topPages).Entries)
            {
                var value = entry.Metrics.First().IntegerValue;
                var url = entry.Dimensions.Single(x => x.Name == "ga:pagePath").Value.ToLowerInvariant();
                var title = entry.Dimensions.Single(x => x.Name == "ga:pageTitle").Value;

                if (!model.PageViews.ContainsKey(url))
                    model.PageViews.Add(url, 0);
                model.PageViews[url] += value;

                if (!model.PageTitles.ContainsKey(url))
                    model.PageTitles.Add(url, title);
            }

            // Get Top Referrers
            var topReferrers = new DataQuery("ga:112003699", from, to)
            {
                Metrics = "ga:visits",
                Dimensions = "ga:source,ga:medium",
                Sort = "-ga:visits",
                Filters = "ga:medium==referral",
                NumberToRetrieve = 5
            };
            foreach (DataEntry entry in analytics.Query(topReferrers).Entries)
            {
                var visitCount = entry.Metrics.First().IntegerValue;
                var source = entry.Dimensions.Single(x => x.Name == "ga:source").Value.ToLowerInvariant();

                model.TopReferrers.Add(source, visitCount);
            }

            // Get Top Searches
            var topSearches = new DataQuery("ga:112003699", from, to)
            {
                Metrics = "ga:visits",
                Dimensions = "ga:keyword",
                Sort = "-ga:visits",
                Filters = "ga:keyword!=(not set);ga:keyword!=(not provided)",
                NumberToRetrieve = 5
            };
            foreach (DataEntry entry in analytics.Query(topSearches).Entries)
            {
                var visitCount = entry.Metrics.First().IntegerValue;
                var source = entry.Dimensions.Single(x => x.Name == "ga:keyword").Value.ToLowerInvariant();

                model.TopSearches.Add(source, visitCount);
            }





            //string[] scopes = new string[] { AnalyticsService.Scope.Analytics, AnalyticsService.Scope.AnalyticsReadonly }; // view and manage your Google Analytics data




            //var keyFilePath = @"D:\Thanh\MVC\_2016\Project_TTN\WEBSITE\MvcThienThaoNguyen\MvcThienThaoNguyen\App_Data\Thienthaonguyen-d49e1b2d5c61.p12";    // Downloaded from https://console.developers.google.com
            //var serviceAccountEmail = "407408718192.apps.googleusercontent.com";  // found https://console.developers.google.com

            ////loading the Key file
            //var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);
            //var credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
            //{
            //    Scopes = scopes
            //}.FromCertificate(certificate));

            //var service = new AnalyticsService(new BaseClientService.Initializer()
            //{
            //    HttpClientInitializer = credential,
            //    ApplicationName = "Analytics API Sample",
            //});

            //MetadataResource.ColumnsResource.ListRequest request = service.Metadata.Columns.List("ga");

            //AnalyticsService services = new AnalyticsService(new BaseClientService.Initializer()
            //{
            //    ApiKey = "AIzaSyBoepcYaSUTA6AWxnBMyq9DQzjd1XNICz4",  // from https://console.developers.google.com (Public API access)
            //    ApplicationName = "Analytics API Sample",
            //});


            //// get sample statistics for first profile found
            //DataResource.GaResource.GetRequest data = service.Data.Ga.Get(
            //    "ga:ga:112003699",
            //    "2015-10-01", // start date 
            //    "today", // end date
            //    "ga:visitors,ga:pageviews" // metrics to include in report 
            //    );

            //data.Dimensions = "ga:date";
            ////request.Filters = "";
            //data.Sort = "ga:date";
            //data.StartIndex = 1;
            //data.MaxResults = 500;

            //GaData results = data.Execute();

            return model;
        }




    }
}


