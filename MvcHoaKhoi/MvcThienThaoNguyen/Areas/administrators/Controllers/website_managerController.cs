﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.IO;
using MvcWebsiteOcc.Areas.administrators.Models;

namespace MvcWebsiteOcc.Areas.administrators.Controllers
{
    public class website_managerController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();

        /// <summary>
        /// Languages
        /// </summary>
        /// <returns></returns>
        #region Language
        #region view
        [Authorize(Roles = "superadmin")]
        public ActionResult languages_manager()
        {
            //get all language
            var list = occSysDB.Languages.ToList();
            return View(list);
        }
        [HttpPost]
        [Authorize(Roles = "superadmin")]
        public ActionResult languages_manager(IEnumerable<int> itemID, string deleteOK)
        {
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get lang to delete
                    var langDel = occSysDB.Languages.FirstOrDefault(p => p.Lang_ID.Equals(id));
                    if (langDel != null)
                    {
                        //delete file xml
                        var newPathXml = Server.MapPath("/Localization/") + langDel.Lang_Xml;
                        if (System.IO.File.Exists(newPathXml))
                        {
                            System.IO.File.Delete(newPathXml);
                        }
                        //delete image
                        var newPath = Server.MapPath("/Uploaded/Images/language/") + langDel.Lang_Flag;
                        if (System.IO.File.Exists(newPath))
                        {
                            System.IO.File.Delete(newPath);
                        }

                        //delete lang
                        occSysDB.Languages.Remove(langDel);

                    }
                }
                //save action
                occSysDB.SaveChanges();
            }


            //get all language
            return RedirectToAction("languages_manager");
        }
        #endregion
        #region add
        [Authorize(Roles = "superadmin")]
        public ActionResult languages_add()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "superadmin")]
        public ActionResult languages_add(Language newLang, string save, HttpPostedFileBase file_lang, HttpPostedFileBase file_image)
        {
            if (ModelState.IsValid)
            {
                if (file_image != null)
                {
                    if (file_lang != null)
                    {
                        //upload lang image
                        string duoiXml = Path.GetExtension(file_lang.FileName);
                        string fileXml = newLang.Lang_File.Trim() + duoiXml;
                        string fileXMLUrl = Path.Combine(Server.MapPath("/Localization"), Path.GetFileName(fileXml));

                        var newPathXml = Server.MapPath("/Localization");
                        if (!Directory.Exists(newPathXml))
                        {
                            Directory.CreateDirectory(newPathXml);
                        }

                        newLang.Lang_Xml = fileXml;
                        //save file lang
                        file_lang.SaveAs(fileXMLUrl);
                    }
                    else
                    {
                        newLang.Lang_File = newLang.Lang_File.Trim();
                    }

                    //upload lang image
                    string duoiFile = Path.GetExtension(file_image.FileName);
                    string fileName = newLang.Lang_Code.Trim() + duoiFile;
                    string fileUrl = Path.Combine(Server.MapPath("/Uploaded/Images/language"), Path.GetFileName(fileName));

                    var newPath = Server.MapPath("/Uploaded/Images/language");
                    if (!Directory.Exists(newPath))
                    {
                        Directory.CreateDirectory(newPath);
                    }

                    //save lang image
                    file_image.SaveAs(fileUrl);

                    //save lang image
                    newLang.Lang_Flag = fileName;

                    //add new lang
                    newLang.Lang_Name = newLang.Lang_Name.Trim();
                    newLang.Lang_Code = newLang.Lang_Code.ToLower().Trim();
                    occSysDB.Languages.Add(newLang);
                    //set lang default
                    if (newLang.IsDefault == true)
                    {
                        var langDefault = occSysDB.Languages.FirstOrDefault(p => p.IsDefault == true && p.Lang_ID != newLang.Lang_ID);
                        if (langDefault != null)
                        {
                            langDefault.IsDefault = false;
                        }

                    }
                    //save action
                    occSysDB.SaveChanges();

                    if (save == "save")
                    {
                        return RedirectToAction("languages_manager");
                    }
                    else
                    {
                        return RedirectToAction("languages_add");
                    }
                }
                else
                {
                    ViewBag.Mess = "Bạn chưa upload ảnh quốc kỳ !";
                }

            }
            return View(newLang);
        }


        #endregion
        #region edit
        [Authorize(Roles = "superadmin")]
        public ActionResult languages_edit(int id)
        {
            //get lang
            var detail = occSysDB.Languages.FirstOrDefault(p => p.Lang_ID.Equals(id));
            if (detail == null)
            {
                return RedirectToAction("languages_manager");
            }
            return View(detail);
        }

        [HttpPost]
        public ActionResult languages_edit(int Lang_ID, Language updateLang, HttpPostedFileBase file_lang)
        {
            if (ModelState.IsValid)
            {
                //get lang
                var detail = occSysDB.Languages.FirstOrDefault(p => p.Lang_ID.Equals(Lang_ID));
                detail.Lang_Name = updateLang.Lang_Name;
                detail.Lang_File = updateLang.Lang_File;
                detail.Lang_Code = updateLang.Lang_Code;

                if (file_lang != null)
                {
                    string duoiXml = Path.GetExtension(file_lang.FileName);
                    string fileXml = detail.Lang_File.Trim() + duoiXml;
                    string fileXMLUrl = Path.Combine(Server.MapPath("/Localization"), Path.GetFileName(fileXml));

                    //get new file
                    detail.Lang_Xml = fileXml;
                    //save file lang
                    file_lang.SaveAs(fileXMLUrl);
                }
                //save action
                occSysDB.SaveChanges();

                return RedirectToAction("languages_manager");
            }

            //if something wrong
            return View(updateLang);
        }



        public void updateFlag(int id, HttpPostedFileBase file_image)
        {
            //get lang
            var detail = occSysDB.Languages.FirstOrDefault(p => p.Lang_ID.Equals(id));

            //upload lang image
            string duoiFile = Path.GetExtension(file_image.FileName);
            string fileName = detail.Lang_Code.Trim() + duoiFile;
            string fileUrl = Path.Combine(Server.MapPath("/Uploaded/Images/language"), Path.GetFileName(fileName));

            //save lang image
            file_image.SaveAs(fileUrl);
            //save lang image
            detail.Lang_Flag = fileName;
            //save action
            occSysDB.SaveChanges();


        }
        public void updateXml(int id, HttpPostedFileBase file_lang)
        {
            //get lang
            var detail = occSysDB.Languages.FirstOrDefault(p => p.Lang_ID.Equals(id));

            //upload lang image
            string duoiXml = Path.GetExtension(file_lang.FileName);
            string fileXml = detail.Lang_File.Trim() + duoiXml;
            string fileXMLUrl = Path.Combine(Server.MapPath("/Localization"), Path.GetFileName(fileXml));

            //get new file
            detail.Lang_Xml = fileXml;
            //save file lang
            file_lang.SaveAs(fileXMLUrl);
            //save action
            occSysDB.SaveChanges();
        }
        #endregion
        #endregion

        /// <summary>
        /// Website Detail
        /// </summary>
        /// <returns></returns>
        #region Website Detail
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult webdetails_manager()
        {
            //get detail
            var detail = occSysDB.WebsiteDetails.ToList();

            //get lang list
            var langs = occSysDB.Languages.ToList();
            ViewBag.LangList = langs;

            //get lang default
            var lang = occSysDB.Languages.FirstOrDefault();
            if (langs.Count > 0)
            {
                lang = occSysDB.Languages.FirstOrDefault(p=>p.IsDefault == true);
            }

            //if detail null add new detail
            if (detail.Count == 0)
            {
                //add new
                WebsiteDetail newDetail = new WebsiteDetail();
                newDetail.Phone = "0164 857 0184";
                newDetail.Fax = "";
                newDetail.Hotline = "0164 857 0184";
                newDetail.Email = "thanhtv.its@gmail.com";
                newDetail.Facebook = "https://www.facebook.com/thanh.its";
                newDetail.Logo = "occLogo.png";
                newDetail.latitude = "21.021816";
                newDetail.longitude = "105.818143";

                newDetail.NameCompany = "Neo Company";
                newDetail.NameShort = "Neo Company";
                newDetail.Slogan = "Tầm nhìn sáng tạo";
                newDetail.Address = "9/8 Lang Ha, Ba Dinh, Hanoi";
                newDetail.Description = @"Giới thiệu công ty";
                newDetail.Lang_ID = lang.Lang_ID;

                //add to DB
                occSysDB.WebsiteDetails.Add(newDetail);
                //save action
                occSysDB.SaveChanges();
            }

            return View(detail);

        }
        #endregion
        #region add
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult webdetails_add()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get detail
            var detail = occSysDB.WebsiteDetails.FirstOrDefault();
            ViewBag.Detail = detail;

            return View();
        }
        #endregion

        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult webdetails_edit()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get detail
            var detail = occSysDB.WebsiteDetails.FirstOrDefault();
            ViewBag.Detail = detail;

            return View(detail);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult webdetails_edit(string editID, FormCollection form, WebsiteDetail detail_des, HttpPostedFileBase file_image)
        {
            if (ModelState.IsValid)
            {
                //update detail
                var update = occSysDB.WebsiteDetails.FirstOrDefault(p => p.Web_ID.Equals(editID));
                update.Phone = form["SoDienThoai"].ToString().Trim();
                update.Fax = form["Fax"].ToString().Trim();
                update.Email = form["Email"].ToString().Trim();
                update.Hotline = form["Hotline"].ToString().Trim();
                update.Google = form["Google"].ToString().Trim();
                update.Facebook = form["Facebook"].ToString().Trim();
                update.Twitter = form["Twitter"].ToString().Trim();
                update.Website = form["Website"].ToString().Trim();

                //update.latitude = form["latitude"].ToString().Trim();
                //update.longitude = form["longitude"].ToString().Trim();

                //update logo if logo null
                if (file_image != null)
                {
                    updateLogo(editID, file_image);
                }
                //save action
                occSysDB.SaveChanges();
                return RedirectToAction("webdetails_edit");
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get detail
            var detail = occSysDB.WebsiteDetails.FirstOrDefault();
            ViewBag.Detail = detail;

            return View(detail_des);
        }

        public void updateLogo(string id, HttpPostedFileBase file_image)
        {
            //get lang
            var detail = occSysDB.WebsiteDetails.FirstOrDefault(p => p.Web_ID.Equals(id));

            //upload lang image
            string duoiFile = Path.GetExtension(file_image.FileName);
            string fileName = OccConvert.KhongDau(detail.NameCompany.ToLower().Trim()) + duoiFile;
            string fileUrl = Path.Combine(Server.MapPath("/Uploaded/Images/logo"), Path.GetFileName(fileName));

            //save lang image
            file_image.SaveAs(fileUrl);
            //save lang image
            detail.Logo = fileName;
            //save action
            occSysDB.SaveChanges();
        }
        #endregion
        #endregion

        /// <summary>
        ///   Hot line
        /// </summary>
        /// <returns></returns>
        #region Hotline
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult hotlines_manager()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.HotLines.OrderBy(p=>p.SortOrder).ToList();
            return View(list);
        }
        [HttpPost]
        public ActionResult hotlines_manager(IEnumerable<int> itemID, IEnumerable<int> SlideID, List<int> SortOrder, string deleteOK, string update)
        {
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var del = occSysDB.HotLines.FirstOrDefault(p => p.HotID.Equals(id));
                    //already
                    if (del != null)
                    {
                        //delete slide
                        occSysDB.HotLines.Remove(del);
                        //save action
                        occSysDB.SaveChanges();
                    }
                }
            }

            //update sortorder
            int sortorder = 0;
            if (SortOrder != null && update == "OK")
            {
                foreach (var slideid in SlideID)
                {
                    //get slider
                    var slideUpdate = occSysDB.HotLines.FirstOrDefault(p => p.HotID.Equals(slideid));
                    if (slideUpdate != null)
                    {
                        //update sort order
                        slideUpdate.SortOrder = SortOrder[sortorder];
                    }
                    //up index
                    sortorder++;
                }
                //save
                occSysDB.SaveChanges();
            }

            return RedirectToAction("hotlines_manager");
        }
        #endregion
        #region add
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult hotlines_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliders = occSysDB.HotLines.Where(p => p.LinkID == 0).ToList();
            if (sliders.Count > 0)
            {
                ViewBag.SortOrder = sliders.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            if (linkID != null)
            {
                var linkObj = occSysDB.HotLines.FirstOrDefault(p => p.HotID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }
            }

            if (langID != null)
            {
                ViewBag.LangAdd = langID;
            }

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult hotlines_add(HotLine newDetail, string save, int? LinkID)
        {
            if (ModelState.IsValid)
            {

                //get slide
                var alls = occSysDB.HotLines.ToList();
                //new slide
                HotLine newHot = new HotLine();

                if (newDetail.IsActive != null)
                {
                    newHot.IsActive = true;
                }
                else
                {
                    newHot.IsActive = false;
                }

                newHot.HotTitle = newDetail.HotTitle;
                newHot.HotNumber = newDetail.HotNumber;
                newHot.Lang_ID = newDetail.Lang_ID;

                if (LinkID != null)
                {
                    newHot.LinkID = LinkID;
                }
                else
                {
                    newHot.LinkID = 0;
                }

                if (newDetail.SortOrder != null)
                {
                    newHot.SortOrder = newDetail.SortOrder;
                }
                else
                {
                    if (alls.Count > 0)
                    {
                        newHot.SortOrder = alls.Count + 1;
                    }
                    else
                    {
                        newHot.SortOrder = 1;
                    }
                }

                //add slide
                occSysDB.HotLines.Add(newHot);

                occSysDB.SaveChanges();

                if (save == "save")
                {
                    return RedirectToAction("hotlines_manager");
                }
                else
                {
                    return RedirectToAction("hotlines_add");
                }


            }
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliderChecks = occSysDB.HotLines.ToList();
            if (sliderChecks.Count > 0)
            {
                ViewBag.SortOrder = sliderChecks.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //if something wrong 
            return View(newDetail);
        }


        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult hotlines_edit(int id)
        {
            //get detail
            var detail = occSysDB.HotLines.FirstOrDefault(p => p.HotID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.HotLines.Where(p => p.LinkID == detail.HotID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.HotLines.FirstOrDefault(p => p.HotID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult hotlines_edit(HotLine updateDeatail, int editID)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var sliders = occSysDB.HotLines.ToList();
                //get update
                var update = occSysDB.HotLines.FirstOrDefault(p => p.HotID.Equals(editID));
                update.HotTitle = updateDeatail.HotTitle;
                update.HotNumber = updateDeatail.HotNumber;
                update.Lang_ID = updateDeatail.Lang_ID;
                if (updateDeatail.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }

                if (updateDeatail.SortOrder != null)
                {
                    update.SortOrder = updateDeatail.SortOrder;
                }
                else
                {
                    if (sliders.Count > 0)
                    {
                        update.SortOrder = sliders.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }
                //save
                occSysDB.SaveChanges();

                return RedirectToAction("hotlines_manager");
            }

            //get detail
            var detail = occSysDB.HotLines.FirstOrDefault(p => p.HotID.Equals(editID));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }
        #endregion
        #endregion

        /// <summary>
        ///   Orientation
        /// </summary>
        /// <returns></returns>
        #region    Orientation
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult oriens_manager()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.Orientations.OrderBy(p=>p.SortOrder).ToList();
            return View(list);
        }
        [HttpPost]
        public ActionResult oriens_manager(IEnumerable<int> itemID, IEnumerable<int> updateID, List<int> SortOrder, string deleteOK, string update)
        {
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var del = occSysDB.Orientations.FirstOrDefault(p => p.OriID.Equals(id));
                    //already
                    if (del != null)
                    {
                        //delete slide
                        occSysDB.Orientations.Remove(del);
                        //save action
                        occSysDB.SaveChanges();
                    }
                }
            }

            //update sortorder
            int sortorder = 0;
            if (SortOrder != null && update == "OK")
            {
                foreach (var slideid in updateID)
                {
                    //get slider
                    var slideUpdate = occSysDB.Orientations.FirstOrDefault(p => p.OriID.Equals(slideid));
                    if (slideUpdate != null)
                    {
                        //update sort order
                        slideUpdate.SortOrder = SortOrder[sortorder];
                    }
                    //up index
                    sortorder++;
                }
                //save
                occSysDB.SaveChanges();
            }

            return RedirectToAction("oriens_manager");
        }
        #endregion
        #region add
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult oriens_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliders = occSysDB.Orientations.Where(p=>p.LinkID == 0).ToList();
            if (sliders.Count > 0)
            {
                ViewBag.SortOrder = sliders.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //get linkID in lang default
            if (linkID != null)
            {
                var linkObj = occSysDB.Orientations.FirstOrDefault(p => p.OriID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }
            }

            if (langID != null)
            {
                ViewBag.LangAdd = langID;
            }


            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult oriens_add(Orientation newDetail, string save, string LinkID)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var alls = occSysDB.Orientations.Where(p=>p.LinkID == 0).ToList();
                //new slide
                Orientation newHot = new Orientation();

                if (newDetail.IsActive != null)
                {
                    newHot.IsActive = true;
                }
                else
                {
                    newHot.IsActive = false;
                }
                if (LinkID != null)
                {
                    newHot.LinkID = int.Parse(LinkID);
                }
                else
                {
                    newHot.LinkID = 0;
                }

                newHot.OriTitle = newDetail.OriTitle;
                newHot.OriDescription = newDetail.OriDescription;
                newHot.Lang_ID = newDetail.Lang_ID;

                if (newDetail.SortOrder != null)
                {
                    newHot.SortOrder = newDetail.SortOrder;
                }
                else
                {
                    if (alls.Count > 0)
                    {
                        newHot.SortOrder = alls.Count + 1;
                    }
                    else
                    {
                        newHot.SortOrder = 1;
                    }
                }

                //add slide
                occSysDB.Orientations.Add(newHot);

                occSysDB.SaveChanges();

                if (save == "save")
                {
                    return RedirectToAction("oriens_manager");
                }
                else
                {
                    return RedirectToAction("oriens_add");
                }


            }
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliderChecks = occSysDB.Orientations.ToList();
            if (sliderChecks.Count > 0)
            {
                ViewBag.SortOrder = sliderChecks.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //if something wrong 
            return View(newDetail);
        }


        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult oriens_edit(int id)
        {
            //get detail
            var detail = occSysDB.Orientations.FirstOrDefault(p => p.OriID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.Orientations.Where(p => p.LinkID == detail.OriID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.Orientations.FirstOrDefault(p => p.OriID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult oriens_edit(Orientation updateDeatail, int editID)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var sliders = occSysDB.Orientations.Where(p=>p.LinkID == 0).ToList();
                //get update
                var update = occSysDB.Orientations.FirstOrDefault(p => p.OriID.Equals(editID));
                update.OriTitle = updateDeatail.OriTitle;
                update.OriDescription = updateDeatail.OriDescription;
                update.Lang_ID = updateDeatail.Lang_ID;
                if (updateDeatail.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }

                if (updateDeatail.SortOrder != null)
                {
                    update.SortOrder = updateDeatail.SortOrder;
                }
                else
                {
                    if (sliders.Count > 0)
                    {
                        update.SortOrder = sliders.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }
                //save
                occSysDB.SaveChanges();

                return RedirectToAction("oriens_manager");
            }

            //get detail
            var detail = occSysDB.Orientations.FirstOrDefault(p => p.OriID.Equals(editID));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }
        #endregion
        #endregion

        /// <summary>
        ///   AddressBook
        /// </summary>
        /// <returns></returns>
        #region    AddressBook
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult address_manager()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.AddressBooks.OrderBy(p=>p.SortOrder).ToList();
            return View(list);
        }
        [HttpPost]
        public ActionResult address_manager(IEnumerable<int> itemID, IEnumerable<int> updateID, List<int> SortOrder, string deleteOK, string update)
        {
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var del = occSysDB.AddressBooks.FirstOrDefault(p => p.AddID.Equals(id));
                    //already
                    if (del != null)
                    {
                        //delete slide
                        occSysDB.AddressBooks.Remove(del);
                        //save action
                        occSysDB.SaveChanges();
                    }
                }
            }

            //update sortorder
            int sortorder = 0;
            if (SortOrder != null && update == "OK")
            {
                foreach (var slideid in updateID)
                {
                    //get slider
                    var slideUpdate = occSysDB.AddressBooks.FirstOrDefault(p => p.AddID.Equals(slideid));
                    if (slideUpdate != null)
                    {
                        //update sort order
                        slideUpdate.SortOrder = SortOrder[sortorder];
                    }
                    //up index
                    sortorder++;
                }
                //save
                occSysDB.SaveChanges();
            }

            return RedirectToAction("address_manager");
        }
        #endregion
        #region add
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult address_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliders = occSysDB.AddressBooks.Where(p=>p.LinkID == 0).ToList();
            if (sliders.Count > 0)
            {
                ViewBag.SortOrder = sliders.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //get linkID in lang default
            if (linkID != null)
            {
                var linkObj = occSysDB.AddressBooks.FirstOrDefault(p => p.AddID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }
            }

            if (langID != null)
            {
                ViewBag.LangAdd = langID;
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult address_add(AddressBook newDetail, string save, int? LinkID)
        {
            if (ModelState.IsValid)
            {

                //get slide
                var alls = occSysDB.AddressBooks.ToList();
                //new slide
                AddressBook newHot = new AddressBook();

                if (newDetail.IsActive != null)
                {
                    newHot.IsActive = true;
                }
                else
                {
                    newHot.IsActive = false;
                }

                if (LinkID != null)
                {

                    newHot.LinkID = LinkID;
                }
                else
                {

                    newHot.LinkID = 0;
                }

                newHot.AddTitle = newDetail.AddTitle;
                newHot.AddDescription = newDetail.AddDescription;
                newHot.Lang_ID = newDetail.Lang_ID;

                if (newDetail.SortOrder != null)
                {
                    newHot.SortOrder = newDetail.SortOrder;
                }
                else
                {
                    if (alls.Count > 0)
                    {
                        newHot.SortOrder = alls.Count + 1;
                    }
                    else
                    {
                        newHot.SortOrder = 1;
                    }
                }

                //add slide
                occSysDB.AddressBooks.Add(newHot);

                occSysDB.SaveChanges();

                if (save == "save")
                {
                    return RedirectToAction("address_manager");
                }
                else
                {
                    return RedirectToAction("address_add");
                }


            }
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliderChecks = occSysDB.AddressBooks.Where(p => p.LinkID == 0).ToList();
            if (sliderChecks.Count > 0)
            {
                ViewBag.SortOrder = sliderChecks.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //if something wrong 
            return View(newDetail);
        }


        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult address_edit(int id)
        {
            //get detail
            var detail = occSysDB.AddressBooks.FirstOrDefault(p => p.AddID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.AddressBooks.Where(p => p.LinkID == detail.AddID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.AddressBooks.FirstOrDefault(p => p.AddID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult address_edit(AddressBook updateDeatail, int editID)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var sliders = occSysDB.AddressBooks.ToList();
                //get update
                var update = occSysDB.AddressBooks.FirstOrDefault(p => p.AddID.Equals(editID));
                update.AddTitle = updateDeatail.AddTitle;
                update.AddDescription = updateDeatail.AddDescription;
                update.Lang_ID = updateDeatail.Lang_ID;
                if (updateDeatail.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }

                if (updateDeatail.SortOrder != null)
                {
                    update.SortOrder = updateDeatail.SortOrder;
                }
                else
                {
                    if (sliders.Count > 0)
                    {
                        update.SortOrder = sliders.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }
                //save
                occSysDB.SaveChanges();

                return RedirectToAction("address_manager");
            }

            //get detail
            var detail = occSysDB.AddressBooks.FirstOrDefault(p => p.AddID.Equals(editID));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }
        #endregion
        #endregion

        /// <summary>
        ///   Slide Show
        /// </summary>
        /// <returns></returns>
        #region Slide Show
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult sliders_manager()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.Sliders.OrderBy(p=>p.SortOrder).ToList();
            return View(list);
        }
        [HttpPost]
        public ActionResult sliders_manager(IEnumerable<int> itemID, IEnumerable<int> SlideID, List<int> SortOrder, string deleteOK, string update)
        {
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var sliderDel = occSysDB.Sliders.FirstOrDefault(p => p.SlideID.Equals(id));
                    //already
                    if (sliderDel != null)
                    {
                        //delete slide
                        occSysDB.Sliders.Remove(sliderDel);
                        //save action
                        occSysDB.SaveChanges();
                    }
                }
            }

            //update sortorder
            int sortorder = 0;
            if (SortOrder != null && update == "OK")
            {
                foreach (var slideid in SlideID)
                {
                    //get slider
                    var slideUpdate = occSysDB.Sliders.FirstOrDefault(p => p.SlideID.Equals(slideid));
                    if (slideUpdate != null)
                    {
                        //update sort order
                        slideUpdate.SortOrder = SortOrder[sortorder];
                    }
                    //up index
                    sortorder++;
                }
                //save
                occSysDB.SaveChanges();
            }

            return RedirectToAction("sliders_manager");
        }
        #endregion 

        #region add
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult sliders_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliders = occSysDB.Sliders.Where(p=>p.LinkID == 0).ToList();
            if (sliders.Count > 0)
            {
                ViewBag.SortOrder = sliders.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            if (linkID != null)
            {
                var linkObj = occSysDB.Sliders.FirstOrDefault(p => p.SlideID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }

            }

            if (langID != null)
            {
                ViewBag.LangAdd = langID;
            }

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult sliders_add(Slider newDetail, HttpPostedFileBase file_image, string save, int? LinkID, int? Lang_ID)
        {
            if (ModelState.IsValid)
            {
                bool formIsvalid = false;
                if (file_image != null && LinkID != null)
                {
                    formIsvalid = true;
                }
                if (file_image == null && LinkID != null)
                {
                    formIsvalid = true;
                }
                if (file_image != null && LinkID == null)
                {
                    formIsvalid = true;
                }

                if (formIsvalid == true)
                {
                    //get slide
                    var sliders = occSysDB.Sliders.Where(p=>p.LinkID == 0).ToList();

                    //get file name
                    string nameSlide = "";
                    if (sliders.Count > 0)
                    {
                        nameSlide = "websiteSlide" + (sliders[sliders.Count - 1].SlideID + 1);
                    }
                    else
                    {
                        nameSlide = "websiteSlide" + 1;
                    }
                    //new slide
                    Slider newSlide = new Slider();
                    newSlide.Link = newDetail.Link;
                    if (LinkID != null)
                    {
                        newSlide.LinkID = LinkID;
                    }
                    else
                    {
                        newSlide.LinkID = 0;
                    }
                    if (newDetail.IsActive != null)
                    {
                        newSlide.IsActive = true;
                    }
                    else
                    {
                        newSlide.IsActive = false;
                    }

                    if (newDetail.IsBlank != null)
                    {
                        newSlide.IsBlank = true;
                    }
                    else
                    {
                        newSlide.IsBlank = false;
                    }
                    newSlide.SlideTitle = newDetail.SlideTitle;
                    newSlide.SlideSlogan = newDetail.SlideSlogan;
                    newSlide.Lang_ID = newDetail.Lang_ID;
                    newSlide.Style = newDetail.Style;

                    if (newDetail.SortOrder != null)
                    {
                        newSlide.SortOrder = newDetail.SortOrder;
                    }
                    else
                    {
                        if (sliders.Count > 0)
                        {
                            newSlide.SortOrder = sliders.Count + 1;
                        }
                        else
                        {
                            newSlide.SortOrder = 1;
                        }
                    }
                    //add slide
                    occSysDB.Sliders.Add(newSlide);
                    //save
                    occSysDB.SaveChanges();

                    if (file_image != null)
                    {
                        string duoiAnh = Path.GetExtension(file_image.FileName);
                        string fileName = nameSlide + duoiAnh;
                        string fileUrl = Path.Combine(Server.MapPath("/Uploaded/Images/slideshow"), Path.GetFileName(fileName));

                        //create folder
                        var newPathXml = Server.MapPath("/Uploaded/Images/slideshow");
                        if (!Directory.Exists(newPathXml))
                        {
                            Directory.CreateDirectory(newPathXml);
                        }

                        //save file to folder
                        file_image.SaveAs(fileUrl);
                        newSlide.SlideImage = fileName;
                    }
                    else
                    {
                        newSlide.SlideImage = newDetail.SlideImage;
                    }

                    //save
                    occSysDB.SaveChanges();

                    if (save == "save")
                    {
                        return RedirectToAction("sliders_manager");
                    }
                    else
                    {
                        return RedirectToAction("sliders_add");
                    }
                }
                else
                {
                    ViewBag.Mess = "Bạn chưa upload ảnh";
                }

            }


            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliderChecks = occSysDB.Sliders.ToList();
            if (sliderChecks.Count > 0)
            {
                ViewBag.SortOrder = sliderChecks.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            if (LinkID != null)
            {
                var linkObj = occSysDB.Sliders.FirstOrDefault(p => p.SlideID == LinkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }

            }

            if (Lang_ID != null)
            {
                ViewBag.LangAdd = Lang_ID;
            }
            //if something wrong 
            return View(newDetail);
        }


        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult sliders_edit(int id)
        {
            //get detail
            var detail = occSysDB.Sliders.FirstOrDefault(p => p.SlideID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.Sliders.Where(p => p.LinkID == detail.SlideID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.Sliders.FirstOrDefault(p => p.SlideID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult sliders_edit(Slider updateDeatail, int editID)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var sliders = occSysDB.Sliders.Where(p=>p.LinkID == 0).ToList();
                //get update
                var update = occSysDB.Sliders.FirstOrDefault(p => p.SlideID.Equals(editID));
                update.Link = updateDeatail.Link;
                update.SlideTitle = updateDeatail.SlideTitle;
                update.SlideSlogan = updateDeatail.SlideSlogan;
                update.Lang_ID = updateDeatail.Lang_ID;
                update.Style = updateDeatail.Style;

                if (updateDeatail.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }

                if (updateDeatail.IsBlank != null)
                {
                    update.IsBlank = true;
                }
                else
                {
                    update.IsBlank = false;
                }

                if (updateDeatail.SortOrder != null)
                {
                    update.SortOrder = updateDeatail.SortOrder;
                }
                else
                {
                    if (sliders.Count > 0)
                    {
                        update.SortOrder = sliders.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }
                //save
                occSysDB.SaveChanges();

                return RedirectToAction("sliders_manager");
            }

            //get detail
            var detail = occSysDB.Sliders.FirstOrDefault(p => p.SlideID.Equals(editID));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }

        public void updateSlider(int id, HttpPostedFileBase file_image)
        {
            //get slide
            var sliders = occSysDB.Sliders.FirstOrDefault(p => p.SlideID.Equals(id));

            //get file name
            string nameSlide = "websiteSlide" + sliders.SlideID;

            string duoiAnh = Path.GetExtension(file_image.FileName);
            string fileName = nameSlide + duoiAnh;
            string fileUrl = Path.Combine(Server.MapPath("/Uploaded/Images/slideshow"), Path.GetFileName(fileName));

            //save file to folder
            file_image.SaveAs(fileUrl);
            sliders.SlideImage = fileName;
            //save action
            occSysDB.SaveChanges();
        }

        #endregion
        #endregion

        /// <summary>
        ///  About Us
        /// </summary>
        /// <returns></returns>
        #region About Us
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult about_manager()
        {
            //get all detail
            var detail = occSysDB.AboutUs.ToList();

            if (detail.Count == 0)
            {
                //add new
                AboutUs newDetail = new AboutUs();
                newDetail.AboutID = "occwebabout";
                newDetail.Image = "occLogo.png";

                //add to DB
                occSysDB.AboutUs.Add(newDetail);

                //add detail with lang
                var langs = occSysDB.Languages.ToList();
                if (langs.Count > 0)
                {
                    foreach (var lang in langs)
                    {
                        AboutUs_Description newDetail_Des = new AboutUs_Description();
                        newDetail_Des.NameCompany = "Công ty TNHH Quảng Cáo & Thương Mại Tầm Nhìn";
                        newDetail_Des.Description = @"<p>OCC được th&agrave;nh lập v&agrave;o ng&agrave;y 28/06/2004 l&agrave; c&ocirc;ng ty ti&ecirc;n phong trong lĩnh vực thiết kế đồ họa, x&acirc;y dựng v&agrave; ph&aacute;t triển hệ thống nhận diện thương hiệu tại Việt Nam.</p>
                                                    <p>OCC lu&ocirc;n t&igrave;m t&ograve;i v&agrave; s&aacute;ng tạo kh&ocirc;ng ngừng t&igrave;m đến những hướng đi, tạo dựng những con đường mới cho ch&iacute;nh m&igrave;nh v&agrave; những kh&aacute;ch h&agrave;ng của OCC.</p>
                                                    <p>Một trong những điều th&uacute; vị khi l&agrave;m việc với ch&uacute;ng t&ocirc;i l&agrave; sự th&acirc;n thiện, nghệ thuật tư duy s&aacute;ng tạo hợp l&yacute;, lu&ocirc;n lu&ocirc;n chia sẻ lợi &iacute;ch cho kh&aacute;ch h&agrave;ng của bạn v&agrave; do đ&oacute; cung cấp cho họ những giải ph&aacute;p tốt nhất.</p>
                                                    <p>OCC đ&atilde; v&agrave; đang rất tự tin đi tr&ecirc;n con đường của m&igrave;nh vừa x&acirc;y dựng, ph&aacute;t triển đặt những vi&ecirc;n gạch đầu ti&ecirc;n cho những thương hiệu Việt đặc biệt hơn thương hiệu mang &ldquo;Chi dẫn địa l&yacute; v&ugrave;ng miền của Việt Nam&rdquo;.</p>";
                        newDetail_Des.Lang_ID = lang.Lang_ID;
                        newDetail_Des.AboutID = newDetail.AboutID;

                        //add to DB
                        occSysDB.AboutUs_Descriptions.Add(newDetail_Des);
                        occSysDB.SaveChanges();
                    }
                }
                //save action
                occSysDB.SaveChanges();
            }

            return RedirectToAction("about_edit");
        }
        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult about_edit()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get detail
            var detail = occSysDB.AboutUs.FirstOrDefault();
            ViewBag.Detail = detail;

            //get detail des
            var detail_des = detail.AboutUs_Descriptions.ToList();

            if (lang.Count > detail_des.Count)
            {
                var desDefaul = occSysDB.AboutUs_Descriptions.FirstOrDefault(p => p.Language.IsDefault == true);
                foreach (var lng in lang)
                {
                    if (desDefaul.Lang_ID != lng.Lang_ID)
                    {
                        AboutUs_Description newDetail_Des = new AboutUs_Description();
                        newDetail_Des.NameCompany = desDefaul.NameCompany;
                        newDetail_Des.Description = desDefaul.Description;
                        newDetail_Des.Lang_ID = lng.Lang_ID;
                        newDetail_Des.AboutID = desDefaul.AboutID;

                        //add to DB
                        occSysDB.AboutUs_Descriptions.Add(newDetail_Des);
                        occSysDB.SaveChanges();
                    }
                }

            }

            //anht hanht ich
            var anhtts = occSysDB.AboutUs_Achievements.ToList();
            ViewBag.AchiImages = anhtts;

            return View(detail_des);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult about_edit(string editID, FormCollection form, IEnumerable<AboutUs_Description> detail_des, HttpPostedFileBase file_image, IEnumerable<HttpPostedFileBase> file_images)
        {
            if (ModelState.IsValid)
            {
                //update detail
                var update = occSysDB.AboutUs.FirstOrDefault(p => p.AboutID.Equals(editID));

                //update detail description with lang
                foreach (var update_des in detail_des)
                {
                    var updateDetail = occSysDB.AboutUs_Descriptions.FirstOrDefault(p => p.AboutID == update.AboutID && p.Lang_ID == update_des.Lang_ID);

                    updateDetail.NameCompany = update_des.NameCompany.Trim();
                    updateDetail.Description = update_des.Description;
                }

                //update logo if logo null
                if (file_image != null)
                {
                    updateImageAbout(editID, file_image);
                }

                //upload image in album
                if (file_images != null && file_images.FirstOrDefault() != null)
                {

                    //create folder
                    var newPathImage = Server.MapPath("/Uploaded/Images/gioithieu/anh-thanh-tich");

                    //create folder
                    if (!Directory.Exists(newPathImage))
                    {
                        Directory.CreateDirectory(newPathImage);
                    }

                    foreach (var img in file_images)
                    {
                        //set new
                        AboutUs_Achievements newImg = new AboutUs_Achievements();
                        //add to DB
                        occSysDB.AboutUs_Achievements.Add(newImg);
                        occSysDB.SaveChanges();

                        //upload image
                        string duoiAnhs = Path.GetExtension(img.FileName);
                        string fileNames = "anh-thanhtich-" + newImg.AchiID + duoiAnhs;
                        //create folder
                        string fileUrls = Path.Combine(newPathImage, Path.GetFileName(fileNames));

                        //save
                        img.SaveAs(fileUrls);
                        newImg.AchiImage = fileNames;

                    }
                }

                //save action
                occSysDB.SaveChanges();
                return RedirectToAction("about_edit");
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get detail
            var detail = occSysDB.AboutUs.FirstOrDefault();
            ViewBag.Detail = detail;

            return View(detail_des);
        }
        public void updateImageAbout(string id, HttpPostedFileBase file_image)
        {
            //get lang
            var detail = occSysDB.AboutUs.FirstOrDefault(p => p.AboutID.Equals(id));

            //upload lang image
            string duoiFile = Path.GetExtension(file_image.FileName);
            string fileName = OccConvert.KhongDau(detail.AboutUs_Descriptions.FirstOrDefault().NameCompany.ToLower().Trim()) + duoiFile;
            string fileUrl = Path.Combine(Server.MapPath("/Uploaded/Images/aboutus"), Path.GetFileName(fileName));

            //save lang image
            file_image.SaveAs(fileUrl);
            //save lang image
            detail.Image = fileName;
            //save action
            occSysDB.SaveChanges();
        }

        public void updateImageAchi(IEnumerable<HttpPostedFileBase> file_images)
        {
            //create folder
            var newPathImage = Server.MapPath("/Uploaded/Images/gioithieu/anh-thanh-tich");

            foreach (var img in file_images)
            {
                //set new
                AboutUs_Achievements newImg = new AboutUs_Achievements();
                //add to DB
                occSysDB.AboutUs_Achievements.Add(newImg);
                occSysDB.SaveChanges();

                //upload image
                string duoiAnhs = Path.GetExtension(img.FileName);
                string fileNames = "anh-thanhtich-" + newImg.AchiID + duoiAnhs;
                //create folder
                string fileUrls = Path.Combine(newPathImage, Path.GetFileName(fileNames));

                //save
                img.SaveAs(fileUrls);
                newImg.AchiImage = fileNames;

            }
            //save action
            occSysDB.SaveChanges();
        }
        public void deleteImageAchi(string fileNames)
        {
            foreach (var fullName in fileNames)
            {
                //string tenanh = fullName.Substring(fullName.LastIndexOf("/")).Replace("/", "");

                var imageToDelete = occSysDB.AboutUs_Achievements.FirstOrDefault(p => p.AchiImage == fileNames);

                if (imageToDelete != null)
                {
                    var path = Server.MapPath("/Uploaded/Images/gioithieu/anh-thanh-tich");
                    string fileUrl = Path.Combine(path, Path.GetFileName(imageToDelete.AchiImage));

                    if (System.IO.File.Exists(fileUrl))
                    {
                        System.IO.File.Delete(fileUrl);
                    }
                    occSysDB.AboutUs_Achievements.Remove(imageToDelete);
                    //save action
                    occSysDB.SaveChanges();
                }
            }

        }
        #endregion
        #endregion

        /// <summary>
        ///   About Slider
        /// </summary>
        /// <returns></returns>
        #region About slider
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult about_sliders_manager()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.AboutSliders.OrderBy(p=>p.SortOrder).ToList();
            return View(list);
        }
        [HttpPost]
        public ActionResult about_sliders_manager(IEnumerable<int> itemID, IEnumerable<int> updateID, List<int> SortOrder, string deleteOK, string update)
        {
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var sliderDel = occSysDB.AboutSliders.FirstOrDefault(p => p.ASID.Equals(id));
                    //already
                    if (sliderDel != null)
                    {
                        //delete image
                        var newPath = Server.MapPath("/Uploaded/Images/about-slider/") + sliderDel.ASImage;
                        if (System.IO.File.Exists(newPath))
                        {
                            System.IO.File.Delete(newPath);
                        }
                        //delete slide
                        occSysDB.AboutSliders.Remove(sliderDel);
                        //save action
                        occSysDB.SaveChanges();
                    }
                }
            }

            //update sortorder
            int sortorder = 0;
            if (SortOrder != null && update == "OK")
            {
                foreach (var slideid in updateID)
                {
                    //get slider
                    var slideUpdate = occSysDB.AboutSliders.FirstOrDefault(p => p.ASID.Equals(slideid));
                    if (slideUpdate != null)
                    {
                        //update sort order
                        slideUpdate.SortOrder = SortOrder[sortorder];
                    }
                    //up index
                    sortorder++;
                }
                //save
                occSysDB.SaveChanges();
            }

            return RedirectToAction("about_sliders_manager");
        }
        #endregion
        #region add
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult about_sliders_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliders = occSysDB.AboutSliders.ToList();
            if (sliders.Count > 0)
            {
                ViewBag.SortOrder = sliders.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult about_sliders_add(AboutSlider newDetail, HttpPostedFileBase file_image, string save, int? LinkID, int? Lang_ID)
        {
            if (ModelState.IsValid)
            {
                if (file_image != null)
                {
                    //get slide
                    var sliders = occSysDB.AboutSliders.ToList();

                    //get file name
                    string nameSlide = "";
                    if (sliders.Count > 0)
                    {
                        nameSlide = "about-Slide" + (sliders[sliders.Count - 1].ASID + 1);
                    }
                    else
                    {
                        nameSlide = "about-Slide" + 1;
                    }
                    //new slide
                    AboutSlider newSlide = new AboutSlider();
                    if (newDetail.IsActive != null)
                    {
                        newSlide.IsActive = true;
                    }
                    else
                    {
                        newSlide.IsActive = false;
                    }
                    newSlide.ASTitle = newDetail.ASTitle;
                    newSlide.ASDescription = newDetail.ASDescription;
                    newSlide.ASVideo = newDetail.ASVideo;
                    if (newDetail.SortOrder != null)
                    {
                        newSlide.SortOrder = newDetail.SortOrder;
                    }
                    else
                    {
                        if (sliders.Count > 0)
                        {
                            newSlide.SortOrder = sliders.Count + 1;
                        }
                        else
                        {
                            newSlide.SortOrder = 1;
                        }
                    }
                    //add slide
                    occSysDB.AboutSliders.Add(newSlide);
                    //save
                    occSysDB.SaveChanges();

                    if (file_image != null)
                    {
                        string duoiAnh = Path.GetExtension(file_image.FileName);
                        string fileName = nameSlide + duoiAnh;
                        string fileUrl = Path.Combine(Server.MapPath("/Uploaded/Images/about-slider"), Path.GetFileName(fileName));

                        //create folder
                        var newPathXml = Server.MapPath("/Uploaded/Images/about-slider");
                        if (!Directory.Exists(newPathXml))
                        {
                            Directory.CreateDirectory(newPathXml);
                        }

                        //save file to folder
                        file_image.SaveAs(fileUrl);
                        newSlide.ASImage = fileName;
                    }

                    //save
                    occSysDB.SaveChanges();

                    if (save == "save")
                    {
                        return RedirectToAction("about_sliders_manager");
                    }
                    else
                    {
                        return RedirectToAction("about_sliders_add");
                    }
                }
                else
                {
                    ViewBag.Mess = "Bạn chưa upload ảnh";
                }

            }


            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliderChecks = occSysDB.AboutSliders.ToList();
            if (sliderChecks.Count > 0)
            {
                ViewBag.SortOrder = sliderChecks.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            if (LinkID != null)
            {
                var linkObj = occSysDB.Sliders.FirstOrDefault(p => p.SlideID == LinkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }

            }

            if (Lang_ID != null)
            {
                ViewBag.LangAdd = Lang_ID;
            }
            //if something wrong 
            return View(newDetail);
        }


        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult about_sliders_edit(int id)
        {
            //get detail
            var detail = occSysDB.AboutSliders.FirstOrDefault(p => p.ASID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            //var detail_des = detail.Sliders_Descriptions.ToList();

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult about_sliders_edit(AboutSlider updateDeatail, int editID)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var sliders = occSysDB.AboutSliders.ToList();
                //get update
                var update = occSysDB.AboutSliders.FirstOrDefault(p => p.ASID.Equals(editID));
                update.ASTitle = updateDeatail.ASTitle;
                update.ASDescription = updateDeatail.ASDescription;
                update.ASVideo = updateDeatail.ASVideo;
                if (updateDeatail.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }

                if (updateDeatail.SortOrder != null)
                {
                    update.SortOrder = updateDeatail.SortOrder;
                }
                else
                {
                    if (sliders.Count > 0)
                    {
                        update.SortOrder = sliders.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }

                //save
                occSysDB.SaveChanges();

                return RedirectToAction("about_sliders_manager");
            }

            //get detail
            var detail = occSysDB.AboutSliders.FirstOrDefault(p => p.ASID.Equals(editID));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }

        public void updateAboutSlider(int id, HttpPostedFileBase file_image)
        {
            //get slide
            var sliders = occSysDB.AboutSliders.FirstOrDefault(p => p.ASID.Equals(id));

            //get file name
            string nameSlide = "about-Slide" + sliders.ASID;

            string duoiAnh = Path.GetExtension(file_image.FileName);
            string fileName = nameSlide + duoiAnh;
            string fileUrl = Path.Combine(Server.MapPath("/Uploaded/Images/about-slider"), Path.GetFileName(fileName));

            //save file to folder
            file_image.SaveAs(fileUrl);
            sliders.ASImage = fileName;
            //save action
            occSysDB.SaveChanges();
        }

        #endregion
        #endregion

        /// <summary>
        ///  Slogan
        /// </summary>
        /// <returns></returns>
        #region Định hướng
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult slogans_mamager()
        {
            return View();
        }
        #endregion
        #region add
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult slogans_add()
        {
            return View();
        }
        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult slogans_edit()
        {
            return View();
        }
        #endregion
        #endregion

    }
}
