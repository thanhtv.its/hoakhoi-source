﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.IO;
using MvcWebsiteOcc.Areas.administrators.Models;

namespace MvcWebsiteOcc.Areas.administrators.Controllers
{
    public class car_managerController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();

        /// <summary>
        ///   CarService 
        /// </summary>
        /// <returns></returns>
        /// 
        #region CarService
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult car_services_manager()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.CarServices.ToList();
            return View(list);
        }
        [HttpPost]
        public ActionResult car_services_manager(IEnumerable<int> itemID, IEnumerable<int> updateID, List<int> SortOrder, string deleteOK, string update)
        {
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var del = occSysDB.CarServices.FirstOrDefault(p => p.ServID.Equals(id));
                    //already
                    if (del != null)
                    {
                        var posDel = occSysDB.CarServicesImages.Where(p => p.ServID == del.ServID).ToList();
                        if (posDel.Count > 0)
                        {
                            foreach (var pDel in posDel)
                            {
                                occSysDB.CarServicesImages.Remove(pDel);
                            }
                        }

                        //delete slide
                        occSysDB.CarServices.Remove(del);
                        //save action
                        occSysDB.SaveChanges();
                    }
                }
            }

            //update sortorder
            int sortorder = 0;
            if (SortOrder != null && update == "OK")
            {
                foreach (var slideid in updateID)
                {
                    //get slider
                    var slideUpdate = occSysDB.CarServices.FirstOrDefault(p => p.ServID.Equals(slideid));
                    if (slideUpdate != null)
                    {
                        //update sort order
                        slideUpdate.SortOrder = SortOrder[sortorder];
                    }
                    //up index
                    sortorder++;
                }
                //save
                occSysDB.SaveChanges();
            }

            return RedirectToAction("car_services_manager");
        }
        #endregion
        #region add
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult car_services_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            if (langID != null)
            {
                ViewBag.LangAdd = langID;
            }
            //get order slide
            var sliders = occSysDB.CarServices.Where(p=>p.LinkID == 0).ToList();
            if (sliders.Count > 0)
            {
                ViewBag.SortOrder = sliders.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }
            //get linkID in lang default
            if (linkID != null)
            {
                var linkObj = occSysDB.CarServices.FirstOrDefault(p => p.ServID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }
            }

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult car_services_add(CarService newDetail, string save, IEnumerable<HttpPostedFileBase> file_images, string LinkID)
        {
            if (ModelState.IsValid)
            {
                if (file_images != null)
                {
                    //get slide
                    var alls = occSysDB.CarServices.ToList();
                    //new slide
                    CarService newHot = new CarService();

                    newHot.ServName = newDetail.ServName;
                    newHot.ServDescription = newDetail.ServDescription;
                    newHot.ServIcon = newDetail.ServIcon;
                    //set lang
                    newHot.Lang_ID = newDetail.Lang_ID;

                    //set link ID
                    if (LinkID != null)
                    {
                        newHot.LinkID = int.Parse(LinkID);
                    }
                    else
                    {
                        newHot.LinkID = 0;
                    }
                    //set active
                    if (newDetail.IsActive != null)
                    {
                        newHot.IsActive = true;
                    }
                    else
                    {
                        newHot.IsActive = false;
                    }
                    //set order
                    if (newDetail.SortOrder != null)
                    {
                        newHot.SortOrder = newDetail.SortOrder;
                    }
                    else
                    {
                        if (alls.Count > 0)
                        {
                            newHot.SortOrder = alls.Count + 1;
                        }
                        else
                        {
                            newHot.SortOrder = 1;
                        }
                    }

                    //add slide
                    occSysDB.CarServices.Add(newHot);
                    //save first
                    occSysDB.SaveChanges();

                    //create folder
                    //folder
                    string folder = "anh-dich-vu-xe-" + newHot.ServID + "-folder";

                    var newPathImage = Server.MapPath("/Uploaded/Images/dichvuxe/" + folder);
                    if (!Directory.Exists(newPathImage))
                    {
                        Directory.CreateDirectory(newPathImage);
                    }
                    //upload image in album
                    if (file_images != null)
                    {
                        foreach (var img in file_images)
                        {
                            //set new
                            CarServicesImage newImg = new CarServicesImage();
                            newImg.Folder = folder;
                            newImg.ServID = newHot.ServID;

                            //add to DB
                            occSysDB.CarServicesImages.Add(newImg);
                            occSysDB.SaveChanges();

                            //upload image
                            string duoiAnhs = Path.GetExtension(img.FileName);
                            string fileNames = OccConvert.KhongDau(newHot.ServName.ToLower()) + newHot.ServID + "-image-" + newImg.CImgID + duoiAnhs;
                            //create folder
                            string fileUrls = Path.Combine(newPathImage, Path.GetFileName(fileNames));

                            //save
                            img.SaveAs(fileUrls);
                            newImg.ImageName = fileNames;
                            occSysDB.SaveChanges();
                        }
                        occSysDB.SaveChanges();
                    }

                    occSysDB.SaveChanges();
                    if (save == "save")
                    {
                        return RedirectToAction("car_services_manager");
                    }
                    else
                    {
                        return RedirectToAction("car_services_add");
                    }
                }
                else
                {
                    ViewBag.Mess = "Bạn chưa upload ảnh slide";
                }
            }
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliderChecks = occSysDB.CarServices.ToList();
            if (sliderChecks.Count > 0)
            {
                ViewBag.SortOrder = sliderChecks.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //if something wrong 
            return View(newDetail);
        }


        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult car_services_edit(int id)
        {
            //get detail
            var detail = occSysDB.CarServices.FirstOrDefault(p => p.ServID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.CarServices.Where(p => p.LinkID == detail.ServID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.CarServices.FirstOrDefault(p => p.ServID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult car_services_edit(CarService updateDeatail, int editID, IEnumerable<HttpPostedFileBase> file_images)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var alls = occSysDB.CarServices.ToList();
                //get update
                var update = occSysDB.CarServices.FirstOrDefault(p => p.ServID.Equals(editID));

                update.ServName = updateDeatail.ServName;
                update.ServDescription = updateDeatail.ServDescription;
                update.ServIcon = updateDeatail.ServIcon;

                //set active
                if (updateDeatail.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }
                //set order
                if (updateDeatail.SortOrder != null)
                {
                    update.SortOrder = updateDeatail.SortOrder;
                }
                else
                {
                    if (alls.Count > 0)
                    {
                        update.SortOrder = alls.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }

                //create folder
                //folder
                string folder = "anh-dich-vu-xe-" + update.ServID + "-folder";

                var newPathImage = Server.MapPath("/Uploaded/Images/dichvuxe/" + folder);
                if (!Directory.Exists(newPathImage))
                {
                    Directory.CreateDirectory(newPathImage);
                }
                //upload image in album
                if (file_images != null && file_images.FirstOrDefault() != null)
                {
                    foreach (var img in file_images)
                    {
                        //set new
                        CarServicesImage newImg = new CarServicesImage();
                        newImg.Folder = folder;
                        newImg.ServID = update.ServID;

                        //add to DB
                        occSysDB.CarServicesImages.Add(newImg);
                        occSysDB.SaveChanges();

                        //upload image
                        string duoiAnhs = Path.GetExtension(img.FileName);
                        string fileNames = OccConvert.KhongDau(update.ServName.ToLower()) + update.ServID + "-image-" + newImg.CImgID + duoiAnhs;
                        //create folder
                        string fileUrls = Path.Combine(newPathImage, Path.GetFileName(fileNames));

                        //save
                        img.SaveAs(fileUrls);
                        newImg.ImageName = fileNames;
                        occSysDB.SaveChanges();
                    }
                    occSysDB.SaveChanges();
                }

                //save
                occSysDB.SaveChanges();

                return RedirectToAction("car_services_manager");
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }
        //update image album
        public void updateImageInService(int id, HttpPostedFileBase file_images)
        {
            //get slide
            var detail = occSysDB.CarServices.FirstOrDefault(p => p.ServID.Equals(id));

            //folder
            string folder = "anh-dich-vu-xe-" + detail.ServID + "-folder";

            var newPathImage = Server.MapPath("/Uploaded/Images/dichvuxe/" + folder);
            if (!Directory.Exists(newPathImage))
            {
                Directory.CreateDirectory(newPathImage);
            }

            //set new
            CarServicesImage newImg = new CarServicesImage();
            newImg.Folder = folder;
            newImg.ServID = detail.ServID;

            //add to DB
            occSysDB.CarServicesImages.Add(newImg);
            occSysDB.SaveChanges();

            //upload image
            string duoiAnhs = Path.GetExtension(file_images.FileName);
            string fileNames = OccConvert.KhongDau(detail.ServName.ToLower()) + detail.ServID + "-image-" + newImg.CImgID + duoiAnhs;
            //create folder
            string fileUrls = Path.Combine(newPathImage, Path.GetFileName(fileNames));

            //save
            file_images.SaveAs(fileUrls);
            newImg.ImageName = fileNames;

            //save action
            occSysDB.SaveChanges();
        }
        //delete image album
        public void deleteImageInServ(int id, string[] fileNames)
        {
            foreach (var fullName in fileNames)
            {
                string tenanh = fullName.Substring(fullName.LastIndexOf("/")).Replace("/", "");

                var imageToDelete = occSysDB.CarServicesImages.FirstOrDefault(p => p.ServID == id && p.ImageName == tenanh);

                if (imageToDelete != null)
                {
                    var path = Server.MapPath("/Uploaded/Images/dichvuxe/" + imageToDelete.Folder);
                    string fileUrl = Path.Combine(path, Path.GetFileName(imageToDelete.ImageName));

                    if (System.IO.File.Exists(fileUrl))
                    {
                        System.IO.File.Delete(fileUrl);
                    }
                    occSysDB.CarServicesImages.Remove(imageToDelete);
                    //save action
                    occSysDB.SaveChanges();
                }
            }

        }
        #endregion
        #endregion


        /// <summary>
        ///   CarType 
        /// </summary>
        /// <returns></returns>
        /// 
        #region CarType
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult car_types_manager()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.CarTypes.ToList();
            return View(list);
        }
        [HttpPost]
        public ActionResult car_types_manager(IEnumerable<int> itemID, IEnumerable<int> updateID, List<int> SortOrder, string deleteOK, string update)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.CarTypes.ToList();
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var del = occSysDB.CarTypes.FirstOrDefault(p => p.TypeID.Equals(id));
                    //already
                    if (del != null)
                    {
                        var cars = del.CarModels.ToList();
                        if (cars.Count  == 0)
                        {
                            //delete slide
                            occSysDB.CarTypes.Remove(del);
                            //save action
                            occSysDB.SaveChanges(); 
                        }
                        else
                        {
                            ViewBag.Error = "Mục này đang được sử dụng !";
                            return View(list);
                        }
                        
                    }
                }
            }

            //update sortorder
            int sortorder = 0;
            if (SortOrder != null && update == "OK")
            {
                foreach (var slideid in updateID)
                {
                    //get slider
                    var slideUpdate = occSysDB.CarTypes.FirstOrDefault(p => p.TypeID.Equals(slideid));
                    if (slideUpdate != null)
                    {
                        //update sort order
                        slideUpdate.SortOrder = SortOrder[sortorder];
                    }
                    //up index
                    sortorder++;
                }
                //save
                occSysDB.SaveChanges();
            }

            return RedirectToAction("car_types_manager");
        }
        #endregion
        #region add
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult car_types_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            if (langID != null)
            {
                ViewBag.LangAdd = langID;
            }
            //get order slide
            var sliders = occSysDB.CarTypes.Where(p=>p.LinkID == 0).ToList();
            if (sliders.Count > 0)
            {
                ViewBag.SortOrder = sliders.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }
            //get linkID in lang default
            if (linkID != null)
            {
                var linkObj = occSysDB.CarTypes.FirstOrDefault(p => p.TypeID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }
            }

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult car_types_add(CarType newDetail, string save, string LinkID)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var alls = occSysDB.CarTypes.ToList();
                //new slide
                CarType newHot = new CarType();

                newHot.TypeName = newDetail.TypeName;
                //set lang
                newHot.Lang_ID = newDetail.Lang_ID;

                //set link ID
                if (LinkID != null)
                {
                    newHot.LinkID = int.Parse(LinkID);
                }
                else
                {
                    newHot.LinkID = 0;
                }
                //set active
                if (newDetail.IsActive != null)
                {
                    newHot.IsActive = true;
                }
                else
                {
                    newHot.IsActive = false;
                }
                //set order
                if (newDetail.SortOrder != null)
                {
                    newHot.SortOrder = newDetail.SortOrder;
                }
                else
                {
                    if (alls.Count > 0)
                    {
                        newHot.SortOrder = alls.Count + 1;
                    }
                    else
                    {
                        newHot.SortOrder = 1;
                    }
                }

                //add slide
                occSysDB.CarTypes.Add(newHot);
                //save first
                occSysDB.SaveChanges();

                if (save == "save")
                {
                    return RedirectToAction("car_types_manager");
                }
                else
                {
                    return RedirectToAction("car_types_add");
                }

            }
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliderChecks = occSysDB.CarTypes.ToList();
            if (sliderChecks.Count > 0)
            {
                ViewBag.SortOrder = sliderChecks.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //if something wrong 
            return View(newDetail);
        }


        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult car_types_edit(int id)
        {
            //get detail
            var detail = occSysDB.CarTypes.FirstOrDefault(p => p.TypeID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.CarTypes.Where(p => p.LinkID == detail.TypeID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.CarTypes.FirstOrDefault(p => p.TypeID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult car_types_edit(CarType updateDeatail, int editID)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var alls = occSysDB.CarTypes.ToList();
                //get update
                var update = occSysDB.CarTypes.FirstOrDefault(p => p.TypeID.Equals(editID));

                update.TypeName = updateDeatail.TypeName;
                //set active
                if (updateDeatail.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }
                //set order
                if (updateDeatail.SortOrder != null)
                {
                    update.SortOrder = updateDeatail.SortOrder;
                }
                else
                {
                    if (alls.Count > 0)
                    {
                        update.SortOrder = alls.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }

                //save
                occSysDB.SaveChanges();

                return RedirectToAction("car_types_manager");
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }
        #endregion
        #endregion


        /// <summary>
        ///   CarModel 
        /// </summary>
        /// <returns></returns>
        /// 
        #region CarModel
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult car_models_manager()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.CarModels.OrderByDescending(p=>p.ModID).ToList();
            return View(list);
        }
        [HttpPost]
        public ActionResult car_models_manager(IEnumerable<int> itemID, IEnumerable<int> updateID, List<int> SortOrder, string deleteOK, string update)
        {
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var del = occSysDB.CarModels.FirstOrDefault(p => p.ModID.Equals(id));
                    //already
                    if (del != null)
                    {
                        var posDel = occSysDB.CarModelsImages.Where(p => p.ModID == del.ModID).ToList();
                        if (posDel.Count > 0)
                        {
                            foreach (var pDel in posDel)
                            {
                                occSysDB.CarModelsImages.Remove(pDel);
                            }
                        }

                        var typeViews = del.CarViewTypes.ToList();
                        if (typeViews.Count > 0)
                        {
                            foreach (var tpV in typeViews)
                            {
                                occSysDB.CarViewTypes.Remove(tpV);
                            }
                        }

                        var staffs = del.Staffs.ToList();
                        if (staffs.Count > 0)
                        {
                            foreach (var staff in staffs)
                            {
                                staff.ModID = null;
                            }
                        }

                        //delete slide
                        occSysDB.CarModels.Remove(del);
                        //save action
                        occSysDB.SaveChanges();
                    }
                }
            }

            //update sortorder
            //int sortorder = 0;
            //if (SortOrder != null && update == "OK")
            //{
            //    foreach (var slideid in updateID)
            //    {
            //        //get slider
            //        var slideUpdate = occSysDB.CarServices.FirstOrDefault(p => p.ServID.Equals(slideid));
            //        if (slideUpdate != null)
            //        {
            //            //update sort order
            //            slideUpdate.SortOrder = SortOrder[sortorder];
            //        }
            //        //up index
            //        sortorder++;
            //    }
            //    //save
            //    occSysDB.SaveChanges();
            //}

            return RedirectToAction("car_models_manager");
        }
        #endregion
        #region add
        public ActionResult _cars_type_add(int? langID, int? linkID, int? linkTypeID)
        {
            var list = occSysDB.CarTypes.Where(p => p.Language.IsDefault == true).ToList();
            if (langID != null)
            {
                list = occSysDB.CarTypes.Where(p => p.Lang_ID == langID).ToList();
            }

            ViewBag.LinkID = linkID;
            ViewBag.LinkTypeID = linkTypeID;

            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult car_models_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            if (langID != null)
            {
                ViewBag.LangAdd = langID;
            }
            //get order slide
            var sliders = occSysDB.CarModels.ToList();
            if (sliders.Count > 0)
            {
                ViewBag.SortOrder = sliders.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }
            //get linkID in lang default
            if (linkID != null)
            {
                var linkObj = occSysDB.CarModels.FirstOrDefault(p => p.ModID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }
            }

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult car_models_add(CarModel newDetail, string save, IEnumerable<HttpPostedFileBase> file_images, string LinkID, HttpPostedFileBase file_image, string price, IEnumerable<int> CarView)
        {
            if (ModelState.IsValid)
            {
                bool formIsvalid = false;
                if (file_image != null && LinkID != null)
                {
                    formIsvalid = true;
                }
                if (file_image == null && LinkID != null)
                {
                    formIsvalid = true;
                }
                if (file_image != null && LinkID == null)
                {
                    formIsvalid = true;
                }


                if (formIsvalid == true)
                {
                    //get slide
                    var alls = occSysDB.CarModels.ToList();
                    //new slide
                    CarModel newHot = new CarModel();

                    newHot.ModName = newDetail.ModName;
                    newHot.ModDescription = newDetail.ModDescription;
                    newHot.ModBrand = newDetail.ModBrand;

                    newHot.ModColor = newDetail.ModColor;
                    newHot.ModYear = newDetail.ModYear;

                    if (price.Length == 0)
                        newHot.ModPrice = 0;
                    else
                        newHot.ModPrice = decimal.Parse(price.Replace(",", ""));

                    newHot.ModRate = newDetail.ModRate;
                    //set lang
                    newHot.Lang_ID = newDetail.Lang_ID;
                    newHot.TypeID = newDetail.TypeID;

                    //set link ID
                    if (LinkID != null)
                    {
                        newHot.LinkID = int.Parse(LinkID);
                    }
                    else
                    {
                        newHot.LinkID = 0;
                    }
                    //set active
                    if (newDetail.IsActive != null)
                    {
                        newHot.IsActive = true;
                    }
                    else
                    {
                        newHot.IsActive = false;
                    }

                    //add slide
                    occSysDB.CarModels.Add(newHot);
                    //save first
                    occSysDB.SaveChanges();

                    //create folder
                    //folder
                    string folder = OccConvert.KhongDau(newHot.ModName.ToLower()) + newHot.ModID + "-folder";
                    var newPathImage = Server.MapPath("/Uploaded/Images/dongxe/" + folder);
                    if (!Directory.Exists(newPathImage))
                    {
                        Directory.CreateDirectory(newPathImage);
                    }

                    if (file_image != null)
                    {
                        //upload image
                        string duoiAnh = Path.GetExtension(file_image.FileName);
                        string fileName = OccConvert.KhongDau(newHot.ModName.ToLower()) + newHot.ModID + "-dai-dien" + duoiAnh;
                        string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));
                        //save file to folder
                        file_image.SaveAs(fileUrl);
                        //add to album
                        newHot.Folder = folder;
                        newHot.ModImage = fileName;
                    }

                    if (LinkID != null && file_image == null)
                    {
                        var linkObj = occSysDB.CarModels.FirstOrDefault(p => p.ModID == newHot.LinkID);
                        if (linkObj != null)
                        {
                            newHot.Folder = linkObj.Folder;
                            newHot.ModImage = linkObj.ModImage;

                            var imageInLinkObj = linkObj.CarModelsImages.ToList();
                            foreach (var img in imageInLinkObj)
                            {
                                //set new
                                CarModelsImage newImg = new CarModelsImage();
                                newImg.Folder = newHot.Folder;
                                newImg.ModID = newHot.ModID;

                                newImg.ImageName = img.ImageName;
                                //add to DB
                                occSysDB.CarModelsImages.Add(newImg);
                                occSysDB.SaveChanges();
                            }
                        }
                    }


                    //upload image in album
                    if (file_images != null && file_images.FirstOrDefault() != null)
                    {
                        foreach (var img in file_images)
                        {
                            //set new
                            CarModelsImage newImg = new CarModelsImage();
                            newImg.Folder = folder;
                            newImg.ModID = newHot.ModID;

                            //add to DB
                            occSysDB.CarModelsImages.Add(newImg);
                            occSysDB.SaveChanges();

                            //upload image
                            string duoiAnhs = Path.GetExtension(img.FileName);
                            string fileNames = OccConvert.KhongDau(newHot.ModName.ToLower()) + newHot.ModID + "-image-" + newImg.ModImgID + duoiAnhs;
                            //create folder
                            string fileUrls = Path.Combine(newPathImage, Path.GetFileName(fileNames));

                            //save
                            img.SaveAs(fileUrls);
                            newImg.ImageName = fileNames;
                        }
                        occSysDB.SaveChanges();
                    }

                    if (CarView != null)
                    {
                        foreach (var vID in CarView)
                        {
                            CarViewType newViewType = new CarViewType();
                            newViewType.ModID = newHot.ModID;
                            newViewType.ViewType = vID;

                            occSysDB.CarViewTypes.Add(newViewType);
                            occSysDB.SaveChanges();
                        }
                    }

                    occSysDB.SaveChanges();
                    if (save == "save")
                    {
                        return RedirectToAction("car_models_manager");
                    }
                    else
                    {
                        return RedirectToAction("car_models_add");
                    }
                }
                else
                {
                    ViewBag.Mess = "Bạn chưa upload ảnh";
                }
            }
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliderChecks = occSysDB.CarModels.ToList();
            if (sliderChecks.Count > 0)
            {
                ViewBag.SortOrder = sliderChecks.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //if something wrong 
            return View(newDetail);
        }


        #endregion
        #region edit
        public ActionResult _cars_type_edit(int? langID, int? typeID)
        {
            var list = occSysDB.CarTypes.Where(p => p.Lang_ID == langID).ToList();
            ViewBag.TYPEID = typeID;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult car_models_edit(int id)
        {
            //get detail
            var detail = occSysDB.CarModels.FirstOrDefault(p => p.ModID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.CarModels.Where(p => p.LinkID == detail.ModID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.CarModels.FirstOrDefault(p => p.ModID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult car_models_edit(CarModel updateDeatail, int editID, IEnumerable<HttpPostedFileBase> file_images, string price, IEnumerable<int> CarView)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var alls = occSysDB.CarModels.ToList();
                //get update
                var update = occSysDB.CarModels.FirstOrDefault(p => p.ModID.Equals(editID));

                update.ModName = updateDeatail.ModName;
                update.ModDescription = updateDeatail.ModDescription;
                update.ModBrand = updateDeatail.ModBrand;

                update.ModColor = updateDeatail.ModColor;
                update.ModYear = updateDeatail.ModYear;

                if (price.Length == 0)
                    update.ModPrice = 0;
                else
                    update.ModPrice = decimal.Parse(price.Replace(",", ""));

                update.ModRate = updateDeatail.ModRate;
                //set lang
                update.Lang_ID = updateDeatail.Lang_ID;
                update.TypeID = updateDeatail.TypeID;

                //set active
                if (updateDeatail.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }

                //create folder
                //folder
                //create folder
                //folder
                string folder = OccConvert.KhongDau(update.ModName.ToLower()) + update.ModID + "-folder";
                var newPathImage = Server.MapPath("/Uploaded/Images/dongxe/" + folder);
                if (!Directory.Exists(newPathImage))
                {
                    Directory.CreateDirectory(newPathImage);
                }
                //upload image in album
                if (file_images != null && file_images.FirstOrDefault() != null)
                {
                    foreach (var img in file_images)
                    {
                        //set new
                        CarModelsImage newImg = new CarModelsImage();
                        newImg.Folder = folder;
                        newImg.ModID = update.ModID;

                        //add to DB
                        occSysDB.CarModelsImages.Add(newImg);
                        occSysDB.SaveChanges();

                        //upload image
                        string duoiAnhs = Path.GetExtension(img.FileName);
                        string fileNames = OccConvert.KhongDau(update.ModName.ToLower()) + update.ModID + "-image-" + newImg.ModImgID + duoiAnhs;
                        //create folder
                        string fileUrls = Path.Combine(newPathImage, Path.GetFileName(fileNames));

                        //save
                        img.SaveAs(fileUrls);
                        newImg.ImageName = fileNames;
                    }
                    occSysDB.SaveChanges();
                }

                //reset staff view
                var typeViews = update.CarViewTypes.ToList();
                if (typeViews.Count > 0)
                {
                    foreach (var stv in typeViews)
                    {
                        occSysDB.CarViewTypes.Remove(stv);
                    }
                    occSysDB.SaveChanges();
                }

                if (CarView != null)
                {
                    foreach (var vID in CarView)
                    {
                        CarViewType newViewType = new CarViewType();
                        newViewType.ModID = update.ModID;
                        newViewType.ViewType = vID;

                        occSysDB.CarViewTypes.Add(newViewType);
                        occSysDB.SaveChanges();
                    }
                }
                //save
                occSysDB.SaveChanges();

                return RedirectToAction("car_models_manager");
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }
        //update image album
        public void updateImageCarMod(int id, HttpPostedFileBase file_image)
        {
            //get slide
            var detail = occSysDB.CarModels.FirstOrDefault(p => p.ModID.Equals(id));

            //folder
            string folder = OccConvert.KhongDau(detail.ModName.ToLower()) + detail.ModID + "-folder";

            var newPathImage = Server.MapPath("/Uploaded/Images/dongxe/" + folder);
            if (!Directory.Exists(newPathImage))
            {
                Directory.CreateDirectory(newPathImage);
            }
            //upload image
            string duoiAnh = Path.GetExtension(file_image.FileName);
            string fileName = OccConvert.KhongDau(detail.ModName.ToLower()) + detail.ModID + "-dai-dien" + duoiAnh;
            string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));
            //save file to folder
            file_image.SaveAs(fileUrl);
            //add to album
            detail.ModImage = fileName;
            detail.Folder = folder;
            //save action
            occSysDB.SaveChanges();
        }
        //update image album
        public void updateImageInCarMod(int id, HttpPostedFileBase file_images)
        {
            //get slide
            var detail = occSysDB.CarModels.FirstOrDefault(p => p.ModID.Equals(id));

            //folder
            string folder = OccConvert.KhongDau(detail.ModName.ToLower()) + detail.ModID + "-folder";

            var newPathImage = Server.MapPath("/Uploaded/Images/dongxe/" + folder);
            if (!Directory.Exists(newPathImage))
            {
                Directory.CreateDirectory(newPathImage);
            }

            //set new
            CarModelsImage newImg = new CarModelsImage();
            newImg.Folder = folder;
            newImg.ModID = detail.ModID;

            //add to DB
            occSysDB.CarModelsImages.Add(newImg);
            occSysDB.SaveChanges();

            //upload image
            string duoiAnhs = Path.GetExtension(file_images.FileName);
            string fileNames = OccConvert.KhongDau(detail.ModName.ToLower()) + detail.ModID + "-image-" + newImg.ModImgID + duoiAnhs;
            //create folder
            string fileUrls = Path.Combine(newPathImage, Path.GetFileName(fileNames));

            //save
            file_images.SaveAs(fileUrls);
            newImg.ImageName = fileNames;

            //save action
            occSysDB.SaveChanges();
        }
        //delete image album
        public void deleteImageInCarMod(int id, string[] fileNames)
        {
            foreach (var fullName in fileNames)
            {
                string tenanh = fullName.Substring(fullName.LastIndexOf("/")).Replace("/", "");

                var imageToDelete = occSysDB.CarModelsImages.FirstOrDefault(p => p.ModID == id && p.ImageName == tenanh);

                if (imageToDelete != null)
                {
                    var path = Server.MapPath("/Uploaded/Images/dongxe/" + imageToDelete.Folder);
                    string fileUrl = Path.Combine(path, Path.GetFileName(imageToDelete.ImageName));

                    if (System.IO.File.Exists(fileUrl))
                    {
                        System.IO.File.Delete(fileUrl);
                    }
                    occSysDB.CarModelsImages.Remove(imageToDelete);
                    //save action
                    occSysDB.SaveChanges();
                }
            }

        }
        #endregion
        #endregion

    }
}
