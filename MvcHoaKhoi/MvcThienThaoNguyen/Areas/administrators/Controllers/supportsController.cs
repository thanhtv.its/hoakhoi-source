﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.IO;
using MvcWebsiteOcc.Areas.administrators.Models;

namespace MvcWebsiteOcc.Areas.administrators.Controllers
{
    public class supportsController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();
        public ActionResult chat_online()
        {
            return View();
        }
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult customer_message()
        {
            var list = occSysDB.Contacts.OrderByDescending(p => p.CreateDate).ToList();
            return View(list);
        }
        [HttpPost]
        public ActionResult customer_message(IEnumerable<int> itemID, string deleteOK)
        {
            if (deleteOK == "OK" && itemID != null)
            {
                foreach (var id in itemID)
                {
                    var del = occSysDB.Contacts.FirstOrDefault(p => p.conID == id);
                    if (del != null)
                    {
                        occSysDB.Contacts.Remove(del);

                    }
                }
                occSysDB.SaveChanges();
                return RedirectToAction("customer_message");
            }
            //if something wrong
            return RedirectToAction("customer_message");
        }

        public JsonResult _get_cus_message(int id)
        {
            var xeslide = occSysDB.Contacts.FirstOrDefault(p => p.conID == id);
            // Convert the Product entities to ProductViewModel instances
            //List<Contact> result = new List<Contact>();

            //result.Add(new Contact
            //{
            //    CusName = xeslide.CusName,
            //    CusPhone = xeslide.CusPhone,
            //    CusEmail = xeslide.CusEmail,
            //    CusMessage = xeslide.CusMessage
            //});
            List<string> result = new List<string>();
            result.Add(xeslide.CusName);
            result.Add(xeslide.CusMessage);
            result.Add(xeslide.CusEmail);
            result.Add(xeslide.CusPhone);

            return Json(xeslide, JsonRequestBehavior.AllowGet);

        }

    }
}
