﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.IO;
using MvcWebsiteOcc.Areas.administrators.Models;

namespace MvcWebsiteOcc.Areas.administrators.Controllers
{
    public class events_managerController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();

        /// <summary>
        ///   event Category
        /// </summary>
        /// <returns></returns>
        /// 
        #region event Category
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult eventscate_manager()
        {   
            //get list
            var list = occSysDB.EventsCategories.OrderBy(p => p.SortOrder).ToList();

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(list);
        }
        [HttpPost]
        public ActionResult eventscate_manager(IEnumerable<int> itemID, string deleteOK, string update, IEnumerable<int> updateID, List<int> SortOrder)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get list
            var list = occSysDB.EventsCategories.ToList();
            #region delete
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get cate
                    var cate = occSysDB.EventsCategories.FirstOrDefault(p => p.EventCateID.Equals(id));

                    //if cate exits
                    if (cate != null)
                    {
                        //get details in cate
                        //var newsInCate = cate.News.ToList();

                        ////get child cate
                        //var childCate = occSysDB.NewsCategories.Where(p => p.ParentID == cate.NewCatID).ToList();

                        ////if details in cate not exits
                        //if (newsInCate.Count == 0)
                        //{
                        //    //check child cate
                        //    if (childCate.Count > 0)
                        //    {
                        //        foreach (var child in childCate)
                        //        {
                        //            //get details of child cate 
                        //            var newsInChilds = child.News.ToList();
                        //            //if details not exits
                        //            if (newsInChilds.Count == 0)
                        //            {
                        //                //delete childe cate
                        //                occSysDB.NewsCategories.Remove(child);
                        //            }
                        //            else
                        //            {
                        //                ViewBag.Error = "Chuyên mục đang được sử dụng !";
                        //                return View(list);
                        //            }
                        //        }
                        //    }
                            //delete cate
                        occSysDB.EventsCategories.Remove(cate);
                        //}
                        //else
                        //{
                        //    ViewBag.Error = "Chuyên mục đang được sử dụng !";
                        //    return View(list);
                        //}
                    }

                }
                occSysDB.SaveChanges();
                return RedirectToAction("eventscate_manager");
            }
            #endregion

            #region update order
            if (updateID != null && update == "OK")
            {
                int order = 0;
                foreach (var upID in updateID)
                {
                    var updateCate = occSysDB.EventsCategories.FirstOrDefault(p => p.EventCateID.Equals(upID));
                    if (updateCate != null)
                    {
                        updateCate.SortOrder = SortOrder[order];
                    }
                    order++;
                }
                //save update
                occSysDB.SaveChanges();
                return RedirectToAction("eventscate_manager");
            }
            #endregion

            //if something  wrong
            ViewBag.Error = "Chuyên mục đang được sử dụng !";
            return View(list);
        }
        #endregion
        #region add

        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult eventscate_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            if (linkID != null)
            {
                var linkObj = occSysDB.EventsCategories.FirstOrDefault(p => p.EventCateID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }

            }
            //get all cate
            var allCate = occSysDB.EventsCategories.Where(p => p.LinkID == 0).ToList();
            //set order
            if (allCate.Count > 0)
            {
                ViewBag.SortOrder = allCate.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            ViewBag.LangAdd = langID;
            return View();
        }
        [HttpPost]
        public ActionResult eventscate_add(EventsCategory newCate_Add, string save, string LinkID)
        {
            if (ModelState.IsValid)
            {
                //get all cate
                var allCate = occSysDB.EventsCategories.ToList();


                if (newCate_Add.SortOrder != 0)
                {
                    newCate_Add.SortOrder = newCate_Add.SortOrder;
                }
                else
                {
                    //set order
                    if (allCate.Count > 0)
                    {
                        newCate_Add.SortOrder = allCate.Count + 1;
                    }
                    else
                    {
                        newCate_Add.SortOrder = 1;
                    }
                }

                if (newCate_Add.IsActive != null)
                {
                    newCate_Add.IsActive = true;
                }
                else
                {

                    newCate_Add.IsActive = false;

                }


    
                //link obj
                if (LinkID != null)
                {
                    newCate_Add.LinkID = int.Parse(LinkID);
                }
                else
                {
                    newCate_Add.LinkID = 0;
                }

                //save cate
                occSysDB.EventsCategories.Add(newCate_Add);
                occSysDB.SaveChanges();

                //save action
                if (save == "save")
                {
                    return RedirectToAction("eventscate_manager");
                }
                else
                {
                    return RedirectToAction("eventscate_add");
                }
            }
            else
            {
                ViewBag.Error = "Bạn chưa nhập đầy đủ thông tin !";
                //if something wrongs
                //get lang
                var lang = occSysDB.Languages.ToList();
                ViewBag.LangList = lang;

                //get all cate
                var allCate = occSysDB.EventsCategories.ToList();
                //set order
                if (allCate.Count > 0)
                {
                    ViewBag.SortOrder = allCate.Count + 1;
                }
                else
                {
                    ViewBag.SortOrder = 1;
                }

                return View(newCate_Add);
            }

        }
        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult eventscate_edit(int id)
        {
            //get detail
            var detail = occSysDB.EventsCategories.FirstOrDefault(p => p.EventCateID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.EventsCategories.Where(p => p.LinkID == detail.EventCateID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.EventsCategories.FirstOrDefault(p => p.EventCateID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }
        [HttpPost]
        public ActionResult eventscate_edit(int editID, EventsCategory updateNewsCate)
        {
            if (ModelState.IsValid)
            {
                var allCate = occSysDB.NewsCategories.ToList();

                //get detail
                var update = occSysDB.EventsCategories.FirstOrDefault(p => p.EventCateID.Equals(editID));
                update.EventCateName = updateNewsCate.EventCateName;
                //set parent cate


                update.Lang_ID = updateNewsCate.Lang_ID;

                if (updateNewsCate.SortOrder != 0)
                {
                    update.SortOrder = updateNewsCate.SortOrder;
                }
                else
                {
                    //set order
                    if (allCate.Count > 0)
                    {
                        update.SortOrder = allCate.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }
                if (updateNewsCate.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }
                //seo
                update.MetaTitle = updateNewsCate.MetaTitle;
                update.MetaKeywords = updateNewsCate.MetaKeywords;
                update.MetaDescription = updateNewsCate.MetaDescription;

                //save action
                occSysDB.SaveChanges();

                return RedirectToAction("eventscate_manager");

            }
            //get detail
            var detail = occSysDB.EventsCategories.FirstOrDefault(p => p.EventCateID.Equals(editID));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(updateNewsCate);
        }

        #endregion
        #endregion

        /// <summary>
        ///   Events 
        /// </summary>
        /// <returns></returns>
        /// 
        #region Events
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult events_views()
        {
            //get list view 
            var list = occSysDB.Events.OrderByDescending(p => p.TimeStart).ToList();

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(list);
        }
        [HttpPost]
        public ActionResult events_views(IEnumerable<int> itemID, string deleteOK)
        {
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get dtail
                    var newsDel = occSysDB.Events.FirstOrDefault(p => p.EventID == id);
                    //delete detail
                    occSysDB.Events.Remove(newsDel);
                    occSysDB.SaveChanges();
                }
            }
            return RedirectToAction("events_views");

        }
        #endregion
        #region add
        //Dropdown List
        public ActionResult _events_cate_add(int? langID, int? linkCateID)
        {
            var list = occSysDB.EventsCategories.Where(p => p.Language.IsDefault == true).ToList();
            if (langID != null)
            {
                list = occSysDB.EventsCategories.Where(p => p.Lang_ID == langID).ToList();
            }
            ViewBag.LinkCateID = linkCateID;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult events_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            if (langID != null)
            {
                ViewBag.LangAdd = langID;
            }

            //get linkID in lang default
            if (linkID != null)
            {
                var linkObj = occSysDB.Events.FirstOrDefault(p => p.EventID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }
            }

            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult events_add(Event newDetail, HttpPostedFileBase file_image, HttpPostedFileBase file_image_logo, HttpPostedFileBase file_download, string save, string date_start_end, string LinkID)
        {

            if (ModelState.IsValid)
            {
                bool formIsvalid = false;
                if (file_image != null && LinkID != null)
                {
                    formIsvalid = true;
                }
                if (file_image == null && LinkID != null)
                {
                    formIsvalid = true;
                }
                if (file_image != null && LinkID == null)
                {
                    formIsvalid = true;
                }

                if (formIsvalid == true)
                {
                    //get new detail
                    Event newNews = new Event();
                    string[] date = date_start_end.Split('-');
                    string startDate = date[0].Trim();
                    string endDate = date[1].Trim();

                    newNews.TimeStart = DateTime.ParseExact(startDate, "dd/MM/yyyy", null);
                    newNews.TimeEnd = DateTime.ParseExact(endDate, "dd/MM/yyyy", null);

                    newNews.EventName = newDetail.EventName;
                    newNews.Address = newDetail.Address;
                    newNews.ShortDescription = newDetail.ShortDescription;
                    newNews.Lang_ID = newDetail.Lang_ID;
                    newNews.Subdomain = newDetail.Subdomain;
                    newNews.BackGround = newDetail.BackGround;
                    if (LinkID != null)
                    {
                        newNews.LinkID = int.Parse(LinkID);
                    }
                    else
                    {
                        newNews.LinkID = 0;
                    }

                    newNews.EventCateID = newDetail.EventCateID;
                    if (newDetail.IsActive != null)
                    {
                        newNews.IsActive = true;
                    }
                    else
                    {
                        newNews.IsActive = false;
                    }



                    //add news to DB
                    occSysDB.Events.Add(newNews);
                    //save first
                    occSysDB.SaveChanges();
                    //create folder
                    var newPathImage = Server.MapPath("/Uploaded/Images/sukien/" + OccConvert.LayUrl(newNews.EventName, newNews.EventID) + "-folder");
                    if (!Directory.Exists(newPathImage))
                    {
                        Directory.CreateDirectory(newPathImage);
                    }

                    if (file_image != null)
                    {
                        //upload image
                        string duoiAnh = Path.GetExtension(file_image.FileName);
                        string fileName = OccConvert.LayUrl(newNews.EventName, newNews.EventID) + "-poster" + duoiAnh;
                        newNews.Folder = OccConvert.LayUrl(newNews.EventName, newNews.EventID) + "-folder";
                        string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));

                        //save file to folder
                        file_image.SaveAs(fileUrl);

                        //get image
                        newNews.EventPoster = fileName;
                    }
                    else
                    {
                        newNews.Folder = newDetail.Folder;
                        newNews.EventPoster = newDetail.EventPoster;
                    }

                    if (file_image_logo != null)
                    {
                        //upload image
                        string duoiAnh = Path.GetExtension(file_image_logo.FileName);
                        string fileName = OccConvert.LayUrl(newNews.EventName, newNews.EventID) + "-logo" + duoiAnh;
                        string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));

                        //save file to folder
                        file_image_logo.SaveAs(fileUrl);

                        //get image
                        newNews.EventLogo = fileName;
                    }
                    else
                    {
                        newNews.Folder = newDetail.Folder;
                        newNews.EventLogo = newDetail.EventLogo;
                    }

                    //save file download
                    if (file_download != null)
                    {
                        //upload image
                        string duoiFile = Path.GetExtension(file_download.FileName);
                        string fileNameF = OccConvert.LayUrl(newNews.EventName, newNews.EventID) + "-file" + duoiFile;
                        string fileUrlF = Path.Combine(newPathImage, Path.GetFileName(fileNameF));
                        //save file to folder
                        file_download.SaveAs(fileUrlF);

                        //get file
                        newNews.FileDownload = fileNameF;
                    }

                    //save
                    occSysDB.SaveChanges();

                    if (save == "save")
                    {
                        return RedirectToAction("events_views");
                    }
                    else
                    {
                        return RedirectToAction("events_add");
                    }

                }
                else
                {
                    ViewBag.Mess = "Bạn chưa upload ảnh";
                }
            }

            //if something wrong 
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(newDetail);
        }
        #endregion
        #region edit
        //Dropdown List
        public ActionResult _events_cate_edit(int? newCateID, int? langID)
        {
            var list = occSysDB.EventsCategories.Where(p => p.Lang_ID == langID).ToList();
            ViewBag.EventCateID = newCateID;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult events_edit(int id)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get detail
            var detail = occSysDB.Events.FirstOrDefault(p => p.EventID == id);
            ViewBag.Detail = detail;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.Events.Where(p => p.LinkID == detail.EventID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.Events.FirstOrDefault(p => p.EventID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }


            return View(detail);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult events_edit(int editID, Event updateDetail, HttpPostedFileBase file_download,  HttpPostedFileBase file_image_logo, HttpPostedFileBase file_image, string save, string date_start_end)
        {

            if (ModelState.IsValid)
            {
                //get  detail
                var updateNews = occSysDB.Events.FirstOrDefault(p => p.EventID.Equals(editID));

                updateNews.EventName = updateDetail.EventName;
                updateNews.Address = updateDetail.Address;
                updateNews.ShortDescription = updateDetail.ShortDescription;
                updateNews.Lang_ID = updateDetail.Lang_ID;
                updateNews.EventCateID = updateDetail.EventCateID;
                updateNews.Subdomain = updateDetail.Subdomain;
                updateNews.BackGround = updateDetail.BackGround;

                string[] date = date_start_end.Split('-');
                string startDate = date[0].Trim();
                string endDate = date[1].Trim();

                updateNews.TimeStart = DateTime.ParseExact(startDate, "dd/MM/yyyy", null);
                updateNews.TimeEnd = DateTime.ParseExact(endDate, "dd/MM/yyyy", null);

                if (updateDetail.IsActive != null)
                {
                    updateNews.IsActive = true;
                }
                else
                {
                    updateNews.IsActive = false;
                }

                //save file download
                if (file_download != null)
                {
                    updateEventFile(updateNews.EventID, file_download);
                }

                //create folder
                var newPathImage = Server.MapPath("/Uploaded/Images/sukien/" + updateNews.Folder);

                if (file_image != null)
                {
                    //upload image
                    string duoiAnh = Path.GetExtension(file_image.FileName);
                    string fileName = OccConvert.LayUrl(updateNews.EventName, updateNews.EventID) + "-poster" + duoiAnh;
                    string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));

                    //save file to folder
                    file_image.SaveAs(fileUrl);

                    //get image
                    updateNews.EventPoster = fileName;
                }

                if (file_image_logo != null)
                {
                    //upload image
                    string duoiAnh = Path.GetExtension(file_image_logo.FileName);
                    string fileName = OccConvert.LayUrl(updateNews.EventName, updateNews.EventID) + "-logo" + duoiAnh;
                    string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));

                    //save file to folder
                    file_image_logo.SaveAs(fileUrl);

                    //get image
                    updateNews.EventLogo = fileName;
                }
                

                //save file download
                if (file_download != null)
                {
                    //upload image
                    string duoiFile = Path.GetExtension(file_download.FileName);
                    string fileNameF = OccConvert.LayUrl(updateNews.EventName, updateNews.EventID) + "-file" + duoiFile;
                    string fileUrlF = Path.Combine(newPathImage, Path.GetFileName(fileNameF));
                    //save file to folder
                    file_download.SaveAs(fileUrlF);

                    //get file
                    updateNews.FileDownload = fileNameF;
                }

                //save
                occSysDB.SaveChanges();
                return RedirectToAction("events_views");
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(updateDetail);
        }

        public void updateEventPoster(int id, HttpPostedFileBase file_image)
        {
            //get slide
            var detail = occSysDB.Events.FirstOrDefault(p => p.EventID.Equals(id));

            //upload image
            string duoiAnh = Path.GetExtension(file_image.FileName);
            string fileName = OccConvert.LayUrl(detail.EventName, detail.EventID) + "-poster" + duoiAnh;
            //create folder
            var newPathImage = Server.MapPath("/Uploaded/Images/sukien/" + detail.Folder);

            string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));
            //save file to folder
            file_image.SaveAs(fileUrl);

            //get image
            detail.EventPoster = fileName;
            //save action
            occSysDB.SaveChanges();
        }
        public void updateEventLogo(int id, HttpPostedFileBase file_image_logo)
        {
            //get slide
            var detail = occSysDB.Events.FirstOrDefault(p => p.EventID.Equals(id));

            //upload image
            string duoiAnh = Path.GetExtension(file_image_logo.FileName);
            string fileName = OccConvert.LayUrl(detail.EventName, detail.EventID) + "-logo" + duoiAnh;
            //create folder
            var newPathImage = Server.MapPath("/Uploaded/Images/sukien/" + detail.Folder);

            string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));
            //save file to folder
            file_image_logo.SaveAs(fileUrl);

            //get image
            detail.EventLogo = fileName;
            //save action
            occSysDB.SaveChanges();
        }
        public void updateEventFile(int id, HttpPostedFileBase file_download)
        {
            //get slide
            var detail = occSysDB.Events.FirstOrDefault(p => p.EventID.Equals(id));

            //upload image
            string duoiFile = Path.GetExtension(file_download.FileName);
            string fileNameF = OccConvert.LayUrl(detail.EventName, detail.EventID) + "-file" + duoiFile;
            var newPathImage = Server.MapPath("/Uploaded/Images/sukien/" + detail.Folder);
            string fileUrlF = Path.Combine(newPathImage, Path.GetFileName(fileNameF));


            //save file to folder
            file_download.SaveAs(fileUrlF);

            //get file
            detail.FileDownload = fileNameF;

            //save action
            occSysDB.SaveChanges();
        }
        #endregion
        #endregion


        /// <summary>
        ///   event detail
        /// </summary>
        /// <returns></returns>
        /// 
        #region event detail
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult eventsdetail_manager()
        {
            //get list
            var list = occSysDB.EventsDetails.OrderBy(p => p.SortOrder).ToList();

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(list);
        }
        [HttpPost]
        public ActionResult eventsdetail_manager(IEnumerable<int> itemID, string deleteOK, string update, IEnumerable<int> updateID, List<int> SortOrder)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get list
            var list = occSysDB.EventsDetails.ToList();
            #region delete
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get cate
                    var cate = occSysDB.EventsDetails.FirstOrDefault(p => p.DetailID.Equals(id));

                    //if cate exits
                    if (cate != null)
                    {
                        //get details in cate
                        //var newsInCate = cate.News.ToList();

                        ////get child cate
                        //var childCate = occSysDB.NewsCategories.Where(p => p.ParentID == cate.NewCatID).ToList();

                        ////if details in cate not exits
                        //if (newsInCate.Count == 0)
                        //{
                        //    //check child cate
                        //    if (childCate.Count > 0)
                        //    {
                        //        foreach (var child in childCate)
                        //        {
                        //            //get details of child cate 
                        //            var newsInChilds = child.News.ToList();
                        //            //if details not exits
                        //            if (newsInChilds.Count == 0)
                        //            {
                        //                //delete childe cate
                        //                occSysDB.NewsCategories.Remove(child);
                        //            }
                        //            else
                        //            {
                        //                ViewBag.Error = "Chuyên mục đang được sử dụng !";
                        //                return View(list);
                        //            }
                        //        }
                        //    }
                        //delete cate
                        occSysDB.EventsDetails.Remove(cate);
                        //}
                        //else
                        //{
                        //    ViewBag.Error = "Chuyên mục đang được sử dụng !";
                        //    return View(list);
                        //}
                    }

                }
                occSysDB.SaveChanges();
                return RedirectToAction("eventsdetail_manager");
            }
            #endregion

            #region update order
            if (updateID != null && update == "OK")
            {
                int order = 0;
                foreach (var upID in updateID)
                {
                    var updateCate = occSysDB.EventsDetails.FirstOrDefault(p => p.DetailID.Equals(upID));
                    if (updateCate != null)
                    {
                        updateCate.SortOrder = SortOrder[order];
                    }
                    order++;
                }
                //save update
                occSysDB.SaveChanges();
                return RedirectToAction("eventsdetail_manager");
            }
            #endregion

            //if something  wrong
            ViewBag.Error = "Chuyên mục đang được sử dụng !";
            return View(list);
        }
        #endregion
        #region add
        //Dropdown List
        public ActionResult _events_detail_add(int? langID, int? linkCateID)
        {
            var list = occSysDB.Events.Where(p => p.Language.IsDefault == true).ToList();
            if (langID != null)
            {
                list = occSysDB.Events.Where(p => p.Lang_ID == langID).ToList();
            }
            ViewBag.LinkCateID = linkCateID;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult eventsdetail_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            if (linkID != null)
            {
                var linkObj = occSysDB.EventsDetails.FirstOrDefault(p => p.DetailID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }

            }
            //get all cate
            var allCate = occSysDB.EventsDetails.Where(p => p.LinkID == 0).ToList();
            //set order
            if (allCate.Count > 0)
            {
                ViewBag.SortOrder = allCate.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            ViewBag.LangAdd = langID;
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult eventsdetail_add(EventsDetail newCate_Add, string save, string LinkID)
        {
            if (ModelState.IsValid)
            {
                //get all cate
                var allCate = occSysDB.EventsDetails.ToList();


                if (newCate_Add.SortOrder != 0)
                {
                    newCate_Add.SortOrder = newCate_Add.SortOrder;
                }
                else
                {
                    //set order
                    if (allCate.Count > 0)
                    {
                        newCate_Add.SortOrder = allCate.Count + 1;
                    }
                    else
                    {
                        newCate_Add.SortOrder = 1;
                    }
                }

                if (newCate_Add.IsActive != null)
                {
                    newCate_Add.IsActive = true;
                }
                else
                {

                    newCate_Add.IsActive = false;

                }

                //link obj
                if (LinkID != null)
                {
                    newCate_Add.LinkID = int.Parse(LinkID);
                }
                else
                {
                    newCate_Add.LinkID = 0;
                }

                //save cate
                occSysDB.EventsDetails.Add(newCate_Add);
                occSysDB.SaveChanges();

                //save action
                if (save == "save")
                {
                    return RedirectToAction("eventsdetail_manager");
                }
                else
                {
                    return RedirectToAction("eventsdetail_add");
                }
            }
            else
            {
                ViewBag.Error = "Bạn chưa nhập đầy đủ thông tin !";
                //if something wrongs
                //get lang
                var lang = occSysDB.Languages.ToList();
                ViewBag.LangList = lang;

                //get all cate
                var allCate = occSysDB.EventsCategories.ToList();
                //set order
                if (allCate.Count > 0)
                {
                    ViewBag.SortOrder = allCate.Count + 1;
                }
                else
                {
                    ViewBag.SortOrder = 1;
                }

                return View(newCate_Add);
            }

        }
        #endregion
        #region edit
        //Dropdown List
        public ActionResult _events_detail_edit(int? newCateID, int? langID)
        {
            var list = occSysDB.Events.Where(p => p.Lang_ID == langID).ToList();
            ViewBag.EventCateID = newCateID;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult eventsdetail_edit(int id)
        {
            //get detail
            var detail = occSysDB.EventsDetails.FirstOrDefault(p => p.DetailID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.EventsDetails.Where(p => p.LinkID == detail.DetailID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.EventsDetails.FirstOrDefault(p => p.DetailID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult eventsdetail_edit(int editID, EventsDetail updateNewsCate)
        {
            if (ModelState.IsValid)
            {
                var allCate = occSysDB.EventsDetails.ToList();

                //get detail
                var update = occSysDB.EventsDetails.FirstOrDefault(p => p.DetailID.Equals(editID));
                update.DetailName = updateNewsCate.DetailName;
                //set parent cate
                update.DetailDescription = updateNewsCate.DetailDescription;

                update.Lang_ID = updateNewsCate.Lang_ID;

                if (updateNewsCate.SortOrder != 0)
                {
                    update.SortOrder = updateNewsCate.SortOrder;
                }
                else
                {
                    //set order
                    if (allCate.Count > 0)
                    {
                        update.SortOrder = allCate.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }

                if (updateNewsCate.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }

                //save action
                occSysDB.SaveChanges();

                return RedirectToAction("eventsdetail_manager");

            }
            //get detail
            var detail = occSysDB.EventsDetails.FirstOrDefault(p => p.DetailID.Equals(editID));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(updateNewsCate);
        }

        #endregion
        #endregion

        /// <summary>
        ///   event timline cate
        /// </summary>
        /// <returns></returns>
        /// 
        #region event timeline cate
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult eventstime_cate_manager()
        {
            //get list
            var list = occSysDB.EventsTimeCategories.OrderBy(p => p.SortOrder).ToList();

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(list);
        }
        [HttpPost]
        public ActionResult eventstime_cate_manager(IEnumerable<int> itemID, string deleteOK, string update, IEnumerable<int> updateID, List<int> SortOrder)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get list
            var list = occSysDB.EventsTimeCategories.ToList();
            #region delete
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get cate
                    var cate = occSysDB.EventsTimeCategories.FirstOrDefault(p => p.ETimeCateID.Equals(id));

                    //if cate exits
                    if (cate != null)
                    {
                        //get details in cate
                        //var newsInCate = cate.News.ToList();

                        ////get child cate
                        //var childCate = occSysDB.NewsCategories.Where(p => p.ParentID == cate.NewCatID).ToList();

                        ////if details in cate not exits
                        //if (newsInCate.Count == 0)
                        //{
                        //    //check child cate
                        //    if (childCate.Count > 0)
                        //    {
                        //        foreach (var child in childCate)
                        //        {
                        //            //get details of child cate 
                        //            var newsInChilds = child.News.ToList();
                        //            //if details not exits
                        //            if (newsInChilds.Count == 0)
                        //            {
                        //                //delete childe cate
                        //                occSysDB.NewsCategories.Remove(child);
                        //            }
                        //            else
                        //            {
                        //                ViewBag.Error = "Chuyên mục đang được sử dụng !";
                        //                return View(list);
                        //            }
                        //        }
                        //    }
                        //delete cate
                        occSysDB.EventsTimeCategories.Remove(cate);
                        //}
                        //else
                        //{
                        //    ViewBag.Error = "Chuyên mục đang được sử dụng !";
                        //    return View(list);
                        //}
                    }

                }
                occSysDB.SaveChanges();
                return RedirectToAction("eventstime_cate_manager");
            }
            #endregion

            #region update order
            if (updateID != null && update == "OK")
            {
                int order = 0;
                foreach (var upID in updateID)
                {
                    var updateCate = occSysDB.EventsTimeCategories.FirstOrDefault(p => p.ETimeCateID.Equals(upID));
                    if (updateCate != null)
                    {
                        updateCate.SortOrder = SortOrder[order];
                    }
                    order++;
                }
                //save update
                occSysDB.SaveChanges();
                return RedirectToAction("eventstime_cate_manager");
            }
            #endregion

            //if something  wrong
            ViewBag.Error = "Chuyên mục đang được sử dụng !";
            return View(list);
        }
        #endregion
        #region add
        //Dropdown List
        public ActionResult _events_time_add(int? langID, int? linkCateID)
        {
            var list = occSysDB.Events.Where(p => p.Language.IsDefault == true).ToList();
            if (langID != null)
            {
                list = occSysDB.Events.Where(p => p.Lang_ID == langID).ToList();
            }
            ViewBag.LinkCateID = linkCateID;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult eventstime_cate_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            if (linkID != null)
            {
                var linkObj = occSysDB.EventsTimeCategories.FirstOrDefault(p => p.ETimeCateID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }

            }
            //get all cate
            var allCate = occSysDB.EventsTimeCategories.Where(p => p.LinkID == 0).ToList();
            //set order
            if (allCate.Count > 0)
            {
                ViewBag.SortOrder = allCate.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            ViewBag.LangAdd = langID;
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult eventstime_cate_add(EventsTimeCategory newCate_Add, string save, string LinkID, string date_start_end)
        {
            if (ModelState.IsValid)
            {
                //get all cate
                var allCate = occSysDB.EventsTimeCategories.ToList();


                if (newCate_Add.SortOrder != 0)
                {
                    newCate_Add.SortOrder = newCate_Add.SortOrder;
                }
                else
                {
                    //set order
                    if (allCate.Count > 0)
                    {
                        newCate_Add.SortOrder = allCate.Count + 1;
                    }
                    else
                    {
                        newCate_Add.SortOrder = 1;
                    }
                }

                if (newCate_Add.IsActive != null)
                {
                    newCate_Add.IsActive = true;
                }
                else
                {
                    newCate_Add.IsActive = false;
                }



                //link obj
                if (LinkID != null)
                {
                    newCate_Add.LinkID = int.Parse(LinkID);
                }
                else
                {
                    newCate_Add.LinkID = 0;
                }

                string[] date = date_start_end.Split('-');
                string startDate = date[0].Trim();
                string endDate = date[1].Trim();

                newCate_Add.ETimeStart = DateTime.ParseExact(startDate, "dd/MM/yyyy", null);
                newCate_Add.ETimeEnd = DateTime.ParseExact(endDate, "dd/MM/yyyy", null);

                //save cate
                occSysDB.EventsTimeCategories.Add(newCate_Add);
                occSysDB.SaveChanges();

                //save action
                if (save == "save")
                {
                    return RedirectToAction("eventstime_cate_manager");
                }
                else
                {
                    return RedirectToAction("eventstime_cate_add");
                }
            }
            else
            {
                ViewBag.Error = "Bạn chưa nhập đầy đủ thông tin !";
                //if something wrongs
                //get lang
                var lang = occSysDB.Languages.ToList();
                ViewBag.LangList = lang;

                //get all cate
                var allCate = occSysDB.EventsTimeCategories.ToList();
                //set order
                if (allCate.Count > 0)
                {
                    ViewBag.SortOrder = allCate.Count + 1;
                }
                else
                {
                    ViewBag.SortOrder = 1;
                }

                return View(newCate_Add);
            }

        }
        #endregion
        #region edit
        //Dropdown List
        public ActionResult _events_time_edit(int? newCateID, int? langID)
        {
            var list = occSysDB.Events.Where(p => p.Lang_ID == langID).ToList();
            ViewBag.EventCateID = newCateID;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult eventstime_cate_edit(int id)
        {
            //get detail
            var detail = occSysDB.EventsTimeCategories.FirstOrDefault(p => p.ETimeCateID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.EventsTimeCategories.Where(p => p.LinkID == detail.ETimeCateID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.EventsTimeCategories.FirstOrDefault(p => p.ETimeCateID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult eventstime_cate_edit(int editID, EventsTimeCategory updateNewsCate, string date_start_end)
        {
            if (ModelState.IsValid)
            {
                var allCate = occSysDB.NewsCategories.ToList();

                //get detail
                var update = occSysDB.EventsTimeCategories.FirstOrDefault(p => p.ETimeCateID.Equals(editID));
                update.ETimeName = updateNewsCate.ETimeName;
                update.ETimeDescription = updateNewsCate.ETimeDescription;

                string[] date = date_start_end.Split('-');
                string startDate = date[0].Trim();
                string endDate = date[1].Trim();

                update.ETimeStart = DateTime.ParseExact(startDate, "dd/MM/yyyy", null);
                update.ETimeEnd = DateTime.ParseExact(endDate, "dd/MM/yyyy", null);
                update.Lang_ID = updateNewsCate.Lang_ID;

                if (updateNewsCate.SortOrder != 0)
                {
                    update.SortOrder = updateNewsCate.SortOrder;
                }
                else
                {
                    //set order
                    if (allCate.Count > 0)
                    {
                        update.SortOrder = allCate.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }
                if (updateNewsCate.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }

                //save action
                occSysDB.SaveChanges();

                return RedirectToAction("eventstime_cate_manager");

            }
            //get detail
            var detail = occSysDB.EventsTimeCategories.FirstOrDefault(p => p.ETimeCateID.Equals(editID));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(updateNewsCate);
        }

        #endregion
        #endregion

        /// <summary>
        ///   event timline
        /// </summary>
        /// <returns></returns>
        /// 
        #region event timeline
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult eventstime_manager()
        {
            //get list
            var list = occSysDB.EventsTimes.OrderBy(p => p.SortOrder).ToList();

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(list);
        }
        [HttpPost]
        public ActionResult eventstime_manager(IEnumerable<int> itemID, string deleteOK, string update, IEnumerable<int> updateID, List<int> SortOrder)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get list
            var list = occSysDB.EventsTimes.ToList();
            #region delete
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get cate
                    var cate = occSysDB.EventsTimes.FirstOrDefault(p => p.ETimeID.Equals(id));

                    //if cate exits
                    if (cate != null)
                    {
                        //get details in cate
                        //var newsInCate = cate.News.ToList();

                        ////get child cate
                        //var childCate = occSysDB.NewsCategories.Where(p => p.ParentID == cate.NewCatID).ToList();

                        ////if details in cate not exits
                        //if (newsInCate.Count == 0)
                        //{
                        //    //check child cate
                        //    if (childCate.Count > 0)
                        //    {
                        //        foreach (var child in childCate)
                        //        {
                        //            //get details of child cate 
                        //            var newsInChilds = child.News.ToList();
                        //            //if details not exits
                        //            if (newsInChilds.Count == 0)
                        //            {
                        //                //delete childe cate
                        //                occSysDB.NewsCategories.Remove(child);
                        //            }
                        //            else
                        //            {
                        //                ViewBag.Error = "Chuyên mục đang được sử dụng !";
                        //                return View(list);
                        //            }
                        //        }
                        //    }
                        //delete cate
                        occSysDB.EventsTimes.Remove(cate);
                        //}
                        //else
                        //{
                        //    ViewBag.Error = "Chuyên mục đang được sử dụng !";
                        //    return View(list);
                        //}
                    }

                }
                occSysDB.SaveChanges();
                return RedirectToAction("eventstime_manager");
            }
            #endregion

            #region update order
            if (updateID != null && update == "OK")
            {
                int order = 0;
                foreach (var upID in updateID)
                {
                    var updateCate = occSysDB.EventsTimes.FirstOrDefault(p => p.ETimeID.Equals(upID));
                    if (updateCate != null)
                    {
                        updateCate.SortOrder = SortOrder[order];
                    }
                    order++;
                }                                             
                //save update
                occSysDB.SaveChanges();
                return RedirectToAction("eventstime_manager");
            }
            #endregion

            //if something  wrong
            ViewBag.Error = "Chuyên mục đang được sử dụng !";
            return View(list);
        }
        #endregion
        #region add
        //Dropdown List
        public ActionResult _events_timecate_add(int? langID, int? linkCateID)
        {
            var list = occSysDB.EventsTimeCategories.Where(p => p.Language.IsDefault == true).ToList();
            if (langID != null)
            {
                list = occSysDB.EventsTimeCategories.Where(p => p.Lang_ID == langID).ToList();
            }
            ViewBag.LinkCateID = linkCateID;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult eventstime_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            if (linkID != null)
            {
                var linkObj = occSysDB.EventsTimes.FirstOrDefault(p => p.ETimeID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }

            }
            //get all cate
            var allCate = occSysDB.EventsTimes.Where(p => p.LinkID == 0).ToList();
            //set order
            if (allCate.Count > 0)
            {
                ViewBag.SortOrder = allCate.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            ViewBag.LangAdd = langID;
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult eventstime_add(EventsTime newCate_Add, string save, string LinkID)
        {
            if (ModelState.IsValid)
            {
                //get all cate
                var allCate = occSysDB.EventsTimes.ToList();


                if (newCate_Add.SortOrder != 0)
                {
                    newCate_Add.SortOrder = newCate_Add.SortOrder;
                }
                else
                {
                    //set order
                    if (allCate.Count > 0)
                    {
                        newCate_Add.SortOrder = allCate.Count + 1;
                    }
                    else
                    {
                        newCate_Add.SortOrder = 1;
                    }
                }

                if (newCate_Add.IsActive != null)
                {
                    newCate_Add.IsActive = true;
                }
                else
                {
                    newCate_Add.IsActive = false;
                }



                //link obj
                if (LinkID != null)
                {
                    newCate_Add.LinkID = int.Parse(LinkID);
                }
                else
                {
                    newCate_Add.LinkID = 0;
                }

                //string[] date = date_start_end.Split('-');
                //string startDate = date[0].Trim();
                //string endDate = date[1].Trim();

                //newCate_Add.ETimeStart = DateTime.ParseExact(startDate, "dd/MM/yyyy", null);
                //newCate_Add.ETimeEnd = DateTime.ParseExact(endDate, "dd/MM/yyyy", null);

                //save cate
                occSysDB.EventsTimes.Add(newCate_Add);
                occSysDB.SaveChanges();

                //save action
                if (save == "save")
                {
                    return RedirectToAction("eventstime_manager");
                }
                else
                {
                    return RedirectToAction("eventstime_add");
                }
            }
            else
            {
                ViewBag.Error = "Bạn chưa nhập đầy đủ thông tin !";
                //if something wrongs
                //get lang
                var lang = occSysDB.Languages.ToList();
                ViewBag.LangList = lang;

                //get all cate
                var allCate = occSysDB.EventsTimes.ToList();
                //set order
                if (allCate.Count > 0)
                {
                    ViewBag.SortOrder = allCate.Count + 1;
                }
                else
                {
                    ViewBag.SortOrder = 1;
                }

                return View(newCate_Add);
            }

        }
        #endregion
        #region edit
        //Dropdown List
        public ActionResult _events_timecate_edit(int? newCateID, int? langID)
        {
            var list = occSysDB.EventsTimeCategories.Where(p => p.Lang_ID == langID).ToList();
            ViewBag.EventCateID = newCateID;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult eventstime_edit(int id)
        {
            //get detail
            var detail = occSysDB.EventsTimes.FirstOrDefault(p => p.ETimeID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.EventsTimes.Where(p => p.LinkID == detail.ETimeID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.EventsTimes.FirstOrDefault(p => p.ETimeID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult eventstime_edit(int editID, EventsTime updateNewsCate)
        {
            if (ModelState.IsValid)
            {
                var allCate = occSysDB.EventsTimes.ToList();

                //get detail
                var update = occSysDB.EventsTimes.FirstOrDefault(p => p.ETimeID.Equals(editID));
                update.ETimeName = updateNewsCate.ETimeName;
                update.ETimeDescription = updateNewsCate.ETimeDescription;
                update.ETime = updateNewsCate.ETime;
                update.ETimeCateID = updateNewsCate.ETimeCateID;

                //string[] date = date_start_end.Split('-');
                //string startDate = date[0].Trim();
                //string endDate = date[1].Trim();

                //update.ETimeStart = DateTime.ParseExact(startDate, "dd/MM/yyyy", null);
                //update.ETimeEnd = DateTime.ParseExact(endDate, "dd/MM/yyyy", null);

                update.Lang_ID = updateNewsCate.Lang_ID;

                if (updateNewsCate.SortOrder != 0)
                {
                    update.SortOrder = updateNewsCate.SortOrder;
                }
                else
                {
                    //set order
                    if (allCate.Count > 0)
                    {
                        update.SortOrder = allCate.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }
                if (updateNewsCate.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }

                //save action
                occSysDB.SaveChanges();

                return RedirectToAction("eventstime_manager");

            }
            //get detail
            var detail = occSysDB.EventsTimes.FirstOrDefault(p => p.ETimeID.Equals(editID));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(updateNewsCate);
        }

        #endregion
        #endregion

        /// <summary>
        ///   event girl
        /// </summary>
        /// <returns></returns>
        /// 
        #region event girl
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult eventsgirl_manager()
        {
            //get list
            var list = occSysDB.EventsGirls.ToList();

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(list);
        }
        [HttpPost]
        public ActionResult eventsgirl_manager(IEnumerable<int> itemID, string deleteOK, string update, IEnumerable<int> updateID, List<int> SortOrder)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get list
            var list = occSysDB.EventsGirls.ToList();
            #region delete
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get cate
                    var cate = occSysDB.EventsGirls.FirstOrDefault(p => p.EGirlID.Equals(id));

                    //if cate exits
                    if (cate != null)
                    {
                        //get details in cate
                        //var newsInCate = cate.News.ToList();

                        ////get child cate
                        //var childCate = occSysDB.NewsCategories.Where(p => p.ParentID == cate.NewCatID).ToList();

                        ////if details in cate not exits
                        //if (newsInCate.Count == 0)
                        //{
                        //    //check child cate
                        //    if (childCate.Count > 0)
                        //    {
                        //        foreach (var child in childCate)
                        //        {
                        //            //get details of child cate 
                        //            var newsInChilds = child.News.ToList();
                        //            //if details not exits
                        //            if (newsInChilds.Count == 0)
                        //            {
                        //                //delete childe cate
                        //                occSysDB.NewsCategories.Remove(child);
                        //            }
                        //            else
                        //            {
                        //                ViewBag.Error = "Chuyên mục đang được sử dụng !";
                        //                return View(list);
                        //            }
                        //        }
                        //    }
                        //delete cate
                        occSysDB.EventsGirls.Remove(cate);
                        //}
                        //else
                        //{
                        //    ViewBag.Error = "Chuyên mục đang được sử dụng !";
                        //    return View(list);
                        //}
                    }

                }
                occSysDB.SaveChanges();
                return RedirectToAction("eventsgirl_manager");
            }
            #endregion

            #region update order
            if (updateID != null && update == "OK")
            {
                int order = 0;
                foreach (var upID in updateID)
                {
                    var updateCate = occSysDB.EventsDetails.FirstOrDefault(p => p.DetailID.Equals(upID));
                    if (updateCate != null)
                    {
                        updateCate.SortOrder = SortOrder[order];
                    }
                    order++;
                }
                //save update
                occSysDB.SaveChanges();
                return RedirectToAction("eventsgirl_manager");
            }
            #endregion

            //if something  wrong
            ViewBag.Error = "Chuyên mục đang được sử dụng !";
            return View(list);
        }
        #endregion
        #region add
        //Dropdown List
        public ActionResult _events_girl_add(int? langID, int? linkCateID)
        {
            var list = occSysDB.Events.Where(p => p.Language.IsDefault == true).ToList();
            if (langID != null)
            {
                list = occSysDB.Events.Where(p => p.Lang_ID == langID).ToList();
            }
            ViewBag.LinkCateID = linkCateID;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult eventsgirl_add()
        {
            //Status
            var status = occSysDB.EventsGirlStatus.OrderBy(p => p.SortOrder).ToList();
            ViewBag.Status = status;
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult eventsgirl_add(EventsGirl newCate_Add, string save, string LinkID, HttpPostedFileBase file_image, IEnumerable<HttpPostedFileBase> file_images, string ebirthDay)
        {
            if (ModelState.IsValid)
            {
                //get all cate
                var allCate = occSysDB.EventsGirls.ToList();

                var newGirl = new EventsGirl();

                string newsDate = ebirthDay.Replace("/", "").Replace("_", "").ToString();

                if (newsDate != "")
                {
                    newGirl.Birtday = DateTime.ParseExact(ebirthDay, "dd/MM/yyyy", null);
                }
                else
                {
                    newGirl.Birtday = DateTime.ParseExact("28/06/1989", "dd/MM/yyyy", null);
                }

                newGirl.Name = newCate_Add.Name;
                newGirl.AddressBorn = newCate_Add.AddressBorn;
                newGirl.AddressNow = newCate_Add.AddressNow;
                newGirl.Mobile = newCate_Add.Mobile;
                newGirl.Email = newCate_Add.Email;
                newGirl.Facebook = newCate_Add.Facebook;
                newGirl.Love = newCate_Add.Love;
                newGirl.EventID = newCate_Add.EventID;

                newGirl.School = newCate_Add.School;
                newGirl.EClass = newCate_Add.EClass;
                newGirl.Grade = newCate_Add.Grade;
                newGirl.Height = newCate_Add.Height;
                newGirl.Weight = newCate_Add.Weight;
                newGirl.ThreeRing = newCate_Add.ThreeRing;

                newGirl.EStatusID = newCate_Add.EStatusID;
                newGirl.Code = newCate_Add.Code;
                newGirl.SMS = newCate_Add.SMS;
                newGirl.Web = newCate_Add.Web;
                newGirl.Rank = newCate_Add.Rank;

                occSysDB.EventsGirls.Add(newGirl);
                occSysDB.SaveChanges();

                var newPathImage = Server.MapPath("/Uploaded/Images/sukien/thi-sinh-du-thi");
                if (!Directory.Exists(newPathImage))
                {
                    Directory.CreateDirectory(newPathImage);
                }

                if (file_image != null)
                {
                    //upload image
                    string duoiAnh = Path.GetExtension(file_image.FileName);
                    string fileName = OccConvert.KhongDau(newGirl.Name.ToLower().Trim()) + newGirl.EGirlID + duoiAnh;
                    string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));

                    //save file to folder
                    file_image.SaveAs(fileUrl);

                    //get image
                    newGirl.Image = fileName;
                }

                string imgFolder = OccConvert.KhongDau(newGirl.Name.ToLower().Trim()) + newGirl.EGirlID;
                string mapPatch = Server.MapPath("/Uploaded/Images/sukien/thi-sinh-du-thi/" + imgFolder);
                if (!Directory.Exists(mapPatch))
                {
                    Directory.CreateDirectory(mapPatch);
                }
                if (file_images != null)
                {
                    foreach (var img in file_images)
                    {
                        //set new
                        var newImg = new EventsGirlImage();
                        newImg.EImgFolder = imgFolder;
                        newImg.EGirlID = newGirl.EGirlID;

                        //add to DB
                        occSysDB.EventsGirlImages.Add(newImg);
                        occSysDB.SaveChanges();

                        //upload image
                        string duoiAnhs = Path.GetExtension(img.FileName);
                        string fileNames = imgFolder + "-" + newImg.EImgID + duoiAnhs;
                        //create folder
                        string fileUrls = Path.Combine(mapPatch, Path.GetFileName(fileNames));

                        //save
                        img.SaveAs(fileUrls);
                        newImg.EImgName = fileNames;
                        occSysDB.SaveChanges();
                    }
                }

                //save cate
        
                occSysDB.SaveChanges();

                //save action
                if (save == "save")
                {
                    return RedirectToAction("eventsgirl_manager");
                }
                else
                {
                    return RedirectToAction("eventsgirl_add");
                }
            }
            else
            {
                ViewBag.Error = "Bạn chưa nhập đầy đủ thông tin !";
                //if something wrongs
                //get lang
                var lang = occSysDB.Languages.ToList();
                ViewBag.LangList = lang;
                //Status
                var status = occSysDB.EventsGirlStatus.OrderBy(p => p.SortOrder).ToList();
                ViewBag.Status = status;

                return View(newCate_Add);
            }

        }
        #endregion
        #region edit
        //Dropdown List
        public ActionResult _events_girl_edit(int? newCateID, int? langID)
        {
            var list = occSysDB.Events.Where(p => p.Language.IsDefault == true).ToList();
            if (langID != null)
            {
                list = occSysDB.Events.Where(p => p.Lang_ID == langID).ToList();
            }
            ViewBag.EventCateID = newCateID;

            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult eventsgirl_edit(int id)
        {
            //get detail
            var detail = occSysDB.EventsGirls.FirstOrDefault(p => p.EGirlID.Equals(id));
            ViewBag.Detail = detail;

            //Status
            var status = occSysDB.EventsGirlStatus.OrderBy(p => p.SortOrder).ToList();
            ViewBag.Status = status;

            return View(detail);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult eventsgirl_edit(int editID, EventsGirl updateNewsCate, HttpPostedFileBase file_image, IEnumerable<HttpPostedFileBase> file_images, string ebirthDay)
        {
            if (ModelState.IsValid)
            {
                var allCate = occSysDB.EventsGirls.ToList();

                //get detail
                var update = occSysDB.EventsGirls.FirstOrDefault(p => p.EGirlID.Equals(editID));

                string newsDate = ebirthDay.Replace("/", "").Replace("_", "").ToString();

                if (newsDate != "")
                {
                    update.Birtday = DateTime.ParseExact(ebirthDay, "dd/MM/yyyy", null);
                }
                else
                {
                    update.Birtday = DateTime.ParseExact("28/06/1989", "dd/MM/yyyy", null);
                }
                update.Name = updateNewsCate.Name;
                update.AddressBorn = updateNewsCate.AddressBorn;
                update.AddressNow = updateNewsCate.AddressNow;
                update.Mobile = updateNewsCate.Mobile;
                update.Email = updateNewsCate.Email;
                update.Facebook = updateNewsCate.Facebook;
                update.Love = updateNewsCate.Love;
                update.EventID = updateNewsCate.EventID;

                update.School = updateNewsCate.School;
                update.EClass = updateNewsCate.EClass;
                update.Grade = updateNewsCate.Grade;
                update.Height = updateNewsCate.Height;
                update.Weight = updateNewsCate.Weight;
                update.ThreeRing = updateNewsCate.ThreeRing;

                update.EStatusID = updateNewsCate.EStatusID;
                update.Code = updateNewsCate.Code;
                update.SMS = updateNewsCate.SMS;
                update.Web = updateNewsCate.Web;
                update.Rank = updateNewsCate.Rank;

                var newPathImage = Server.MapPath("/Uploaded/Images/sukien/thi-sinh-du-thi");
                if (!Directory.Exists(newPathImage))
                {
                    Directory.CreateDirectory(newPathImage);
                }

                if (file_image != null)
                {
                    //upload image
                    string duoiAnh = Path.GetExtension(file_image.FileName);
                    string fileName = OccConvert.KhongDau(update.Name.ToLower().Trim()) + update.EGirlID + duoiAnh;
                    string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));

                    //save file to folder
                    file_image.SaveAs(fileUrl);

                    //get image
                    update.Image = fileName;
                }

                string imgFolder = OccConvert.KhongDau(update.Name.ToLower().Trim()) + update.EGirlID;
                string mapPatch = Server.MapPath("/Uploaded/Images/sukien/thi-sinh-du-thi/" + imgFolder);
                if (!Directory.Exists(mapPatch))
                {
                    Directory.CreateDirectory(mapPatch);
                }
                if (file_images != null)
                {
                    foreach (var img in file_images)
                    {
                        //set new
                        var newImg = new EventsGirlImage();
                        newImg.EImgFolder = imgFolder;
                        newImg.EGirlID = update.EGirlID;

                        //add to DB
                        occSysDB.EventsGirlImages.Add(newImg);
                        occSysDB.SaveChanges();

                        //upload image
                        string duoiAnhs = Path.GetExtension(img.FileName);
                        string fileNames = imgFolder + "-" + newImg.EImgID + duoiAnhs;
                        //create folder
                        string fileUrls = Path.Combine(mapPatch, Path.GetFileName(fileNames));

                        //save
                        img.SaveAs(fileUrls);
                        newImg.EImgName = fileNames;
                        occSysDB.SaveChanges();
                    }
                }
                //save action
                occSysDB.SaveChanges();

                return RedirectToAction("eventsgirl_manager");

            }
            //get detail
            var detail = occSysDB.EventsDetails.FirstOrDefault(p => p.DetailID.Equals(editID));
            ViewBag.Detail = detail;

            //Status
            var status = occSysDB.EventsGirlStatus.OrderBy(p => p.SortOrder).ToList();
            ViewBag.Status = status;
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(updateNewsCate);
        }


        //update image album
        public void updateGirlImage(int id, HttpPostedFileBase file_image)
        {
            //get slide
            var detail = occSysDB.EventsGirls.FirstOrDefault(p => p.EGirlID.Equals(id));
            //upload image
            string duoiAnh = Path.GetExtension(file_image.FileName);
            string fileName = OccConvert.KhongDau(detail.Name.ToLower().Trim()) + detail.EGirlID + duoiAnh;
            //create folder
            var newPathImage = Server.MapPath("/Uploaded/Images/sukien/thi-sinh-du-thi");
            string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));

            //save file to folder
            file_image.SaveAs(fileUrl);
            //add to album
            detail.Image = fileName;
            //save action
            occSysDB.SaveChanges();
        }
        //update image album
        public void updateImageInAlbum(int id, HttpPostedFileBase file_images)
        {
            //get slide
            var detail = occSysDB.EventsGirls.FirstOrDefault(p => p.EGirlID.Equals(id));

            string imgFolder = OccConvert.KhongDau(detail.Name.ToLower().Trim()) + detail.EGirlID;
            string mapPatch = Server.MapPath("/Uploaded/Images/sukien/thi-sinh-du-thi/" + imgFolder);
            if (!Directory.Exists(mapPatch))
            {
                Directory.CreateDirectory(mapPatch);
            }
            //set new
            var newImg = new EventsGirlImage();
            newImg.EImgFolder = imgFolder;
            newImg.EGirlID = detail.EGirlID;

            //add to DB
            occSysDB.EventsGirlImages.Add(newImg);
            occSysDB.SaveChanges();

            //upload image
            string duoiAnhs = Path.GetExtension(file_images.FileName);
            string fileNames = imgFolder + "-" + newImg.EImgID + duoiAnhs;
            //create folder
            string fileUrls = Path.Combine(mapPatch, Path.GetFileName(fileNames));

            //save
            file_images.SaveAs(fileUrls);
            newImg.EImgName = fileNames;

            //save action
            occSysDB.SaveChanges();
        }
        //delete image album
        public void deleteImageInAlbum(int id, string[] fileNames)
        {
            foreach (var fullName in fileNames)
            {
                string tenanh = fullName.Substring(fullName.LastIndexOf("/")).Replace("/", "");

                var imageToDelete = occSysDB.EventsGirlImages.FirstOrDefault(p => p.EGirlID == id && p.EImgName == tenanh);

                if (imageToDelete != null)
                {
                    occSysDB.EventsGirlImages.Remove(imageToDelete);
                    //save action
                    occSysDB.SaveChanges();
                }
            }

        }
        #endregion
        #endregion

        /// <summary>
        ///   girl Status 
        /// </summary>
        /// <returns></returns>
        /// 
        #region girl Statuse
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult girlstatus_manager()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.EventsGirlStatus.OrderBy(p => p.SortOrder).ToList();
            return View(list);
        }
        [HttpPost]
        public ActionResult girlstatus_manager(IEnumerable<int> itemID, IEnumerable<int> updateID, List<int> SortOrder, string deleteOK, string update)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get slide
            var list = occSysDB.EventsGirlStatus.ToList();
            //delete slide
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get slider
                    var del = occSysDB.EventsGirlStatus.FirstOrDefault(p => p.EStatusID.Equals(id));
                    //already
                    if (del != null)
                    {
                        var books = del.EventsGirls.ToList();
                        if (books.Count == 0)
                        {
                            //delete slide
                            occSysDB.EventsGirlStatus.Remove(del);
                            //save action
                            occSysDB.SaveChanges();
                        }
                        else
                        {
                            ViewBag.Error = "Mục này đang được sử dụng !";
                            return View(list);
                        }

                    }
                }
            }

            //update sortorder
            int sortorder = 0;
            if (SortOrder != null && update == "OK")
            {
                foreach (var slideid in updateID)
                {
                    //get slider
                    var slideUpdate = occSysDB.EventsGirlStatus.FirstOrDefault(p => p.EStatusID.Equals(slideid));
                    if (slideUpdate != null)
                    {
                        //update sort order
                        slideUpdate.SortOrder = SortOrder[sortorder];
                    }
                    //up index
                    sortorder++;
                }
                //save
                occSysDB.SaveChanges();
            }

            return RedirectToAction("girlstatus_manager");
        }
        #endregion
        #region add
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult girlstatus_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliders = occSysDB.EventsGirlStatus.ToList();
            if (sliders.Count > 0)
            {
                ViewBag.SortOrder = sliders.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult girlstatus_add(EventsGirlStatus newDetail, string save)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var alls = occSysDB.EventsGirlStatus.ToList();
                //new slide
                var newHot = new EventsGirlStatus();

                newHot.EStatusName = newDetail.EStatusName;
                newHot.Color = newDetail.Color;
                //set active
                if (newDetail.IsActive != null)
                {
                    newHot.IsActive = true;
                }
                else
                {
                    newHot.IsActive = false;
                }
                //set order
                if (newDetail.SortOrder != null)
                {
                    newHot.SortOrder = newDetail.SortOrder;
                }
                else
                {
                    if (alls.Count > 0)
                    {
                        newHot.SortOrder = alls.Count + 1;
                    }
                    else
                    {
                        newHot.SortOrder = 1;
                    }
                }

                //add slide
                occSysDB.EventsGirlStatus.Add(newHot);
                //save first
                occSysDB.SaveChanges();

                if (save == "save")
                {
                    return RedirectToAction("girlstatus_manager");
                }
                else
                {
                    return RedirectToAction("girlstatus_add");
                }

            }
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get order slide
            var sliderChecks = occSysDB.EventsGirlStatus.ToList();
            if (sliderChecks.Count > 0)
            {
                ViewBag.SortOrder = sliderChecks.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //if something wrong 
            return View(newDetail);
        }


        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult girlstatus_edit(int id)
        {
            //get detail
            var detail = occSysDB.EventsGirlStatus.FirstOrDefault(p => p.EStatusID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult girlstatus_edit(EventsGirlStatus updateDeatail, int editID, string sColor)
        {
            if (ModelState.IsValid)
            {
                //get slide
                var alls = occSysDB.StatusOrders.ToList();
                //get update
                var update = occSysDB.EventsGirlStatus.FirstOrDefault(p => p.EStatusID.Equals(editID));

                update.EStatusName = updateDeatail.EStatusName;

                if (sColor != null)
                {
                    update.Color = sColor; 
                }
                

                //set active
                if (updateDeatail.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }
                //set order
                if (updateDeatail.SortOrder != null)
                {
                    update.SortOrder = updateDeatail.SortOrder;
                }
                else
                {
                    if (alls.Count > 0)
                    {
                        update.SortOrder = alls.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }

                //save
                occSysDB.SaveChanges();

                return RedirectToAction("girlstatus_manager");
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //if something wrong 
            return View(updateDeatail);
        }



        #endregion
        #endregion


    }
}
