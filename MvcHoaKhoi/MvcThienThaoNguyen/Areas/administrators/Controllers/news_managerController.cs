﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.IO;
using MvcWebsiteOcc.Areas.administrators.Models;
using System.Text.RegularExpressions;


namespace MvcWebsiteOcc.Areas.administrators.Controllers
{
    public class news_managerController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();

        /// <summary>
        ///   News Category
        /// </summary>
        /// <returns></returns>
        /// 
        #region News Category
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult newscate_manager()
        {   //get list
            var list = occSysDB.NewsCategories.OrderBy(p => p.SortOrder).ToList();

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(list);
        }
        [HttpPost]
        public ActionResult newscate_manager(IEnumerable<int> itemID, string deleteOK, string update, IEnumerable<int> updateID, List<int> SortOrder)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get list
            var list = occSysDB.NewsCategories.ToList();
            #region delete
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get cate
                    var cate = occSysDB.NewsCategories.FirstOrDefault(p => p.NewCatID.Equals(id));

                    //if cate exits
                    if (cate != null)
                    {
                        //get details in cate
                        var newsInCate = cate.News.ToList();

                        //get child cate
                        var childCate = occSysDB.NewsCategories.Where(p => p.ParentID == cate.NewCatID).ToList();

                        //if details in cate not exits
                        if (newsInCate.Count == 0)
                        {
                            //check child cate
                            if (childCate.Count > 0)
                            {
                                foreach (var child in childCate)
                                {
                                    //get details of child cate 
                                    var newsInChilds = child.News.ToList();
                                    //if details not exits
                                    if (newsInChilds.Count == 0)
                                    {
                                        //delete childe cate
                                        occSysDB.NewsCategories.Remove(child);
                                    }
                                    else
                                    {
                                        ViewBag.Error = "Chuyên mục đang được sử dụng !";
                                        return View(list);
                                    }
                                }
                            }
                            //delete cate
                            occSysDB.NewsCategories.Remove(cate);
                        }
                        else
                        {
                            ViewBag.Error = "Chuyên mục đang được sử dụng !";
                            return View(list);
                        }
                    }

                }
                occSysDB.SaveChanges();
                return RedirectToAction("newscate_manager");
            }
            #endregion

            #region update order
            if (updateID != null && update == "OK")
            {
                int order = 0;
                foreach (var upID in updateID)
                {
                    var updateCate = occSysDB.NewsCategories.FirstOrDefault(p => p.NewCatID.Equals(upID));
                    if (updateCate != null)
                    {
                        updateCate.SortOrder = SortOrder[order];
                    }
                    order++;
                }
                //save update
                occSysDB.SaveChanges();
                return RedirectToAction("newscate_manager");
            }
            #endregion

            //if something  wrong
            ViewBag.Error = "Chuyên mục đang được sử dụng !";
            return View(list);
        }
        #endregion
        #region add
        //Dropdown List
        public ActionResult _parent_newscate_add(int? langID, int? linkID, int? linkParentID)
        {
            if (langID != null)
            {
                var list = occSysDB.NewsCategories.Where(p => p.ParentID.Equals(0) && p.Lang_ID == langID).ToList();
                if (linkID != null)
                {
                    var isChild = occSysDB.NewsCategories.FirstOrDefault(p => p.NewCatID == linkID);
                    var hasChild = occSysDB.NewsCategories.Where(p => p.ParentID == linkID).ToList();
                    ViewBag.LinkCateID = linkID;
                    ViewBag.LinkParentID = linkParentID;
                    //it is parent
                    if (isChild.ParentID == 0 && hasChild.Count > 0)
                    {
                        list = list;
                    }

                    if (isChild.ParentID == 0 && hasChild.Count == 0)
                    {
                        list = occSysDB.NewsCategories.Where(p => p.ParentID.Equals(0) && p.Lang_ID == langID).ToList();
                    }

                    //it is child
                    if (isChild.ParentID > 0)
                    {
                        list = occSysDB.NewsCategories.Where(p => p.ParentID.Equals(0) && p.Lang_ID == langID).ToList();
                    }
                }
                return PartialView(list);
            }
            else
            {
                var list = occSysDB.NewsCategories.Where(p => p.ParentID.Equals(0) && p.Language.IsDefault == true).ToList();
                if (linkID != null)
                {
                    var isChild = occSysDB.NewsCategories.FirstOrDefault(p => p.NewCatID == linkID);
                    var hasChild = occSysDB.NewsCategories.Where(p => p.ParentID == linkID).ToList();
                    ViewBag.LinkCateID = linkID;
                    ViewBag.LinkParentID = linkParentID;
                    //it is parent
                    if (isChild.ParentID == 0 && hasChild.Count > 0)
                    {
                        list = list;
                    }

                    if (isChild.ParentID == 0 && hasChild.Count == 0)
                    {
                        list = occSysDB.NewsCategories.Where(p => p.ParentID.Equals(0) && p.Lang_ID == langID).ToList();
                    }

                    //it is child
                    if (isChild.ParentID > 0)
                    {
                        list = occSysDB.NewsCategories.Where(p => p.ParentID.Equals(0) && p.Lang_ID == langID).ToList();
                    }
                }
                return PartialView(list);
            }
        }

        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult newscate_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            if (linkID != null)
            {
                var linkObj = occSysDB.NewsCategories.FirstOrDefault(p => p.NewCatID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }

            }
            //get all cate
            var allCate = occSysDB.NewsCategories.Where(p => p.LinkID == 0).ToList();
            //set order
            if (allCate.Count > 0)
            {
                ViewBag.SortOrder = allCate.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            //get type
            var types = occSysDB.TypeNews.ToList();
            ViewBag.Types = types;
            ViewBag.LangAdd = langID;
            return View();
        }
        [HttpPost]
        public ActionResult newscate_add(NewsCategory newCate_Add, string save, string LinkID)
        {
            if (ModelState.IsValid)
            {
                //get all cate
                var allCate = occSysDB.NewsCategories.ToList();


                if (newCate_Add.SortOrder != 0)
                {
                    newCate_Add.SortOrder = newCate_Add.SortOrder;
                }
                else
                {
                    //set order
                    if (allCate.Count > 0)
                    {
                        newCate_Add.SortOrder = allCate.Count + 1;
                    }
                    else
                    {
                        newCate_Add.SortOrder = 1;
                    }
                }

                if (newCate_Add.IsActive != null)
                {
                    newCate_Add.IsActive = true;
                }
                else
                {

                    newCate_Add.IsActive = false;

                }

                if (newCate_Add.TID == null)
                {
                    newCate_Add.TID = 1;
                }

                //set parent cate
                if (newCate_Add.ParentID > 0)
                {
                    var parent = occSysDB.NewsCategories.FirstOrDefault(p => p.NewCatID == newCate_Add.ParentID && p.Lang_ID == newCate_Add.Lang_ID);

                    if (parent != null)
                    {
                        newCate_Add.ParentName = parent.NewsCatName;
                    }
                }
                //link obj
                if (LinkID != null)
                {
                    newCate_Add.LinkID = int.Parse(LinkID);
                }
                else
                {
                    newCate_Add.LinkID = 0;
                }

                //save cate
                occSysDB.NewsCategories.Add(newCate_Add);
                occSysDB.SaveChanges();

                //save action
                if (save == "save")
                {
                    return RedirectToAction("newscate_manager");
                }
                else
                {
                    return RedirectToAction("newscate_add");
                }
            }
            else
            {
                ViewBag.Error = "Bạn chưa nhập đầy đủ thông tin !";
                //if something wrongs
                //get lang
                var lang = occSysDB.Languages.ToList();
                ViewBag.LangList = lang;

                //get all cate
                var allCate = occSysDB.NewsCategories.ToList();
                //set order
                if (allCate.Count > 0)
                {
                    ViewBag.SortOrder = allCate.Count + 1;
                }
                else
                {
                    ViewBag.SortOrder = 1;
                }

                return View(newCate_Add);
            }

        }
        #endregion
        #region edit
        //Dropdown List
        public ActionResult _parent_newscate_edit(int? parentID, int? cateID, int? langID)
        {
            ViewBag.ParentID = parentID;
            var list = occSysDB.NewsCategories.Where(p => p.ParentID.Equals(0) && p.NewCatID != cateID && p.Lang_ID == langID).ToList();
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult newscate_edit(int id)
        {
            //get detail
            var detail = occSysDB.NewsCategories.FirstOrDefault(p => p.NewCatID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.NewsCategories.Where(p => p.LinkID == detail.NewCatID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.NewsCategories.FirstOrDefault(p => p.NewCatID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }
            //get type
            var types = occSysDB.TypeNews.ToList();
            ViewBag.Types = types;

            return View(detail);
        }
        [HttpPost]
        public ActionResult newscate_edit(int editID, NewsCategory updateNewsCate)
        {
            if (ModelState.IsValid)
            {
                var allCate = occSysDB.NewsCategories.ToList();

                //get detail
                var update = occSysDB.NewsCategories.FirstOrDefault(p => p.NewCatID.Equals(editID));
                update.NewsCatName = updateNewsCate.NewsCatName;
                update.NewsCateDescription = updateNewsCate.NewsCateDescription;
                //set parent cate

                if (updateNewsCate.ParentID > 0)
                {
                    update.ParentID = updateNewsCate.ParentID;
                    var parent = occSysDB.NewsCategories.FirstOrDefault(p => p.NewCatID == updateNewsCate.ParentID);
                    if (parent != null)
                    {
                        updateNewsCate.ParentName = parent.NewsCatName;
                    }
                }

                update.Lang_ID = updateNewsCate.Lang_ID;

                if (updateNewsCate.SortOrder != 0)
                {
                    update.SortOrder = updateNewsCate.SortOrder;
                }
                else
                {
                    //set order
                    if (allCate.Count > 0)
                    {
                        update.SortOrder = allCate.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }
                update.TID = updateNewsCate.TID;

                if (updateNewsCate.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }
                //seo
                update.MetaTitle = updateNewsCate.MetaTitle;
                update.MetaKeywords = updateNewsCate.MetaKeywords;
                update.MetaDescription = updateNewsCate.MetaDescription;

                //save action
                occSysDB.SaveChanges();

                return RedirectToAction("newscate_manager");

            }
            //get detail
            var detail = occSysDB.NewsCategories.FirstOrDefault(p => p.NewCatID.Equals(editID));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(updateNewsCate);
        }

        #endregion
        #endregion

        /// <summary>
        ///   News 
        /// </summary>
        /// <returns></returns>
        /// 
        #region News
        #region view
        public ActionResult _menu_news()
        {
            var statusLst = occSysDB.TypeNews.ToList();

            ViewBag.NewOrder = occSysDB.News.Count();

            return PartialView(statusLst);
        }


        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult news_views(string type)
        {
            //get list view 
            var list = occSysDB.News.OrderByDescending(p => p.CreateDate).ToList();

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;


            if (type != null)
            {
                int a = type.LastIndexOf("-");
                string title = type.Substring(a).Replace("-", "");
                string b = Regex.Match(title, @"\d+").Value;
                int id = int.Parse(b);

                var statusOrder = occSysDB.TypeNews.FirstOrDefault(p => p.TID == id);
                if (statusOrder != null)
                {
                    list = occSysDB.News.Where(p => p.NewsCategories.TID == statusOrder.TID).OrderByDescending(p => p.CreateDate).ToList();
                }
            }

            return View(list);
        }
        [HttpPost]
        public ActionResult news_views(IEnumerable<int> itemID, string deleteOK, string type)
        {
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get dtail
                    var newsDel = occSysDB.News.FirstOrDefault(p => p.NewsID == id);
                    //delete detail
                    occSysDB.News.Remove(newsDel);
                    occSysDB.SaveChanges();
                }
            }
            return RedirectToAction("news_views", "news_manager", new { type = type });

        }
        #endregion
        #region add
        //Dropdown List
        public ActionResult _news_newscate_add(int? langID, int? linkCateID)
        {
            var list = occSysDB.NewsCategories.Where(p => p.Language.IsDefault == true).ToList();
            if (langID != null)
            {
                list = occSysDB.NewsCategories.Where(p => p.Lang_ID == langID).ToList();
            }
            ViewBag.LinkCateID = linkCateID;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult news_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            if (langID != null)
            {
                ViewBag.LangAdd = langID;
            }

            //get linkID in lang default
            if (linkID != null)
            {
                var linkObj = occSysDB.News.FirstOrDefault(p => p.NewsID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }
            }

            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult news_add(News newDetail, HttpPostedFileBase file_image, HttpPostedFileBase file_download, string save, string createDateView, string LinkID)
        {

            if (ModelState.IsValid)
            {
                bool formIsvalid = true;
                //if (file_image != null && LinkID != null)
                //{
                //    formIsvalid = true;
                //}
                //if (file_image == null && LinkID != null)
                //{
                //    formIsvalid = true;
                //}
                //if (file_image != null && LinkID == null)
                //{
                //    formIsvalid = true;
                //}

                if (formIsvalid == true)
                {
                    //get new detail
                    News newNews = new News();
                    string newsDate = createDateView.Replace("/", "").Replace("_", "").ToString();

                    if (newsDate != "")
                    {
                        newNews.CreateDate = DateTime.ParseExact(createDateView, "dd/MM/yyyy", null) + DateTime.Now.TimeOfDay;
                    }
                    else
                    {
                        newNews.CreateDate = DateTime.Now;
                    }


                    newNews.ViewCount = 0;
                    newNews.NewsVideo = newDetail.NewsVideo;
                    newNews.NewsTitle = newDetail.NewsTitle;
                    //add details des with lang
                    var getUser = occSysDB.Users.FirstOrDefault(p => p.UserName == System.Web.HttpContext.Current.User.Identity.Name);
                    newNews.NewsAuthor = getUser.FullName;

                    newNews.ShortDescription = newDetail.ShortDescription;
                    newNews.FullDescription = newDetail.FullDescription;
                    newNews.Lang_ID = newDetail.Lang_ID;
                    newNews.EventID = newDetail.EventID;
                    if (LinkID != null)
                    {
                        newNews.LinkID = int.Parse(LinkID);
                    }
                    else
                    {
                        newNews.LinkID = 0;
                    }

                    newNews.NewCatID = newDetail.NewCatID;
                    if (newDetail.IsActive != null)
                    {
                        newNews.IsActive = true;
                    }
                    else
                    {
                        newNews.IsActive = false;
                    }

                    if (newDetail.IsHome != null)
                    {
                        newNews.IsHome = newDetail.IsHome;
                    }
                    else
                    {
                        newNews.IsHome = false;
                    }

                    if (newDetail.IsHot != null)
                    {
                        newNews.IsHot = newDetail.IsHot;
                    }
                    else
                    {
                        newNews.IsHot = false;
                    }


                    //add news to DB
                    occSysDB.News.Add(newNews);
                    //save first
                    occSysDB.SaveChanges();
                    //create folder
                    var newPathImage = Server.MapPath("/Uploaded/Images/tintuc/anh-tin-" + newNews.NewsID + "-folder");
                    if (!Directory.Exists(newPathImage))
                    {
                        Directory.CreateDirectory(newPathImage);
                    }

                    if (file_image != null)
                    {
                        //upload image
                        string duoiAnh = Path.GetExtension(file_image.FileName);
                        string fileName = "anh-tin-tuc-" + newNews.NewsID + duoiAnh;
                        newNews.Folder = "anh-tin-" + newNews.NewsID + "-folder/";
                        string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));

                        //save file to folder
                        file_image.SaveAs(fileUrl);

                        //get image
                        newNews.NewsImage = fileName;
                    }
                    else
                    {
                        newNews.Folder = newDetail.Folder;
                        newNews.NewsImage = newDetail.NewsImage;
                    }

                    //save file download
                    if (file_download != null)
                    {
                        //upload image
                        string duoiFile = Path.GetExtension(file_download.FileName);
                        string fileNameF = "file-tin-tuc-" + newNews.NewsID + duoiFile;
                        string fileUrlF = Path.Combine(newPathImage, Path.GetFileName(fileNameF));
                        //save file to folder
                        file_download.SaveAs(fileUrlF);

                        //get file
                        newNews.FileDownload = fileNameF;
                    }

                    //save
                    occSysDB.SaveChanges();

                    if (save == "save")
                    {
                        return RedirectToAction("news_views");
                    }
                    else
                    {
                        return RedirectToAction("news_add");
                    }

                }
                else
                {
                    ViewBag.Mess = "Bạn chưa upload ảnh";
                }
            }

            //if something wrong 
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(newDetail);
        }
        #endregion
        #region edit
        //Dropdown List
        public ActionResult _news_newscate_edit(int? newCateID, int? langID)
        {
            var list = occSysDB.NewsCategories.Where(p => p.Lang_ID == langID).ToList();
            ViewBag.NewsCateID = newCateID;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult news_edit(int id)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get detail
            var detail = occSysDB.News.FirstOrDefault(p => p.NewsID == id);
            ViewBag.Detail = detail;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.News.Where(p => p.LinkID == detail.NewsID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.News.FirstOrDefault(p => p.NewsID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }


            return View(detail);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult news_edit(int editID, News updateDetail, HttpPostedFileBase file_download, string createDateView)
        {

            if (ModelState.IsValid)
            {
                //get  detail
                var updateNews = occSysDB.News.FirstOrDefault(p => p.NewsID.Equals(editID));
                updateNews.NewsVideo = updateDetail.NewsVideo;
                updateNews.NewsTitle = updateDetail.NewsTitle;
                //add details des with lang
                var getUser = occSysDB.Users.FirstOrDefault(p => p.UserName == System.Web.HttpContext.Current.User.Identity.Name);
                updateNews.NewsAuthor = getUser.FullName;

                updateNews.ShortDescription = updateDetail.ShortDescription;
                updateNews.FullDescription = updateDetail.FullDescription;
                updateNews.Lang_ID = updateDetail.Lang_ID;
                updateNews.NewCatID = updateDetail.NewCatID;

                string newsDate = createDateView.Replace("/", "").Replace("_", "").ToString();

                if (newsDate != "")
                {
                    updateNews.CreateDate = DateTime.ParseExact(createDateView, "dd/MM/yyyy", null) + DateTime.Now.TimeOfDay;
                }
                else
                {
                    updateNews.CreateDate = DateTime.Now;
                }

                updateNews.EventID = updateDetail.EventID;

                if (updateDetail.IsActive != null)
                {
                    updateNews.IsActive = true;
                }
                else
                {
                    updateNews.IsActive = false;
                }

                if (updateDetail.IsHome != null)
                {
                    updateNews.IsHome = updateDetail.IsHome;
                }
                else
                {
                    updateNews.IsHome = false;
                }
                if (updateDetail.IsHot != null)
                {
                    updateNews.IsHot = updateDetail.IsHot;
                }
                else
                {
                    updateNews.IsHot = false;
                }

                //save file download
                if (file_download != null)
                {
                    updateNewsFile(updateNews.NewsID, file_download);
                }

                //save
                occSysDB.SaveChanges();
                return RedirectToAction("news_views");
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(updateDetail);
        }

        public void updateNewsImage(int id, HttpPostedFileBase file_image)
        {
            //get slide
            var detail = occSysDB.News.FirstOrDefault(p => p.NewsID.Equals(id));

            //upload image
            string duoiAnh = Path.GetExtension(file_image.FileName);
            string fileName = "anh-tin-tuc-" + detail.NewsID + duoiAnh;
            //create folder
            var newPathImage = Server.MapPath("/Uploaded/Images/tintuc/anh-tin-" + detail.NewsID + "-folder");

            if (!Directory.Exists(newPathImage))
            {
                Directory.CreateDirectory(newPathImage);
            }
            string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));
            //save file to folder
            file_image.SaveAs(fileUrl);

            //get image
            detail.NewsImage = fileName;
            //save action
            occSysDB.SaveChanges();
        }
        public void updateNewsFile(int id, HttpPostedFileBase file_download)
        {
            //get slide
            var detail = occSysDB.News.FirstOrDefault(p => p.NewsID.Equals(id));

            //upload image
            string duoiFile = Path.GetExtension(file_download.FileName);
            string fileNameF = "file-tin-tuc-" + detail.NewsID + duoiFile;
            var newPathImage = Server.MapPath("/Uploaded/Images/tintuc/anh-tin-" + detail.NewsID + "-folder");
            string fileUrlF = Path.Combine(newPathImage, Path.GetFileName(fileNameF));


            //save file to folder
            file_download.SaveAs(fileUrlF);

            //get file
            detail.FileDownload = fileNameF;

            //save action
            occSysDB.SaveChanges();
        }
        #endregion
        #endregion

        /// <summary>
        ///   album Category
        /// </summary>
        /// <returns></returns>
        /// 
        #region album Category
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult albumscate_manager()
        {
            //get list
            var list = occSysDB.AlbumsCategories.OrderBy(p => p.SortOrder).ToList();

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(list);
        }
        [HttpPost]
        public ActionResult albumscate_manager(IEnumerable<int> itemID, string deleteOK, string update, IEnumerable<int> updateID, List<int> SortOrder)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            //get list
            var list = occSysDB.AlbumsCategories.ToList();
            #region delete
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get cate
                    var cate = occSysDB.AlbumsCategories.FirstOrDefault(p => p.AlCateID.Equals(id));

                    //if cate exits
                    if (cate != null)
                    {
                        //get details in cate
                        //var newsInCate = cate.News.ToList();

                        ////get child cate
                        //var childCate = occSysDB.NewsCategories.Where(p => p.ParentID == cate.NewCatID).ToList();

                        ////if details in cate not exits
                        //if (newsInCate.Count == 0)
                        //{
                        //    //check child cate
                        //    if (childCate.Count > 0)
                        //    {
                        //        foreach (var child in childCate)
                        //        {
                        //            //get details of child cate 
                        //            var newsInChilds = child.News.ToList();
                        //            //if details not exits
                        //            if (newsInChilds.Count == 0)
                        //            {
                        //                //delete childe cate
                        //                occSysDB.NewsCategories.Remove(child);
                        //            }
                        //            else
                        //            {
                        //                ViewBag.Error = "Chuyên mục đang được sử dụng !";
                        //                return View(list);
                        //            }
                        //        }
                        //    }
                        //delete cate
                        occSysDB.AlbumsCategories.Remove(cate);
                        //}
                        //else
                        //{
                        //    ViewBag.Error = "Chuyên mục đang được sử dụng !";
                        //    return View(list);
                        //}
                    }

                }
                occSysDB.SaveChanges();
                return RedirectToAction("albumscate_manager");
            }
            #endregion

            #region update order
            if (updateID != null && update == "OK")
            {
                int order = 0;
                foreach (var upID in updateID)
                {
                    var updateCate = occSysDB.AlbumsCategories.FirstOrDefault(p => p.AlCateID.Equals(upID));
                    if (updateCate != null)
                    {
                        updateCate.SortOrder = SortOrder[order];
                    }
                    order++;
                }
                //save update
                occSysDB.SaveChanges();
                return RedirectToAction("albumscate_manager");
            }
            #endregion

            //if something  wrong
            ViewBag.Error = "Chuyên mục đang được sử dụng !";
            return View(list);
        }
        #endregion
        #region add

        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult albumscate_add(int? linkID, int? langID)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            if (linkID != null)
            {
                var linkObj = occSysDB.AlbumsCategories.FirstOrDefault(p => p.AlCateID == linkID);
                if (linkObj != null)
                {
                    ViewBag.LinkObj = linkObj;
                }

            }
            //get all cate
            var allCate = occSysDB.AlbumsCategories.Where(p => p.LinkID == 0).ToList();
            //set order
            if (allCate.Count > 0)
            {
                ViewBag.SortOrder = allCate.Count + 1;
            }
            else
            {
                ViewBag.SortOrder = 1;
            }

            ViewBag.LangAdd = langID;
            return View();
        }
        [HttpPost]
        public ActionResult albumscate_add(AlbumsCategory newCate_Add, string save, string LinkID)
        {
            if (ModelState.IsValid)
            {
                //get all cate
                var allCate = occSysDB.AlbumsCategories.ToList();


                if (newCate_Add.SortOrder != 0)
                {
                    newCate_Add.SortOrder = newCate_Add.SortOrder;
                }
                else
                {
                    //set order
                    if (allCate.Count > 0)
                    {
                        newCate_Add.SortOrder = allCate.Count + 1;
                    }
                    else
                    {
                        newCate_Add.SortOrder = 1;
                    }
                }

                if (newCate_Add.IsActive != null)
                {
                    newCate_Add.IsActive = true;
                }
                else
                {
                    newCate_Add.IsActive = false;
                }



                //link obj
                if (LinkID != null)
                {
                    newCate_Add.LinkID = int.Parse(LinkID);
                }
                else
                {
                    newCate_Add.LinkID = 0;
                }

                //save cate
                occSysDB.AlbumsCategories.Add(newCate_Add);
                occSysDB.SaveChanges();

                //save action
                if (save == "save")
                {
                    return RedirectToAction("albumscate_manager");
                }
                else
                {
                    return RedirectToAction("albumscate_add");
                }
            }
            else
            {
                ViewBag.Error = "Bạn chưa nhập đầy đủ thông tin !";
                //if something wrongs
                //get lang
                var lang = occSysDB.Languages.ToList();
                ViewBag.LangList = lang;

                //get all cate
                var allCate = occSysDB.AlbumsCategories.ToList();
                //set order
                if (allCate.Count > 0)
                {
                    ViewBag.SortOrder = allCate.Count + 1;
                }
                else
                {
                    ViewBag.SortOrder = 1;
                }

                return View(newCate_Add);
            }

        }
        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, edit")]
        public ActionResult albumscate_edit(int id)
        {
            //get detail
            var detail = occSysDB.AlbumsCategories.FirstOrDefault(p => p.AlCateID.Equals(id));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get list des of detail
            if (detail != null)
            {
                if (detail.LinkID == 0)
                {
                    //get list detail link
                    var listLinkObj = occSysDB.AlbumsCategories.Where(p => p.LinkID == detail.AlCateID).ToList();
                    ViewBag.LinkObjs = listLinkObj;
                    ViewBag.LinkObj = null;
                }
                else
                {
                    var linkObj = occSysDB.AlbumsCategories.FirstOrDefault(p => p.AlCateID == detail.LinkID);
                    ViewBag.LinkObj = linkObj;
                    ViewBag.LinkObjs = null;
                }
            }

            return View(detail);
        }
        [HttpPost]
        public ActionResult albumscate_edit(int editID, AlbumsCategory updateNewsCate)
        {
            if (ModelState.IsValid)
            {
                var allCate = occSysDB.AlbumsCategories.ToList();

                //get detail
                var update = occSysDB.AlbumsCategories.FirstOrDefault(p => p.AlCateID.Equals(editID));
                update.AlCateName = updateNewsCate.AlCateName;
                //set parent cate


                update.Lang_ID = updateNewsCate.Lang_ID;

                if (updateNewsCate.SortOrder != 0)
                {
                    update.SortOrder = updateNewsCate.SortOrder;
                }
                else
                {
                    //set order
                    if (allCate.Count > 0)
                    {
                        update.SortOrder = allCate.Count + 1;
                    }
                    else
                    {
                        update.SortOrder = 1;
                    }
                }
                if (updateNewsCate.IsActive != null)
                {
                    update.IsActive = true;
                }
                else
                {
                    update.IsActive = false;
                }
                ////seo
                //update.MetaTitle = updateNewsCate.MetaTitle;
                //update.MetaKeywords = updateNewsCate.MetaKeywords;
                //update.MetaDescription = updateNewsCate.MetaDescription;

                //save action
                occSysDB.SaveChanges();

                return RedirectToAction("albumscate_manager");

            }
            //get detail
            var detail = occSysDB.AlbumsCategories.FirstOrDefault(p => p.AlCateID.Equals(editID));
            ViewBag.Detail = detail;

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(updateNewsCate);
        }

        #endregion
        #endregion

        /// <summary>
        ///   Album 
        /// </summary>
        /// <returns></returns>
        /// 
        #region Album
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult albums_manager()
        {
            //get list
            var list = occSysDB.Albums.ToList();

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;
            return View(list);

        }
        [HttpPost]
        public ActionResult albums_manager(IEnumerable<int> itemID, string deleteOK)
        {
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get detail to del
                    var detailDel = occSysDB.Albums.FirstOrDefault(p => p.AlbumID == id);

                    if (detailDel != null)
                    {
                        //get imges of detail
                        var imagesDel = detailDel.AlbumImages.ToList();

                        //delete images
                        foreach (var imgDel in imagesDel)
                        {
                            occSysDB.AlbumImages.Remove(imgDel);
                        }

                        //delete detail
                        occSysDB.Albums.Remove(detailDel);
                        occSysDB.SaveChanges();

                    }
                }

                occSysDB.SaveChanges();

                return RedirectToAction("albums_manager");
            }

            return RedirectToAction("albums_manager");
        }
        #endregion
        #region add
        //Dropdown List
        public ActionResult _events_album_add(int? langID, int? linkCateID)
        {
            var list = occSysDB.Events.Where(p => p.Language.IsDefault == true).ToList();
            if (langID != null)
            {
                list = occSysDB.Events.Where(p => p.Lang_ID == langID).ToList();
            }
            ViewBag.LinkCateID = linkCateID;
            return PartialView(list);
        }
        public ActionResult _album_albumcate_add(int? langID, int? linkCateID)
        {
            var list = occSysDB.AlbumsCategories.Where(p => p.Language.IsDefault == true).ToList();
            if (langID != null)
            {
                list = occSysDB.AlbumsCategories.Where(p => p.Lang_ID == langID).ToList();
            }
            ViewBag.LinkCateID = linkCateID;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult albums_add()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult albums_add(Album newAlbumDes, FormCollection form, HttpPostedFileBase file_image, IEnumerable<HttpPostedFileBase> file_images, string save)
        {
            if (ModelState.IsValid)
            {
                if (file_image != null)
                {
                    //set new
                    Album album = new Album();
                    album.CreateDate = DateTime.Now.Date;

                    album.AlbumName = newAlbumDes.AlbumName;
                    album.Description = newAlbumDes.Description;
                    album.Lang_ID = newAlbumDes.Lang_ID;
                    album.EventID = newAlbumDes.EventID;
                    album.AlCateID = newAlbumDes.AlCateID;

                    if (form.AllKeys.Contains("IsActive"))
                    {
                        album.IsActive = true;
                    }
                    else
                    {
                        album.IsActive = false;
                    }
                    //save album
                    occSysDB.Albums.Add(album);
                    occSysDB.SaveChanges();

                    //folder
                    album.Folder = "anh-album-" + album.AlbumID + "-folder";

                    //upload image
                    string duoiAnh = Path.GetExtension(file_image.FileName);
                    string fileName = "anh-dai-dien-album-" + album.AlbumID + duoiAnh;
                    //create folder
                    var newPathImage = Server.MapPath("/Uploaded/Images/albums/" + album.Folder);
                    string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));

                    //create folder
                    if (!Directory.Exists(newPathImage))
                    {
                        Directory.CreateDirectory(newPathImage);
                    }

                    //save file to folder
                    file_image.SaveAs(fileUrl);
                    //add to album
                    album.Image = fileName;

                    //upload image in album
                    if (file_images != null && file_images.FirstOrDefault() != null)
                    {
                        foreach (var img in file_images)
                        {
                            //set new
                            AlbumImage newImg = new AlbumImage();
                            newImg.Folder = album.Folder;
                            newImg.AlbumID = album.AlbumID;

                            //add to DB
                            occSysDB.AlbumImages.Add(newImg);
                            occSysDB.SaveChanges();

                            //upload image
                            string duoiAnhs = Path.GetExtension(img.FileName);
                            string fileNames = "anh-album-" + album.AlbumID + "-" + newImg.ImgID + duoiAnhs;
                            //create folder
                            string fileUrls = Path.Combine(newPathImage, Path.GetFileName(fileNames));

                            //save
                            img.SaveAs(fileUrls);
                            newImg.ImageName = fileNames;
                            occSysDB.SaveChanges();
                        }
                    }

                    //save action
                    occSysDB.SaveChanges();

                    if (save == "save")
                    {
                        return RedirectToAction("albums_manager");
                    }
                    else
                    {
                        return RedirectToAction("albums_add");
                    }

                }
                else
                {
                    ViewBag.Mess = "Bạn chưa up ảnh đại diện";
                }
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(newAlbumDes);
        }
        #endregion
        #region edit
        //Dropdown List
        public ActionResult _events_album_edit(int? newCateID, int? langID)
        {
            var list = occSysDB.Events.Where(p => p.Lang_ID == langID).ToList();
            ViewBag.CateID = newCateID;
            return PartialView(list);
        }
        //Dropdown List
        public ActionResult _album_albumcate_edit(int? newCateID, int? langID)
        {
            var list = occSysDB.AlbumsCategories.Where(p => p.Lang_ID == langID).ToList();
            ViewBag.CateID = newCateID;
            return PartialView(list);
        }
        [Authorize(Roles = "superadmin , admin, edit, create")]
        public ActionResult albums_edit(int id)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get detail
            var detail = occSysDB.Albums.FirstOrDefault(p => p.AlbumID == id);

            //get image
            var images = detail.AlbumImages.ToList();
            ViewBag.Images = images;

            return View(detail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult albums_edit(int editID, Album albumDes, IEnumerable<HttpPostedFileBase> file_images, FormCollection form)
        {
            if (ModelState.IsValid)
            {
                //get update
                var album = occSysDB.Albums.FirstOrDefault(p => p.AlbumID == editID);

                album.AlbumName = albumDes.AlbumName;
                album.Description = albumDes.Description;
                album.EventID = albumDes.EventID;
                album.AlCateID = albumDes.AlCateID;

                if (form.AllKeys.Contains("IsActive"))
                {
                    album.IsActive = true;
                }
                else
                {
                    album.IsActive = false;
                }


                //upload image in album
                if (file_images != null && file_images.FirstOrDefault() != null)
                {
                    foreach (var img in file_images)
                    {
                        //set new
                        AlbumImage newImg = new AlbumImage();
                        newImg.Folder = album.Folder;
                        newImg.AlbumID = album.AlbumID;

                        //add to DB
                        occSysDB.AlbumImages.Add(newImg);
                        occSysDB.SaveChanges();

                        //upload image
                        string duoiAnhs = Path.GetExtension(img.FileName);
                        string fileNames = "anh-album-" + album.AlbumID + "-" + newImg.ImgID + duoiAnhs;
                        //create folder
                        var newPathImage = Server.MapPath("/Uploaded/Images/albums/" + album.Folder);
                        string fileUrls = Path.Combine(newPathImage, Path.GetFileName(fileNames));

                        //save
                        img.SaveAs(fileUrls);
                        newImg.ImageName = fileNames;

                    }
                }

                //save action
                occSysDB.SaveChanges();
                return RedirectToAction("albums_manager");
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get detail
            var detail = occSysDB.Albums.FirstOrDefault(p => p.AlbumID == editID);
            ViewBag.Detail = detail;

            //get image
            var images = detail.AlbumImages.ToList();
            ViewBag.Images = images;

            return View(albumDes);
        }
        //update image album
        public void updateImageAlbum(int id, HttpPostedFileBase file_image)
        {
            //get slide
            var detail = occSysDB.Albums.FirstOrDefault(p => p.AlbumID.Equals(id));
            //upload image
            string duoiAnh = Path.GetExtension(file_image.FileName);
            string fileName = "anh-dai-dien-album-" + detail.AlbumID + duoiAnh;
            //create folder
            var newPathImage = Server.MapPath("/Uploaded/Images/albums/" + detail.Folder);
            string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));

            //save file to folder
            file_image.SaveAs(fileUrl);
            //add to album
            detail.Image = fileName;
            //save action
            occSysDB.SaveChanges();
        }
        //update image album
        public void updateImageInAlbum(int id, HttpPostedFileBase file_images)
        {
            //get slide
            var detail = occSysDB.Albums.FirstOrDefault(p => p.AlbumID.Equals(id));

            //set new
            AlbumImage newImg = new AlbumImage();
            newImg.Folder = detail.Folder;
            newImg.AlbumID = detail.AlbumID;

            //add to DB
            occSysDB.AlbumImages.Add(newImg);
            occSysDB.SaveChanges();

            //upload image
            string duoiAnhs = Path.GetExtension(file_images.FileName);
            string fileNames = "anh-album-" + detail.AlbumID + "-" + newImg.ImgID + duoiAnhs;
            //create folder
            var newPathImage = Server.MapPath("/Uploaded/Images/albums/" + detail.Folder);
            string fileUrls = Path.Combine(newPathImage, Path.GetFileName(fileNames));

            //save
            file_images.SaveAs(fileUrls);
            newImg.ImageName = fileNames;

            //save action
            occSysDB.SaveChanges();
        }
        //delete image album
        public void deleteImageInAlbum(int id, string[] fileNames)
        {
            foreach (var fullName in fileNames)
            {
                string tenanh = fullName.Substring(fullName.LastIndexOf("/")).Replace("/", "");

                var imageToDelete = occSysDB.AlbumImages.FirstOrDefault(p => p.AlbumID == id && p.ImageName == tenanh);

                if (imageToDelete != null)
                {
                    var path = Server.MapPath("/Uploaded/Images/albums/" + imageToDelete.Folder);
                    string fileUrl = Path.Combine(path, Path.GetFileName(imageToDelete.ImageName));

                    if (System.IO.File.Exists(fileUrl))
                    {
                        System.IO.File.Delete(fileUrl);
                    }
                    occSysDB.AlbumImages.Remove(imageToDelete);
                    //save action
                    occSysDB.SaveChanges();
                }
            }

        }
        #endregion

        #endregion


        /// <summary>
        ///   Adv 
        /// </summary>
        /// <returns></returns>
        /// 
        #region Adv
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult advs_manager()
        {
            //get list
            var list = occSysDB.Advertisements.ToList();
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(list);
        }
        [HttpPost]
        public ActionResult advs_manager(IEnumerable<int> itemID, string deleteOK)
        {
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    //get del
                    var advDel = occSysDB.Advertisements.FirstOrDefault(p => p.AdvID == id);

                    if (advDel != null)
                    {
                        //del detail
                        occSysDB.Advertisements.Remove(advDel);
                        occSysDB.SaveChanges();
                    }
                }
            }

            return RedirectToAction("advs_manager");
        }
        #endregion
        #region add
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult advs_add()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult advs_add(Advertisement newAdvDes, string date_accept, FormCollection form, HttpPostedFileBase file_image, string save, string LinkID)
        {
            if (ModelState.IsValid)
            {
                if (file_image != null)
                {
                    //set new
                    Advertisement newAdv = new Advertisement();

                    newAdv.AdvTitle = newAdvDes.AdvTitle;
                    newAdv.AdvSlogan = newAdvDes.AdvSlogan;
                    newAdv.AdvShortDescription = newAdvDes.AdvShortDescription;

                    newAdv.AdvID = newAdv.AdvID;
                    newAdv.Lang_ID = newAdvDes.Lang_ID;

                    if (LinkID != null)
                    {
                        newAdv.LinkID = int.Parse(LinkID);
                    }
                    else
                    {
                        newAdv.LinkID = 0;
                    }

                    if (date_accept != null)
                    {
                        string[] date = date_accept.Split('-');
                        string startDate = date[0].Trim();
                        string endDate = date[1].Trim();

                        newAdv.StartDate = DateTime.ParseExact(startDate, "dd/MM/yyyy", null);
                        newAdv.EndDate = DateTime.ParseExact(endDate, "dd/MM/yyyy", null);
                    }
                    else
                    {
                        newAdv.StartDate = DateTime.Now.Date;
                        newAdv.EndDate = DateTime.Now.Date;
                    }


                    if (form.AllKeys.Contains("IsActive"))
                    {
                        newAdv.IsActive = true;
                    }
                    else
                    {
                        newAdv.IsActive = false;
                    }

                    if (form.AllKeys.Contains("AdvLink"))
                    {
                        newAdv.AdvLink = form["AdvLink"].ToString();
                    }


                    //add to DB
                    occSysDB.Advertisements.Add(newAdv);
                    //save
                    occSysDB.SaveChanges();

                    //upload image
                    string duoiAnhs = Path.GetExtension(file_image.FileName);
                    string fileNames = "anh-qc-" + newAdv.AdvID + duoiAnhs;
                    //create folder
                    var newPathImage = Server.MapPath("/Uploaded/Images/Advs");
                    string fileUrls = Path.Combine(newPathImage, Path.GetFileName(fileNames));

                    //create folder
                    if (!Directory.Exists(newPathImage))
                    {
                        Directory.CreateDirectory(newPathImage);
                    }
                    //save
                    file_image.SaveAs(fileUrls);
                    newAdv.AdvImage = fileNames;


                    //save action
                    occSysDB.SaveChanges();

                    if (save == "save")
                    {
                        return RedirectToAction("advs_manager");
                    }
                    else
                    {
                        return RedirectToAction("advs_add");
                    }

                }
                else
                {
                    ViewBag.Mess = "Bạn chưa up ảnh";
                }

            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(newAdvDes);
        }
        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, edit, create")]
        public ActionResult advs_edit(int id)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get detail
            var detail = occSysDB.Advertisements.FirstOrDefault(p => p.AdvID == id);

            return View(detail);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult advs_edit(int editID, Advertisement updateAdvDes, FormCollection form, string date_accept)
        {

            if (ModelState.IsValid)
            {
                //set update
                var adv = occSysDB.Advertisements.FirstOrDefault(p => p.AdvID == editID);
                adv.AdvTitle = updateAdvDes.AdvTitle;
                adv.AdvSlogan = updateAdvDes.AdvSlogan;
                adv.AdvShortDescription = updateAdvDes.AdvShortDescription;
                if (date_accept != null)
                {
                    string[] date = date_accept.Split('-');
                    string startDate = date[0].Trim();
                    string endDate = date[1].Trim();

                    adv.StartDate = DateTime.ParseExact(startDate, "dd/MM/yyyy", null);
                    adv.EndDate = DateTime.ParseExact(endDate, "dd/MM/yyyy", null);
                }
                else
                {
                    adv.StartDate = DateTime.Now.Date;
                    adv.EndDate = DateTime.Now.Date;
                }


                if (form.AllKeys.Contains("IsActive"))
                {
                    adv.IsActive = true;
                }
                else
                {
                    adv.IsActive = false;
                }

                if (form.AllKeys.Contains("AdvLink"))
                {
                    adv.AdvLink = form["AdvLink"].ToString();
                }
                //save action
                occSysDB.SaveChanges();


                return RedirectToAction("advs_manager");
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get detail
            var detail = occSysDB.Advertisements.FirstOrDefault(p => p.AdvID == editID);
            ViewBag.Detail = detail;

            return View(updateAdvDes);
        }
        //update image album
        public void updateImageAdv(int id, HttpPostedFileBase file_image)
        {
            //get slide
            var adv = occSysDB.Advertisements.FirstOrDefault(p => p.AdvID.Equals(id));

            //upload image
            string duoiAnhs = Path.GetExtension(file_image.FileName);
            string fileNames = "anh-qc-" + adv.AdvID + duoiAnhs;
            //create folder
            var newPathImage = Server.MapPath("/Uploaded/Images/Advs");
            string fileUrls = Path.Combine(newPathImage, Path.GetFileName(fileNames));
            //save
            file_image.SaveAs(fileUrls);
            adv.AdvImage = fileNames;

            //save action
            occSysDB.SaveChanges();
        }

        #endregion
        #endregion

        /// <summary>
        /// File download
        /// </summary>
        /// <returns></returns>
        #region File Download
        #region view
        [Authorize(Roles = "superadmin , admin, read, edit, create")]
        public ActionResult files_manager()
        {
            //get list
            var list = occSysDB.FileDownloads.ToList();
            return View(list);
        }

        [HttpPost]
        public ActionResult files_manager(IEnumerable<int> itemID, string deleteOK, string update, IEnumerable<int> updateID, List<int> SortOrder)
        {
            //delete files
            if (itemID != null && deleteOK == "OK")
            {
                foreach (var id in itemID)
                {
                    var delete = occSysDB.FileDownloads.FirstOrDefault(p => p.FileID == id);
                    if (delete != null)
                    {
                        occSysDB.FileDownloads.Remove(delete);
                    }
                }
                occSysDB.SaveChanges();
            }
            //update sort order
            if (update == "OK" && updateID != null)
            {
                int order = 0;
                foreach (var uid in updateID)
                {
                    var updateF = occSysDB.FileDownloads.FirstOrDefault(p => p.FileID == uid);
                    if (updateF != null)
                    {
                        updateF.SortOrder = SortOrder[order];

                    }
                    order++;
                }
                occSysDB.SaveChanges();
            }

            return RedirectToAction("files_manager");
        }

        #endregion
        #region add
        [Authorize(Roles = "superadmin , admin, create")]
        public ActionResult files_add()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult files_add(FileDownload newDetail, HttpPostedFileBase file_download, string save)
        {

            if (ModelState.IsValid)
            {
                if (file_download != null)
                {
                    //get all
                    var all = occSysDB.FileDownloads.ToList();
                    //add file
                    FileDownload newFile = new FileDownload();
                    newFile.FileTitle = newDetail.FileTitle;
                    newFile.FileDescription = newDetail.FileDescription;
                    newFile.Lang_ID = newDetail.Lang_ID;
                    newFile.IsActive = newDetail.IsActive;
                    newFile.CreateDate = DateTime.Now;

                    //set order
                    if (all.Count > 0)
                    {
                        newFile.SortOrder = all.Count + 1;
                    }
                    else
                    {
                        newFile.SortOrder = 1;
                    }

                    //add to DB
                    occSysDB.FileDownloads.Add(newFile);
                    //save first
                    occSysDB.SaveChanges();

                    //upload image
                    var newPathImage = Server.MapPath("/Uploaded/Files/file-download");

                    string duoiFile = Path.GetExtension(file_download.FileName);
                    string fileNameF = OccConvert.KhongDau(newFile.FileTitle) + "-" + newFile.FileID + duoiFile;
                    string fileUrlF = Path.Combine(newPathImage, Path.GetFileName(fileNameF));

                    //create folder
                    if (!Directory.Exists(newPathImage))
                    {
                        Directory.CreateDirectory(newPathImage);
                    }

                    //save file to folder
                    file_download.SaveAs(fileUrlF);

                    //get file
                    newFile.FileName = fileNameF;

                    //save
                    occSysDB.SaveChanges();

                    if (save == "save")
                    {
                        return RedirectToAction("files_manager");
                    }
                    else
                    {
                        return RedirectToAction("files_add");
                    }

                }
                else
                {
                    ViewBag.Mess = "Bạn chưa upload file";
                }
            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(newDetail);
        }

        #endregion
        #region edit
        [Authorize(Roles = "superadmin , admin, create, create, edit")]
        public ActionResult files_edit(int id)
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            //get detail
            var detail = occSysDB.FileDownloads.FirstOrDefault(p => p.FileID == id);

            return View(detail);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult files_edit(int editID, FileDownload updateDetail, HttpPostedFileBase file_download)
        {
            if (ModelState.IsValid)
            {

                //get all
                var all = occSysDB.FileDownloads.Where(p => p.FileID != editID).ToList();
                //add file
                var updateFile = occSysDB.FileDownloads.FirstOrDefault(p => p.FileID == editID);
                updateFile.FileTitle = updateDetail.FileTitle;
                updateFile.FileDescription = updateDetail.FileDescription;
                updateFile.Lang_ID = updateDetail.Lang_ID;
                updateFile.IsActive = updateDetail.IsActive;

                if (updateDetail.SortOrder > 0)
                {
                    updateFile.SortOrder = updateDetail.SortOrder;
                }
                else
                {
                    //set order
                    if (all.Count > 0)
                    {
                        updateFile.SortOrder = all.Count + 1;
                    }
                    else
                    {
                        updateFile.SortOrder = 1;
                    }
                }

                if (file_download != null)
                {
                    //upload image
                    var newPathImage = Server.MapPath("/Uploaded/Files/file-download");

                    string duoiFile = Path.GetExtension(file_download.FileName);
                    string fileNameF = OccConvert.KhongDau(updateFile.FileTitle) + "-" + updateFile.FileID + duoiFile;
                    string fileUrlF = Path.Combine(newPathImage, Path.GetFileName(fileNameF));

                    //create folder
                    if (!Directory.Exists(newPathImage))
                    {
                        Directory.CreateDirectory(newPathImage);
                    }

                    //save file to folder
                    file_download.SaveAs(fileUrlF);

                    //get file
                    updateFile.FileName = fileNameF;
                }


                //save
                occSysDB.SaveChanges();


                return RedirectToAction("files_manager");

            }

            //get lang
            var lang = occSysDB.Languages.ToList();
            ViewBag.LangList = lang;

            return View(updateDetail);
        }

        public void updateFile(int id, HttpPostedFileBase file_download)
        {
            //get slide
            var detail = occSysDB.FileDownloads.FirstOrDefault(p => p.FileID.Equals(id));

            //upload image
            var newPathImage = Server.MapPath("/Uploaded/Files/file-download");

            string duoiFile = Path.GetExtension(file_download.FileName);
            string fileNameF = OccConvert.KhongDau(detail.FileTitle) + "-" + detail.FileID + duoiFile;
            string fileUrlF = Path.Combine(newPathImage, Path.GetFileName(fileNameF));
            //save file to folder
            file_download.SaveAs(fileUrlF);

            //get file
            detail.FileName = fileNameF;

            //save action
            occSysDB.SaveChanges();
        }
        #endregion
        #endregion

        /// <summary>
        /// upload function
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        #region upload
        [HttpPost]
        public JsonResult uploadImageNews(HttpPostedFileBase file)
        {
            // TODO: your validation goes here, 
            // eg: file != null && file.ContentType.StartsWith("image/") etc...
            string stringUrl = "";
            if (file != null)
            {
                //upload image
                string fileNameF = file.FileName;
                var newPathImage = Server.MapPath("/Uploaded/Images/anh-noi-dung");
                var imageUrl = Path.Combine(newPathImage, Path.GetFileName(fileNameF));
                //save file to folder
                file.SaveAs(imageUrl);

                stringUrl = "/Uploaded/Images/anh-noi-dung/" + fileNameF;

                return Json(stringUrl);
            }

            return Json(stringUrl);
        }
        #endregion

    }
}
