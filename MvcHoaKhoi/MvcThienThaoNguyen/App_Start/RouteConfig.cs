﻿
using MvcThienThaoNguyen.Models;
using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MvcThienThaoNguyen
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //su kien
            routes.MapRoute(
              name: "event all",
              url: "su-kien.html",
              defaults: new { controller = "events", action = "index", name = UrlParameter.Optional }
            );

            routes.MapRoute(
        name: "event all by date",
        url: "su-kien/ngay-dien-ra/{name}.html",
        defaults: new { controller = "events", action = "view_by_date", name = UrlParameter.Optional }
      );

            routes.MapRoute(
              name: "event mo",
              url: "get_more_girl_events",
              defaults: new { controller = "common", action = "_get_girl_more", name = UrlParameter.Optional }
            );
            routes.MapRoute(
              name: "event moss",
              url: "get_more_news",
              defaults: new { controller = "news", action = "_get_news_more", name = UrlParameter.Optional }
            );
            routes.MapRoute(
              name: "event mosssa",
              url: "get_more_album",
              defaults: new { controller = "albums", action = "_get_album_more", name = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "event news",
                url: "tin-ben-le.html",
                defaults: new { controller = "events", action = "view_news_by_event", name = UrlParameter.Optional }
              );

            routes.MapRoute(
               name: "event news detail",
               url: "tin-ben-le/{name}.html",
               defaults: new { controller = "events", action = "view_news_detail_by_event", name = UrlParameter.Optional }
             );

            routes.MapRoute(
                name: "event girl",
                url: "thi-sinh-du-thi.html",
                defaults: new { controller = "events", action = "view_girls_by_event", name = UrlParameter.Optional }
              );
            routes.MapRoute(
              name: "event girl detail",
              url: "thi-sinh/{name}.html",
              defaults: new { controller = "events", action = "view_girls_detail_by_event", name = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "event album",
                url: "anh-su-kien.html",
                defaults: new { controller = "events", action = "view_albums_by_event", name = UrlParameter.Optional }
              );
            routes.MapRoute(
          name: "event album detail",
          url: "anh-su-kien/xem/{name}.html",
          defaults: new { controller = "events", action = "view_albums_detail_by_event", name = UrlParameter.Optional }
        );


            routes.MapRoute(
                name: "event clip",
                url: "clip-su-kien.html",
                defaults: new { controller = "events", action = "view_clips_by_event", name = UrlParameter.Optional }
              );

            routes.MapRoute(
            name: "event clip detail",
            url: "clip-su-kien/xem/{name}.html",
            defaults: new { controller = "events", action = "view_clips_detail_by_event", name = UrlParameter.Optional }
          );

            routes.MapRoute(
                name: "event dk",
                url: "dang-ky.html",
                defaults: new { controller = "events", action = "register_event", name = UrlParameter.Optional }
              );

            routes.Add(new EventRoute());


            routes.MapRoute(
              name: "chitiet",
              url: "{controller}/chi-tiet/{name}.html",
              defaults: new { controller = "home", action = "details", name = UrlParameter.Optional }
            );

            //lien he
            routes.MapRoute(
              name: "lien he",
              url: "lien-he.html",
              defaults: new { controller = "contacts", action = "index", id = UrlParameter.Optional }
            );
            //gioi thieu
            routes.MapRoute(
              name: "AboutUs",
              url: "gioi-thieu.html",
              defaults: new { controller = "aboutus", action = "index", id = UrlParameter.Optional }
            );
            //gioi thieu
            routes.MapRoute(
              name: "dich vu",
               url: "dich-vu-xe/{name}.html",
              defaults: new { controller = "carservices", action = "view_detail", name = UrlParameter.Optional }
            );
            //bang gia xe
            routes.MapRoute(
              name: "gia xe",
              url: "bang-gia-xe.html",
              defaults: new { controller = "carprices", action = "index", name = UrlParameter.Optional }
            );
            //con nguoi
            routes.MapRoute(
              name: "People",
              url: "con-nguoi.html",
              defaults: new { controller = "ourpeoples", action = "index", name = UrlParameter.Optional }
            );
            routes.MapRoute(
              name: "People cate",
              url: "con-nguoi/chuc-vu/{name}.html",
              defaults: new { controller = "ourpeoples", action = "view_by_cate", name = UrlParameter.Optional }
            );

            routes.MapRoute(
             name: "nhan vien",
             url: "con-nguoi/nhan-vien/{name}.html",
             defaults: new { controller = "ourpeoples", action = "view_detail", name = UrlParameter.Optional }
           );
            //cac loai xe
            routes.MapRoute(
              name: "xe",
              url: "cac-loai-xe.html",
              defaults: new { controller = "ourcars", action = "index", name = UrlParameter.Optional }
            );
            routes.MapRoute(
              name: "xe cate",
              url: "cac-loai-xe/loai-xe/{name}.html",
              defaults: new { controller = "ourcars", action = "view_by_cate", name = UrlParameter.Optional }
            );

            routes.MapRoute(
             name: "xe chi tiewt",
             url: "cac-loai-xe/chi-tiet-xe/{name}.html",
             defaults: new { controller = "ourcars", action = "view_detail", name = UrlParameter.Optional }
           );

            //tin tuc
            routes.MapRoute(
              name: "tin tuc",
              url: "tin-tuc.html",
              defaults: new { controller = "news", action = "index", name = UrlParameter.Optional }
            );
            routes.MapRoute(
              name: "tin cate",
              url: "tin-tuc/chuyen-muc/{name}.html",
              defaults: new { controller = "news", action = "view_by_cate", name = UrlParameter.Optional }
            );

            routes.MapRoute(
             name: "tin chi tiewt",
             url: "tin-tuc/noi-dung/{name}.html",
             defaults: new { controller = "news", action = "view_detail", name = UrlParameter.Optional }
           );

            //clip
            routes.MapRoute(
              name: "clip",
              url: "clip.html",
              defaults: new { controller = "clips", action = "index", name = UrlParameter.Optional }
            );
            routes.MapRoute(
              name: "clip cate",
              url: "clip/chuyen-muc/{name}.html",
              defaults: new { controller = "clips", action = "view_by_cate", name = UrlParameter.Optional }
            );

            routes.MapRoute(
             name: "clip chi tiewt",
             url: "clip/xem/{name}.html",
             defaults: new { controller = "clips", action = "view_detail", name = UrlParameter.Optional }
           );

            //album
            routes.MapRoute(
              name: "album",
              url: "thu-vien-anh.html",
              defaults: new { controller = "albums", action = "index", name = UrlParameter.Optional }
            );
            routes.MapRoute(
              name: "album cate",
              url: "thu-vien-anh/nhom/{name}.html",
              defaults: new { controller = "albums", action = "view_by_cate", name = UrlParameter.Optional }
            );

            routes.MapRoute(
             name: "album chi tiewt",
             url: "thu-vien-anh/xem/{name}.html",
             defaults: new { controller = "albums", action = "view_detail", name = UrlParameter.Optional }
           );

            //hoi dsap
            routes.MapRoute(
                 name: "hoi dap",
                 url: "hoi-dap.html",
                 defaults: new { controller = "faqs", action = "index", name = UrlParameter.Optional }
             );

            //bookcars
            routes.MapRoute(
                 name: "bookcars",
                 url: "dat-xe.html",
                 defaults: new { controller = "bookcars", action = "index", name = UrlParameter.Optional }
             );
            //orther
            routes.MapRoute(
                 name: "danhmuc",
                 url: "{controller}/danh-muc/{name}.html",
                 defaults: new { controller = "home", action = "viewcat", name = UrlParameter.Optional }
             );



            routes.MapRoute(
                   name: "Default ID",
                   url: "{controller}/{action}/{id}",
                   defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
               );

            routes.MapRoute(
                  name: "Default Name",
                  url: "{controller}/{action}/{name}",
                  defaults: new { controller = "Home", action = "Index", name = UrlParameter.Optional }
              );
        }

    }
}