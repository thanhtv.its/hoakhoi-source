﻿using Insya.Localization;
using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcWebsiteOcc.Controllers
{
    public class aboutusController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();
        string langCode = Localization.Get("lng").ToLower();

        public ActionResult index()
        {
            //get detail
            var detail = occSysDB.AboutUs.FirstOrDefault();
            //get detail dé in lang
            ViewBag.DetailInLang = detail.AboutUs_Descriptions.FirstOrDefault(p => p.Language.Lang_Code == langCode);
            //Image of detail
            ViewBag.ImageAbouts = occSysDB.AboutUs_Achievements.ToList();
            //orientation of company
            ViewBag.Oriens = occSysDB.Orientations.Where(p => p.Language.Lang_Code == langCode).OrderBy(p => p.SortOrder).ToList();
            //Director
            var staffs =  occSysDB.Staffs.Where(p => p.Language.Lang_Code == langCode && p.StaffViews.Where(a => a.ViewType == 1).Count() > 0).ToList();
            ViewBag.StaffInDirs = staffs;
            //COn tact
            ViewBag.Contacts = occSysDB.AddressBooks.Where(p => p.Language.Lang_Code == langCode).OrderBy(p => p.SortOrder).ToList();

            //return view
            return View(detail);
        }

    }
}
