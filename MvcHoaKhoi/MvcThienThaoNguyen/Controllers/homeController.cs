﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcWebsiteOcc.Controllers
{
    public class homeController : Controller
    {
        [AllowAnonymous]
        public ActionResult index()
        {
            return View();
        }

        [Authorize]
        public ActionResult myAccount()
        {
            return View();
        }

        [Authorize(Roles="admin")]
        public ActionResult admin()
        {
            return View();
        }


        [Authorize(Roles = "user")]
        public ActionResult userIndex()
        {
            return View();
        }


    }
}
