﻿using Insya.Localization;
using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Text.RegularExpressions;

namespace MvcWebsiteOcc.Controllers
{
    public class ourpeoplesController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();
        string langCode = Localization.Get("lng").ToLower();


        public ActionResult index(int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var list = occSysDB.Staffs.Where(p => p.Language.Lang_Code == langCode).ToList();
            return View(list.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult view_by_cate(string name, int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            var list = occSysDB.Staffs.Where(p => p.Language.Lang_Code == langCode).ToList();

            if (name == null)
            {
                return View(list.ToPagedList(pageNumber, pageSize));
            }
            else
            {
                int a = name.LastIndexOf("-");
                string title = name.Substring(a).Replace("-", "");
                string b = Regex.Match(title, @"\d+").Value;
                int id = int.Parse(b);
                var chucvus = occSysDB.Positions.FirstOrDefault(p => p.PosID == id && p.Language.Lang_Code == langCode);
                list = chucvus.Staffs.ToList();

                return View(list.ToPagedList(pageNumber, pageSize));
            }
        }

        public ActionResult view_detail(string name)
        {
            int a = name.LastIndexOf("-");
            string title = name.Substring(a).Replace("-", "");
            string b = Regex.Match(title, @"\d+").Value;
            int id = int.Parse(b);
            var viewDetail = occSysDB.Staffs.FirstOrDefault(p => p.StaffID == id);
            return View(viewDetail);

        }

        public ActionResult _orther_staffs(int? staffID)
        {
            var ortherStaffs = occSysDB.Staffs.Where(p => p.StaffID != staffID && p.Language.Lang_Code == langCode).OrderBy(p => Guid.NewGuid()).Take(8).ToList();
            return PartialView(ortherStaffs);
        }

    }
}
