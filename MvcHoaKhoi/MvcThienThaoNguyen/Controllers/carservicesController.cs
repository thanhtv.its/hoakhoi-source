﻿using Insya.Localization;
using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Text.RegularExpressions;

namespace MvcWebsiteOcc.Controllers
{
    public class carservicesController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();
        string langCode = Localization.Get("lng").ToLower();

        public ActionResult view_detail(string name)
        {
            //get id
            int a = name.LastIndexOf("-");
            string title = name.Substring(a).Replace("-", "");
            string b = Regex.Match(title, @"\d+").Value;
            int id = int.Parse(b);
            //get service by ID
            var viewDetail = occSysDB.CarServices.FirstOrDefault(p => p.ServID == id);
            //return view
            return View(viewDetail);

        }

        public ActionResult _orther_service(int id)
        {
            //get orther services 
            var view = occSysDB.CarServices.OrderBy(p => p.SortOrder).Where(p => p.ServID != id && p.Language.Lang_Code == langCode).ToList();
            //return view
            return PartialView(view);
        }

    }
}
