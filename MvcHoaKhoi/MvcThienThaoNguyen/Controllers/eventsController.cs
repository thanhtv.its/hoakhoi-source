﻿using Insya.Localization;
using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Text.RegularExpressions;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using MvcWebsiteOcc.Areas.administrators.Models;
using reCAPTCHA.MVC;
using System.IO;

namespace MvcWebsiteOcc.Controllers
{
    public class eventsController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();
        string langCode = Localization.Get("lng").ToLower();

        public ActionResult index(int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            var list = occSysDB.Events.Where(p => p.Language.Lang_Code == langCode && p.IsActive == true).OrderByDescending(p => p.TimeStart).ToList();
            //if (list.Count == 0)
            //{
            //    list = occSysDB.News.ToList();
            //}

            return View(list.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult view_by_date(string name, int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            var list = occSysDB.Events.Where(p => p.Language.Lang_Code == langCode && p.IsActive == true).OrderByDescending(p => p.TimeStart).ToList();

            //if (list.Count == 0)
            //{
            //    list = occSysDB.News.ToList();
            //}

            if (name == null)
            {
                return View(list.ToPagedList(pageNumber, pageSize));
            }
            else
            {
                int a = name.LastIndexOf("-");
                string title = name.Substring(a).Replace("-", "");
                string b = Regex.Match(title, @"\d+").Value;

                string one = b.Insert(2, "/");
                string two = one.Insert(5, "/");


                DateTime date = DateTime.ParseExact(two, "dd/MM/yyyy", null);

                //var cates = occSysDB.AlbumsCategories.FirstOrDefault(p => p.AlCateID == id);

                list = occSysDB.Events.Where(p => p.TimeStart == date && p.IsActive == true).OrderBy(p => p.SortOrder).ToList();


                //if (list.Count == 0)
                //{
                //    list = occSysDB.News.ToList();
                //}

                return View(list.ToPagedList(pageNumber, pageSize));
            }
        }


        public ActionResult view_by_cate(string name, int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            var list = occSysDB.Albums.Where(p => p.Language.Lang_Code == langCode && p.IsActive == true).OrderBy(p => p.SortOrder).ToList();

            //if (list.Count == 0)
            //{
            //    list = occSysDB.News.ToList();
            //}

            if (name == null)
            {
                return View(list.ToPagedList(pageNumber, pageSize));
            }
            else
            {
                int a = name.LastIndexOf("-");
                string title = name.Substring(a).Replace("-", "");
                string b = Regex.Match(title, @"\d+").Value;
                int id = int.Parse(b);
                var cates = occSysDB.AlbumsCategories.FirstOrDefault(p => p.AlCateID == id);


                if (cates != null)
                {
                    list = occSysDB.Albums.Where(p => p.AlCateID == cates.AlCateID && p.IsActive == true).OrderBy(p => p.SortOrder).ToList();
                }
                else
                {
                    list = new List<Areas.administrators.Models.Album>();
                }

                //if (list.Count == 0)
                //{
                //    list = occSysDB.News.ToList();
                //}

                return View(list.ToPagedList(pageNumber, pageSize));
            }
        }

        public ActionResult view_detail(string name, string subdomain)
        {
            var viewDetail = occSysDB.Events.FirstOrDefault();
            if (name != null)
            {
                int a = name.LastIndexOf("-");
                string title = name.Substring(a).Replace("-", "");
                string b = Regex.Match(title, @"\d+").Value;
                int id = int.Parse(b);

                viewDetail = occSysDB.Events.FirstOrDefault(p => p.EventID == id);
            }

            if (subdomain != null)
            {
                viewDetail = occSysDB.Events.FirstOrDefault(p => p.Subdomain == subdomain);
                ViewBag.Folder = viewDetail.Folder;
                ViewBag.Logo = viewDetail.EventLogo;
                ViewBag.BG = viewDetail.BackGround;
            }
            var thisinhs = viewDetail.EventsGirls.OrderBy(p=>p.Rank).Take(4).ToList();
            ViewBag.ThiSinhs = thisinhs;

            //var listOrthers = occSysDB.NewsCategories.Where(p => p.Language.Lang_Code == langCode && p.TID == 2).OrderBy(p => p.SortOrder).ToList();
            //ViewBag.Orthers = listOrthers;

            return View(viewDetail);

        }



        public ActionResult view_news_by_event(int? eventID)
        {
            var evt = occSysDB.Events.FirstOrDefault(p => p.EventID == eventID);

            var url = Request.Headers["HOST"];
            var index = url.IndexOf(".");

            if (index < 0)
                return null;

            var subDomain = url.Substring(0, index);
            var viewDetail = new Event();
            var orthers = new List<News>();
            if (subDomain != null)
            {
                viewDetail = occSysDB.Events.FirstOrDefault(p => p.Subdomain == subDomain);
                ViewBag.Folder = viewDetail.Folder;
                ViewBag.Logo = viewDetail.EventLogo;
                ViewBag.BG = viewDetail.BackGround;
                orthers = occSysDB.News.Where(p => p.NewsCategories.TID == 1 && p.EventID == viewDetail.EventID).OrderByDescending(p => p.CreateDate).Take(4).ToList();
            }

            return View(orthers);
        }
        public ActionResult view_news_detail_by_event(string name, string subdomain)
        {
            var url = Request.Headers["HOST"];
            var index = url.IndexOf(".");

            if (index < 0)
                return null;

            var subDomain = url.Substring(0, index);
            var viewDetail = occSysDB.Events.FirstOrDefault();
            var detail = new News();
            if (subDomain != null)
            {
                viewDetail = occSysDB.Events.FirstOrDefault(p => p.Subdomain == subDomain);
                ViewBag.Folder = viewDetail.Folder;
                ViewBag.Logo = viewDetail.EventLogo;
                ViewBag.BG = viewDetail.BackGround;
            }
            if (name != null)
            {
                int a = name.LastIndexOf("-");
                string title = name.Substring(a).Replace("-", "");
                string b = Regex.Match(title, @"\d+").Value;
                int id = int.Parse(b);

                detail = occSysDB.News.FirstOrDefault(p => p.NewsID == id);

                detail.ViewCount += 1;
                occSysDB.SaveChanges();

            }

            return View(detail);
        }

        public ActionResult view_albums_by_event(int? eventID, string subdomain)
        {
            var evt = occSysDB.Events.FirstOrDefault(p => p.EventID == eventID);

            var url = Request.Headers["HOST"];
            var index = url.IndexOf(".");

            if (index < 0)
                return null;

            var subDomain = url.Substring(0, index);
            var viewDetail = occSysDB.Events.FirstOrDefault();
            if (subDomain != null)
            {
                viewDetail = occSysDB.Events.FirstOrDefault(p => p.Subdomain == subDomain);
                ViewBag.Folder = viewDetail.Folder;
                ViewBag.Logo = viewDetail.EventLogo;
                ViewBag.BG = viewDetail.BackGround;
            }

            var orthers = viewDetail.Albums.OrderByDescending(p => p.CreateDate).ToList();
            return View(orthers);
        }
        public ActionResult view_albums_detail_by_event(string name, string subdomain)
        {
            var url = Request.Headers["HOST"];
            var index = url.IndexOf(".");

            if (index < 0)
                return null;

            var subDomain = url.Substring(0, index);
            var viewDetail = occSysDB.Events.FirstOrDefault();
            var girl = new Album();
            if (subDomain != null)
            {
                viewDetail = occSysDB.Events.FirstOrDefault(p => p.Subdomain == subDomain);
                ViewBag.Folder = viewDetail.Folder;
                ViewBag.Logo = viewDetail.EventLogo;
                ViewBag.BG = viewDetail.BackGround;
            }
            if (name != null)
            {
                int a = name.LastIndexOf("-");
                string title = name.Substring(a).Replace("-", "");
                string b = Regex.Match(title, @"\d+").Value;
                int id = int.Parse(b);

                girl = occSysDB.Albums.FirstOrDefault(p => p.AlbumID == id);

                occSysDB.SaveChanges();

            }

            return View(girl);
        }

        public ActionResult view_clips_by_event(int? eventID, string subdomain)
        {
            var evt = occSysDB.Events.FirstOrDefault(p => p.EventID == eventID);
            var url = Request.Headers["HOST"];
            var index = url.IndexOf(".");

            if (index < 0)
                return null;

            var subDomain = url.Substring(0, index);
            var viewDetail = occSysDB.Events.FirstOrDefault();
            if (subDomain != null)
            {
                viewDetail = occSysDB.Events.FirstOrDefault(p => p.Subdomain == subDomain);
                ViewBag.Folder = viewDetail.Folder;
                ViewBag.Logo = viewDetail.EventLogo;
                ViewBag.BG = viewDetail.BackGround;
            }

            var orthers = viewDetail.News.Where(p => p.NewsCategories.TID == 2).OrderByDescending(p => p.CreateDate).ToList();
            return View(orthers);
        }

        public ActionResult view_clips_detail_by_event(string name, string subdomain)
        {
            var url = Request.Headers["HOST"];
            var index = url.IndexOf(".");

            if (index < 0)
                return null;

            var subDomain = url.Substring(0, index);
            var viewDetail = occSysDB.Events.FirstOrDefault();
            var girl = new News();
            var listOrthers = new List<News>();

            if (name != null)
            {
                int a = name.LastIndexOf("-");
                string title = name.Substring(a).Replace("-", "");
                string b = Regex.Match(title, @"\d+").Value;
                int id = int.Parse(b);

                girl = occSysDB.News.FirstOrDefault(p => p.NewsID == id);

            }

            if (subDomain != null)
            {
                viewDetail = occSysDB.Events.FirstOrDefault(p => p.Subdomain == subDomain);
                ViewBag.Folder = viewDetail.Folder;
                ViewBag.Logo = viewDetail.EventLogo;
                ViewBag.BG = viewDetail.BackGround;

                listOrthers = viewDetail.News.Where(p => p.NewsCategories.TID == 2 && p.NewsID != girl.NewsID).OrderByDescending(p => p.CreateDate).ToList();

                ViewBag.Orthers = listOrthers;
            }


            return View(girl);
        }

        public ActionResult view_girls_by_event(int? eventID, string subdomain, int? page)
        {
            var evt = occSysDB.Events.FirstOrDefault(p => p.EventID == eventID);
            var url = Request.Headers["HOST"];
            var index = url.IndexOf(".");

            if (index < 0)
                return null;

            var subDomain = url.Substring(0, index);
            var viewDetail = occSysDB.Events.FirstOrDefault();
            var orthers = new List<EventsGirl>();
            int pageSize = 4;
            int pageNumber = (page ?? 1);
            if (subDomain != null)
            {
                viewDetail = occSysDB.Events.FirstOrDefault(p => p.Subdomain == subDomain);
                orthers = viewDetail.EventsGirls.OrderBy(p => p.Rank).ToList();
                ViewBag.Folder = viewDetail.Folder;
                ViewBag.Logo = viewDetail.EventLogo;
                ViewBag.BG = viewDetail.BackGround;
            }

            return View(orthers.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult view_girls_detail_by_event(string name, string subdomain)
        {
            var url = Request.Headers["HOST"];
            var index = url.IndexOf(".");

            if (index < 0)
                return null;

            var subDomain = url.Substring(0, index);
            var viewDetail = occSysDB.Events.FirstOrDefault();
            var girl = new EventsGirl();
            if (subDomain != null)
            {
                viewDetail = occSysDB.Events.FirstOrDefault(p => p.Subdomain == subDomain);
                ViewBag.Folder = viewDetail.Folder;
                ViewBag.Logo = viewDetail.EventLogo;
                ViewBag.BG = viewDetail.BackGround;
            }
            if (name != null)
            {
                int a = name.LastIndexOf("-");
                string title = name.Substring(a).Replace("-", "");
                string b = Regex.Match(title, @"\d+").Value;
                int id = int.Parse(b);

                girl = occSysDB.EventsGirls.FirstOrDefault(p => p.EGirlID == id);
                if (girl.ViewCount != null)
                {
                    girl.ViewCount += 1;
                }
                else
                {
                    girl.ViewCount = 0;
                    girl.ViewCount += 1;
                }

                occSysDB.SaveChanges();

            }

            return View(girl);
        }

        public ActionResult register_event()
        {
            var url = Request.Headers["HOST"];
            var index = url.IndexOf(".");

            if (index < 0)
                return null;

            var subDomain = url.Substring(0, index);
            var viewDetail = occSysDB.Events.FirstOrDefault();
            if (subDomain != null)
            {
                viewDetail = occSysDB.Events.FirstOrDefault(p => p.Subdomain == subDomain);
                ViewBag.Folder = viewDetail.Folder;
                ViewBag.Logo = viewDetail.EventLogo;
                ViewBag.BG = viewDetail.BackGround;
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [CaptchaValidator]
        public ActionResult register_event(string subdomain, EventsGirl newRegister, HttpPostedFileBase file_image, string ebirthDay, string esubject, bool captchaValid)
        {
            if (ModelState.IsValid)
            {
                if (subdomain != null)
                {
                    var evnt = occSysDB.Events.FirstOrDefault(p => p.Subdomain == subdomain);
                    if (evnt != null)
                    {
                        var hotgirl = new EventsGirl();
                        hotgirl.Name = newRegister.Name;

                        string newsDate = ebirthDay.Replace("/", "").Replace("_", "").ToString();

                        if (newsDate != "")
                        {
                            hotgirl.Birtday = DateTime.ParseExact(ebirthDay, "dd/MM/yyyy", null);
                        }
                        else
                        {
                            hotgirl.Birtday = DateTime.Now;
                        }

                        hotgirl.AddressBorn = newRegister.AddressBorn;
                        hotgirl.Height = newRegister.Height;
                        hotgirl.Weight = newRegister.Weight;
                        hotgirl.ThreeRing = newRegister.ThreeRing;
                        hotgirl.Love = newRegister.Love;
                        hotgirl.AddressNow = newRegister.AddressNow;
                        hotgirl.School = newRegister.School;
                        hotgirl.EClass = newRegister.EClass;

                        hotgirl.Grade = newRegister.Grade;
                        hotgirl.Mobile = newRegister.Mobile;
                        hotgirl.Email = newRegister.Email;
                        hotgirl.Facebook = newRegister.Facebook;

                        hotgirl.EventID = evnt.EventID;

                        occSysDB.EventsGirls.Add(hotgirl);
                        occSysDB.SaveChanges();

                        var newPathImage = Server.MapPath("/Uploaded/Images/sukien/thi-sinh-du-thi");
                        if (!Directory.Exists(newPathImage))
                        {
                            Directory.CreateDirectory(newPathImage);
                        }

                        if (file_image != null)
                        {
                            //upload image
                            string duoiAnh = Path.GetExtension(file_image.FileName);
                            string fileName = OccConvert.KhongDau(hotgirl.Name.ToLower().Trim()) + hotgirl.EGirlID + duoiAnh;
                            string fileUrl = Path.Combine(newPathImage, Path.GetFileName(fileName));

                            //save file to folder
                            file_image.SaveAs(fileUrl);

                            //get image
                            hotgirl.Image = fileName;
                        }

                        occSysDB.SaveChanges();

                        string smtpUserName = "support@occ.com.vn";
                        string smtpPassword = "vcswljfyxrvmgwdv";
                        string smtpHost = "smtp.gmail.com";
                        int smtpPort = 25;

                        //read template mail
                        var sr = new StreamReader(Server.MapPath("/Views/EmailTemplates/DangKy.txt"));

                        //sent to
                        string emailTo = "info@occ.com.vn "; // Khi có liên hệ sẽ gửi về thư của mình
                        string emailTo2 = "imiss@gsmedia.vn"; // Khi có liên hệ sẽ gửi về thư của mình
                        string subject = "ĐĂNG KÝ ONLINE - " + evnt.EventName;

                        //email content

                        var tencuocthi = evnt.EventName;
                        //[0]
                        string ngaydat = OccConvert.ViewDateFull(DateTime.Now.Date);
                        //[1]
                        string anhthisinh = Request.Url.Host.ToLower() + "/Uploaded/Images/sukien/thi-sinh-du-thi/" + hotgirl.Image;
                        //[2]
                        string tenthisinh = hotgirl.Name;
                        //[3]
                        string noisinh = hotgirl.AddressBorn;
                        //[4]
                        string ngaysinh = hotgirl.Birtday.ToString("dd/MM/yyyy");
                        //[5]
                        string chieucao = hotgirl.Height;
                        //[6]
                        string cannang = hotgirl.Weight;
                        //[7]
                        string bvong = hotgirl.ThreeRing;
                        //[8]
                        string sothich = hotgirl.Love;

                        //[9]
                        string choo = hotgirl.AddressNow;
                        //[10]
                        string truong = hotgirl.School;
                        //[11]
                        string lop = hotgirl.EClass;
                        //[12]
                        string diem = hotgirl.Grade;
                        //[13]
                        string sdt = hotgirl.Mobile;
                        //[14]
                        string email = hotgirl.Email;
                        //[15]
                        string face = hotgirl.Facebook;

                        //[19]
                        //string linkedit = Request.Url.Host.ToLower() + "/administrators/bookcars_manager/bookorders_detail_edit/" + newOrder.BookID;




                        //and email content
                        string body = sr.ReadToEnd();
                        string mailBody = string.Format(body,
                                                        tencuocthi, ngaydat,
                                                        anhthisinh, tenthisinh, noisinh, ngaysinh, chieucao, cannang, bvong, sothich,
                                                         choo, truong, lop, diem, sdt, email, face
                                                        );

                        //string body = string.Format("Bạn vừa nhận được 1 lệnh đặt xe mới có mã lệnh <b>{0}</b>.",
                        //    newOrder.OrderID);

                        //email service
                        OccSendMail serviceMail = new OccSendMail();

                        //check send
                        bool kq =
                            serviceMail.SendTwo(
                            smtpUserName,
                            smtpPassword,
                            smtpHost,
                            smtpPort,
                            emailTo,
                            emailTo2,
                            subject,
                            mailBody);

                        serviceMail.SendToMe(
                          smtpUserName,
                          smtpPassword,
                          smtpHost,
                          smtpPort,
                          "support@occ.com.vn",
                          subject,
                          mailBody);

                        if (kq)
                        {
                            ModelState.AddModelError("Success", "Cảm ơn bạn đã liên hệ với chúng tôi.");
                            return RedirectToAction("view_detail");
                        }
                        else
                        {
                            ModelState.AddModelError("Error", "Gửi tin nhắn thất bại, vui lòng thử lại.");
                            return RedirectToAction("view_detail");
                        }
                    }
                }
            }

            var url = Request.Headers["HOST"];
            var index = url.IndexOf(".");

            if (index < 0)
                return null;

            var subDomain = url.Substring(0, index);
            var viewDetail = occSysDB.Events.FirstOrDefault();
            if (subDomain != null)
            {
                viewDetail = occSysDB.Events.FirstOrDefault(p => p.Subdomain == subDomain);
                ViewBag.Folder = viewDetail.Folder;
                ViewBag.Logo = viewDetail.EventLogo;
                ViewBag.BG = viewDetail.BackGround;
            }
            return View(newRegister);
        }

        public ActionResult _orther_clips(int? currentID)
        {
            var orthers = occSysDB.News.Where(p => p.NewsID != currentID && p.Language.Lang_Code == langCode).OrderBy(p => Guid.NewGuid()).Take(2).ToList();
            return PartialView(orthers);
        }
    }
}
