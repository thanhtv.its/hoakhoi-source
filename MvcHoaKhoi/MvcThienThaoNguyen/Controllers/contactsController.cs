﻿using Insya.Localization;
using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Text.RegularExpressions;

namespace MvcWebsiteOcc.Controllers
{
    public class contactsController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();
        string langCode = Localization.Get("lng").ToLower();

        public ActionResult index()
        {
            var viewDetail = occSysDB.WebsiteDetails.FirstOrDefault();


            return View(viewDetail);
        }

    }
}
