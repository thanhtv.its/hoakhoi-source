﻿using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MvcWebsiteOcc.Controllers
{
    public class AccountController : Controller
    {

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Login login, string ReturnUrl = "")
        {
            //using (OccDBEntities dc = new OccDBEntities())
            //{
            //    var user = dc.Users.Where(a => a.UserName.Equals(login.UserName) && a.Password.Equals(login.Password)).FirstOrDefault();
            //    if (user != null)
            //    {
            //        FormsAuthentication.SetAuthCookie(user.UserName, login.RememberMe);
            //        if (Url.IsLocalUrl(ReturnUrl))
            //        {
            //            return Redirect(ReturnUrl);
            //        }
            //        else
            //        {
            //            return RedirectToAction("myAccount", "Home");
            //        }
            //    }
            //}

            if (ModelState.IsValid)
            {
                var isValid = Membership.ValidateUser(login.UserName, OccConvert.MD5Hash(login.Password));
                if (Url.IsLocalUrl(ReturnUrl) && isValid == true)
                {
                    FormsAuthentication.SetAuthCookie(login.UserName, login.RememberMe);
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return View(login);
                }
            }

            ModelState.Remove("Password");
            return View();
        }

        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

    }
}
