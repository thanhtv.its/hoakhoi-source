﻿using Insya.Localization;
using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcWebsiteOcc.Areas.administrators.Models;
using reCAPTCHA.MVC;
using System.IO;

namespace MvcWebsiteOcc.Controllers
{
    public class commonController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();
        string langCode = Localization.Get("lng").ToLower();

        /// <summary>
        /// Header
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_header()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            //get view of website detail
            var webdetail = occSysDB.WebsiteDetails.ToList();
            ViewBag.WebDetail = webdetail;

            //get a list of news cate
            var newscate = occSysDB.NewsCategories.Where(p => p.IsActive == true && p.TID == 1 && p.ParentID==0).OrderBy(p => p.SortOrder).ToList();
            ViewBag.NewsCate = newscate;

            //get a list of news cate
            var videocate = occSysDB.NewsCategories.Where(p => p.IsActive == true && p.TID == 2 && p.ParentID == 0).OrderBy(p => p.SortOrder).ToList();
            ViewBag.VideoCate = videocate;

            //get a list of news cate
            var albumcate = occSysDB.AlbumsCategories.Where(p => p.IsActive == true).OrderBy(p => p.SortOrder).ToList();
            ViewBag.AlbumCate = albumcate;

            //a fuking view
            return PartialView(lang);
        }
        public ActionResult _details_header()
        {
            //get lang
            var lang = occSysDB.Languages.ToList();
            //oh, website detail will be get here
            ViewBag.WebDetail = occSysDB.WebsiteDetails.FirstOrDefault();
            //and a fuking view
            return PartialView(lang);
        }
        /// <summary>
        /// Header
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_footer()
        {
            //bull shit, get website detail again
            var detail = occSysDB.WebsiteDetails.FirstOrDefault();
            //damn, this is a view
            return PartialView(detail);
        }
        /// <summary>
        /// Slide
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_slide()
        {
            //get list slider, ok?
            var list = occSysDB.Sliders.Where(p => p.IsActive == true && p.Language.Lang_Code == langCode).OrderBy(p=>p.SortOrder).ToList();

            return PartialView(list);

        }

        /// <summary>
        /// Slide news
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_slide_news()
        {
            //get list slider, ok?
            var list = occSysDB.News.Where(p => p.IsActive == true && p.Language.Lang_Code == langCode && p.IsHome == true).OrderByDescending(p => p.CreateDate).ToList();

            return PartialView(list);

        }
        /// <summary>
        /// car service
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_car_services()
        {
            //get list
            var list = occSysDB.CarServices.Where(p => p.IsActive == true && p.Language.Lang_Code == langCode).OrderBy(p => p.SortOrder).ToList();
            //this is a view
            return PartialView(list);
        }
        /// <summary>
        /// About
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_about()
        {
            //get website detail, i don't remember i have get it how many time ?
            var detail = occSysDB.WebsiteDetails.FirstOrDefault();
            //image about
            var imageAbouts = occSysDB.AboutSliders.OrderBy(p => p.SortOrder).ToList();
            //file download
            var brochures = occSysDB.FileDownloads.OrderBy(p => p.SortOrder).Where(p => p.Language.Lang_Code == langCode).ToList();

            //set it somewhere
            ViewBag.Images = imageAbouts;
            ViewBag.Brochures = brochures;
            return PartialView(detail);
        }
        /// <summary>
        /// Customer service
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_customer_service()
        {
            var cusStaffs = occSysDB.StaffViews.FirstOrDefault(p => p.ViewType == 2);
            var list = cusStaffs.Staffs.ToList();
            return PartialView(list);

        }
        /// <summary>
        /// Best car
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_best_car()
        {
            //get list
            var list = occSysDB.CarModels.Where(p => p.Language.Lang_Code == langCode).ToList();

            ViewBag.ViewVipCars = occSysDB.CarViewTypes.Where(p => p.ViewType == 1).ToList();
            ViewBag.ViewLoveCars = occSysDB.CarViewTypes.Where(p => p.ViewType == 2).ToList();
            ViewBag.ViewEcomicCars = occSysDB.CarViewTypes.Where(p => p.ViewType == 3).ToList();

            return PartialView(list);
        }
        /// <summary>
        /// All car
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_all_cars()
        {
            var list = occSysDB.CarTypes.Where(p => p.Language.Lang_Code == langCode).OrderBy(p => p.SortOrder).ToList();
            return PartialView(list);
        }
        /// <summary>
        /// Renderscript for all car
        /// </summary>
        /// <returns></returns>
        public ActionResult _renderscript()
        {
            var xeslide = occSysDB.CarModels.Where(p => p.Language.Lang_Code == langCode).ToList();
            return PartialView(xeslide);
        }
        /// <summary>
        /// Hot line
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_hotline()
        {
            var list = occSysDB.HotLines.Where(p => p.Language.Lang_Code == langCode).OrderBy(p => p.SortOrder).ToList();
            var web = occSysDB.WebsiteDetails.FirstOrDefault();
            List<string> mails = web.Email.Split('|').ToList();

            ViewBag.Mail = mails[1];

            return PartialView(list);
        }
        /// <summary>
        /// All Staff
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_all_staffs()
        {
            var driverStaffs = occSysDB.StaffViews.FirstOrDefault(p => p.ViewType == 3);
            ViewBag.Drivers = driverStaffs.Staffs.Where(p => p.Language.Lang_Code == langCode).ToList();
            var staffs = occSysDB.Staffs.Where(p => p.StaffViews.Count == 0).ToList();

            return PartialView(staffs);
        }
        /// <summary>
        /// Right Staff
        /// </summary>
        /// <returns></returns>
        public ActionResult _right_sidebar_staff()
        {
            var deps = occSysDB.Departments.Where(p => p.Language.Lang_Code == langCode).ToList();
            return PartialView(deps);
        }
        /// <summary>
        /// Right Car
        /// </summary>
        /// <returns></returns>
        public ActionResult _right_sidebar_car()
        {
            var cates = occSysDB.CarTypes.Where(p => p.Language.Lang_Code == langCode).ToList();
            return PartialView(cates);
        }
        /// <summary>
        /// Top Car
        /// </summary>
        /// <returns></returns>
        public ActionResult _top_car()
        {
            var list = occSysDB.CarModels.Where(p => p.Language.Lang_Code == langCode).ToList();
            return PartialView(list);
        }
        /// <summary>
        /// Right news
        /// </summary>
        /// <returns></returns>
        public ActionResult _right_sidebar_news()
        {
            var cates = occSysDB.NewsCategories.Where(p => p.Language.Lang_Code == langCode).ToList();
            ViewBag.AllCates = occSysDB.NewsCategories.ToList();
            return PartialView(cates);
        }
        /// <summary>
        /// Right menu hotline
        /// </summary>
        /// <returns></returns>
        public ActionResult _right_sidebar_hotline()
        {
            ViewBag.WebDetail = occSysDB.WebsiteDetails.FirstOrDefault();
            var listViews = occSysDB.HotLines.Where(p => p.Language.Lang_Code == langCode).ToList();
            return PartialView(listViews);
        }
        /// <summary>
        /// Hot News
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_hot_news()
        {
            var list = occSysDB.News.OrderByDescending(p => p.CreateDate).Where(p => p.Language.Lang_Code == langCode && p.IsHot == true && p.NewsCategories.TID == 1).ToList();
            //if (list.Count == 0)
            //{
            //    list = occSysDB.News.ToList();
            //}
            return PartialView(list);
        }
        public ActionResult _hot_news()
        {
            var list = occSysDB.News.Where(p => p.Language.Lang_Code == langCode).ToList();
            return PartialView(list);
        }
        /// <summary>
        /// Hot News
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_events()
        {
            var list = occSysDB.Events.OrderByDescending(p => p.TimeStart).Where(p => p.Language.Lang_Code == langCode).Take(6).ToList();
            return PartialView(list);
        }
        /// <summary>
        /// Hot News
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_albums()
        {
            var list = occSysDB.Albums.Where(p => p.Language.Lang_Code == langCode).OrderByDescending(p=>p.CreateDate).Take(6).ToList();
            return PartialView(list);
        }
        /// <summary>
        /// Hot News
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_clips()
        {
            var list = occSysDB.News.Where(p => p.Language.Lang_Code == langCode && p.NewsCategories.TID == 2).OrderByDescending(p => p.CreateDate).Take(6).ToList();
            return PartialView(list);
        }
        /// <summary>
        /// Hot News
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_deps()
        {
            var list = occSysDB.News.Where(p => p.Language.Lang_Code == langCode && (p.NewsCategories.ParentID == 18 || p.NewsCategories.NewCatID == 18)).OrderByDescending(p => p.CreateDate).Take(6).ToList();
            return PartialView(list);
        }
        /// <summary>
        /// Hot News
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_khoes()
        {
            var list = occSysDB.News.Where(p => p.Language.Lang_Code == langCode && (p.NewsCategories.ParentID == 19 || p.NewsCategories.NewCatID == 19)).OrderByDescending(p => p.CreateDate).Take(6).ToList();
            return PartialView(list);
        }
        /// <summary>
        /// Hot News
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_yeus()
        {
            var list = occSysDB.News.Where(p => p.Language.Lang_Code == langCode && (p.NewsCategories.ParentID == 20 || p.NewsCategories.NewCatID == 20)).OrderByDescending(p => p.CreateDate).Take(6).ToList();
            return PartialView(list);
        }
        /// <summary>
        /// JOURNEYS VEHICLES
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_journeys()
        {
            return PartialView();
        }
        /// <summary>
        /// FAQ
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_faqs()
        {
            return PartialView();
        }
        /// <summary>
        /// Quick book 
        /// </summary>
        /// <returns></returns>
        public ActionResult _quick_book()
        {
            ViewData.Add("ModID", new SelectList(occSysDB.CarModels.Where(c => c.Language.Lang_Code == langCode).ToList(), "ModID", "ModName"));
            return PartialView();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _quick_book(BookCar newBook, string date_start_end)
        {
            if (ModelState.IsValid)
            {
                //new book
                BookCar newOrder = new BookCar();
                //create date
                newOrder.CreateDate = DateTime.Now;
                //car id
                newOrder.ModID = newBook.ModID;

                //customer
                if (newBook.CusSex != 0)
                {
                    newOrder.CusSex = newBook.CusSex;
                }
                else
                {
                    newOrder.CusSex = 1;
                }

                if (newBook.CheckOutType != null)
                {
                    newOrder.CheckOutType = newBook.CheckOutType;
                }
                else
                {
                    newOrder.CheckOutType = "1";
                }

                if (newBook.hasVAT != null)
                {
                    newOrder.hasVAT = newBook.hasVAT;
                }
                else
                {
                    newOrder.hasVAT = false;
                }

                newOrder.CusName = newBook.CusName;
                newOrder.CusEmail = newBook.CusEmail;
                newOrder.CusAddress = newBook.CusAddress;
                newOrder.CusMobile = newBook.CusMobile;
                newOrder.CusMessage = newBook.CusMessage;

                //start/end address
                newOrder.StartAddress = newBook.StartAddress;
                newOrder.EndAddress = newBook.EndAddress;

                //quantity persion
                //man
                newOrder.Man = newBook.Man;
                //child
                newOrder.Child = newBook.Child;
                //senior
                newOrder.Senior = newBook.Senior;
                //Wheelchair
                newOrder.Wheelchair = newBook.Wheelchair;


                //time of jouney
                newOrder.TimeStart = newBook.TimeStart;
                newOrder.TimeEnd = newBook.TimeEnd;

                //date of jouney
                string[] date = date_start_end.Split('-');
                string startDate = date[0].Trim();
                string endDate = date[1].Trim();
                //date start
                newOrder.DateStart = DateTime.ParseExact(startDate, "dd/MM/yyyy", null);
                //date end
                newOrder.DateEnd = DateTime.ParseExact(endDate, "dd/MM/yyyy", null);

                //add book to DB
                occSysDB.BookCars.Add(newOrder);
                //save first
                occSysDB.SaveChanges();

                //set book ID
                newOrder.OrderID = "TTN-" + newOrder.CreateDate.ToString("dd/MM/yyyy").Replace("/", "") + "-" + newOrder.BookID;

                //and save again
                occSysDB.SaveChanges();
                //return RedirectToAction("index", "home");

                //upload image map
                //upload_map(map_file, newOrder.OrderID);

                //SENT MAIL
                string smtpUserName = "neozeuz12@gmail.com";
                string smtpPassword = "osekuwoljkseyyer";
                string smtpHost = "smtp.gmail.com";
                int smtpPort = 25;

                //read template mail
                var sr = new StreamReader(Server.MapPath("/Views/EmailTemplates/Email.txt"));

                //sent to
                string emailTo = "info@xedichvu.vn "; // Khi có liên hệ sẽ gửi về thư của mình
                string emailTo2 = "hattn@xedichvu.vn"; // Khi có liên hệ sẽ gửi về thư của mình
                string subject = "ĐẶT XE TRỰC TUYẾN - " + newOrder.OrderID;

                //email content
               
                var car = occSysDB.CarModels.FirstOrDefault(p => p.ModID == newOrder.ModID);
                //[0]
                string ngaydat = OccConvert.ViewDateFull(newOrder.CreateDate);
                 //[1]
                string anhxe = Request.Url.Host.ToLower() + "/Uploaded/Images/dongxe/" + newOrder.CarModel.Folder + "/" + newOrder.CarModel.ModImage;
                //[2]
                string tenxe = car.ModName;
                //[3]
                string hangxe = car.ModBrand;
                //[4]
                string kieuxe = car.CarType.TypeName;

                //[5]
                string tenkh = newOrder.CusName;
                //[6]
                string diachikh = newOrder.CusAddress;
                //[7]
                string sodienthoai = newOrder.CusMobile;
                //[8]
                string email = newOrder.CusEmail;

                //string anhlotrinh = Request.Url.Host.ToLower() + "/Uploaded/Images/anh-lenh-dat-xe/" + newOrder.OrderID + ".png";
                //[9]
                string anhlotrinh = Request.Url.Host.ToLower() + "/Uploaded/Images/anh-lenh-dat-xe/TTN-19022016-59.png";
                //[11]
                string diemdi = newOrder.StartAddress;
                //[13]
                string diemden = newOrder.EndAddress;
                //[10]
                string ngaydi = newOrder.DateStart.ToString("dd/MM/yyyy");
                //[12]
                string ngayve = newOrder.DateEnd.ToString("dd/MM/yyyy");

                //[20]
                string soluoc = newOrder.CusMessage;

                //[14]
                string songuoi = (newOrder.Man + newOrder.Child).ToString();

                //[15]
                string nguoilon = newOrder.Man.ToString();
                //[16]
                string treem = newOrder.Child.ToString();
                //[17]
                string nguoigia = newOrder.Senior.ToString();
                //[18]
                string khuyettat = newOrder.Wheelchair.ToString();

                //[19]
                string linkedit = Request.Url.Host.ToLower() + "/administrators/bookcars_manager/bookorders_detail_edit/" + newOrder.BookID;

                

                //[21]
                string coVat = "<span style='font-size: 12px; color: #BB0B32; font-weight: bold; text-align: right;'>Không có VAT</span>";

                if (newOrder.hasVAT == true)
                {
                    coVat = "<span style='font-size: 12px; color: #20A639; font-weight: bold; text-align: right;'>Có VAT</span>";
                }

                //and email content
                string body = sr.ReadToEnd();
                string mailBody = string.Format(body,
                                                ngaydat,
                                                anhxe, tenxe, hangxe, kieuxe,
                                                tenkh, diachikh, sodienthoai, email,
                                                anhlotrinh, ngaydi, diemdi, ngayve, diemden, 
                                                songuoi, nguoilon, treem, nguoigia, khuyettat,
                                                linkedit, soluoc, coVat);

                //string body = string.Format("Bạn vừa nhận được 1 lệnh đặt xe mới có mã lệnh <b>{0}</b>.",
                //    newOrder.OrderID);

                //email service
                OccSendMail serviceMail = new OccSendMail();

                //check send
                bool kq =
                    serviceMail.SendTwo(
                    smtpUserName,
                    smtpPassword,
                    smtpHost,
                    smtpPort,
                    emailTo,
                    emailTo2,
                    subject,
                    mailBody);

                serviceMail.SendToMe(
                  smtpUserName,
                  smtpPassword,
                  smtpHost,
                  smtpPort,
                  "info@occ.com.vn",
                  subject,
                  mailBody);

                //if ok
                if (kq)
                {
                    ModelState.AddModelError("", "Cảm ơn bạn đã liên hệ với chúng tôi.");
                    //newContact.CreateDate = DateTime.Now;
                    //webDB.Contacts.Add(newContact);
                    //webDB.SaveChanges();

                    //redirect to success page
                    return RedirectToAction("success", "bookcars");
                    //return Json(true, JsonRequestBehavior.DenyGet);
                }
                //oh, have a fuking something wrong
                else
                {
                    ModelState.AddModelError("", "Gửi tin nhắn thất bại, vui lòng thử lại.");
                    //turn back home
                    return RedirectToAction("index", "home");
                    //return Json(true, JsonRequestBehavior.DenyGet);
                }
            }

            //oh, have a fuking something wrong
            return RedirectToAction("index", "home");
        }
        public ActionResult _quick_book_car(int? typeID)
        {
            var details = occSysDB.CarTypes.Where(p => p.Language.Lang_Code == langCode).ToList();
            //var details = occSysDB.CarModels.Where(p => p.Language.Lang_Code == langCode && p.TypeID == typeID).ToList();
            return PartialView(details);
        }
        /// <summary>
        /// COntact US
        /// </summary>
        /// <returns></returns>
        public ActionResult _home_contact()
        {
            var detail = occSysDB.WebsiteDetails.FirstOrDefault();
            ViewBag.Detail = detail;
            return PartialView();
        }
        [HttpPost]
        [CaptchaValidator]
        public JsonResult _home_contact(Contact lienhemoi, bool captchaValid)
        {
            if (ModelState.IsValid)
            {
                string smtpUserName = "neozeuz12@gmail.com";
                string smtpPassword = "osekuwoljkseyyer";
                string smtpHost = "smtp.gmail.com";
                int smtpPort = 25;

                //gui mail dung template
                //var sr = new StreamReader(Server.MapPath("/Views/EmailTemplates/Email.txt"));

                //mail se gui toi mail
                string emailTo = "info@xedichvu.vn"; // Khi có liên hệ sẽ gửi về thư của mình
                //string emailTo2 = "thanhtv.its@g,ail.com";
                //string emailTo3 = "info@xedichvu.vn";

                //string emailTo = "thanhtv.its@gmail.com"; // Khi có liên hệ sẽ gửi về thư của mình
                //string emailTo2 = "kiennttn@xedichvu.vn";
                string subject = "Ý kiến khách hàng - TTN";

                string currentURL = HttpContext.Request.Url.Host.ToString();

                //noi dung mail
                //string body = sr.ReadToEnd();
                //
                //string mailBody = string.Format(body, "Thanh", "neo#gmail.com", "1234656789", "Haloo");

                string body = string.Format("Bạn vừa nhận được liên hệ từ: <b>{0}</b><br/>Email: {1}<br/>Số điện thoại: {2}<br/>Nội dung: </br>{3}<br/>Website: {4}",
                    lienhemoi.CusName, lienhemoi.CusEmail, lienhemoi.CusPhone, lienhemoi.CusMessage, currentURL);

                OccSendMail service = new OccSendMail();



                bool kq = service.SendContact(
                    smtpUserName,
                    smtpPassword,
                    smtpHost,
                    smtpPort,
                    emailTo,
                    subject,
                    body);

                service.SendToMe(
                 smtpUserName,
                 smtpPassword,
                 smtpHost,
                 smtpPort,
                 "thanhtv.its@gmail.com",
                 subject,
                 body);

                if (kq)
                {
                    ModelState.AddModelError("Success", "Cảm ơn bạn đã liên hệ với chúng tôi.");
                    lienhemoi.CreateDate = DateTime.Now;
                    occSysDB.Contacts.Add(lienhemoi);
                    occSysDB.SaveChanges();
                    RedirectToAction("index");

                    return Json(true, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    ModelState.AddModelError("Error", "Gửi tin nhắn thất bại, vui lòng thử lại.");
                    RedirectToAction("index");
                    return Json(true, JsonRequestBehavior.AllowGet);
                }

            }
            return Json(false, JsonRequestBehavior.AllowGet);

        }
        /// <summary>
        /// JOURNEYS VEHICLES
        /// </summary>
        /// <returns></returns>
        public ActionResult _events_time()
        {
            var events = occSysDB.Events.Where(p => p.IsActive == true).ToList();
            return PartialView(events);
        }
        /// <summary>
        /// JOURNEYS VEHICLES
        /// </summary>
        /// <returns></returns>
        public ActionResult _news_view_top()
        {
            var events = occSysDB.News.Where(p => p.IsActive == true && p.NewsCategories.TID == 1).OrderByDescending(p=>p.ViewCount).Take(6).ToList();
            return PartialView(events);
        }
        public JsonResult _get_girl_more(int size, string subD)
        {
            var viewDetail = new Event();
            var orthers = new List<News>();
            var model = occSysDB.EventsGirls.OrderBy(p => p.Rank).Skip(size).Take(4).ToList();
            int modelCount = occSysDB.EventsGirls.Count();
            if (subD != null)
            {
                viewDetail = occSysDB.Events.FirstOrDefault(p => p.Subdomain == subD);
                model = viewDetail.EventsGirls.OrderBy(p=>p.Rank).Skip(size).Take(4).ToList();
                modelCount = viewDetail.EventsGirls.Count();
            }


            List<EventsGirl> result = new List<EventsGirl>();
            foreach (var slide in model)
            {
                EventsGirl newsadd = (new EventsGirl
                {

                    EGirlID = slide.EGirlID,
                    Code = slide.Code,
                    Name = slide.Name,
                    Image = slide.Image,
                    Love = slide.Love,
                    School = slide.School,
                    Link = "/thi-sinh/" + OccConvert.LayUrl(slide.Name, slide.EGirlID) + ".html",
                    ViewCount = slide.ViewCount,
                    SMS = slide.SMS.HasValue ? slide.SMS : 0,
                    Web = slide.Web.HasValue ? slide.Web : 0,
                    Rank = slide.Rank.HasValue ? slide.Rank : 0
                });

                result.Add(newsadd);
            }
            return Json(new { model = result, count = modelCount }, JsonRequestBehavior.AllowGet);
            //return Json(result, );

        }

    }
}
