﻿using Insya.Localization;
using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcWebsiteOcc.Areas.administrators.Models;
using System.IO;

namespace MvcWebsiteOcc.Controllers
{

    public class bookcarsController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();
        string langCode = Localization.Get("lng").ToLower();

        public ActionResult index(int? carID)
        {
            //if have car ID
            if (carID != null)
            {
                //get car
                var carList = occSysDB.CarModels.FirstOrDefault(p => p.ModID == carID);
                //check car exits
                if (carList != null)
                {
                    ViewBag.CarID = carList.ModID;
                    ViewBag.Cars = carList;
                    //drivers
                    var taixes = carList.Staffs.Where(p => p.CarModel.TypeID == carList.TypeID).ToList();
                    ViewBag.TaiXes = taixes;
                    //services
                    var dichvus = occSysDB.BooksServices.ToList();
                    ViewBag.DichVus = dichvus;

                    //return view
                    return View();
                }
                else
                {
                    //if car dont exits
                    return RedirectToAction("index", "home");
                }
            }
            //if car ID null
            else
            {
                return RedirectToAction("index", "home");
            }
        }

        public ActionResult success()
        {
            return View();
        }

        public ActionResult _cartypes()
        {
            //car type
            var list = occSysDB.CarTypes.Where(p => p.Language.Lang_Code == langCode).ToList();
            return PartialView(list);
        }

        public ActionResult _orther_cars(int? typeID)
        {
            //car list 
            var list = occSysDB.CarModels.Where(p => p.Language.Lang_Code == langCode).ToList();
            if (typeID != null)
            {
                //car in type 
                list = list.Where(p => p.TypeID == typeID).ToList();
            }
            //return view
            return PartialView(list);
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult index(BookCar newBook, string price, string date_start_end, IEnumerable<int> book_services, string map_file, int ModID)
        {
            if (ModelState.IsValid)
            {
                //new book
                BookCar newOrder = new BookCar();

                //create date
                newOrder.CreateDate = DateTime.Now;

                //car id
                newOrder.ModID = newBook.ModID;
                // staff
                newOrder.StaffID = newBook.StaffID;
                //customer
                newOrder.CusSex = newBook.CusSex;
                newOrder.CusName = newBook.CusName;
                newOrder.CusEmail = newBook.CusEmail;
                newOrder.CusAddress = newBook.CusAddress;
                newOrder.CusMobile = newBook.CusMobile;
                newOrder.CusMessage = newBook.CusMessage;

                if (newBook.CheckOutType != null)
                {
                    newOrder.CheckOutType = newBook.CheckOutType;
                }
                else
                {
                    newOrder.CheckOutType = "1";
                }

                if (newBook.hasVAT != null)
                {
                    newOrder.hasVAT = newBook.hasVAT;
                }
                else
                {
                    newOrder.hasVAT = false;
                }
              

                //lat and long
                newOrder.StartLat = newBook.StartLat;
                newOrder.StartLong = newBook.StartLong;
                //start/end address
                newOrder.StartAddress = newBook.StartAddress;
                newOrder.EndAddress = newBook.EndAddress;
                //lat and long
                newOrder.EndLat = newBook.EndLat;
                newOrder.EndLong = newBook.EndLong;

                //quantity persion
                //man
                newOrder.Man = newBook.Man;
                //child
                newOrder.Child = newBook.Child;
                //senior
                newOrder.Senior = newBook.Senior;
                //Wheelchair
                newOrder.Wheelchair = newBook.Wheelchair;

                //time of jouney
                newOrder.TimeStart = newBook.TimeStart;
                newOrder.TimeEnd = newBook.TimeEnd;

                //date of jouney
                string[] date = date_start_end.Split('-');
                string startDate = date[0].Trim();
                string endDate = date[1].Trim();
                //date start
                newOrder.DateStart = DateTime.ParseExact(startDate, "dd/MM/yyyy", null);
                //date end
                newOrder.DateEnd = DateTime.ParseExact(endDate, "dd/MM/yyyy", null);

                //add book to DB
                occSysDB.BookCars.Add(newOrder);

                //save first
                occSysDB.SaveChanges();

                //book services, If they not null
                if (book_services != null)
                {
                    foreach (var bserv in book_services)
                    {
                        //get service
                        var service = occSysDB.BooksServices.FirstOrDefault(p => p.CServID == bserv);
                        //if it exits
                        if (service != null)
                        {
                            //add to book
                            service.BookCars.Add(newOrder);
                            //save now
                            occSysDB.SaveChanges();
                        }
                    }
                }

                //set book ID
                newOrder.OrderID = "TTN-" + newOrder.CreateDate.ToString("dd/MM/yyyy").Replace("/", "") + "-" + newOrder.BookID;

                //and save again
                occSysDB.SaveChanges();

                //return RedirectToAction("index", "home");

                //upload image map
                //upload_map(map_file, newOrder.OrderID);
                //SENT MAIL
                string smtpUserName = "neozeuz12@gmail.com";
                string smtpPassword = "osekuwoljkseyyer";
                string smtpHost = "smtp.gmail.com";
                int smtpPort = 25;

                //read template mail
                var sr = new StreamReader(Server.MapPath("/Views/EmailTemplates/Email.txt"));

                //sent to
                string emailTo = "info@xedichvu.vn "; // Khi có liên hệ sẽ gửi về thư của mình
                string emailTo2 = "hattn@xedichvu.vn"; // Khi có liên hệ sẽ gửi về thư của mình
                string subject = "ĐẶT XE TRỰC TUYẾN - " + newOrder.OrderID;

                //email content

                var car = occSysDB.CarModels.FirstOrDefault(p => p.ModID == newOrder.ModID);
                //[0]
                string ngaydat = OccConvert.ViewDateFull(newOrder.CreateDate);
                //[1]
                string anhxe = Request.Url.Host.ToLower() + "/Uploaded/Images/dongxe/" + newOrder.CarModel.Folder + "/" + newOrder.CarModel.ModImage;
                //[2]
                string tenxe = car.ModName;
                //[3]
                string hangxe = car.ModBrand;
                //[4]
                string kieuxe = car.CarType.TypeName;

                //[5]
                string tenkh = newOrder.CusName;
                //[6]
                string diachikh = newOrder.CusAddress;
                //[7]
                string sodienthoai = newOrder.CusMobile;
                //[8]
                string email = newOrder.CusEmail;

                //string anhlotrinh = Request.Url.Host.ToLower() + "/Uploaded/Images/anh-lenh-dat-xe/" + newOrder.OrderID + ".png";
                //[9]
                string anhlotrinh = Request.Url.Host.ToLower() + "/Uploaded/Images/anh-lenh-dat-xe/TTN-19022016-59.png";
                //[11]
                string diemdi = newOrder.StartAddress;
                //[13]
                string diemden = newOrder.EndAddress;
                //[10]
                string ngaydi = newOrder.DateStart.ToString("dd/MM/yyyy");
                //[12]
                string ngayve = newOrder.DateEnd.ToString("dd/MM/yyyy");

                //[20]
                string soluoc = newOrder.CusMessage;

                //[14]
                string songuoi = (newOrder.Man + newOrder.Child).ToString();

                //[15]
                string nguoilon = newOrder.Man.ToString();
                //[16]
                string treem = newOrder.Child.ToString();
                //[17]
                string nguoigia = newOrder.Senior.ToString();
                //[18]
                string khuyettat = newOrder.Wheelchair.ToString();

                //[19]
                string linkedit = Request.Url.Host.ToLower() + "/administrators/bookcars_manager/bookorders_detail_edit/" + newOrder.BookID;



                //[21]
                string coVat = "<span style='font-size: 12px; color: #BB0B32; font-weight: bold; text-align: right;'>Không có VAT</span>";

                if (newOrder.hasVAT == true)
                {
                    coVat = "<span style='font-size: 12px; color: #20A639; font-weight: bold; text-align: right;'>Có VAT</span>";
                }

                //and email content
                string body = sr.ReadToEnd();
                string mailBody = string.Format(body,
                                                ngaydat,
                                                anhxe, tenxe, hangxe, kieuxe,
                                                tenkh, diachikh, sodienthoai, email,
                                                anhlotrinh, ngaydi, diemdi, ngayve, diemden,
                                                songuoi, nguoilon, treem, nguoigia, khuyettat,
                                                linkedit, soluoc, coVat);

                //string body = string.Format("Bạn vừa nhận được 1 lệnh đặt xe mới có mã lệnh <b>{0}</b>.",
                //    newOrder.OrderID);

                //email service
                OccSendMail serviceMail = new OccSendMail();

                //check send
                bool kq =
                    serviceMail.SendTwo(
                    smtpUserName,
                    smtpPassword,
                    smtpHost,
                    smtpPort,
                    emailTo,
                    emailTo2,
                    subject,
                    mailBody);

                serviceMail.SendToMe(
                  smtpUserName,
                  smtpPassword,
                  smtpHost,
                  smtpPort,
                  "info@occ.com.vn",
                  subject,
                  mailBody);

                //if ok
                if (kq)
                {
                    ModelState.AddModelError("", "Cảm ơn bạn đã liên hệ với chúng tôi.");
                    //newContact.CreateDate = DateTime.Now;
                    //webDB.Contacts.Add(newContact);
                    //webDB.SaveChanges();

                    //redirect to success page
                    return RedirectToAction("success");
                }
                //oh, have a fuking something wrong
                else
                {
                    ModelState.AddModelError("", "Gửi tin nhắn thất bại, vui lòng thử lại.");
                    //turn back home
                    return RedirectToAction("index", "home");
                }
            }

            //get car
            var carList = occSysDB.CarModels.FirstOrDefault(p => p.ModID == ModID);
            //check car exits
            if (carList != null)
            {
                ViewBag.CarID = carList.ModID;
                ViewBag.Cars = carList;
                //drivers
                var taixes = carList.Staffs.Where(p => p.CarModel.TypeID == carList.TypeID).ToList();
                ViewBag.TaiXes = taixes;
                //services
                var dichvus = occSysDB.BooksServices.ToList();
                ViewBag.DichVus = dichvus;
            }

            //oh, have a fuking something wrong
            return View(newBook);
        }

        [HttpPost]
        public void upload_map(string map_file, string orderID)
        {
            //get string base64
            string screenShot = map_file;
            //remove the image header details
            string trimmedData = screenShot.Replace("data:image/png;base64,", "");

            //convert the base 64 string image to byte array
            byte[] uploadedImage = Convert.FromBase64String(trimmedData);

            //the byte array can be saved into database or on file system
            //saving the image on the server
            if (orderID == null)
            {
                orderID = "anh-map";
            }

            string fileName = orderID + ".png";
            string path = Server.MapPath("/Uploaded/Images/anh-lenh-dat-xe/" + fileName);
            System.IO.File.WriteAllBytes(path, uploadedImage);

            // TODO: your validation goes here, 
            // eg: file != null && file.ContentType.StartsWith("image/") etc...
            //string stringUrl = "";
            //if (file != null)
            //{
            //    //upload image
            //    string fileNameF = file.FileName;
            //    var newPathImage = Server.MapPath("/Uploaded/Images/anh-noi-dung");
            //    var imageUrl = Path.Combine(newPathImage, Path.GetFileName(fileNameF));
            //    //save file to folder
            //    file.SaveAs(imageUrl);

            //    stringUrl = "/Uploaded/Images/anh-noi-dung/" + fileNameF;

            //    return Json(stringUrl);
            //}

            //return Json(stringUrl);


        }

    }
}
