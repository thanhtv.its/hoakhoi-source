﻿using Insya.Localization;
using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Text.RegularExpressions;
using MvcWebsiteOcc.Areas.administrators.Models;

namespace MvcWebsiteOcc.Controllerss
{
    public class albumsController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();
        string langCode = Localization.Get("lng").ToLower();

        public ActionResult index(int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            var list = occSysDB.Albums.Where(p => p.Language.Lang_Code == langCode && p.IsActive == true).OrderByDescending(p => p.CreateDate).ToList();
            //if (list.Count == 0)
            //{
            //    list = occSysDB.News.ToList();
            //}

            return View(list.ToPagedList(pageNumber, pageSize));
        }

        public JsonResult _get_album_more(int size, int? cateID, string subD)
        {
            int take = 20;
            var model = occSysDB.Albums.OrderByDescending(p => p.CreateDate).Where(p => p.Language.Lang_Code == langCode).Skip(size).Take(take).ToList();
            int modelCount = occSysDB.Albums.Where(p => p.Language.Lang_Code == langCode).OrderByDescending(p => p.CreateDate).Count();
            // Convert the Product entities to ProductViewModel instances


            if (cateID != null)
            {
                model = occSysDB.Albums.OrderByDescending(p => p.CreateDate).Where(p => p.Language.Lang_Code == langCode && p.AlCateID == cateID).Skip(size).Take(take).ToList();
                modelCount = occSysDB.Albums.Where(p => p.Language.Lang_Code == langCode && p.AlCateID == cateID).OrderByDescending(p => p.CreateDate).Count();
            }

            if (subD != null && subD != "")
            {
                var events = occSysDB.Events.FirstOrDefault(p => p.Subdomain == subD);
                model = occSysDB.Albums.OrderByDescending(p => p.CreateDate).Where(p => p.Language.Lang_Code == langCode && p.EventID == events.EventID).Skip(size).Take(take).ToList();
                modelCount = occSysDB.Albums.Where(p => p.Language.Lang_Code == langCode && p.EventID == events.EventID).OrderByDescending(p => p.CreateDate).Count();
            }


            List<Album> result = new List<Album>();
            foreach (var slide in model)
            {
                Album newsadd = (new Album
                {

                    AlbumID = slide.AlbumID,
                    AlbumName = slide.AlbumName,
                    Folder = slide.Folder,
                    Image = slide.Image,
                    Love = OccConvert.GetLikes("http://" + Request.Url.Host + "/thu-vien-anh/xem/" + OccConvert.LayUrl(slide.AlbumName, slide.AlbumID) + ".html"),
                    Link = "/tin-tuc/noi-dung/" + OccConvert.LayUrl(slide.AlbumName, slide.AlbumID) + ".html",
                    LinkCate = "/tin-tuc/chuyen-muc/" + OccConvert.LayUrl(slide.AlbumsCategories.AlCateName, slide.AlbumsCategories.AlCateID) + ".html",
                    CateName = slide.AlbumsCategories.AlCateName,
                    CreateDateString = OccConvert.ViewDateFull(slide.CreateDate) + " / " + slide.AlbumImages.Count + " ảnh"


                });

                result.Add(newsadd);
            }
            return Json(new { model = result, count = modelCount }, JsonRequestBehavior.AllowGet);
            //return Json(result, );

        }
        public ActionResult view_by_cate(string name, int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            var list = occSysDB.Albums.Where(p => p.Language.Lang_Code == langCode && p.IsActive == true).OrderByDescending(p => p.CreateDate).ToList();

            //if (list.Count == 0)
            //{
            //    list = occSysDB.News.ToList();
            //}

            if (name == null)
            {
                return View(list.ToPagedList(pageNumber, pageSize));
            }
            else
            {
                int a = name.LastIndexOf("-");
                string title = name.Substring(a).Replace("-", "");
                string b = Regex.Match(title, @"\d+").Value;
                int id = int.Parse(b);
                var cates = occSysDB.AlbumsCategories.FirstOrDefault(p => p.AlCateID == id);


                if (cates != null)
                {
                    list = occSysDB.Albums.Where(p => p.AlCateID == cates.AlCateID && p.IsActive == true).OrderByDescending(p => p.CreateDate).ToList();
                    ViewBag.CateID = cates.AlCateID;
                }
                else
                {
                    list = new List<Areas.administrators.Models.Album>();
                }

                //if (list.Count == 0)
                //{
                //    list = occSysDB.News.ToList();
                //}

                return View(list.ToPagedList(pageNumber, pageSize));
            }
        }

        public ActionResult view_detail(string name)
        {
            int a = name.LastIndexOf("-");
            string title = name.Substring(a).Replace("-", "");
            string b = Regex.Match(title, @"\d+").Value;
            int id = int.Parse(b);
            var viewDetail = occSysDB.Albums.FirstOrDefault(p => p.AlbumID == id);

            //var listHots = occSysDB.News.OrderByDescending(p => p.CreateDate).Where(p => p.Language.Lang_Code == langCode && p.IsHot == true && p.NewsCategories.TID == 2 && p.NewsID != viewDetail.NewsID).Take(8).ToList();
            //ViewBag.VideoHot = listHots;

            //var listOrthers = occSysDB.NewsCategories.Where(p => p.Language.Lang_Code == langCode && p.TID == 2).OrderBy(p => p.SortOrder).ToList();
            //ViewBag.Orthers = listOrthers;

            return View(viewDetail);

        }

        public ActionResult _orther_clips(int? currentID)
        {
            var orthers = occSysDB.News.Where(p => p.NewsID != currentID && p.Language.Lang_Code == langCode).OrderBy(p => Guid.NewGuid()).Take(2).ToList();
            return PartialView(orthers);
        }

    }
}
