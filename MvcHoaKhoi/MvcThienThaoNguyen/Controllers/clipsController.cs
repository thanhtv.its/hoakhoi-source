﻿using Insya.Localization;
using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Text.RegularExpressions;

namespace MvcWebsiteOcc.Controllers
{
    public class clipsController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();
        string langCode = Localization.Get("lng").ToLower();

        public ActionResult index(int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            var list = occSysDB.NewsCategories.Where(p => p.Language.Lang_Code == langCode && p.TID == 2).OrderBy(p => p.SortOrder).ToList();
            //if (list.Count == 0)
            //{
            //    list = occSysDB.News.ToList();
            //}
            int clips = 0;
            foreach (var item in list)
            {
                clips += item.News.Count;
            }

            ViewBag.ClipCount = clips;

            return View(list.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Hot News
        /// </summary>
        /// <returns></returns>
        public ActionResult _hot_clips()
        {
            var list = occSysDB.News.OrderByDescending(p => p.CreateDate).Where(p => p.Language.Lang_Code == langCode && p.IsHot == true && p.NewsCategories.TID == 2).Take(14).ToList();
            //if (list.Count == 0)
            //{
            //    list = occSysDB.News.ToList();
            //}
            return PartialView(list);
        }

        public ActionResult view_by_cate(string name, int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            var list = occSysDB.News.Where(p => p.Language.Lang_Code == langCode && p.NewsCategories.TID == 2).ToList();

            //if (list.Count == 0)
            //{
            //    list = occSysDB.News.ToList();
            //}

            if (name == null)
            {
                return View(list.ToPagedList(pageNumber, pageSize));
            }
            else
            {
                int a = name.LastIndexOf("-");
                string title = name.Substring(a).Replace("-", "");
                string b = Regex.Match(title, @"\d+").Value;
                int id = int.Parse(b);
                var cates = occSysDB.NewsCategories.FirstOrDefault(p => p.NewCatID == id);


                if (cates != null)
                {
                    list = occSysDB.News.Where(p => p.NewsCategories.ParentID == cates.NewCatID || p.NewCatID == cates.NewCatID && p.NewsCategories.TID == 2).OrderByDescending(p => p.IsHot).ToList();
                    var listHots = list.Where(p => p.IsHot == true).Take(14).ToList();

                    list = list.Skip(listHots.Count).ToList();


                    ViewBag.Hots = listHots;
                }
                else
                {
                    list = new List<Areas.administrators.Models.News>();
                    ViewBag.Hots = new List<Areas.administrators.Models.News>();
                }

                //if (list.Count == 0)
                //{
                //    list = occSysDB.News.ToList();
                //}

                return View(list.ToPagedList(pageNumber, pageSize));
            }
        }

        /// <summary>
        /// Hot News
        /// </summary>
        /// <returns></returns>
        public ActionResult _hot_clips_by_cate(int? id)
        {
            var list = occSysDB.News.OrderByDescending(p => p.CreateDate).Where(p => p.Language.Lang_Code == langCode && p.IsHot == true && p.NewCatID == id).Take(14).ToList();
            //if (list.Count == 0)
            //{
            //    list = occSysDB.News.ToList();
            //}
            return PartialView(list);
        }

        public ActionResult view_detail(string name)
        {
            int a = name.LastIndexOf("-");
            string title = name.Substring(a).Replace("-", "");
            string b = Regex.Match(title, @"\d+").Value;
            int id = int.Parse(b);
            var viewDetail = occSysDB.News.FirstOrDefault(p => p.NewsID == id);

            var listHots = occSysDB.News.OrderByDescending(p => p.CreateDate).Where(p => p.Language.Lang_Code == langCode && p.IsHot == true && p.NewsCategories.TID == 2 && p.NewsID != viewDetail.NewsID).Take(8).ToList();
            ViewBag.VideoHot = listHots;

            var listOrthers = occSysDB.NewsCategories.Where(p => p.Language.Lang_Code == langCode && p.TID == 2).OrderBy(p => p.SortOrder).ToList();
            ViewBag.Orthers = listOrthers;

            return View(viewDetail);

        }

        public ActionResult _orther_clips(int? currentID)
        {
            var orthers = occSysDB.News.Where(p => p.NewsID != currentID && p.Language.Lang_Code == langCode).OrderBy(p => Guid.NewGuid()).Take(2).ToList();
            return PartialView(orthers);
        }

    }
}
