﻿using Insya.Localization;
using MvcWebsiteOcc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Text.RegularExpressions;
using System.IO;
using MvcWebsiteOcc.Areas.administrators.Models;

namespace MvcWebsiteOcc.Controllers
{
    public class newsController : Controller
    {
        private readonly OccDBEntities occSysDB = new OccDBEntities();
        string langCode = Localization.Get("lng").ToLower();

        public ActionResult index(int? page)
        {
            int pageSize = 4;
            int pageNumber = (page ?? 1);
            var list = occSysDB.News.Where(p => p.Language.Lang_Code == langCode && p.NewsCategories.TID == 1).OrderByDescending(p => p.CreateDate).Take(20).ToList();
            //if (list.Count == 0)
            //{
            //    list = occSysDB.News.ToList();
            //}

            //list = new List<Areas.administrators.Models.News>();

            return View(list);
        }

        [HttpPost]
        public ActionResult LoadMore(int size)
        {
            var model = occSysDB.News.OrderByDescending(p => p.CreateDate).Where(p => p.Language.Lang_Code == langCode && p.NewsCategories.TID == 1).Skip(size).Take(20).ToList();
            int modelCount = occSysDB.News.Where(p => p.Language.Lang_Code == langCode && p.NewsCategories.TID == 1).OrderByDescending(p => p.CreateDate).Count();
            if (model.Any())
            {
                string modelString = _renderview_to_string("_load_more", model);

                return Json(new { ModelString = modelString, ModelCount  = modelCount});
            }
            return Json(model);
        }
        public string _renderview_to_string(string viewName, object model)
        {
            ViewData.Model = model;


            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public JsonResult _get_news_more(int size, int? cateID, string subD)
        {
            int take = 20;

            var model = occSysDB.News.OrderByDescending(p => p.CreateDate).Where(p => p.Language.Lang_Code == langCode && p.NewsCategories.TID == 1).Skip(size).Take(take).ToList();
            int modelCount = occSysDB.News.Where(p => p.Language.Lang_Code == langCode && p.NewsCategories.TID == 1 ).OrderByDescending(p => p.CreateDate).Count();
            // Convert the Product entities to ProductViewModel instances


            if (cateID != null)
            {
                model = occSysDB.News.OrderByDescending(p => p.CreateDate).Where(p => p.Language.Lang_Code == langCode && p.NewsCategories.TID == 1 && (p.NewsCategories.ParentID == cateID || p.NewCatID == cateID)).Skip(size).Take(take).ToList();
                modelCount = occSysDB.News.Where(p => p.Language.Lang_Code == langCode && p.NewsCategories.TID == 1 && (p.NewsCategories.ParentID == cateID || p.NewCatID == cateID)).OrderByDescending(p => p.CreateDate).Count();
            }

            if (subD != null && subD != "")
            {
                var events = occSysDB.Events.FirstOrDefault(p=>p.Subdomain == subD);
                model = occSysDB.News.OrderByDescending(p => p.CreateDate).Where(p => p.Language.Lang_Code == langCode && p.NewsCategories.TID == 1 && p.EventID == events.EventID).Skip(size).Take(take).ToList();
                modelCount = occSysDB.News.Where(p => p.Language.Lang_Code == langCode && p.NewsCategories.TID == 1 && p.EventID == events.EventID).OrderByDescending(p => p.CreateDate).Count();
            }

            List<News> result = new List<News>();
            foreach (var slide in model)
            {
                News newsadd = (new News
                {

                    NewsID = slide.NewsID,
                    NewsTitle = slide.NewsTitle,
                    Folder = slide.Folder,
                    NewsImage = slide.NewsImage , 
                    ShortDescription = slide.ShortDescription,
                    Link = "/tin-tuc/noi-dung/"+OccConvert.LayUrl(slide.NewsTitle, slide.NewsID)+".html"  ,
                    LinkCate = "/tin-tuc/chuyen-muc/"+OccConvert.LayUrl(slide.NewsCategories.NewsCatName, slide.NewsCategories.NewCatID)+".html" ,
                    NewsCateName = slide.NewsCategories.NewsCatName ,
                    CreateDateString = OccConvert.ViewDateFull(slide.CreateDate)
                    

                });

                result.Add(newsadd);
            }
            return Json(new { model = result, count = modelCount }, JsonRequestBehavior.AllowGet);
            //return Json(result, );

        }

        public ActionResult view_by_cate(string name, int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            var list = occSysDB.News.Where(p => p.Language.Lang_Code == langCode && p.NewsCategories.TID == 1).ToList();

            //if (list.Count == 0)
            //{
            //    list = occSysDB.News.ToList();
            //}

            if (name == null)
            {
                return View(list.ToPagedList(pageNumber, pageSize));
            }
            else
            {
                int a = name.LastIndexOf("-");
                string title = name.Substring(a).Replace("-", "");
                string b = Regex.Match(title, @"\d+").Value;
                int id = int.Parse(b);
                var cates = occSysDB.NewsCategories.FirstOrDefault(p => p.NewCatID == id);

                if (cates != null)
                {
                    list = occSysDB.News.Where(p => p.NewsCategories.ParentID == cates.NewCatID || p.NewCatID == cates.NewCatID && p.NewsCategories.TID == 1).OrderByDescending(p => p.CreateDate).ToList();
                    ViewBag.CateID = cates.NewCatID;
                }
                else
                {
                    list = new List<Areas.administrators.Models.News>();
                }

                //if (list.Count == 0)
                //{
                //    list = occSysDB.News.ToList();
                //}

                return View(list.ToPagedList(pageNumber, pageSize));
            }
        }

        public ActionResult view_detail(string name)
        {
            int a = name.LastIndexOf("-");
            string title = name.Substring(a).Replace("-", "");
            string b = Regex.Match(title, @"\d+").Value;
            int id = int.Parse(b);
            var viewDetail = occSysDB.News.FirstOrDefault(p => p.NewsID == id);
            viewDetail.ViewCount += 1;
            occSysDB.SaveChanges();
            return View(viewDetail);

        }

        public ActionResult _orther_news(int? currentID)
        {
            var orthers = occSysDB.News.Where(p => p.NewsID != currentID && p.Language.Lang_Code == langCode && p.NewsCategories.TID == 1).OrderBy(p => Guid.NewGuid()).Take(12).ToList();
            return PartialView(orthers);
        }

    }
}
